/*
** IPVIRTUAL_C
**
** $Id: ipvirtual.c,v 1.20 2003/07/31 17:59:15 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/


#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>

#include "module.h"
#include "mod_privmsg.h"
#include "mod_bdd.h"
#include "mod_tools.h"
#include "mod_notify.h"


static int handle;

static unsigned long long estado_aleatorio;

static void imprime_ayuda(int remitente)
{
  envia_nick(handle, remitente, "DESCIFRA <ip virtual> <clave>");
  envia_nick(handle, remitente, "Fin de la ayuda");
}

static void tea_decode(unsigned int *v, unsigned int *k)
{
  unsigned int n = 32, sum, y = v[0], z = v[1], delta = 0x9e3779b9;

  sum = delta << 5;
  /* start cycle */
  while (n-- > 0) {
    z -= ((y << 4) + 0) ^ (y + sum) ^ ((y >> 5) + 0);
    y -= ((z << 4) + k[0]) ^ (z + sum) ^ ((z >> 5) + k[1]);
    sum -= delta;
  }
  /* end cycle */
  v[0] = y;
  v[1] = z;
}


static void privmsg(int nick, int remitente, char *mensaje)
{
  char flags[1024];
  char buf[1024];
  unsigned char *p;
  unsigned int v[2], k[2];

  flags[0] = '\0';
  lee_flags_nick(remitente, flags);
  if (!strchr(flags, 'h') && !strchr(flags, 'o'))
    return;

  hace_log(remitente, mensaje);

  p = strchr(mensaje, ' ');
  if (!p || strncmp("descifra ", mensaje, 9)) {
    imprime_ayuda(remitente);
    return;
  }

  mensaje = p + 1;
  p = strchr(mensaje, ' ');
  if (!p) {
    imprime_ayuda(remitente);
    return;
  }

  *p++ = '\0';

  if ((strlen(mensaje) != 21) || (strlen(p) != 12)) {
    imprime_ayuda(remitente);
    return;
  }

  v[0] = base64toint(mensaje);
  v[1] = base64toint(mensaje + 7);

  k[0] = base64toint(p);
  k[1] = base64toint(p + 6);

  tea_decode(v, k);

  v[0] = (v[0] ^ k[0]) & 0xffff0000ul;
  if (v[0]) {
    envia_nick(nick, remitente,
               "La IP virtual y la clave que indicas no coinciden");
    return;
  }

  v[0] = htonl(v[1]);
  p = (unsigned char *) v;
  sprintf(buf, "IP descifrada: %d.%d.%d.%d", *p, *(p + 1), *(p + 2),
          *(p + 3));
  envia_nick(nick, remitente, buf);
}

static void fin(void)
{
}

void timer(void)
{
  time_t now = time(NULL);
  unsigned long long k[2];
  unsigned long long result;
  FILE *f;
  char buf[33];
  char clave[16];
  char buf_time[100];

  notifica_timer(now + 86400, timer); /* Se ejecuta de nuevo en 24 horas */
  k[0] = now ^ get_entropia() ^ estado_aleatorio ^ gethrtime();
  /* Dejamos k[1] sin inicializar, para que contribuya a la aleatoriedad */
  tea_crypt((unsigned long *) &estado_aleatorio, (unsigned long *) &result,
            (unsigned long *) &k);
  estado_aleatorio ^= result;
  strcpy(clave, inttobase64(result >> 32));
  strcat(clave, inttobase64(result & 0xfffffffful));
  bdd_envia_registro('v', ".", clave);
  f = fopen("/export/home/irc/olimpo/claves_de_cifrado", "a");
  strcpy(buf, ctime_sincronizado(&now, buf_time));
  *(buf + strlen(buf) - 1) = '\0'; /* Eliminamos el '\n' final */
  fprintf(f, "%s: %s\n", buf, clave);
  fclose(f);
  hace_log2("Generamos una nueva clave de proteccion de IPs");
}

int inicio(void)
{
  time_t t;
  struct tm tm;

  especifica_fin(fin);
  comentario_modulo("Modulo de descifrado de IPs $Revision: 1.20 $");
  handle = nuevo_nick("ipvirtual", "+odkirh", privmsg);
  if (handle < 0)
    return -1;

  t = time(NULL);
  estado_aleatorio = t ^ get_entropia() ^ gethrtime();
  do {
    t += 3600;                  /* An~adimos una hora */
    assert(0);                  /* LA SIGUIENTE FUNCION NO ES THREAD SAFE EN SOLARIS 2.5.1 */
    gmtime_r(&t, &tm);
  } while (tm.tm_hour != 4);

  notifica_timer(t, timer);

  return 0;
}
