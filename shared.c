/*
** SHARED_C
**
** $Id: shared.c,v 1.46 2003/07/31 17:59:15 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#include "shared.h"
#include "db.h"
#include "modulos.h"


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <assert.h>



int soc;
char uplink[3];
time_t siguiente_settime;

void *db_ilines;
void *db_varios;

long db[DB_FINAL - DB_INICIO + 1];

unsigned long long num_conexiones = 0;
long num_usuarios = 0;
long num_usuarios_max = 0;
time_t tiempo_num_usuarios_max = 0;

long num_usuarios_hist[LONG_HISTORICO];
long num_conexiones_hist[LONG_HISTORICO];
time_t tiempo_hist[LONG_HISTORICO];
long puntero_historico = 0;

unsigned long long entropia;


unsigned long nodos_conectados[128]; /* Un bit por nodo */


char convert2y[64] = {
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
  'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
  'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
  'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
  '[', ']'
};

unsigned char convert2n[] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 0, 0,
  0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
  15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 62, 0, 63, 0, 0, 0, 26, 27, 28,
  29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,
  48,
  49, 50, 51
};

unsigned char tolowertab[] = {
  0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa,
  0xb, 0xc, 0xd, 0xe, 0xf, 0x10, 0x11, 0x12, 0x13, 0x14,
  0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d,
  0x1e, 0x1f,
  ' ', '!', '"', '#', '$', '%', '&', 0x27, '(', ')',
  '*', '+', ',', '-', '.', '/',
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
  ':', ';', '<', '=', '>', '?',
  '@', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
  'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
  't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~',
  '_',
  '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
  'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
  't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~',
  0x7f,
  0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
  0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f,
  0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99,
  0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f,
  0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9,
  0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf,
  0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9,
  0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf,
  0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9,
  0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf,
  0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9,
  0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf,
  0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9,
  0xea, 0xeb, 0xec, 0xed, 0xee, 0xef,
  0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9,
  0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff
};


unsigned int base64toint(const char *str)
{
  register unsigned int i;
  i = convert2n[(unsigned char) str[5]];
  i += convert2n[(unsigned char) str[4]] << 6;
  i += convert2n[(unsigned char) str[3]] << 12;
  i += convert2n[(unsigned char) str[2]] << 18;
  i += convert2n[(unsigned char) str[1]] << 24;
  i += convert2n[(unsigned char) str[0]] << 30;
  return i;
}

const char *inttobase64(unsigned int i)
{
  static char base64buf[7];
  base64buf[0] = convert2y[(i >> 30) & 0x3f];
  base64buf[1] = convert2y[(i >> 24) & 0x3f];
  base64buf[2] = convert2y[(i >> 18) & 0x3f];
  base64buf[3] = convert2y[(i >> 12) & 0x3f];
  base64buf[4] = convert2y[(i >> 6) & 0x3f];
  base64buf[5] = convert2y[i & 0x3f];
  /* base64buf[6] = 0; (static is initialized 0) */
  return base64buf;
}

int segmentacion(segmentos * seg, char *cadena, char *buf, int max_segm)
{
  if (max_segm > MAX_NUM_SEGMENTOS)
    max_segm = MAX_NUM_SEGMENTOS;
  seg->num_segm = 0;
  if (buf != NULL)
    strcpy(buf, cadena);
  else
    buf = cadena;

  while (*buf) {
    if ((*buf == ' ') || (*buf == '\t')) { /* Nos saltamos lo nulo */
      buf++;
      continue;
    }
    /*
     ** Esto se hace aqu\155 por si los segmentos van
     ** seguidos de m\172ltiples blancos.
     */
    if (seg->num_segm >= max_segm)
      return seg->num_segm;

    if ((*buf == ':') && (seg->num_segm)) {
      /* \077El segmento final es nulo? */
      if (*buf++ == '\0')
        return seg->num_segm;
      seg->segm[seg->num_segm++] = buf;
      return seg->num_segm;
    }
    seg->segm[seg->num_segm++] = buf;
    while (*(++buf) && (*buf != ' ') && (*buf != '\t'));
    if (!*buf)
      return seg->num_segm;
    *buf++ = '\0';
  }
  return seg->num_segm;
}

char *dessegmentar(segmentos * seg, char *buf)
{
  int c;

  buf[0] = '\0';
  for (c = 0; c < seg->num_segm - 1; c++) {
    strcat(buf, seg->segm[c]);
    strcat(buf, " ");
  }
  if (strchr(seg->segm[c], ' ') || strchr(seg->segm[c], '\t'))
    strcat(buf, ":");
  strcat(buf, seg->segm[c]);
  return buf;
}

void guarda_log(char *linea, char *modulo)
{
#ifndef DEVELOP
  int handle;
  char buf[2048];
  char buf_time[100];
  time_t now = time(NULL);

  handle = open("olimpo.log", O_APPEND | O_WRONLY | O_CREAT, 0644);
  if (handle != -1) {
    sprintf(buf, "%s", ctime_sincronizado(&now, buf_time));

/* Con 24 no imprime el \n\0 final */
    sprintf(buf + 24, " [%s] %s\n", modulo, linea);
    write(handle, buf, strlen(buf));
    close(handle);
  }
#endif
}

void guarda_log_nick(char *linea, char *modulo, char *nick)
{
  char buf[2048];
  char *p;

  p = encuentra_usuario_nick(nick);
  if (!p)
    p = "";
  sprintf(buf, "[%s] %s", p, linea);
  guarda_log(buf, modulo);
}

void guarda_log_nick_compress(char *linea, char *modulo, char *nick_compress)
{
  char buf[2048];
  char *p;

  p = encuentra_usuario_compress(nick_compress);
  if (!p)
    p = "";
  sprintf(buf, "[%s] %s", p, linea);
  guarda_log(buf, modulo);
}

unsigned char *nick_normalizado(unsigned char *nick, unsigned char *dest)
{
  unsigned char *p = dest;

  while ((*p++ = tolowertab[*nick++]));
  return dest;
}

void tea_crypt(const unsigned long *const v, unsigned long *const w,
               const unsigned long *const k)
{
  register unsigned long y = v[0], z = v[1], sum = 0, delta = 0x9E3779B9,
    a = k[0], b = k[1], c = k[2], d = k[3], n = 32;

  while (n-- > 0) {
    sum += delta;
    y += ((z << 4) + a) ^ ((z + sum) ^ ((z >> 5) + b));
    z += ((y << 4) + c) ^ ((y + sum) ^ ((y >> 5) + d));
  }

  w[0] = y;
  w[1] = z;
}

void tea_decrypt(const unsigned long *const v, unsigned long *const w,
                 const unsigned long *const k)
{
  register unsigned long y = v[0], z = v[1], sum = 0xC6EF3720,
    delta = 0x9E3779B9, a = k[0], b = k[1], c = k[2], d = k[3], n = 32;

  /* sum = delta<<5, in general sum = delta * n */

  while (n-- > 0) {
    z -= ((y << 4) + c) ^ (y + sum) ^ ((y >> 5) + d);
    y -= ((z << 4) + a) ^ (z + sum) ^ ((z >> 5) + b);
    sum -= delta;
  }

  w[0] = y;
  w[1] = z;
}

static int numeric_nodo2numeric(unsigned char *numeric)
{
  int numeric2;
  unsigned char n2;

  numeric2 = convert2n[*numeric];
  n2 = *(numeric + 1);
  if ((n2 != '\0') && (n2 != ' ')) {
    numeric2 <<= 6;
    numeric2 += convert2n[n2];
  }
  return numeric2;
}

void borra_nodos_conectados(void)
{
  assert(sizeof(unsigned long) == 4);
  assert(sizeof(nodos_conectados) == 512); /* 512*8 bits */
  assert((1ul << 0) == 1);

  memset(nodos_conectados, 0, sizeof(nodos_conectados));
}

int nodo_esta_conectado(char *numeric)
{
  int numeric2;

  numeric2 = numeric_nodo2numeric(numeric);
  return nodos_conectados[numeric2 / 32] & (1ul << (numeric2 & 31));
}

void nodo_conectado(char *numeric)
{
  int numeric2;

  numeric2 = numeric_nodo2numeric(numeric);
  nodos_conectados[numeric2 / 32] |= 1ul << (numeric2 & 31);
}

void nodo_desconectado(char *numeric)
{
  int numeric2;

  numeric2 = numeric_nodo2numeric(numeric);
  nodos_conectados[numeric2 / 32] &=
    ~((unsigned long) (1ul << (numeric2 & 31)));
}
