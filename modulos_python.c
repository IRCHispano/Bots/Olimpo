/*
** MODULOS_PYTHON_C
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

/*
    $Id: modulos_python.c,v 1.134 2003/07/31 17:59:15 jcea Exp $
*/

#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <assert.h>

#include <Python.h>

#include "shared.h"
#include "config.h"

#include "modulos_python.h"

#include "module.h"
#include "mod_privmsg.h"
#include "mod_servmsg.h"
#include "mod_notify.h"
#include "mod_notify_db.h"
#include "mod_berkeleydb.h"
#include "mod_tools.h"
#include "mod_bdd.h"
#include "mod_ipc.h"


PyThreadState *mainThreadState = NULL;
int mainThreadStateCONTADOR = 1; /* Cuando inicializmos el interprete PYTHON nos da la possesion del LOCK */

#define ENTER_PYTHON2 if(!mainThreadStateCONTADOR) {PyEval_AcquireLock();PyThreadState_Swap(mainThreadState);} \
                          mainThreadStateCONTADOR++;
#define ENTER_PYTHON {ENTER_PYTHON2
#define EXIT_PYTHON2 mainThreadStateCONTADOR--; if(!mainThreadStateCONTADOR) {PyThreadState_Swap(NULL);PyEval_ReleaseLock();}
#define EXIT_PYTHON EXIT_PYTHON2}


struct modulo {
  char *nombre;
  PyObject *lib;
  PyObject *inicio;
  struct modulo **anterior;
  struct modulo *siguiente;
};

static struct modulo *modulos = NULL;

static struct modulo *modulo_python_actual = NULL;

char *ctime_sincronizado(const time_t * clock, char *buf)
{
  char *p;

  ENTER_PYTHON p = ctime_r(clock, buf);
  EXIT_PYTHON return p;
}

struct tm *localtime_sincronizado(const time_t * clock, struct tm *res)
{
  struct tm *tm;

  ENTER_PYTHON tm = localtime_r(clock, res);
  EXIT_PYTHON return tm;
}

void libera_objeto_python(void *objeto)
{
  if (!objeto)
    return;
  ENTER_PYTHON;
  Py_DECREF((PyObject *) objeto); /* No puede ser NULL porque ya lo comprobamos antes */
  EXIT_PYTHON;
}

static void modulo_excepcion(void)
{
  PyErr_Print();
}

void modulo_python_privmsg(void *modulo_python, void *privmsg, int handle,
                           int handle2, char *mensaje)
{
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(privmsg, "iis", handle, handle2, mensaje);
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

void modulo_python_servmsg(void *modulo_python, void *servmsg, int handle,
                           char *remitente, char *comando)
{
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(servmsg, "iss", handle, remitente, comando);
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

int modulo_python_intercepta_comando(void *modulo_python,
                                     void *intercepta_comando, char *comando)
{
  int r;
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(intercepta_comando, "s", comando);
  if (!result) {
    modulo_excepcion();
    r = 0;
  }
  else {
    //if(PyInt_Check(result))
    r = 0;
  }
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
  return r;
}

void modulo_python_notifica_nick_registrado(void *modulo_python,
                                            void *notifica_nick_registrado,
                                            int handle)
{
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(notifica_nick_registrado, "i", handle);
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

void modulo_python_notifica_nick_entrada(void *modulo_python,
                                         void *notifica_nick_entrada,
                                         int handle)
{
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(notifica_nick_entrada, "i", handle);
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

void modulo_python_notifica_nick_salida(void *modulo_python,
                                        void *notifica_nick_salida,
                                        int handle)
{
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(notifica_nick_salida, "i", handle);
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

static PyObject *notify_methods_notifica_nicks_registrados(PyObject * self)
{
  notifica_nicks_registrados();

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *notify_methods_notifica_nicks(PyObject * self)
{
  notifica_nicks();

  Py_INCREF(Py_None);
  return Py_None;
}

void modulo_python_notifica_server_join(void *modulo_python,
                                        void *notifica_server_join,
                                        char *server, char *id, char *padre)
{
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result =
    PyObject_CallFunction(notifica_server_join, "sss", server, id, padre);
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

void modulo_python_notifica_server_split(void *modulo_python,
                                         void *notifica_server_split,
                                         char *server, char *causa)
{
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(notifica_server_split, "ss", server, causa);
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

void modulo_python_notifica_servers_split_end(void *modulo_python, void
                                              *notifica_servers_split_end)
{
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(notifica_servers_split_end, "");
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

void modulo_python_notifica_timer(void *modulo_python, void *notifica_timer)
{
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(notifica_timer, NULL);
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);

/*
** Esta rutina decrementa, de forma excepcional,
** el contador de referencias.
*/
  Py_XDECREF((PyObject *) notifica_timer);

  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

void modulo_python_notifica_IRQ(void *modulo_python, void *notifica_IRQ)
{
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(notifica_IRQ, NULL);
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

void modulo_python_notifica_db_nickdrop(void *modulo_python,
                                        void *notifica_nickdrop,
                                        char *nick, int num_serie)
{
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(notifica_nickdrop, "si", nick, num_serie);
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

void modulo_python_inicio(void *modulo_python)
{
  PyObject *result;
  struct modulo *m = modulo_python;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(m->inicio, NULL);
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

void modulo_python_fin(void *modulo_python, void *fin)
{
  PyObject *result;
  struct modulo *backup_modulo_python_actual;

  backup_modulo_python_actual = modulo_python_actual;
  modulo_python_actual = modulo_python;

  ENTER_PYTHON;
  result = PyObject_CallFunction(fin, NULL);
  if (!result)
    modulo_excepcion();
  Py_XDECREF(result);
  EXIT_PYTHON;
  modulo_python_actual = backup_modulo_python_actual;
}

int es_modulo_python(char *nombre)
{
  return !strcmp(nombre + strlen(nombre) - 3, ".py");
}

void descarga_modulo_python(void *modulo_python)
{
  PyObject *dict;
  struct modulo *m = modulo_python;

  ENTER_PYTHON;
  dict = PyImport_GetModuleDict(); /* Shared; no hay que liberarla */

/* http://sourceforge.net/tracker/index.php?func=detail&aid=498915&group_id=5470&atid=105470 */
  //PyDict_DelItemString(dict, m->nombre); /* Eliminamos modulo antiguo */
  PyMapping_DelItemString(dict, m->nombre); /* Eliminamos modulo antiguo */

  free(m->nombre);
  *m->anterior = m->siguiente;
  if (m->siguiente)
    m->siguiente->anterior = m->anterior;

  Py_DECREF(m->lib);
  Py_DECREF(m->inicio);

  EXIT_PYTHON;
  free(m);
}

char *nuevo_modulo_python(char *nombre, void **modulo_python)
{
  char buf[1024];
  struct modulo *m;
  PyObject *dict, *mod, *mod_inicio;

  assert(es_modulo_python(nombre));
  assert(modulo_python);

  modulo_python_actual = NULL;

  strcpy(buf, nombre);
  buf[strlen(buf) - 3] = '\0';  /* Eliminamos extension */

  ENTER_PYTHON;
  dict = PyImport_GetModuleDict(); /* Shared; no hay que liberarla */
/* http://sourceforge.net/tracker/index.php?func=detail&aid=498915&group_id=5470&atid=105470 */
  //PyDict_DelItemString(dict, buf); /* Eliminamos modulo antiguo */
  PyMapping_DelItemString(dict, buf); /* Eliminamos modulo antiguo */

  PyErr_Clear();                /* Los errores hasta aqui son normales */

  mod = PyImport_ImportModule(buf);
  if (mod == NULL) {
    modulo_excepcion();
    EXIT_PYTHON2;
    return "Error al importar el modulo";
  }

/*
** Tenemos el modulo. Ahora miramos la funcion de 'arranque'
*/
  mod_inicio = PyObject_GetAttrString(mod, "inicio");
  if (mod_inicio == NULL) {
    Py_DECREF(mod);
    EXIT_PYTHON2;
    return "El modulo indicado no tiene un atributo 'inicio'";
  }

  if (!PyCallable_Check(mod_inicio)) {
    Py_DECREF(mod);
    Py_DECREF(mod_inicio);
    EXIT_PYTHON2;
    return "El atributo 'inicio' del modulo indicado no es una funcion";
  }

  EXIT_PYTHON;
  m = malloc(sizeof(struct modulo));
  if (!m)
    return "No hay memoria";

  nombre = malloc(strlen(buf) + 1);
  if (!nombre)
    return "No hay memoria";

  strcpy(nombre, buf);

  *modulo_python = m;
  m->nombre = nombre;
  m->lib = mod;
  m->inicio = mod_inicio;
  m->siguiente = modulos;
  m->anterior = &modulos;
  modulos = m;

  return NULL;
}

static PyObject *olimpo_methods_especifica_fin(PyObject * self,
                                               PyObject * fin)
{
  if (!PyCallable_Check(fin)) {
    PyErr_SetString(PyExc_TypeError,
                    "El objeto pasado a 'especifica_fin' no es una funcion valida");
    return NULL;
  }
  Py_XINCREF(fin);
  especifica_fin((void *) fin);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *olimpo_methods_comentario_modulo(PyObject * self,
                                                  PyObject * args)
{
  char *comentario;

  if (!PyArg_ParseTuple(args, "s", &comentario))
    return NULL;
  comentario_modulo(comentario);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *olimpo_methods_hace_log(PyObject * self, PyObject * args)
{
  int nick;
  char *comentario;

  if (!PyArg_ParseTuple(args, "is", &nick, &comentario))
    return NULL;
  hace_log(nick, comentario);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *olimpo_methods_hace_log2(PyObject * self, PyObject * args)
{
  char *comentario;

  if (!PyArg_ParseTuple(args, "s", &comentario))
    return NULL;
  hace_log2(comentario);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *olimpo_methods_debug(PyObject * self)
{
  return PyInt_FromLong(debug());
}

static PyObject *privmsg_methods_nuevo_nick(PyObject * self, PyObject * args)
{
  char *nick, *modos;
  PyObject *funcion;
  int ok;
  int result;

  ok = PyArg_ParseTuple(args, "ssO", &nick, &modos, &funcion);

  if (!ok)
    return NULL;
  if (!PyCallable_Check(funcion)) {
    PyErr_SetString(PyExc_TypeError,
                    "El objeto pasado a 'nuevo_nick' no es una funcion valida");
    return NULL;
  }
  Py_XINCREF(funcion);
  result = nuevo_nick(nick, modos, (void *) funcion);
  return PyInt_FromLong(result);
}

static PyObject *privmsg_methods_quit_nick(PyObject * self, PyObject * args)
{
  int handle;

  if (!PyArg_ParseTuple(args, "i", &handle))
    return NULL;

  quit_nick(handle);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *privmsg_methods_envia_nick(PyObject * self, PyObject * args)
{
  int handle, destino;
  char *mensaje;
  int result;

  if (!PyArg_ParseTuple(args, "iis", &handle, &destino, &mensaje))
    return NULL;
  result = envia_nick(handle, destino, mensaje);
  return PyInt_FromLong((long) result);
}

static PyObject *privmsg_methods_lee_flags_nick(PyObject * self,
                                                PyObject * args)
{
  int handle;
  char flags[1024];
  char *result;

  if (!PyArg_ParseTuple(args, "i", &handle))
    return NULL;
  result = lee_flags_nick(handle, flags);
  if (result)
    return PyString_FromString(result);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *privmsg_methods_lee_host_nick(PyObject * self,
                                               PyObject * args)
{
  int handle;
  char host[1024];
  char *result;

  if (!PyArg_ParseTuple(args, "i", &handle))
    return NULL;
  result = lee_host_nick(handle, host);
  if (result)
    return PyString_FromString(result);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *privmsg_methods_lee_ip_nick(PyObject * self, PyObject * args)
{
  int handle;
  unsigned long result;

  if (!PyArg_ParseTuple(args, "i", &handle))
    return NULL;
  result = lee_ip_nick(handle);
  if (result)
    return Py_BuildValue("(iiii)", (result >> 24) & 255, (result >> 16) & 255,
                         (result >> 8) & 255, result & 255);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *privmsg_methods_lee_nick(PyObject * self, PyObject * args)
{
  int handle;
  char nick[1024];
  char *result;

  if (!PyArg_ParseTuple(args, "i", &handle))
    return NULL;
  result = lee_nick(handle, nick);
  if (result)
    return PyString_FromString(result);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *privmsg_methods_lee_nick_normalizado(PyObject * self,
                                                      PyObject * args)
{
  int handle;
  char nick[1024];
  char *result;

  if (!PyArg_ParseTuple(args, "i", &handle))
    return NULL;
  result = lee_nick_normalizado(handle, nick);
  if (result)
    return PyString_FromString(result);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *privmsg_methods_lee_handle(PyObject * self, PyObject * args)
{
  char *nick;
  int handle;

  if (!PyArg_ParseTuple(args, "s", &nick))
    return NULL;

  handle = lee_handle(nick);
  if (handle == -1) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  return PyInt_FromLong(handle);
}

static PyObject *servmsg_methods_serv_callback(PyObject * self,
                                               PyObject * args)
{
  int handle;
  PyObject *funcion;
  int ok;
  int result;

  ok = PyArg_ParseTuple(args, "iO", &handle, &funcion);
  if (!ok)
    return NULL;
  if (funcion == Py_None) {     /* Es un 'singleton' y se puede comparar 'a saco' */
    funcion = NULL;
  }
  else {
    if (!PyCallable_Check(funcion)) {
      PyErr_SetString(PyExc_TypeError,
                      "El objeto pasado a 'serv_callback' no es una funcion valida");
      return NULL;
    }
    Py_XINCREF(funcion);
  }
  result = serv_callback(handle, (void *) funcion);
  return PyInt_FromLong(result);
}

static PyObject *servmsg_methods_envia_raw(PyObject * self, PyObject * args)
{
  int handle;
  char *comando;
  int result;

  if (!PyArg_ParseTuple(args, "is", &handle, &comando))
    return NULL;
  result = envia_raw(handle, comando);
  return PyInt_FromLong((long) result);
}

static PyObject *servmsg_methods_envia_raw2(PyObject * self, PyObject * args)
{
  char *comando;
  int result;

  if (!PyArg_ParseTuple(args, "s", &comando))
    return NULL;
  result = envia_raw2(comando);
  return PyInt_FromLong((long) result);
}

static PyObject *servmsg_methods_intercepta_comando(PyObject * self,
                                                    PyObject * args)
{
  PyObject *funcion;
  char *comando;
  int ok;

  ok = PyArg_ParseTuple(args, "sO", &comando, &funcion);
  if (!ok)
    return NULL;
  if (funcion == Py_None) {     /* Es un 'singleton' y se puede comparar 'a saco' */
    funcion = NULL;
  }
  else {
    if (!PyCallable_Check(funcion)) {
      PyErr_SetString(PyExc_TypeError,
                      "El objeto pasado a 'intercepta_comando' no es una funcion valida");
      return NULL;
    }
    Py_XINCREF(funcion);
  }
  intercepta_comando(comando, (void *) funcion);
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *notify_methods_notifica_nick_registrado(PyObject * self,
                                                         PyObject * funcion)
{
  if (funcion == Py_None) {     /* Es un 'singleton' y se puede comparar 'a saco' */
    notifica_nick_registrado(NULL);
  }
  else {
    if (!PyCallable_Check(funcion)) {
      PyErr_SetString(PyExc_TypeError,
                      "El objeto pasado a 'notifica_nick_registrado' no es una funcion valida");
      return NULL;
    }

    Py_XINCREF(funcion);
    notifica_nick_registrado((void *) funcion);
  }

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *notify_methods_notifica_nick_entrada(PyObject * self,
                                                      PyObject * funcion)
{
  if (funcion == Py_None) {     /* Es un 'singleton' y se puede comparar 'a saco' */
    notifica_nick_entrada(NULL);
  }
  else {
    if (!PyCallable_Check(funcion)) {
      PyErr_SetString(PyExc_TypeError,
                      "El objeto pasado a 'notifica_nick_entrada' no es una funcion valida");
      return NULL;
    }

    Py_XINCREF(funcion);
    notifica_nick_entrada((void *) funcion);
  }

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *notify_methods_notifica_nick_salida(PyObject * self,
                                                     PyObject * funcion)
{
  if (funcion == Py_None) {     /* Es un 'singleton' y se puede comparar 'a saco' */
    notifica_nick_salida(NULL);
  }
  else {
    if (!PyCallable_Check(funcion)) {
      PyErr_SetString(PyExc_TypeError,
                      "El objeto pasado a 'notifica_nick_salida' no es una funcion valida");
      return NULL;
    }

    Py_XINCREF(funcion);
    notifica_nick_salida((void *) funcion);
  }

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *notify_methods_notifica_timer(PyObject * self,
                                               PyObject * args)
{
  PyObject *funcion;
  int tiempo;
  int ok;

  ok = PyArg_ParseTuple(args, "iO", &tiempo, &funcion);
  if (!ok)
    return NULL;

  if (funcion != Py_None) {     /* Es un 'singleton' y se puede comparar 'a saco' */
    if (!PyCallable_Check(funcion)) {
      PyErr_SetString(PyExc_TypeError,
                      "El objeto pasado a 'notifica_timer' no es una funcion valida");
      return NULL;
    }
  }
  else {
    tiempo = 0;
  }

  Py_XINCREF(funcion);
  notifica_timer(tiempo, (void *) funcion);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *notify_methods_notifica_server_join(PyObject * self,
                                                     PyObject * funcion)
{
  if (funcion == Py_None) {     /* Es un 'singleton' y se puede comparar 'a saco' */
    notifica_server_join(NULL);
  }
  else {
    if (!PyCallable_Check(funcion)) {
      PyErr_SetString(PyExc_TypeError,
                      "El objeto pasado a 'notifica_server_join' no es una funcion valida");
      return NULL;
    }

    Py_XINCREF(funcion);
    notifica_server_join((void *) funcion);
  }

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *notify_methods_notifica_server_split(PyObject * self,
                                                      PyObject * funcion)
{
  if (funcion == Py_None) {     /* Es un 'singleton' y se puede comparar 'a saco' */
    notifica_server_split(NULL);
  }
  else {
    if (!PyCallable_Check(funcion)) {
      PyErr_SetString(PyExc_TypeError,
                      "El objeto pasado a 'notifica_server_split' no es una funcion valida");
      return NULL;
    }

    Py_XINCREF(funcion);
    notifica_server_split((void *) funcion);
  }

  Py_INCREF(Py_None);
  return Py_None;
}


static PyObject *notify_methods_notifica_servers_split_end(PyObject * self,
                                                           PyObject * funcion)
{
  if (funcion == Py_None) {     /* Es un 'singleton' y se puede comparar 'a saco' */
    notifica_servers_split_end(NULL);
  }
  else {
    if (!PyCallable_Check(funcion)) {
      PyErr_SetString(PyExc_TypeError,
                      "El objeto pasado a 'notifica_servers_split_end' no es una funcion valida");
      return NULL;
    }

    Py_XINCREF(funcion);
    notifica_servers_split_end((void *) funcion);
  }

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *notify_methods_notifica_servers(PyObject * self)
{
/*
** Lo que sigue no es necesario con METH_NOARGS
** pero lo mantengo con fines de documentacion historica

  int ok;

  ok = PyArg_ParseTuple(args, "");

  if (!ok)
    return NULL;
*/

  notifica_servers();

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *notify_db_methods_notifica_nickdrop(PyObject * self,
                                                     PyObject * args)
{
  int num_serie;
  PyObject *funcion;
  int ok;

  ok = PyArg_ParseTuple(args, "Oi", &funcion, &num_serie);

  if (!ok)
    return NULL;

  if (funcion == Py_None) {     /* Es un 'singleton' y se puede comparar 'a saco' */
    notifica_db_nickdrop(NULL, num_serie);
  }
  else {
    if (!PyCallable_Check(funcion)) {
      PyErr_SetString(PyExc_TypeError,
                      "El objeto pasado a 'notifica_nickdrop' no es una funcion valida");
      return NULL;
    }

    Py_XINCREF(funcion);
    notifica_db_nickdrop((void *) funcion, num_serie);
  }

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *bdd_methods_envia_registro(PyObject * self, PyObject * args)
{
  unsigned char bd;
  char *clave, *valor;

  if (!PyArg_ParseTuple(args, "csz", &bd, &clave, &valor))
    return NULL;

  return PyInt_FromLong(bdd_envia_registro(bd, clave, valor));
}

static PyObject *bdd_methods_flujo_abierto(PyObject * self, PyObject * args)
{
  unsigned char bd;

  if (!PyArg_ParseTuple(args, "c", &bd))
    return NULL;

  if (bdd_flujo_abierto(bd)) {
    Py_INCREF(Py_True);
    return Py_True;
  }
  else {
    Py_INCREF(Py_False);
    return Py_False;
  }
}

static PyObject *tools_methods_base64toint(PyObject * self, PyObject * args)
{
  char *cadena;
  unsigned long result;

  if (!PyArg_ParseTuple(args, "s", &cadena))
    return NULL;

  result = base64toint(cadena);
  return Py_BuildValue("(ii)", result >> 16, result & 65535);
}

PyObject *tools_methods_handle2nicknumeric(PyObject * self, PyObject * args)
{
  char buf[6];
  int handle;

  if (!PyArg_ParseTuple(args, "i", &handle))
    return NULL;

  handle2nicknumeric(handle, buf);

  return PyString_FromString(buf);
}

PyObject *tools_methods_nicknumeric2handle(PyObject * self, PyObject * args)
{
  char *numeric;
  long handle;

  if (!PyArg_ParseTuple(args, "s", &numeric))
    return NULL;

  handle = nicknumeric2handle(numeric);

  return PyInt_FromLong(handle);
}

PyObject *tools_methods_handle2servernumeric(PyObject * self, PyObject * args)
{
  char buf[3];
  int handle;

  if (!PyArg_ParseTuple(args, "i", &handle))
    return NULL;

  handle2servernumeric(handle, buf);

  return PyString_FromString(buf);
}

PyObject *tools_methods_servernumeric2handle(PyObject * self, PyObject * args)
{
  char *numeric;
  long handle;

  if (!PyArg_ParseTuple(args, "s", &numeric))
    return NULL;

  handle = servernumeric2handle(numeric);

  return PyInt_FromLong(handle);
}

static PyObject *tools_methods_get_entropia(PyObject * self)
{
  unsigned long long result;

  result = get_entropia();
  return Py_BuildValue("(iiii)", result >> 48, (result >> 32) & 65535,
                       (result >> 16) & 65535, result & 65535);
}

static PyObject *tools_methods_set_entropia(PyObject * self, PyObject * args)
{
  int result;

  if (!PyArg_ParseTuple(args, "i", &result))
    return NULL;

  set_entropia(result);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *tools_methods_tea_crypt(PyObject * self, PyObject * args)
{
  unsigned long v[2], x[2], k[4];
  unsigned int v_hi_hi, v_hi_lo, v_lo_hi, v_lo_lo;
  unsigned int k_hi_hi_hi, k_hi_hi_lo, k_hi_lo_hi, k_hi_lo_lo;
  unsigned int k_lo_hi_hi, k_lo_hi_lo, k_lo_lo_hi, k_lo_lo_lo;

  if (!PyArg_ParseTuple
      (args, "(iiii)(iiiiiiii)", &v_hi_hi, &v_hi_lo, &v_lo_hi, &v_lo_lo,
       &k_hi_hi_hi, &k_hi_hi_lo, &k_hi_lo_hi, &k_hi_lo_lo, &k_lo_hi_hi,
       &k_lo_hi_lo, &k_lo_lo_hi, &k_lo_lo_lo))
    return NULL;

  v[0] = (((unsigned long) v_hi_hi) << 16) + v_hi_lo;
  v[1] = (((unsigned long) v_lo_hi) << 16) + v_lo_lo;
  k[0] = (((unsigned long) k_hi_hi_hi) << 16) + k_hi_hi_lo;
  k[1] = (((unsigned long) k_hi_lo_hi) << 16) + k_hi_lo_lo;
  k[2] = (((unsigned long) k_lo_hi_hi) << 16) + k_lo_hi_lo;
  k[3] = (((unsigned long) k_lo_lo_hi) << 16) + k_lo_lo_lo;
  tea_crypt(v, x, k);
  v_hi_hi = x[0] >> 16;
  v_hi_lo = x[0] & 65535;
  v_lo_hi = x[1] >> 16;
  v_lo_lo = x[1] & 65535;
  return Py_BuildValue("(iiii)", v_hi_hi, v_hi_lo, v_lo_hi, v_lo_lo);
}

static PyObject *tools_methods_tea_decrypt(PyObject * self, PyObject * args)
{
  unsigned long v[2], x[2], k[4];
  unsigned int v_hi_hi, v_hi_lo, v_lo_hi, v_lo_lo;
  unsigned int k_hi_hi_hi, k_hi_hi_lo, k_hi_lo_hi, k_hi_lo_lo;
  unsigned int k_lo_hi_hi, k_lo_hi_lo, k_lo_lo_hi, k_lo_lo_lo;

  if (!PyArg_ParseTuple
      (args, "(iiii)(iiiiiiii)", &v_hi_hi, &v_hi_lo, &v_lo_hi, &v_lo_lo,
       &k_hi_hi_hi, &k_hi_hi_lo, &k_hi_lo_hi, &k_hi_lo_lo, &k_lo_hi_hi,
       &k_lo_hi_lo, &k_lo_lo_hi, &k_lo_lo_lo))
    return NULL;

  v[0] = (((unsigned long) v_hi_hi) << 16) + v_hi_lo;
  v[1] = (((unsigned long) v_lo_hi) << 16) + v_lo_lo;
  k[0] = (((unsigned long) k_hi_hi_hi) << 16) + k_hi_hi_lo;
  k[1] = (((unsigned long) k_hi_lo_hi) << 16) + k_hi_lo_lo;
  k[2] = (((unsigned long) k_lo_hi_hi) << 16) + k_lo_hi_lo;
  k[3] = (((unsigned long) k_lo_lo_hi) << 16) + k_lo_lo_lo;
  tea_decrypt(v, x, k);
  v_hi_hi = x[0] >> 16;
  v_hi_lo = x[0] & 65535;
  v_lo_hi = x[1] >> 16;
  v_lo_lo = x[1] & 65535;
  return Py_BuildValue("(iiii)", v_hi_hi, v_hi_lo, v_lo_hi, v_lo_lo);
}

static PyObject *tools_methods_inttobase64(PyObject * self, PyObject * args)
{
  int hi, lo;
  const char *result;

  if (!PyArg_ParseTuple(args, "(ii)", &hi, &lo))
    return NULL;

  result = inttobase64((((unsigned long) hi) << 16) + lo);

  return PyString_FromString(result);
}

static PyObject *tools_methods_which_server(PyObject * self, PyObject * args)
{
  char buf[1024], *result;
  int handle;

  if (!PyArg_ParseTuple(args, "i", &handle))
    return NULL;

  result = which_server(handle, buf);

  if (!result) {
    Py_INCREF(Py_None);
    return Py_None;
  }

  return PyString_FromString(result);
}

static PyObject *tools_methods_nick_normalizado(PyObject * self,
                                                PyObject * args)
{
  char buf[1024];
  char *p;

  if (!PyArg_ParseTuple(args, "s", &p))
    return NULL;

  return PyString_FromString(nick_normalizado(p, buf));
}

static PyObject *berkeleydb_methods_version(PyObject * self)
{
  char *result;

  result = mod_version_db();
  if (result)
    return PyString_FromString(result);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *berkeleydb_methods_abre_db(PyObject * self, PyObject * args)
{
  char *nombre;
  p_db result;

  if (!PyArg_ParseTuple(args, "s", &nombre))
    return NULL;
  result = mod_abre_db(nombre);
  if (result)
    return PyCObject_FromVoidPtr(result, NULL);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *berkeleydb_methods_cierra_db(PyObject * self,
                                              PyObject * CObject)
{
  p_db result;

  if (!PyCObject_Check(CObject)) {
    PyErr_SetString(PyExc_TypeError, "Parametro de tipo incorrecto");
    return NULL;
  }

  result = PyCObject_AsVoidPtr(CObject);
  mod_cierra_db(result);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *berkeleydb_methods_inicia_txn(PyObject * self)
{
  txn result;

  result = mod_inicia_txn_db();
  if (result)
    return PyCObject_FromVoidPtr(result, NULL);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *berkeleydb_methods_inicia_txn_no_sync(PyObject * self)
{
  txn result;

  result = mod_inicia_txn_db_no_sync();
  if (result)
    return PyCObject_FromVoidPtr(result, NULL);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *berkeleydb_methods_inicia_txn_no_wait(PyObject * self)
{
  txn result;

  result = mod_inicia_txn_db_no_wait();
  if (result)
    return PyCObject_FromVoidPtr(result, NULL);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *berkeleydb_methods_compromete_txn(PyObject * self,
                                                   PyObject * CObject)
{
  p_db result;

  if (!PyCObject_Check(CObject)) {
    PyErr_SetString(PyExc_TypeError, "Parametro de tipo incorrecto");
    return NULL;
  }

  result = PyCObject_AsVoidPtr(CObject);
  mod_compromete_txn_db(result);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *berkeleydb_methods_aborta_txn(PyObject * self,
                                               PyObject * CObject)
{
  p_db result;

  if (!PyCObject_Check(CObject)) {
    PyErr_SetString(PyExc_TypeError, "Parametro de tipo incorrecto");
    return NULL;
  }

  result = PyCObject_AsVoidPtr(CObject);
  mod_aborta_txn_db(result);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *berkeleydb_methods_get(PyObject * self, PyObject * args)
{
  PyObject *db, *txn;
  char *key;
  int key_len;
  dbt clave, contenido;
  PyObject *content;
  int result;

  if (!PyArg_ParseTuple(args, "OOs#", &db, &txn, &key, &key_len))
    return NULL;

  if (txn == Py_None) {         /* Es un 'singleton' y se puede comparar 'a saco' */
    txn = NULL;
  }

  if ((!PyCObject_Check(db)) || (txn && !PyCObject_Check(txn))) {
    PyErr_SetString(PyExc_TypeError, "Parametro de tipo incorrecto");
    return NULL;
  }

  if (!key_len) {
    PyErr_SetString(PyExc_TypeError, "No se puede utilizar una clave vacia");
    return NULL;
  }

  clave.datos = key;
  clave.len = key_len;

  if (txn) {
    result =
      mod_get_db(PyCObject_AsVoidPtr(db), PyCObject_AsVoidPtr(txn), &clave,
                 &contenido);
  }
  else {
    result = mod_get_db(PyCObject_AsVoidPtr(db), NULL, &clave, &contenido);
  }

  if (result) {
    return Py_BuildValue("iO", result, Py_None);
  }

  if (contenido.len) {
    content = PyString_FromStringAndSize(contenido.datos, contenido.len);
    free(contenido.datos);
  }
  else {
    content = PyString_FromString("");
  }
  return Py_BuildValue("iN", result, content);
}

static PyObject *berkeleydb_methods_put(PyObject * self, PyObject * args)
{
  PyObject *db, *txn;
  char *key;
  int key_len;
  char *content;
  int content_len;
  dbt clave, contenido;
  int result;

  if (!PyArg_ParseTuple
      (args, "OOs#s#", &db, &txn, &key, &key_len, &content, &content_len))
    return NULL;

  if (txn == Py_None) {         /* Es un 'singleton' y se puede comparar 'a saco' */
    txn = NULL;
  }

  if ((!PyCObject_Check(db)) || (txn && !PyCObject_Check(txn))) {
    PyErr_SetString(PyExc_TypeError, "Parametro de tipo incorrecto");
    return NULL;
  }

  if (!key_len) {
    PyErr_SetString(PyExc_TypeError, "No se puede utilizar una clave vacia");
    return NULL;
  }

  clave.datos = key;
  clave.len = key_len;
  contenido.datos = content;
  contenido.len = content_len;

  if (txn) {
    result =
      mod_put_db(PyCObject_AsVoidPtr(db), PyCObject_AsVoidPtr(txn), &clave,
                 &contenido);
  }
  else {
    result = mod_put_db(PyCObject_AsVoidPtr(db), NULL, &clave, &contenido);
  }

  return PyInt_FromLong(result);
}

static PyObject *berkeleydb_methods_delete(PyObject * self, PyObject * args)
{
  PyObject *db, *txn;
  char *key;
  int key_len;
  dbt clave;
  int result;

  if (!PyArg_ParseTuple(args, "OOs#", &db, &txn, &key, &key_len))
    return NULL;

  if (txn == Py_None) {         /* Es un 'singleton' y se puede comparar 'a saco' */
    txn = NULL;
  }

  if ((!PyCObject_Check(db)) || (txn && !PyCObject_Check(txn))) {
    PyErr_SetString(PyExc_TypeError, "Parametro de tipo incorrecto");
    return NULL;
  }

  if (!key_len) {
    PyErr_SetString(PyExc_TypeError, "No se puede utilizar una clave vacia");
    return NULL;
  }

  clave.datos = key;
  clave.len = key_len;

  if (txn) {
    result =
      mod_del_db(PyCObject_AsVoidPtr(db), PyCObject_AsVoidPtr(txn), &clave);
  }
  else {
    result = mod_del_db(PyCObject_AsVoidPtr(db), NULL, &clave);
  }

  return PyInt_FromLong(result);
}


static PyObject *berkeleydb_methods_cursor(PyObject * self, PyObject * args)
{
  PyObject *db, *txn;
  c_db *result;
  dbt clave;
  int bulk;

  if (!PyArg_ParseTuple
      (args, "OOz#i", &db, &txn, &(clave.datos), &(clave.len), &bulk))
    return NULL;

  if ((!PyCObject_Check(db)) || (!PyCObject_Check(txn))) {
    PyErr_SetString(PyExc_TypeError, "Parametro de tipo incorrecto");
    return NULL;
  }

  result =
    mod_cursor_db(PyCObject_AsVoidPtr(db), PyCObject_AsVoidPtr(txn),
                  clave.datos ? &clave : NULL, bulk);

  if (result)
    return PyCObject_FromVoidPtr(result, NULL);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *berkeleydb_methods_cursor_get(PyObject * self,
                                               PyObject * cursor)
{
  dbt clave, contenido;
  PyObject *key, *content;
  int result;
  c_db *c;

  if (!PyCObject_Check(cursor)) {
    PyErr_SetString(PyExc_TypeError, "Parametro de tipo incorrecto");
    return NULL;
  }

  c = PyCObject_AsVoidPtr(cursor);

  result = mod_cursor_get_db(c, &clave, &contenido);

  if (result) {
    return Py_BuildValue("iOO", result, Py_None, Py_None);
  }

  if (clave.len) {
    key = PyString_FromStringAndSize(clave.datos, clave.len);
    if (!(c->mem))
      free(clave.datos);
  }
  else {
    key = PyString_FromString("");
  }

  if (contenido.len) {
    content = PyString_FromStringAndSize(contenido.datos, contenido.len);
    if (!(c->mem))
      free(contenido.datos);
  }
  else {
    content = PyString_FromString("");
  }

  return Py_BuildValue("iNN", result, key, content);
}

static PyObject *berkeleydb_methods_cursor_close(PyObject * self,
                                                 PyObject * cursor)
{
  int result;

  if (!PyCObject_Check(cursor)) {
    PyErr_SetString(PyExc_TypeError, "Parametro de tipo incorrecto");
    return NULL;
  }

  result = mod_cursor_close_db(PyCObject_AsVoidPtr(cursor));

  return PyInt_FromLong(result);
}

static PyObject *berkeleydb_methods_error(PyObject * self)
{
  char *result;

  result = mod_strerror_db();
  if (result)
    return PyString_FromString(result);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *ipc_methods_nombre_modulo(PyObject * self)
{
  return PyString_FromString(nombre_modulo());
}

static PyObject *ipc_methods_existe_nombre_modulo(PyObject * self,
                                                  PyObject * args)
{
  char *nombre;

  if (!PyArg_ParseTuple(args, "s", &nombre))
    return NULL;
  return PyInt_FromLong(existe_nombre_modulo(nombre));
}

static PyObject *ipc_methods_id_modulo(PyObject * self, PyObject * args)
{
  char *nombre;

  if (!PyArg_ParseTuple(args, "s", &nombre))
    return NULL;
  return PyInt_FromLong(id_modulo(nombre));
}

static PyObject *ipc_methods_existe_id_modulo(PyObject * self,
                                              PyObject * args)
{
  int id;

  if (!PyArg_ParseTuple(args, "i", &id))
    return NULL;
  return PyInt_FromLong(existe_id_modulo(id));
}

static PyObject *ipc_methods_IRQ_handler(PyObject * self, PyObject * funcion)
{
  if (funcion == Py_None) {     /* Es un 'singleton' y se puede comparar 'a saco' */
    IRQ_handler(NULL);
  }
  else {
    if (!PyCallable_Check(funcion)) {
      PyErr_SetString(PyExc_TypeError,
                      "El objeto pasado a 'IRQ_handler' no es una funcion valida");
      return NULL;
    }
    Py_XINCREF(funcion);
    IRQ_handler((void *) funcion);
  }

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *ipc_methods_IRQ_nombre(PyObject * self, PyObject * args)
{
  char *modulo;
  int ok;

  ok = PyArg_ParseTuple(args, "s", &modulo);
  if (!ok)
    return NULL;

  return PyInt_FromLong(IRQ_nombre(modulo));
}

static PyObject *ipc_methods_IRQ_id(PyObject * self, PyObject * args)
{
  int id;
  int ok;

  ok = PyArg_ParseTuple(args, "i", &id);
  if (!ok)
    return NULL;

  return PyInt_FromLong(IRQ_id(id));
}


static PyMethodDef olimpo_methods[] = {
  {"especifica_fin", (PyCFunction) olimpo_methods_especifica_fin, METH_O},
  {"comentario_modulo", olimpo_methods_comentario_modulo, METH_VARARGS},
  {"hace_log", olimpo_methods_hace_log, METH_VARARGS},
  {"hace_log2", olimpo_methods_hace_log2, METH_VARARGS},
  {"debug", (PyCFunction) olimpo_methods_debug, METH_NOARGS},
  {NULL, NULL, 0}               /* sentinel */
};

static PyMethodDef privmsg_methods[] = {
  {"nuevo_nick", privmsg_methods_nuevo_nick, METH_VARARGS},
  {"quit_nick", privmsg_methods_quit_nick, METH_VARARGS},
  {"envia_nick", privmsg_methods_envia_nick, METH_VARARGS},
  {"lee_host_nick", privmsg_methods_lee_host_nick, METH_VARARGS},
  {"lee_ip_nick", privmsg_methods_lee_ip_nick, METH_VARARGS},
  {"lee_flags_nick", privmsg_methods_lee_flags_nick, METH_VARARGS},
  {"lee_nick", privmsg_methods_lee_nick, METH_VARARGS},
  {"lee_nick_normalizado", privmsg_methods_lee_nick_normalizado,
   METH_VARARGS},
  {"lee_handle", privmsg_methods_lee_handle, METH_VARARGS},
  {NULL, NULL, 0}               /* sentinel */
};

static PyMethodDef bdd_methods[] = {
  {"envia_registro", bdd_methods_envia_registro, METH_VARARGS},
  {"flujo_abierto", bdd_methods_flujo_abierto, METH_VARARGS},
  {NULL, NULL, 0}               /* sentinel */
};

static PyMethodDef tools_methods[] = {
  {"inttobase64", tools_methods_inttobase64, METH_VARARGS},
  {"base64toint", tools_methods_base64toint, METH_VARARGS},
  {"tea_crypt", tools_methods_tea_crypt, METH_VARARGS},
  {"tea_decrypt", tools_methods_tea_decrypt, METH_VARARGS},
  {"which_server", tools_methods_which_server, METH_VARARGS},
  {"nick_normalizado", tools_methods_nick_normalizado, METH_VARARGS},
  {"get_entropia", (PyCFunction) tools_methods_get_entropia, METH_NOARGS},
  {"set_entropia", tools_methods_set_entropia, METH_VARARGS},
  {"handle2nicknumeric", tools_methods_handle2nicknumeric, METH_VARARGS},
  {"nicknumeric2handle", tools_methods_nicknumeric2handle, METH_VARARGS},
  {"handle2servernumeric", tools_methods_handle2servernumeric, METH_VARARGS},
  {"servernumeric2handle", tools_methods_servernumeric2handle, METH_VARARGS},
  {NULL, NULL, 0}               /* sentinel */
};

static PyMethodDef servmsg_methods[] = {
  {"serv_callback", servmsg_methods_serv_callback, METH_VARARGS},
  {"envia_raw", servmsg_methods_envia_raw, METH_VARARGS},
  {"envia_raw2", servmsg_methods_envia_raw2, METH_VARARGS},
  {"intercepta_comando", servmsg_methods_intercepta_comando, METH_VARARGS},
  {NULL, NULL, 0}               /* sentinel */
};

static PyMethodDef notify_methods[] = {
  {"notifica_nick_entrada",
   (PyCFunction) notify_methods_notifica_nick_entrada,
   METH_O},
  {"notifica_nick_salida", (PyCFunction) notify_methods_notifica_nick_salida,
   METH_O},
  {"notifica_nicks", (PyCFunction) notify_methods_notifica_nicks,
   METH_NOARGS},
  {"notifica_nick_registrado",
   (PyCFunction) notify_methods_notifica_nick_registrado,
   METH_O},
  {"notifica_nicks_registrados",
   (PyCFunction) notify_methods_notifica_nicks_registrados,
   METH_NOARGS},
  {"notifica_server_join", (PyCFunction) notify_methods_notifica_server_join,
   METH_O},
  {"notifica_server_split",
   (PyCFunction) notify_methods_notifica_server_split,
   METH_O},
  {"notifica_servers_split_end",
   (PyCFunction) notify_methods_notifica_servers_split_end,
   METH_O},
  {"notifica_servers", (PyCFunction) notify_methods_notifica_servers,
   METH_NOARGS},
  {"notifica_timer", notify_methods_notifica_timer, METH_VARARGS},
  {NULL, NULL, 0}               /* sentinel */
};

static PyMethodDef ipc_methods[] = {
  {"nombre_modulo", (PyCFunction) ipc_methods_nombre_modulo, METH_NOARGS},
  {"existe_nombre_modulo", ipc_methods_existe_nombre_modulo, METH_VARARGS},
  {"id_modulo", ipc_methods_id_modulo, METH_VARARGS},
  {"existe_id_modulo", ipc_methods_existe_id_modulo, METH_VARARGS},
  {"IRQ_handler", (PyCFunction) ipc_methods_IRQ_handler, METH_O},
  {"IRQ_nombre", ipc_methods_IRQ_nombre, METH_VARARGS},
  {"IRQ_id", ipc_methods_IRQ_id, METH_VARARGS},
  {NULL, NULL, 0}               /* sentinel */
};

static PyMethodDef notify_db_methods[] = {
  {"notifica_nickdrop", notify_db_methods_notifica_nickdrop,
   METH_VARARGS},
  {NULL, NULL, 0}               /* sentinel */
};

static PyMethodDef berkeleydb_methods[] = {
  {"version", (PyCFunction) berkeleydb_methods_version,
   METH_NOARGS},
  {"abre_db", berkeleydb_methods_abre_db,
   METH_VARARGS},
  {"cierra_db", (PyCFunction) berkeleydb_methods_cierra_db,
   METH_O},
  {"inicia_txn", (PyCFunction) berkeleydb_methods_inicia_txn,
   METH_NOARGS},
  {"inicia_txn_no_sync", (PyCFunction) berkeleydb_methods_inicia_txn_no_sync,
   METH_NOARGS},
  {"inicia_txn_no_wait", (PyCFunction) berkeleydb_methods_inicia_txn_no_wait,
   METH_NOARGS},
  {"compromete_txn", (PyCFunction) berkeleydb_methods_compromete_txn,
   METH_O},
  {"aborta_txn", (PyCFunction) berkeleydb_methods_aborta_txn,
   METH_O},
  {"get", berkeleydb_methods_get,
   METH_VARARGS},
  {"put", berkeleydb_methods_put,
   METH_VARARGS},
  {"delete", berkeleydb_methods_delete,
   METH_VARARGS},
  {"cursor", berkeleydb_methods_cursor,
   METH_VARARGS},
  {"cursor_get", (PyCFunction) berkeleydb_methods_cursor_get,
   METH_O},
  {"cursor_close", (PyCFunction) berkeleydb_methods_cursor_close,
   METH_O},
  {"error", (PyCFunction) berkeleydb_methods_error,
   METH_NOARGS},
  {NULL, NULL, 0}               /* sentinel */
};



void inicializa_python(void)
{
  PyObject *olimpo, *olimpo_dict, *olimpo_mod;
  char buf[1024];
  char buf_write[1000];

  if (!Py_IsInitialized()) {
    Py_Initialize();
    PyEval_InitThreads();       /* Create and acquire the lock */
    mainThreadState = PyThreadState_Get();
  }

  sprintf(buf_write, "Interprete Python: %s\n", Py_GetVersion());
  write(1, buf_write, strlen(buf_write)); /* STDOUT */

/* Definimos el PATH de busqueda de modulos */
  PyRun_SimpleString("import sys");
  sprintf(buf,
          "sys.path=['%s/modulos_python_core','%s/modulos_python']+sys.path",
          LIB_PATH, LIB_PATH);
  PyRun_SimpleString(buf);
  PyRun_SimpleString("import olimpo_init");

/* Modulos Olimpo */
  olimpo = PyImport_AddModule("Olimpo");
  Py_InitModule("Olimpo", olimpo_methods);
  olimpo_dict = PyObject_GetAttrString(olimpo, "__dict__");

  olimpo_mod = PyImport_AddModule("Olimpo.privmsg");
  Py_InitModule("Olimpo.privmsg", privmsg_methods);
  PyMapping_SetItemString(olimpo_dict, "privmsg", olimpo_mod);

  olimpo_mod = PyImport_AddModule("Olimpo.servmsg");
  Py_InitModule("Olimpo.servmsg", servmsg_methods);
  PyMapping_SetItemString(olimpo_dict, "servmsg", olimpo_mod);

  olimpo_mod = PyImport_AddModule("Olimpo.notify");
  Py_InitModule("Olimpo.notify", notify_methods);
  PyMapping_SetItemString(olimpo_dict, "notify", olimpo_mod);

  olimpo_mod = PyImport_AddModule("Olimpo.notify_db");
  Py_InitModule("Olimpo.notify_db", notify_db_methods);
  PyMapping_SetItemString(olimpo_dict, "notify_db", olimpo_mod);

  olimpo_mod = PyImport_AddModule("Olimpo.bdd");
  Py_InitModule("Olimpo.bdd", bdd_methods);
  PyMapping_SetItemString(olimpo_dict, "bdd", olimpo_mod);

  olimpo_mod = PyImport_AddModule("Olimpo.tools");
  Py_InitModule("Olimpo.tools", tools_methods);
  PyMapping_SetItemString(olimpo_dict, "tools", olimpo_mod);

  olimpo_mod = PyImport_AddModule("Olimpo.ipc");
  Py_InitModule("Olimpo.ipc", ipc_methods);
  PyMapping_SetItemString(olimpo_dict, "ipc", olimpo_mod);

  olimpo_mod = PyImport_AddModule("Olimpo._BerkeleyDB");
  Py_InitModule("Olimpo._BerkeleyDB", berkeleydb_methods);
  PyMapping_SetItemString(olimpo_dict, "_BerkeleyDB", olimpo_mod);

  PyRun_SimpleString("olimpo_init.load_core()");

  EXIT_PYTHON2;
}
