/*
** ILINES_LISTA.C
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <signal.h>

#include "berkeleydb.h"

/*
    $Id: ilines_lista.c,v 1.39 2003/07/31 17:59:15 jcea Exp $
*/

static int senal = 0;

void senal_handler(int s)
{
  senal = s;
}

void errores(int st)
{
  char buf_write[1000];

  sprintf(buf_write, "DB: %s\n", strerror_db(st));
  write(2, buf_write, strlen(buf_write)); /* STDERR */
  exit(-1);
}

int main(void)
{
  void *db_ilines;
  void *db_cursor;
  dbt clave;
  dbt contenido;
  void *transaccion;
  unsigned char *p;
  int st = 0;

  //signal(SIGPIPE, SIG_IGN);

  sigset(SIGHUP, senal_handler);
  sigset(SIGINT, senal_handler);
  sigset(SIGQUIT, senal_handler);
  sigset(SIGKILL, senal_handler);
  sigset(SIGPIPE, senal_handler);
  sigset(SIGALRM, senal_handler);
  sigset(SIGTERM, senal_handler);
  sigset(SIGUSR1, senal_handler);
  sigset(SIGUSR2, senal_handler);
  sigset(SIGPOLL, senal_handler);

  sigset(SIGVTALRM, senal_handler);
  sigset(SIGPROF, senal_handler);
  sigset(SIGXCPU, senal_handler);
  sigset(SIGXFSZ, senal_handler);
  /*
   ** De momento vamos a obviar
   ** las senales en tiempo real.
   */

  {
    char buf_write[1000];

    sprintf(buf_write, "%s\n\n", version_db());
    write(1, buf_write, strlen(buf_write)); /* STDOUT */
  }

  abre_berkeley_db();
  db_ilines = abre_db("db.i");
  if (!db_ilines)
    errores(st);

  transaccion = inicia_txn_db_no_wait();

  db_cursor = cursor_db(db_ilines, transaccion, NULL, 1);
  if (st) {
    aborta_txn_db(transaccion);
    errores(st);
  }

  while (!0) {
    if (senal) {
      printf("\n\nLlega la sen~al %d\n", senal);
      break;
    }
    st = cursor_get_db(db_cursor, &clave, &contenido);
    if (st == BERKELEY_DB_NOTFOUND)
      break;

    if (st) {
      cursor_close_db(db_cursor);
      aborta_txn_db(transaccion);
      errores(st);
      break;
    }

    p = (unsigned char *) (contenido.datos);
    printf("%s %lu %lu %lu %s\n", (char *) clave.datos,
           ((unsigned long) *(p) << 24) + ((unsigned long) *(p + 1) << 16) +
           ((unsigned long) *(p + 2) << 8) + ((unsigned long) *(p + 3)),
           ((unsigned long) *(p + 4) << 24) +
           ((unsigned long) *(p + 5) << 16) +
           ((unsigned long) *(p + 6) << 8) + ((unsigned long) *(p + 7)),
           ((unsigned long) *(p + 8) << 24) +
           ((unsigned long) *(p + 9) << 16) +
           ((unsigned long) *(p + 10) << 8) + ((unsigned long) *(p + 11)),
           (char *) contenido.datos + 12);
/*
** Esto no lo necesitamos en modo BULK
**
**    free(contenido.datos);
**    free(clave.datos);
*/
  }

  st = cursor_close_db(db_cursor);
  if (st) {
    aborta_txn_db(transaccion);
    errores(st);
  }

  compromete_txn_db(transaccion);

  cierra_db(db_ilines);
  if (st)
    errores(st);

  cierra_berkeley_db();

  return 0;
}
