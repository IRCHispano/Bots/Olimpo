/*
** COMANDOS_H
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

/*
    $Id: comandos.h,v 1.9 2002/03/11 11:44:17 jcea Exp $
*/

#ifndef COMANDOS_H
#define COMANDOS_H

void envia_squit_raw(void);

void m_squit(char *p, char *buf);
void m_server(char *p, char *buf);
void m_kill(char *p, char *buf);
void m_quit(char *p, char *buf);
void m_ping(char *p, char *buf);
void m_eob(char *p, char *buf);
void m_eob_ack(char *p, char *buf);
void m_mode(char *p, char *buf);
void m_nick(char *p, char *buf);
void m_db(char *p, char *buf);

void m_manda_modulos(char *p, char *buf);

#include <sys/times.h>
#include <limits.h>

extern clock_t tiempo_inicial;  /* Instante de arranque del programa */

#endif
