/*
** M_DB.H
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

/*
    $Id: m_db.h,v 1.14 2002/06/24 09:10:31 jcea Exp $
*/

#ifndef M_DB_H
#define M_DB_H

int bdd_flujo_abierto2(unsigned char bd);
void m_db(char *p, char *buf);
void bdd_offline(void);
void vuelca_contadores_bdd(void);
void lee_contadores_bdd(void);
void envia_m_db(void);

#endif
