/*
** MOD_TOOLS_H
**
** $Id: mod_tools.h,v 1.11 2002/05/29 15:33:46 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#ifndef MOD_TOOLS_H
#define MOD_TOOLS_H

const char *inttobase64(unsigned int i);
unsigned int base64toint(const char *str);

char *which_server(int handle, char *buf);

unsigned char *nick_normalizado(unsigned char *nick, unsigned char *destino);

unsigned long long get_entropia(void);
void set_entropia(unsigned int);

void tea_crypt(const unsigned long *const v, unsigned long *const w,
               const unsigned long *const k);
void tea_decrypt(const unsigned long *const v, unsigned long *const w,
                 const unsigned long *const k);

char *handle2nicknumeric(int handle, char *buf);
int nicknumeric2handle(char *numeric);
char *handle2servernumeric(int handle, char *buf);
int servernumeric2handle(char *numeric);

#endif
