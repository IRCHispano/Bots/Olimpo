/*
** AGENDA_C
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

 /*
    $Id: agenda.c,v 1.83 2001/02/12 22:35:10 jcea Exp $
  */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>


#include "module.h"
#include "mod_privmsg.h"
#include "mod_notify.h"
#include "mod_notify_db.h"

//#define VERBOSE

#ifdef VERBOSE
/* Esto es solo para DEBUG */
static int yo_;
static int nick_;
#endif

static int handle;
static time_t ahora;

static DB_ENV *db_env;
static DB *db_agenda;

static DB_TXN *transaccion;

struct comando {
  char *comando;
  void (*rutina) (int yo, int nick, char *mensaje);
  int parametros;
};

struct agenda_nick {
  unsigned int version;
  unsigned long timestamp_nick; /* Para detectar re-registros de nicks */
  unsigned int num_entradas;
  unsigned char dias[366];
};

static int d_meses[] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
static char *meses[] = { "Enero", "Febrero", "Marzo", "Abril",
  "Mayo", "Junio", "Julio", "Agosto",
  "Septiembre", "Octubre", "Noviembre", "Diciembre"
};

static struct {
  int num_dia;
  char *signo;
} horoscopo[] = { {
18, "Capricornio"}, {
48, "Acuario"}, {
78, "Piscis"}, {
108, "Aries"}, {
139, "Tauro"}, {
170, "G\xE9minis"}, {
202, "C\xE1ncer"}, {
233, "Leo"}, {
264, "Virgo"}, {
294, "Libra"}, {
324, "Escorpio"}, {
354, "Sagitario"}, {
366, "Capricornio"}};

static int febrero_29 = 0;

#define VERSION_DB_ACTUAL	1

static char *escribe_dia_agenda(char *entrada, int dia, char *p, int len)
{
  char buf[1024];
  DBT clave, contenido;
  int st;

  memset(&clave, 0, sizeof(clave));
  clave.flags = DB_DBT_MALLOC;
  memset(&contenido, 0, sizeof(contenido));
  contenido.flags = DB_DBT_MALLOC;

  sprintf(buf, "%s %d", entrada, dia);
  clave.data = buf;
  clave.size = strlen(buf) + 1; /* Incluye el '\0' */
  if (len > 1) {                /* Si tiene un '\0' tambien esta vacio */
    st = db_agenda->del(db_agenda, transaccion, &clave, 0);
    return p;
  }
  contenido.data = p;
  contenido.size = len;
  st = db_agenda->put(db_agenda, transaccion, &clave, &contenido, 0);
  return p;
}

static char *escribe_dia_agenda_nick(int nick, int dia, char *p, int len)
{
  char n[16];

  n[0] = '\0';
  lee_nick_normalizado(nick, n);
  if (!n[0])
    return NULL;
  return escribe_dia_agenda(n, dia, p, len);
}

static char *lee_dia_agenda(char *entrada, int dia, int *len)
{
  char buf[1024];
  DBT clave, contenido;
  int st;

  memset(&clave, 0, sizeof(clave));
  clave.flags = DB_DBT_MALLOC;
  memset(&contenido, 0, sizeof(contenido));
  contenido.flags = DB_DBT_MALLOC;

  sprintf(buf, "%s %d", entrada, dia);
  clave.data = buf;
  clave.size = strlen(buf) + 1; /* Incluye el '\0' */
  st = db_agenda->get(db_agenda, transaccion, &clave, &contenido, 0);
  if (len)
    *len = contenido.size;
  if (contenido.size == 0)
    return NULL;
  return contenido.data;
}

static char *lee_dia_agenda_nick(int nick, int dia, int *len)
{
  char n[16];

  n[0] = '\0';
  lee_nick_normalizado(nick, n);
  if (!n[0])
    return NULL;
  return lee_dia_agenda(n, dia, len);
}

static struct agenda_nick *lee_cabecera_agenda(char *entrada,
                                               struct agenda_nick **hdr)
{
  DBT clave, contenido;
  int st;

  memset(&clave, 0, sizeof(clave));
  clave.flags = DB_DBT_MALLOC;
  memset(&contenido, 0, sizeof(contenido));
  contenido.flags = DB_DBT_MALLOC;

  clave.data = entrada;
  clave.size = strlen(entrada) + 1; /* Incluye el '\0' */

  st = db_agenda->get(db_agenda, transaccion, &clave, &contenido, 0);

  if (contenido.size == 0) {
    contenido.size = sizeof(struct agenda_nick);
    contenido.data = malloc(contenido.size);
    memset(contenido.data, 0, sizeof(struct agenda_nick));
  }
  *hdr = contenido.data;
  (*hdr)->version = VERSION_DB_ACTUAL;
  return *hdr;
}

static struct agenda_nick *lee_cabecera_agenda_nick(int nick,
                                                    struct agenda_nick **hdr)
{
  char n[16];

  n[0] = '\0';
  lee_nick_normalizado(nick, n);
  if (!n[0])
    return NULL;
  return lee_cabecera_agenda(n, hdr);
}

static struct agenda_nick *escribe_cabecera_agenda(char *entrada,
                                                   struct agenda_nick *hdr)
{
  DBT clave, contenido;
  int st;

  memset(&clave, 0, sizeof(clave));
  clave.flags = DB_DBT_MALLOC;
  memset(&contenido, 0, sizeof(contenido));
  contenido.flags = DB_DBT_MALLOC;

  clave.data = entrada;
  clave.size = strlen(entrada) + 1; /* Incluye el '\0' */

  if (!hdr->num_entradas) {     /* Agenda vacia */
    st = db_agenda->del(db_agenda, transaccion, &clave, 0);
  }
  else {
    hdr->version = VERSION_DB_ACTUAL;
    contenido.data = hdr;
    contenido.size = sizeof(struct agenda_nick);
    st = db_agenda->put(db_agenda, transaccion, &clave, &contenido, 0);
  }
  return hdr;
}

static struct agenda_nick *escribe_cabecera_agenda_nick(int nick, struct agenda_nick
                                                        *hdr)
{
  char n[16];

  n[0] = '\0';
  lee_nick_normalizado(nick, n);
  if (!n[0])
    return NULL;
  return escribe_cabecera_agenda(n, hdr);
}

static int calcula_dias(int dia, int mes)
{
  int d;

  d = dia - 1;
  for (mes -= 2; mes >= 0; mes--) {
    d += d_meses[mes];
  }
  return d;
}

static int lee_mes(int yo, int nick, char *texto, char **p)
{
  int mes;

  while (*texto == ' ')
    texto++;
  for (mes = 11; mes >= 0; mes--) {
    if (!strncasecmp(texto, meses[mes], 3)) {
      mes++;
      break;
    }
  }
  if (mes < 0)
    mes = atoi(texto);
  if ((mes < 1) || (mes > 12)) {
    envia_nick(yo, nick, "El mes debe ser su nombre o un numero 1-12");
    return -1;
  }
  if (p) {
    *p = strchr(texto, ' ');
    if (*p)
      while (**p == ' ')
        *p = *p + 1;
  }
  return mes;
}

static int lee_dias(int yo, int nick, char *texto, int *dia, int *mes,
                    char **p)
{
  int mes2;
  int d;
  char *p2;

  d = atoi(texto);
  p2 = strchr(texto, ' ');
  if (!p2) {
    envia_nick(yo, nick, "Indique un dia y un mes");
    return -1;
  }
  while (*p2 == ' ')
    p2++;
  mes2 = lee_mes(yo, nick, p2, &p2);
  if (mes2 < 0)
    return mes2;
  if ((d < 1) || (d > d_meses[mes2 - 1])) {
    envia_nick(yo, nick, "El numero de dia especificado no es valido");
    return -1;
  }
  if (dia)
    *dia = d;
  if (mes)
    *mes = mes2;
  if (p)
    *p = p2;
  return calcula_dias(d, mes2);
}

static void lista_agenda_dia(int yo, int nick, time_t tiempo)
{
  DBT clave, contenido;
  char buf[1024];
  int dia, dias, mes;
/*
** El sabado tenemos que ponerlo en hexadecimal porque sino toma las letras como
** continuaciones de la secuencia de escape inicial
*/
  char *dia_semana[] =
    { "Domingo", "Lunes", "Martes", "Mi\xE9rcoles", "Jueves", "Viernes",
    "S\xE1\x62\x61\x64o"
  };
  char *p, *p_orig;
  int i;
  struct agenda_nick *agenda;

  cftime(buf, "%d %m %w", &tiempo);
  dia = atoi(buf);
  p = strchr(buf, ' ') + 1;
  mes = atoi(p);
  p = dia_semana[atoi(strchr(p, ' ') + 1)];

  dias = calcula_dias(dia, mes);

  if (!febrero_29 && (dias == 59 || dias == 60)) { /* 29/Feb y 1/Mar -> Imprime el dia anterior */
    febrero_29 = !0;
    lista_agenda_dia(yo, nick, tiempo - 86400);
    febrero_29 = 0;
  }

  for (i = 0; dias > horoscopo[i].num_dia; i++);

  sprintf(buf, "Agenda para el %d de %s: (%s - %s)", dia, meses[mes - 1], p,
          horoscopo[i].signo);
  envia_nick(yo, nick, buf);

  memset(&clave, 0, sizeof(clave));
  clave.flags = DB_DBT_MALLOC;
  memset(&contenido, 0, sizeof(contenido));
  contenido.flags = DB_DBT_MALLOC;

  if (!lee_cabecera_agenda_nick(nick, &agenda)) {
    goto ojo_28_feb;            /* Ojo con el 28 de Febrero */
  }
  if (!agenda->dias[dias]) {
    free(agenda);
/* Ojo con el 28 de Febrero */
    goto ojo_28_feb;
  }

  free(agenda);

  memset(&contenido, 0, sizeof(contenido));
  contenido.flags = DB_DBT_MALLOC;

  if ((p = lee_dia_agenda_nick(nick, dias, NULL)) == NULL)
    goto ojo_28_feb;

  i = 1;
  p_orig = p;
  while (*p) {
    char *p2 = p;

    p = strchr(p, '\n');
    *p++ = '\0';
    sprintf(buf, "   \002%d.\002 %s", i++, p2);
    envia_nick(yo, nick, buf);
  }

  free(p_orig);

/*
** Esto debe ser lo ultimo de la rutina
*/
ojo_28_feb:
  if (!febrero_29 && (dias == 58 || dias == 59)) { /* 28/Feb y 29/Feb -> Imprime dia siguiente */
    febrero_29 = !0;
    lista_agenda_dia(yo, nick, tiempo + 86400);
    febrero_29 = 0;
  }
}

static void cmd_manhana(int yo, int nick, char *mensaje)
{
  lista_agenda_dia(yo, nick, ahora + 86400);
}

static void cmd_hoy(int yo, int nick, char *mensaje)
{
  lista_agenda_dia(yo, nick, ahora);
}

static void cmd_ayer(int yo, int nick, char *mensaje)
{
  lista_agenda_dia(yo, nick, ahora - 86400);
}

static void cmd_mes(int yo, int nick, char *mensaje)
{
  int dias, dias2, mes;
  char buf[1024];
  int i;
  struct agenda_nick *agenda;

  if ((mes = lee_mes(yo, nick, mensaje, NULL)) < 0)
    return;

  dias = calcula_dias(1, mes);

  sprintf(buf, "Dias con entradas, para el mes de %s:", meses[--mes]);
  envia_nick(yo, nick, buf);

  dias2 = d_meses[mes];

  if (!lee_cabecera_agenda_nick(nick, &agenda))
    return;

  for (i = 1; dias2--; dias++, i++) {
    if (agenda->dias[dias]) {
      sprintf(buf, "Dia %d - %d entradas", i, agenda->dias[dias]);
      envia_nick(yo, nick, buf);
    }
  }
  free(agenda);
}

static void cmd_dia(int yo, int nick, char *mensaje)
{
  int dias;
  struct tm *tm;

  if ((dias = lee_dias(yo, nick, mensaje, NULL, NULL, NULL)) < 0)
    return;
  tm=localtime(&ahora);
  dias=ahora+86400*(tm->tm_yday-dias);
  lista_agenda_dia(yo, nick, dias);
}

static void cmd_semana(int yo, int nick, char *mensaje)
{
  time_t tiempo = ahora;
  int i;

  for (i = 0; i < 7; i++, tiempo += 86400) {
    lista_agenda_dia(yo, nick, tiempo);
  }
}

static void cmd_add(int yo, int nick, char *mensaje)
{
  DBT clave, contenido;
  struct agenda_nick *agenda;
  char *p;
  int len;
  int dia, dias, mes;
  char buf[1024];

  if ((dias = lee_dias(yo, nick, mensaje, &dia, &mes, &p)) < 0)
    return;
  if (!p) {
    envia_nick(yo, nick, "Debe introducir un texto");
    return;
  }
  mensaje = p;

  sprintf(buf, "A\361adida entrada '%s' para el %d de %s", mensaje, dia,
          meses[mes - 1]);
  envia_nick(yo, nick, buf);

  memset(&clave, 0, sizeof(clave));
  clave.flags = DB_DBT_MALLOC;
  memset(&contenido, 0, sizeof(contenido));
  contenido.flags = DB_DBT_MALLOC;

  if (!lee_cabecera_agenda_nick(nick, &agenda))
    return;
  agenda->num_entradas++;       /* Una entrada mas */
  if (agenda->num_entradas > 200) {
    envia_nick(yo, nick,
               "Ha superado el numero maximo de registros. Por favor, elimine registros no utilizados");
    free(agenda);
    return;
  }
  agenda->dias[dias]++;         /* Una entrada mas */
  escribe_cabecera_agenda_nick(nick, agenda);
  free(agenda);

  memset(&contenido, 0, sizeof(contenido));
  contenido.flags = DB_DBT_MALLOC;

  p = lee_dia_agenda_nick(nick, dias, &len);
  if (p == NULL) {
    len = strlen(mensaje) + 2;  /* '\n\0' */
    p = malloc(len);
    *p = '\0';
  }
  else {                        /* Ya tiene registros */
    len += strlen(mensaje) + 1; /* '\n'. El '\0' ya estaba */
    p = realloc(p, len);
  }
  strcat(p, mensaje);
  strcat(p, "\n");
  escribe_dia_agenda_nick(nick, dias, p, len);
  free(p);
}

static void cmd_del(int yo, int nick, char *mensaje)
{
  DBT clave, contenido;
  struct agenda_nick *agenda;
  char *p, *p_orig, *p2;
  int len;
  int dia, dias, mes;
  int num;
  char buf[1024];

  if ((dias = lee_dias(yo, nick, mensaje, &dia, &mes, &p)) < 0)
    return;
  if (!p) {
    envia_nick(yo, nick, "Debe introducir el numero de la entrada a borrar");
    return;
  }
  num = atoi(p);
  if (num < 0) {
    envia_nick(yo, nick, "Entrada inexistente");
    return;
  }

  memset(&clave, 0, sizeof(clave));
  clave.flags = DB_DBT_MALLOC;

  if (!lee_cabecera_agenda_nick(nick, &agenda)) {
    envia_nick(yo, nick, "Entrada inexistente");
    return;
  }
  if (agenda->version != VERSION_DB_ACTUAL) {
    free(agenda);
    return;
  }
  if (num < 1 || num > agenda->dias[dias]--) {
    envia_nick(yo, nick, "Entrada inexistente");
    free(agenda);
    return;
  }
  agenda->num_entradas--;       /* Una entrada menos */
  escribe_cabecera_agenda_nick(nick, agenda);
  free(agenda);

  memset(&contenido, 0, sizeof(contenido));
  contenido.flags = DB_DBT_MALLOC;

  p_orig = lee_dia_agenda_nick(nick, dias, &len);
  for (p = p_orig; num--;) {
    p2 = p;
    p = strchr(p, '\n') + 1;
  }

  *(p - 1) = '\0';

/* La clave ocupa parte de 'buf' */
  sprintf(buf, "Eliminada entrada '%s' para el %d de %s", p2,
          dia, meses[mes - 1]);
  envia_nick(yo, nick, buf + clave.size);

  memmove(p2, p, len - (p - (char *) contenido.data));
  len -= p - p2;
  escribe_dia_agenda_nick(nick, dias, p_orig, len);
  free(p_orig);
}

static void cmd_help(int yo, int nick, char *mensaje)
{
  envia_nick(yo, nick, "La agenda permite que un usuario introduzca "
             "recordatorios de cumplea\361os, reuniones, cenas, aniversarios, etc. "
             "Observese que no se indica el a\361o, ya que se trata de una agenda "
             "que se \002repite\002 de forma anual. Se recomienda que para los eventos "
             "puntuales se indique el a\361o entre parentesis.");
  envia_nick(yo, nick, "\002ADD <dia> <mes> <texto>");
  envia_nick(yo, nick, "     A\361ade una nueva entrada.");
  envia_nick(yo, nick, "\002DEL <dia> <mes> <numero>");
  envia_nick(yo, nick, "     Elimina una entrada.");
  envia_nick(yo, nick, "\002DIA <dia> <mes>");
  envia_nick(yo, nick, "     Lista las entradas del dia indicado.");
  envia_nick(yo, nick, "\002MES <mes>");
  envia_nick(yo, nick, "     Lista los dias del mes con entradas.");
  envia_nick(yo, nick, "\002HOY");
  envia_nick(yo, nick, "     Lista las entradas para hoy.");
  envia_nick(yo, nick, "\002MA\321ANA");
  envia_nick(yo, nick, "     Lista las entradas para ma\361ana.");
  envia_nick(yo, nick, "\002SEMANA");
  envia_nick(yo, nick, "     Lista las entradas para los proximos 7 dias.");
  envia_nick(yo, nick, "\002AYER");
  envia_nick(yo, nick, "     Lista las entradas de ayer.");
  envia_nick(yo, nick, "\002HELP");
  envia_nick(yo, nick, "     Muestra esta ayuda.");
}

static void privmsg(int nick, int remitente, char *mensaje)
{
  static time_t ultimo_comando;

  struct comando comandos[] = {
    {"add", cmd_add, !0},
    {"del", cmd_del, !0},
    {"dia", cmd_dia, !0},
    {"hoy", cmd_hoy, 0},
    {"ma\361ana", cmd_manhana, 0},
    {"ma\321ana", cmd_manhana, 0},
    {"semana", cmd_semana, 0},
    {"ayer", cmd_ayer, 0},
    {"mes", cmd_mes, !0},
    {"help", cmd_help, 0},
    {NULL, NULL, 0}
  };
  int i, j;
  char flags[64];

#ifdef VERBOSE
  yo_ = nick;
  nick_ = remitente;
#endif

  ahora = time(NULL);

  flags[0] = '\0';
  lee_flags_nick(remitente, flags);

  if (!strchr(flags, 'r')) {
    envia_nick(nick, remitente,
               "Lo siento, pero este bot esta disponible SOLO para usuarios con nick registrado.");
    return;
  }
  for (i = 0; comandos[i].comando != NULL; i++) {
    j = strlen(comandos[i].comando);
    if (!strncasecmp(mensaje, comandos[i].comando, j) &&
        (mensaje[j] == ' ' || mensaje[j] == '\0')) {
      while (*(mensaje + j) == ' ')
        j++;
      if (comandos[i].parametros && !*(mensaje + j)) {
        envia_nick(nick, remitente,
                   "El comando especificado necesita parametros.");
        break;
      }
      hace_log(remitente, mensaje);
      txn_begin(db_env, NULL, &transaccion, 0);
      comandos[i].rutina(nick, remitente, mensaje + j);
      txn_commit(transaccion, 0);
      break;
    }
  }

/*** Lo quitamos para evitar bucles entre bots
  if (comandos[i].comando == NULL) {
    envia_nick(nick, remitente, "Usa '\002help\002'");
  }
****/

  if (ahora - ultimo_comando > 60) {
    ultimo_comando = ahora;
    db_agenda->sync(db_agenda, 0);
  }
}

static void fin(void)
{
  int st;

  st = db_agenda->close(db_agenda, 0);
}

static void entrada_nick_registrado(int nick)
{
  char n[16];
  char buf[1024];

/* Desarrollo */
  return;

  ahora = time(NULL);
  n[0] = '\0';
  lee_nick(nick, n);
  sprintf(buf, "Buenos dias, \002%s\002. Soy tu agenda personal. "
          "Tus asuntos pendientes para las proximas horas son:", n);
  envia_nick(handle, nick, buf);

  txn_begin(db_env, NULL, &transaccion, 0);

  lista_agenda_dia(handle, nick, ahora); /* Hoy */
  lista_agenda_dia(handle, nick, ahora + 86400); /* Man~ana */

  txn_commit(transaccion, 0);

  envia_nick(handle, nick, "En caso de duda, usa '\002help\002'");
}

static void nickdrop(char *nick, long num_serie)
{
  struct agenda_nick *agenda;
  DBT clave, contenido;
  DB_TXN *transaccion;
  int i;
  int st;
  long n_serie;

  memset(&clave, 0, sizeof(clave));
  clave.flags = DB_DBT_MALLOC;
  memset(&contenido, 0, sizeof(contenido));
  contenido.flags = DB_DBT_MALLOC;

  clave.data = "NICKDROPS";
  clave.size = strlen(clave.data) + 1; /* Incluye el '\0' */

  txn_begin(db_env, NULL, &transaccion, 0);
  st = db_agenda->get(db_agenda, transaccion, &clave, &contenido, 0);

  n_serie = 0;
  if (contenido.size != 0) {
    n_serie = atol(contenido.data);
    free(contenido.data);
  }

  if (n_serie >= num_serie)
    return;

  contenido.data = malloc(100);
  sprintf(contenido.data, "%ld", num_serie);
  contenido.size = strlen(contenido.data) + 1; /* El '\0' */
  st = db_agenda->put(db_agenda, transaccion, &clave, &contenido, 0);
  free(contenido.data);

  lee_cabecera_agenda(nick, &agenda);
  agenda->num_entradas = 0;     /* Esto es un borrado implicito en disco */
  escribe_cabecera_agenda(nick, agenda);
  free(agenda);

  for (i = 0; i < 366; i++) {   /* Elimina los registros de dias */
    escribe_dia_agenda(nick, i, NULL, 0);
  }

  txn_commit(transaccion, 0);

  db_agenda->sync(db_agenda, 0);
}

int inicio(DB_ENV * db)
{
  DBT clave, contenido;
  DB_TXN *transaccion;
  long num_serie;
  int st;

  db_env = db;
  st = db_create(&db_agenda, db_env, 0);
  st = db_agenda->set_malloc(db_agenda, malloc);
  st = db_agenda->set_pagesize(db_agenda, 1024);
  st =
    db_agenda->open(db_agenda, "db.agenda2", NULL, DB_HASH, DB_CREATE, 0600);

  especifica_fin(fin);
  notifica_nick_registrado(entrada_nick_registrado);
  comentario_modulo("Agenda Personal $Revision: 1.83 $");
  handle = nuevo_nick("agenda2", "+odkirh", privmsg);
  if (handle < 0)
    return -1;

  memset(&clave, 0, sizeof(clave));
  clave.flags = DB_DBT_MALLOC;
  memset(&contenido, 0, sizeof(contenido));
  contenido.flags = DB_DBT_MALLOC;

  clave.data = "NICKDROPS";
  clave.size = strlen(clave.data) + 1;

  st = txn_begin(db_env, NULL, &transaccion, 0);
  st = db_agenda->get(db_agenda, transaccion, &clave, &contenido, 0);
  num_serie = 0;
  if (contenido.size) {
    num_serie = atol(contenido.data);
    free(contenido.data);
  }
  st = txn_commit(transaccion, 0);

  notifica_db_nickdrop(nickdrop, num_serie);

  return 0;
}
