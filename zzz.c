/*
# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#define NUM_MALLOCS 100000
char *punt[NUM_MALLOCS];

int main(void)
{
  int i;
  int l;
  int tot = 0;
  char *p;

  for (i = NUM_MALLOCS; i--;) {
    l = (rand() & 63) + 10;
    tot += l;
    punt[i] = malloc(l);
    l = NUM_MALLOCS - 1 - (rand() & (NUM_MALLOCS - i));
    if (punt[l]) {
      free(punt[i]);
      punt[i] = NULL;
    }
  }
  printf("Pedimos %d bytes\n", tot);
//  p = NULL;
//  *p = 0;
}
