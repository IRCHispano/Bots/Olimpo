/*
** BERKELEYDB.C
**
** $Id: berkeleydb.c,v 1.69 2003/07/31 17:59:15 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <db.h>
#include <time.h>
#include <errno.h>

#include <assert.h>

#include "berkeleydb.h"
#include "config.h"
#include "modulos.h"

struct st_opened_databases {
  char *nombre;
  DB *db;
  struct st_opened_databases *next;
  struct st_opened_databases **prev;
};

static struct st_opened_databases *opened_databases = NULL;

static int berkeley_db_abierta = 0;
static DB_ENV *db_env = NULL;

static int st = 0;


static void purga_archivos_log(void)
{
  DB_TXN_STAT *txn_status;
  static time_t last_purge;
  time_t now = time(NULL);
  char **logs, **logs_p;
  int st;

  if (!berkeley_db_abierta)
    return;
  if (now - last_purge < 1 * 60)
    return;

  if (!(db_env->txn_stat(db_env, &txn_status, 0))) {
    unsigned long n_txn;

    n_txn = txn_status->st_nactive;
    free(txn_status);
    if (n_txn)
      return;
  }
  else
    return;

/*
** Hacemos dos checkpoints seguidos para poder eliminar
** los ficheros sin riesgo de no poder hacer una
** recuperacion de la base de datos, si hay problemas.
*/
  st = db_env->txn_checkpoint(db_env, 0, 0, DB_FORCE);
  if (st)
    return;
  st = db_env->txn_checkpoint(db_env, 0, 0, DB_FORCE);
  if (st)
    return;

  st = db_env->log_archive(db_env, &logs, DB_ARCH_ABS);
  if (st)
    return;

  if (logs) {
    logs_p = logs;
    while (*logs_p && *(logs_p + 1) && *(logs_p + 2)) { /* Al menos dejo siempre DOS o TRES ficheros */
      char buf[1000];

      remove(*logs_p);
      sprintf(buf, "\n*** PURGANDO: %s\n\n", *logs_p);
      write(1, buf, strlen(buf)); /* STDOUT */
      logs_p++;
    }
    free(logs);
  }
  last_purge = now;
}

static void cierra_todos_db(void)
{
  if (!berkeley_db_abierta)
    return;

  while (opened_databases) {
    cierra_db(opened_databases);
  }
}

void cierra_berkeley_db(void)
{
  if (berkeley_db_abierta) {
    cierra_todos_db();
    st = db_env->close(db_env, 0);
    db_env = NULL;
  }
  berkeley_db_abierta = 0;
}

char *strerror_db(void)
{
  return db_strerror(st);
}

static void errores_db(int st)
{
  char buf[1000];

  sprintf(buf, "%s\n", db_strerror(st));
  write(2, buf, strlen(buf));   /* STDOUT */
  descarga_modulos();
  cierra_todos_db();
  exit(-1);
}

void abre_berkeley_db(void)
{
  char buf[1000];

  assert(berkeley_db_abierta == 0);
  assert(db_env == NULL);

  sprintf(buf, "Apertura inicial de la base de datos...\n");
  write(1, buf, strlen(buf));   /* STDOUT */
  st = db_env_create(&db_env, 0);
  if (st)
    errores_db(st);

/*
** El taman~o especificado en "bsize" debe
** ser, al menos, cuatro veces inferior a "max".
*/
  st = db_env->set_lg_max(db_env, 2000000);
  if (st)
    errores_db(st);
  st = db_env->set_lg_bsize(db_env, 384 * 1024);
  if (st)
    errores_db(st);

/*
** Aborta transacciones de ejecuciones previas
*/
  st = db_env->set_lk_detect(db_env, DB_LOCK_OLDEST);
  if (st)
    errores_db(st);

  st = db_env->set_cachesize(db_env, 0, 16 * 1024 * 1024, 4);

  st = db_env->set_lk_max_locks(db_env, 10000);
  st = db_env->set_lk_max_objects(db_env, 10000);

  st = db_env->set_alloc(db_env, malloc, realloc, free);
  if (st)
    errores_db(st);

  st = db_env->open(db_env,
#ifndef DEVELOP
                    "db",
#else
                    "db2",
#endif
                    DB_INIT_MPOOL | DB_INIT_LOCK | DB_INIT_LOG | DB_INIT_TXN
                    | DB_CREATE, 0600);
  if (st)
    errores_db(st);

  db_env->set_errfile(db_env, stderr);
  db_env->set_errpfx(db_env, "zz.errpfx");

  st = db_env->set_flags(db_env, DB_AUTO_COMMIT, 1);
  if (st)
    errores_db(st);

  berkeley_db_abierta = !0;
}

/*
** Se puede abrir una misma base de datos varias
** veces sin nigun problema. Cuando se cierre,
** se cierra el descriptor apropiado, aunque haya
** varios abiertos para la misma base de datos.
*/
p_db abre_db(char *nombre)
{
  DB *db;
  struct st_opened_databases *db2;

  assert(berkeley_db_abierta);

  st = db_create(&db, db_env, 0);
  if (st)
    return NULL;
  st = db->set_pagesize(db, 1024);
  if (st)
    return NULL;
  st = db->open(db, NULL, nombre, NULL, DB_HASH, DB_CREATE, 0600);
  if (st)
    return NULL;

  db2 = malloc(sizeof(struct st_opened_databases));
  if (!db2)
    return NULL;

  db2->nombre = malloc(strlen(nombre) + 1);
  if (!db2->nombre) {
    free(db2);
    return NULL;
  }

  strcpy(db2->nombre, nombre);

  db2->prev = &opened_databases;
  db2->db = db;
  db2->next = opened_databases;
  if (opened_databases) {
    opened_databases->prev = &(db2->next);
  }
  opened_databases = db2;

  return db2;
}

void cierra_db(p_db db)
{
  struct st_opened_databases *db2;

  assert(berkeley_db_abierta);

  if (db) {
    db2 = db;
    st = db2->db->close(db2->db, 0);
    if (db2->next)
      db2->next->prev = db2->prev;
    *(db2->prev) = db2->next;
    free(db2->nombre);
    free(db);
  }
}

static txn begin_txn_db(int flags)
{
  DB_TXN *txn;

  assert(berkeley_db_abierta);

  st = db_env->txn_begin(db_env, NULL, &txn, flags);

  assert(!st);
//  if (st)
//    return NULL;

  return txn;
}

txn inicia_txn_db(void)
{
  assert(berkeley_db_abierta);
  return begin_txn_db(0);
}

txn inicia_txn_db_no_sync(void)
{
  assert(berkeley_db_abierta);
  return begin_txn_db(DB_TXN_NOSYNC);
}

txn inicia_txn_db_no_wait(void)
{
  assert(berkeley_db_abierta);
  return begin_txn_db(DB_TXN_NOWAIT);
}

void compromete_txn_db(txn txn)
{
  assert(berkeley_db_abierta);
  ((DB_TXN *) txn)->commit(txn, 0);
  purga_archivos_log();
}

void aborta_txn_db(txn txn)
{
  assert(berkeley_db_abierta);
  ((DB_TXN *) txn)->abort(txn);
  purga_archivos_log();
}

int get_db(p_db db, txn transaccion, dbt * clave, dbt * contenido)
{
  DB *db2 = ((struct st_opened_databases *) db)->db;
  DBT clave2;
  DBT contenido2;

  assert(berkeley_db_abierta);

  memset(&clave2, 0, sizeof(clave2));
  memset(&contenido2, 0, sizeof(contenido2));

  clave2.flags = DB_DBT_MALLOC;
  contenido2.flags = DB_DBT_MALLOC;

  clave2.data = clave->datos;
  clave2.size = clave->len;

  st = db2->get(db2, transaccion, &clave2, &contenido2, 0);

  contenido->datos = contenido2.data;
  contenido->len = contenido2.size;
  return st;
}

int put_db(p_db db, txn transaccion, dbt * clave, dbt * contenido)
{
  DB *db2 = ((struct st_opened_databases *) db)->db;
  DBT clave2;
  DBT contenido2;

  assert(berkeley_db_abierta);

  memset(&clave2, 0, sizeof(clave2));
  memset(&contenido2, 0, sizeof(contenido2));

  clave2.flags = DB_DBT_MALLOC;
  contenido2.flags = DB_DBT_MALLOC;

  clave2.data = clave->datos;
  clave2.size = clave->len;

  contenido2.data = contenido->datos;
  contenido2.size = contenido->len;

  st = db2->put(db2, transaccion, &clave2, &contenido2, 0);

  return st;
}

int del_db(p_db db, txn transaccion, dbt * clave)
{
  DB *db2 = ((struct st_opened_databases *) db)->db;
  DBT clave2;

  assert(berkeley_db_abierta);

  memset(&clave2, 0, sizeof(clave2));

  clave2.flags = DB_DBT_MALLOC;

  clave2.data = clave->datos;
  clave2.size = clave->len;

  st = db2->del(db2, transaccion, &clave2, 0);

  if (st == DB_NOTFOUND)
    st = BERKELEY_DB_NOTFOUND;

  return st;
}

char *version_db(void)
{
  return db_version(NULL, NULL, NULL);
}

c_db *cursor_db(p_db db, txn transaccion, dbt * clave, int bulk)
{
  DB *db2 = ((struct st_opened_databases *) db)->db;
  DBT clave2;
  c_db *p;

  assert(berkeley_db_abierta);

  p = malloc(sizeof(c_db));
  assert(p);
  p->mem = NULL;
  p->mem_len = 0;
  p->estado = NULL;
  if (bulk) {
    p->mem_len = 65536;         /* Taman~o inicial del buffer */
    p->mem = malloc(p->mem_len);
    assert(p->mem);
  }

  st = db2->cursor(db2, transaccion, &(p->cursor), 0);
  if (st)
    return NULL;

  if (clave) {
    memset(&clave2, 0, sizeof(clave2));
    memset(&(p->data), 0, sizeof(p->data));

    clave2.flags = DB_DBT_MALLOC;
    p->data.flags = DB_DBT_MALLOC;

    clave2.data = clave->datos;
    clave2.size = clave->len;

    st = p->cursor->c_get(p->cursor, &clave2, &(p->data), DB_SET);

    if (p->data.size)
      free(p->data.data);

    /*
     ** Si el registro no existe, cerramos el cursor
     ** y empezamos desde el principio.
     */
    if (st == DB_NOTFOUND) {
      p->cursor->c_close(p->cursor);
      st = db2->cursor(db2, transaccion, &(p->cursor), 0);
      if (st)
        return NULL;
    }
  }

  return p;
}

int cursor_get_db(c_db * db_cursor, dbt * clave, dbt * contenido)
{
  DBT clave2;
  int flags = DB_NEXT;

  assert(berkeley_db_abierta);

  memset(&clave2, 0, sizeof(clave2));

  if (db_cursor->mem) {
    if (db_cursor->estado) {
      DB_MULTIPLE_KEY_NEXT((db_cursor->estado), &(db_cursor->data),
                           (clave->datos), (clave->len), (contenido->datos),
                           (contenido->len));
      if (db_cursor->estado)
        return 0;
    }

/*
** Ya no nos queda nada en el buffer. Leemos mas
*/
    memset(&(db_cursor->data), 0, sizeof(db_cursor->data));

    db_cursor->data.data = db_cursor->mem;
    db_cursor->data.ulen = db_cursor->mem_len;
    db_cursor->data.flags = DB_DBT_USERMEM;
    flags |= DB_MULTIPLE_KEY;
  }
  else {
    clave2.flags = DB_DBT_MALLOC;
    db_cursor->data.flags = DB_DBT_MALLOC;

    memset(&(db_cursor->data), 0, sizeof(db_cursor->data));
  }

  st =
    db_cursor->cursor->c_get(db_cursor->cursor, &clave2, &(db_cursor->data),
                             flags);

  if (st == ENOMEM) {           /* Cogemos un bloque mas grande */
    assert(db_cursor->mem);     /* Esto solo puede pasar si estamos en BULK */
    free(db_cursor->mem);
    db_cursor->mem_len = db_cursor->data.size; /* Longitud que necesitamos... */
    db_cursor->mem = malloc(db_cursor->mem_len);
    assert(db_cursor->mem);

    memset(&clave2, 0, sizeof(clave2));
    memset(&(db_cursor->data), 0, sizeof(db_cursor->data));
    db_cursor->data.data = db_cursor->mem;
    db_cursor->data.ulen = db_cursor->mem_len;
    db_cursor->data.flags = DB_DBT_USERMEM;

    st =
      db_cursor->cursor->c_get(db_cursor->cursor, &clave2, &(db_cursor->data),
                               flags);
  }

  if (db_cursor->mem) {         /* Modo BULK */
    if (st == DB_NOTFOUND) {
      return BERKELEY_DB_NOTFOUND;
    }

    DB_MULTIPLE_INIT((db_cursor->estado), &(db_cursor->data));
    assert(db_cursor->estado);
    DB_MULTIPLE_KEY_NEXT((db_cursor->estado), &(db_cursor->data),
                         (clave->datos), (clave->len), (contenido->datos),
                         (contenido->len));
    assert(db_cursor->estado);
    return 0;
  }

/*
** Si llegamos aqui, no estamos en modo "bulk".
*/
  if (st == DB_NOTFOUND) {
    st = BERKELEY_DB_NOTFOUND;
  }

  clave->datos = clave2.data;
  clave->len = clave2.size;

  contenido->datos = db_cursor->data.data;
  contenido->len = db_cursor->data.size;

  return st;
}

int cursor_close_db(c_db * db_cursor)
{
  int st;

  assert(berkeley_db_abierta);
  assert(db_cursor);
  st = db_cursor->cursor->c_close(db_cursor->cursor);
  if (db_cursor->mem)
    free(db_cursor->mem);
  free(db_cursor);

  return st;
}
