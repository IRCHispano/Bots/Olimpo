/*
** ILINES_ALTA.C
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <db.h>
#include <signal.h>

/*
    $Id: ilines_alta.c,v 1.1 2000/03/15 12:42:12 jcea Exp $
*/

static int senal = 0;

void senal_handler(int s)
{
  senal = s;
}

void errores(int st)
{
  fprintf(stderr, "DB: %s\n", db_strerror(st));
  exit(-1);
}

int main(void)
{
  DB *db_ilines;
  DB_ENV *db_env;
  DBT clave;
  DBT contenido;
  char *p;
  unsigned long *p2;
  char buf[4096];
  char buf2[4096];
  char *env[2] = { NULL };
  int st;

  //signal(SIGPIPE, SIG_IGN);

  sigset(SIGHUP, senal_handler);
  sigset(SIGINT, senal_handler);
  sigset(SIGQUIT, senal_handler);
  sigset(SIGKILL, senal_handler);
  sigset(SIGPIPE, senal_handler);
  sigset(SIGALRM, senal_handler);
  sigset(SIGTERM, senal_handler);
  sigset(SIGUSR1, senal_handler);
  sigset(SIGUSR2, senal_handler);
  sigset(SIGPOLL, senal_handler);

  sigset(SIGVTALRM, senal_handler);
  sigset(SIGPROF, senal_handler);
  sigset(SIGXCPU, senal_handler);
  sigset(SIGXFSZ, senal_handler);
  /*
     ** De momento vamos a obviar
     ** las senales en tiempo real.
   */

  printf("%s\n\n", db_version(NULL, NULL, NULL));

  st = db_env_create(&db_env, 0);
  if (st)
    errores(st);
  if (st)
    errores(st);
  st =
    db_env->open(db_env, "db", env,
                 DB_INIT_CDB | DB_INIT_MPOOL | DB_CREATE, 0600);
  if (st)
    errores(st);

  db_env->set_errfile(db_env, stderr);
  db_env->set_errpfx(db_env, "zz.errpfx");

  st = db_create(&db_ilines, db_env, 0);
  if (st)
    errores(st);

  st = db_ilines->set_malloc(db_ilines, malloc);

  st = db_ilines->set_pagesize(db_ilines, 1024);
  if (st)
    errores(st);

  st = db_ilines->open(db_ilines, "db.i", NULL, DB_HASH, DB_CREATE, 0600);
  if (st)
    errores(st);

  p2 = (unsigned long *) buf2;
  *p2++ = htonl(time(NULL));    /* Alta */
  *p2++ = htonl(time(NULL) + 60 * 60 * 24 * 14); /* Expire 14 dias */
  *p2++ = htonl(0);             /* Ultimo acceso */

  while (gets(buf) != NULL) {
    if (senal) {
      printf("\n\nLlega la sen~al %d\n", senal);
      break;
    }

    printf("%s\n", buf);

    p = strchr(buf, ' ');
    *p++ = '\0';
    memset(&clave, 0, sizeof(clave));
    clave.data = buf;
    clave.size = strlen(buf) + 1; /* Incluye el '\0' */
    strcpy((char *) p2, p);
    memset(&contenido, 0, sizeof(contenido));
    contenido.data = buf2;
    contenido.size = strlen(p) + 1 + 12; /* '\0' y timestamps */

    st = db_ilines->put(db_ilines, NULL, &clave, &contenido, 0);
    if (st)
      errores(st);
  }

  st = db_ilines->close(db_ilines, 0);
  if (st)
    errores(st);
  st = db_env->close(db_env, 0);
  if (st)
    errores(st);

  return 0;
}
