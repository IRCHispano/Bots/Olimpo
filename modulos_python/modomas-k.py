# $Id: modomas-k.py,v 1.13 2003/04/11 15:40:43 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


db=None
usuarios=None
handle=None

class class_remitente:
  def __init__(self,remitente,yo):
    self.yo=yo
    self.handle=remitente
    import Olimpo
    self.flags=Olimpo.privmsg.lee_flags_nick(remitente)

  def es_ircop(self):
    return "o" in self.flags

  def es_oper(self):
    return "h" in self.flags
  
  def es_jcea(self) :
    flag=self.es_ircop() and self.es_oper()
    if not flag : return 0
    if self.home()!="gaia.irc-hispano.org" : return 0
    if self.nick_normalizado()!="jcea" : return 0
    return 1
  
  def esta_registrado(self):
    return "r" in self.flags
  
  def home(self) :
    import Olimpo
    return Olimpo.tools.which_server(self.handle)
  
  def nick(self) :
    import Olimpo
    return Olimpo.privmsg.lee_nick(self.handle)

  def nick_normalizado(self) :
    import Olimpo
    return Olimpo.privmsg.lee_nick_normalizado(self.handle)
  
  def envia(self,msg) :
    import Olimpo
    Olimpo.privmsg.envia_nick(self.yo,self.handle,msg)


class privmsg :
  def procesa(self,nick,remitente,mensaje):
    comandos={"help":self.m_help,
      "userlist":self.m_userlist,"useradd":self.m_useradd,"userdel":self.m_userdel,
      "mas-k":self.m_mas_k}

    msg=mensaje.split()
    if not msg : return # Mensaje vacio (por ejemplo, espacios o tabuladores)
    cmd=msg[0].lower()

    remitente=class_remitente(remitente,nick)

    if (not remitente.es_jcea()) and (not remitente.es_oper()) : return

    if comandos.has_key(cmd) :
      comandos[cmd](remitente,mensaje,cmd,msg)

  def m_userlist(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    Olimpo.hace_log(remitente.handle,mensaje)
    remitente.envia("Listado de usuarios registrados:")
    a=usuarios.keys()
    a.sort() # In-Place
    for i in a :
      remitente.envia(i)
    remitente.envia("\002Fin de UserList")

  def m_useradd(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    if remitente.es_jcea() :
      if len(msg)==2 :
        Olimpo.hace_log(remitente.handle,mensaje)
        nick=Olimpo.tools.nick_normalizado(msg[1])
        usuarios[nick]=1
        txn=Olimpo.BerkeleyDB.txn()
        db.put(txn,"usuarios"," ".join(usuarios.keys()))
        txn.compromete()
        remitente.envia("Ahora \002%s\002 ya es un usuario registrado" %(nick))
      else :
        remitente.envia("Numero de parametros incorrecto")
    else :
      remitente.envia("No tienes permiso para realizar esta operacion")

    remitente.envia("\002Fin de UserAdd")

  def m_userdel(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    if remitente.es_jcea() :
      if len(msg)==2 :
        Olimpo.hace_log(remitente.handle,mensaje)
        nick=Olimpo.tools.nick_normalizado(msg[1])
        if usuarios.has_key(nick) :
          del usuarios[nick]
          txn=Olimpo.BerkeleyDB.txn()
          db.put(txn,"usuarios"," ".join(usuarios.keys()))
          txn.compromete()
          remitente.envia("El usuario \002%s\002 ha sido eliminado" %(nick))
        else :
          remitente.envia("\002%s\002 no es usuario registrado" %(nick))
      else :
        remitente.envia("Numero de parametros incorrecto")
    else :
      remitente.envia("No tienes permiso para realizar esta operacion")
 
    remitente.envia("\002Fin de UserDel")

  def m_mas_k(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    if remitente.es_jcea() :
      if len(msg)==2 :
        Olimpo.hace_log(remitente.handle,mensaje)
        nick=Olimpo.tools.nick_normalizado(msg[1])
        Olimpo.servmsg.envia_raw2("MODE %s :+k" %nick)
        remitente.envia("Enviamos modo +k a %s" %nick)
      else :
        remitente.envia("Numero de parametros incorrecto")
    else :
      remitente.envia("No tienes permiso para realizar esta operacion")

    remitente.envia("\002Fin de Mas-K")

  def m_help(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    Olimpo.hace_log(remitente.handle,mensaje)

    if remitente.es_jcea() :
      remitente.envia("\002MAS-K\002 (para uso de JCEA)")
      remitente.envia("     Envia +k a un usuario")
      remitente.envia("\002USERADD\002 (para uso de JCEA)")
      remitente.envia("     Registra un usuario nuevo")
      remitente.envia("\002USERDEL\002 (para uso de JCEA)")
      remitente.envia("     Elimina un usuario registrado")

    remitente.envia("\002USERLIST (opers)")
    remitente.envia("     Proporciona la lista de usuarios registrados")
    remitente.envia("\002HELP (opers)")
    remitente.envia("     Muestra esta ayuda")
    remitente.envia("\002Fin de HELP")
    

class entrada_nick_registrado :
  def procesa(self,nick) :
    import Olimpo
    nick=Olimpo.tools.nick_normalizado(Olimpo.privmsg.lee_nick_normalizado(nick))
    if usuarios.has_key(nick) :
      Olimpo.servmsg.envia_raw2("MODE %s :+k" %nick)

class nickdrop :
  def procesa(self,nick,num_serie) :
    import Olimpo
    # Nos vale con transaccion ACI, en vez de ACID, porque si la perdemos,
    # sincronizaremos cuando recarguemos el modulo.
    txn=Olimpo.BerkeleyDB.txn(Olimpo.BerkeleyDB.txn.NO_SYNC)
    try :
      db.put(txn," NICKDROPS",repr(num_serie))

      if usuarios.has_key(nick) :
        del usuarios[nick]
        db.put(txn,"usuarios"," ".join(usuarios.keys()))
    except :
      txn.aborta()
      raise # Volvemos a levantar la excepcion original

    txn.compromete()

class fin :
  def procesa(self) :
    db.cierra()

def inicio() :
  global db,usuarios,handle

  import Olimpo
  Olimpo.comentario_modulo("Modo +k $Revision: 1.13 $")

  db=Olimpo.BerkeleyDB.db("db.modomas-k")
  txn=Olimpo.BerkeleyDB.txn()
  u=db.get(txn,"usuarios")[1]
  num_reg_nickdrop=db.get(txn," NICKDROPS")[1]
  txn.compromete()

  if num_reg_nickdrop : num_reg_nickdrop=int(num_reg_nickdrop)
  else : num_reg_nickdrop=6214152 # El valor que tenia cuando creamos el modulo

  usuarios={}
  if u:
    usuarios=dict([(i,1) for i in u.split()])

  handle=Olimpo.privmsg.nuevo_nick("modomas-k","+odkirhB",privmsg().procesa)

  Olimpo.especifica_fin(fin().procesa)

  Olimpo.notify.notifica_nick_registrado(entrada_nick_registrado().procesa)

  Olimpo.notify_db.notifica_nickdrop(nickdrop().procesa,num_reg_nickdrop)

