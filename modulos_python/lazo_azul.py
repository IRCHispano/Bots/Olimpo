# $Id: lazo_azul.py,v 1.19 2004/03/12 12:25:36 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import Olimpo

handle_mio=None

db=Olimpo.BerkeleyDB.db("db.lazo-azul")

ya_tienen_frase={}

def privmsg(handle,remitente,mensaje):
  global ya_tienen_frase
  if 'r' not in Olimpo.privmsg.lee_flags_nick(remitente) :
    Olimpo.privmsg.envia_nick(handle_mio,remitente,"Para poder dejar tu frase es imprescindible que tengas tu nick registrado. Por favor, tomate esa molestia. Registra tu nick y expresa tu solidaridad.")
    return 0

  cmd=mensaje.split()[0].lower()

  if (cmd=="help") or (cmd=="ayuda") :
    Olimpo.privmsg.envia_nick(handle_mio,remitente,"Para firmar escribe \002/msg lazo_azul FIRMAR tu_frase_de_protesta\002")
    return 0

  if (cmd!="firma") and (cmd!="firmar") :
    return 0

  nick=Olimpo.privmsg.lee_nick_normalizado(remitente)
  txn=Olimpo.BerkeleyDB.txn()
  try :
    db.put(txn,nick,mensaje)
    Olimpo.privmsg.envia_nick(handle_mio,remitente,"Tu frase queda apuntada en el sistema. Recuerda que si envias varias frases solo se tendra en cuenta la ultima. Gracias por tu tiempo y atencion.")
    Olimpo.hace_log(remitente,mensaje)
    ya_tienen_frase[nick]=None
  finally :
    txn.compromete()

  return 0

class publicidad2nicks(object) :
  def __new__(cls) :
    if not cls.__dict__.has_key("obj") : # Se crea el singleton de "obj"
      cls.obj=object.__new__(cls)
      cls.obj.lista={}
      cls.obj.lista2={}
    obj=cls.obj
    import Olimpo
    import time
    Olimpo.notify.notifica_timer(int(time.time())+60,obj.procesa)
    return obj

  def procesa(self) :
    global ya_tienen_frase
    import Olimpo
    import time
    now=time.time()
    l=self.lista2.keys()
    handle_jcea=Olimpo.privmsg.lee_handle("jcea")
    total,envios,offline=len(l),0,0
    for i in l : # Lista de handles
      nick=Olimpo.privmsg.lee_nick_normalizado(i)
      if nick and (nick not in ya_tienen_frase) :
        Olimpo.privmsg.envia_nick(handle_mio,i,"Atentado terrorista en Madrid. Recogida de firmas en contra del terrorismo y en apoyo de las victimas. Para firmar escribe \002/msg lazo_azul FIRMAR tu_frase_de_protesta\002. Recuerda que si envias varias frases solo se tendra en cuenta la ultima.")
        Olimpo.privmsg.envia_nick(handle_mio,i,"Despues de firmar entra en #lazo_azul, #lazo_azul2, #lazo_azul3 o #lazo_azul4 y unete a la repulsa silenciosa de todos los internautas que chateamos aqui.")
        envios+=1
      else :
        offline+=1
    self.lista2=self.lista
    self.lista={}
    now2=time.time()
    Olimpo.notify.notifica_timer(int(now2)+60,self.procesa)
    if handle_jcea :
      Olimpo.privmsg.envia_nick(handle_mio,handle_jcea,"Entradas: %d - No Registrados: %d - Salidas: %d - Firmas: %d (%f segundos)" %(total,envios,offline,len(ya_tienen_frase),now2-now))

class nick :
  def __init__(self) :
    self.nicks=publicidad2nicks()

  def procesa(self,comando) :
    import Olimpo
    p=comando.find(" :")
    if p>=0 : comando=comando[:p]
    comando=comando.split()
    if len(comando) < 10 : return
    handle=Olimpo.tools.nicknumeric2handle(comando[9])
    self.nicks.lista[handle]=1

def inicio():
  global handle_mio
  Olimpo.comentario_modulo("Recogida de firmas $Revision: 1.19 $")
  handle_mio=Olimpo.privmsg.nuevo_nick("lazo_azul","+odkirhBx",privmsg) # Tiene +x para pillar "bots.irc-hispano.org"
  Olimpo.servmsg.intercepta_comando("NICK",nick().procesa)

  global ya_tienen_frase
  txn=Olimpo.BerkeleyDB.txn()
  cursor=db.cursor(txn,None)
  frases=[]
  grabar_frases=False
  try :
    while True :
      n,frase=cursor.get()[1:3]
      if not n : break
      ya_tienen_frase[n]=None
      if grabar_frases : frases.append(frase)
  finally :
    cursor.close()
    txn.compromete()

  del cursor
  del txn

  if grabar_frases :
    frases="\n".join(frases)
    f=open("/export/home/irc/olimpo/db/frases-lazo_azul","w")
    f.write(frases)
    f.close()
    del f

  del frases


