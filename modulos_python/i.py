# $Id: i.py,v 1.5 2003/12/25 03:50:09 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import Olimpo

contenedor=None

def perico(*args) :
  pass

def fin() :
  Olimpo.ipc.obj_del("i:perico")

def inicio():
  import cdb
  global contenedor
  contenedor=cdb.init("/opt/src/irc/db/db.nicks-historico.CDB.Dic2003")

  Olimpo.ipc.obj_add("i:perico",perico)

  Olimpo.ipc.purge_objects()

  import nick2
  import sys
  import gc
  print >>sys.stderr,sys.getrefcount(nick2.nick_historico.cdb)
  print >>sys.stderr,len(gc.get_referrers(nick2.nick_historico.cdb))
  print >>sys.stderr,gc.get_referrers(nick2.nick_historico.cdb)[0]

