# $Id: aimc.py,v 1.15 2003/10/14 15:07:24 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import Olimpo

def privmsg(handle,remitente,mensaje):
  pass

class publicidad2nicks(object) :
  def __new__(cls) :
    if not cls.__dict__.has_key("obj") : # Se crea el singleton de "obj"
      cls.obj=object.__new__(cls)
      cls.obj.lista={}
      cls.obj.lista2={}
      cls.obj.lista_duplicados={}
      cls.obj.lista_duplicados_ts=0
    obj=cls.obj
    import Olimpo
    import time
    Olimpo.notify.notifica_timer(int(time.time())+60,obj.procesa)
    return obj

  def procesa(self) :
    import Olimpo
    import time
    now=time.time()

    total,duplicados,purgados,offline=len(self.lista2),0,0,0

    if self.lista_duplicados_ts<now :
      self.lista_duplicados_ts=now+3600
      a=self.lista_duplicados.keys()
      b=self.lista_duplicados.values()
      a=zip(b,a)
      del b
      a.sort()
      for i,j in a :
        if i+6*3600<now :
          del self.lista_duplicados[j]
          purgados+=1
        else :
          break # Sabemos que lo siguiente es mas reciente

    l=self.lista2.keys()
    handle_jcea=Olimpo.privmsg.lee_handle("jcea")
    for i in l : # Lista de handles
      flags=Olimpo.privmsg.lee_flags_nick(i)
      if flags!=None :
        nick=Olimpo.privmsg.lee_nick_normalizado(i)
        t=self.lista_duplicados.get(nick,0)
        if t+12*3600<now :
          Olimpo.privmsg.envia_nick(handle_mio,i,"6\252 Encuesta AIMC a usuarios de Internet \241\241Participa y gana...!! Un ordenador port\xe1til, un PDA's I-paq (agenda electr\xf3nica), un tel\xe9fono m\xf3vil, tres PlayStation 2. http://encuesta2003cas.aimc.es/irc")
          self.lista_duplicados[nick]=now
        else :
          duplicados+=1
      else :
        offline+=1
    self.lista2=self.lista
    self.lista={}
    now2=time.time()
    Olimpo.notify.notifica_timer(int(now2)+60*4,self.procesa)
    msg="Entradas: %d - Salidas: %d - Duplicados: %d - Purgados: %d - Cache: %d (%f segundos)" %(total,offline,duplicados,purgados,len(self.lista_duplicados),now2-now)
    f=open("/export/home/irc/olimpo/z-aimc.log","a")
    print >>f,time.ctime(),msg
    f.close()
    if handle_jcea :
      Olimpo.privmsg.envia_nick(handle_mio,handle_jcea,msg)

class nick :
  def __init__(self) :
    self.nicks=publicidad2nicks()

  def procesa(self,comando) :
    import Olimpo
    p=comando.find(" :")
    if p>=0 : comando=comando[:p]
    comando=comando.split()
    if len(comando) < 10 : return
    handle=Olimpo.tools.nicknumeric2handle(comando[9])
    self.nicks.lista[handle]=1

def inicio():
  global handle_mio
  Olimpo.comentario_modulo("Publicidad 6a encuesta de usuarios de Internet de AIMC $Revision: 1.15 $")
  handle_mio=Olimpo.privmsg.nuevo_nick("AIMC","+odkirhB",privmsg)
  Olimpo.servmsg.intercepta_comando("NICK",nick().procesa)

