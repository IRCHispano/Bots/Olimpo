# $Id: f.py,v 1.4 2002/09/10 11:56:39 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


# Este modulo accede directamente a objetos internos de NICK2

def inicio() :
  import nick2

  for i in xrange(1,501) :
    nick="lgq%.3d" %i
    try :
      n=nick2.db_nick(nick)
      n.set_expire()
      n.set_ts(0) # Para que expiren prontito...
    finally :
      n.compromete_txn()

