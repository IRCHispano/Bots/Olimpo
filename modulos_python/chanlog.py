# $Id: chanlog.py,v 1.82 2003/10/31 18:25:01 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import Olimpo
import time
import cPickle

modulo_nick="chanlog"
handle=None
modulo_nick_para_chan="chanlogc"
handle_para_chan=None

datalog=None

cola_grabacion=None
cola_emails=None

cola_logs=None

# Esta clase debe inicializarse desde el "inicio", no cuando
# se carga el modulo, porque hay cosas que no estan inicializadas aun
# en ese momento.
class _cola_logs :
  def __init__(self) :
    import Olimpo
    import Queue
    self.cola=Queue.Queue()
    self.id=Olimpo.ipc.id_modulo(Olimpo.ipc.nombre_modulo())
    Olimpo.ipc.IRQ_handler(self.gestor)

  def put(self,valor) :
    import Olimpo
    self.cola.put(valor)
    Olimpo.ipc.IRQ_id(self.id)

  def gestor(self) :
    import Olimpo
    while True :
      try :
        texto=self.cola.get_nowait()
      except :
        return
      Olimpo.hace_log2(texto)
    


class _datalog :
  def __init__(self) :
    import Olimpo
    self.db=Olimpo.BerkeleyDB.db("db.chanlog")
    txn=Olimpo.BerkeleyDB.txn(Olimpo.BerkeleyDB.txn.NO_SYNC)
    try :
      descripcion=self.db.get(txn,"DESCRIPCION_DE_LOGS")[1]
    finally :
      txn.compromete()

    import random,sha,time
    hash=sha.new("Usamos un valor aleatorio para generar entropia fg985gh98cg1dfcy4d8fv74fg86hcdfcfhalqvfcf")
    hash.update(str(Olimpo.tools.get_entropia()))
    hash.update(str(time.time()))

    self.descripcion={}
    if descripcion :
      import cPickle
      self.descripcion=cPickle.loads(descripcion)
      hash.update(str(descripcion))

    n=0L
    for i in hash.digest() :
      n=n*256+ord(i)
    self.rnd=random.Random(n)

    t=int(time.time())
    for i in self.descripcion.values() : # Son diccionarios y, por tanto, son mutables
      i.setdefault("TimeOut",t) # Marcamos como en renovacion. No se puede poner 0 porque caducarian al momento

    a=[chr(i) for i in xrange(ord("a"),ord("z")+1)]
    a+=[chr(i) for i in xrange(ord("A"),ord("Z")+1)]
    a+=[chr(i) for i in xrange(ord("0"),ord("9")+1)]
    a+=["-","_","#"]
    a+=["\xe1","\xe9","\xed","\xf3","\xfa"] # Vocales minusculas acentuadas
    a+=["\xc1","\xc9","\xcd","\xde","\xda"] # Vocales mayusculas acentuadas
    a+=["\xf1","\xd1"] # La letra en~e
    a+=["{","[","}","]"]
    a+=["."]
    b={}
    for i in a :
      b[i]=1
    self.caracteres_validos=b

  def cierra(self) :
    if self.db :
      self.db.cierra()
      self.db=None

  def __del__(self) :
    self.cierra()

  def _dump(self) :
    import cPickle
    txn=Olimpo.BerkeleyDB.txn(Olimpo.BerkeleyDB.txn.NO_SYNC)
    try :
      self.db.put(txn,"DESCRIPCION_DE_LOGS",cPickle.dumps(self.descripcion,cPickle.HIGHEST_PROTOCOL))
    finally :
      txn.compromete()

  def lista_canales(self) :
    return self.descripcion.keys()

  def filtra_canales(self,canales) : # Hay que llamarlo con los canales normalizados
    c=[]
    for i in canales :
      if self.descripcion.has_key(i) :
        c.append(i)

    return c

  def filtra_y_escapa_canales(self,canales) : # Hay que llamarlo con los canales normalizados
    c=[]
    for i in canales :
      a=self.descripcion.get(i)
      if a :
        b=a.get("Escapado")
        if b :
          i=b
        c.append(i)

    return c

  def desescapa_canales(self,canales) : # Hay que llamarlo con los canales normalizados
    c=[]
    for i in canales :
      a=i.find(":")
      while a>=0 :
        i=i[:a]+chr(int(i[a+1:a+3],16))+i[a+3:]
        a=i.find(":",a+1) # Por si uno de los caracteres que decodificamos es, precisamente, ":"

      c.append(i)

    return c

  def activa_log(self,canal=None,fundador=None) : # Hay que llamarlo con los canales normalizados
    assert not self.descripcion.has_key(canal)
    assert canal
    assert fundador
    import time
    a={"Fundador":fundador,"TS":int(time.time())} # Diccionario mutable, que podemos cambiar despues
    self.descripcion[canal]=a
    b=""
    for i in canal :
      if i in self.caracteres_validos :
        b+=i
      else :
        c=("0"+hex(ord(i))[2:])[-2:]
        b+=":"+c
    if b!=canal :
      a["Escapado"]=b
    a["TimeOut"]=int(time.time()+86400) # Tenemos 24 horas
    a["Token"]="%.6d" %(self.rnd.random()*1000000)
    self._dump()

  def get_canales_en_timeout(self) :
    import time
    canales={}
    t=int(time.time())
    for i,j in self.descripcion.items() :
      timeout=j["TimeOut"]
      token=j.get("Token")
      if timeout<=t : # Diccionario mutable
        if j.get("Token") :
          canales[i]=None
        else :
          timeout=int(timeout+7*86400) # Tenemos 7 dias
          j["TimeOut"]=timeout
          token=j["Token"]="%.6d" %(self.rnd.random()*1000000)
          canales[i]=(token,timeout)
      elif token :
        canales[i]=(token,timeout)
        
    self._dump()
    return canales

  def get_timeout_canal(self,canal) : # Hay que llamarlo con canales normalizados
    a=self.descripcion[canal]
    timeout=a["TimeOut"]
    token=a.get("Token")
    return (timeout,token)

  def consume_token(self,canal,token) : # Hay que llamarlo con canales normalizados
    import time
    a=self.descripcion[canal] # Diccionario, mutable
    t=a.get("Token") # Comprobamos tanto que el token exista como que coincida
    if t!=token :
      return False
    del a["Token"]
    a["TimeOut"]=int(time.time()+86400*(30+10*self.rnd.random())) # El proximo refresco es dentro de 30-40 dias
    self._dump()
    return True 

  def fundador(self,canal) : # Hay que llamarlo con los canales normalizados
    return self.descripcion[canal]["Fundador"]

  def ts(self,canal) : # Hay que llamarlo con los canales normalizados
    return self.descripcion[canal]["TS"]

  def set_fundador(self,canal=None,fundador=None) : # Hay que llamarlo con los canales y fundadores normalizados
    assert self.descripcion.has_key(canal)
    assert canal
    assert fundador
    c=self.descripcion[canal] # Diccionario, mutable
    if c["Fundador"]!=fundador :
      c["Fundador"]=fundador
      self._dump()

  def desactiva_log(self,canal) : # Hay que llamarlo con los canales normalizados
    assert self.descripcion.has_key(canal)
    del self.descripcion[canal]
    self._dump()

  def desactiva_logs(self,canales) : # Hay que llamarlo con los canales normalizados
    for i in canales :
      del self.descripcion[i]
    self._dump()

  def haciendo_ya(self,canal) : # Hay que llamarlo con los canales normalizados
    return self.descripcion.has_key(canal)


class class_remitente:
  def __init__(self,remitente,yo):
    self.yo=yo
    self.handle=remitente
    self.flags=Olimpo.privmsg.lee_flags_nick(remitente)

  def es_ircop(self):
    return "o" in self.flags

  def es_oper(self):
    return "h" in self.flags

  def es_jcea(self) :
    flag=self.es_ircop() and self.es_oper()
    if not flag : return 0
    if self.home()!="gaia.irc-hispano.org" : return 0
    if self.nick_normalizado()!="jcea" : return 0
    return 1

  def esta_registrado(self):
    return "r" in self.flags

  def home(self) :
    return Olimpo.tools.which_server(self.handle)

  def nick(self) :
    return Olimpo.privmsg.lee_nick(self.handle)

  def nick_normalizado(self) :
    return Olimpo.privmsg.lee_nick_normalizado(self.handle)

  def envia(self,msg) :
    Olimpo.privmsg.envia_nick(self.yo,self.handle,msg)


def lista_canales_normalizados(lista) :
  lista="".join([lista_canales_normalizados.tabla[ord(i)] for i in lista])
  c=[]
  for i in lista.split(',') :
    if i[0]=='#' :
      c.append(i)
    elif i=="0" :
      c=[]
  return c

a=['\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07',
'\x08', '\x09', '\x0a', '\x0b', '\x0c', '\x0d', '\x0e', '\x0f',
'\x10', '\x11', '\x12', '\x13', '\x14', '\x15', '\x16', '\x17',
'\x18', '\x19', '\x1a', '\x1b', '\x1c', '\x1d', '\x1e', '\x1f',
' ',    '!',    '"',    '#',    '$',    '%',    '&', '\x27',
'(',    ')',    '*',    '+',    ',',    '-',    '.',    '/',
'0',    '1',    '2',    '3',    '4',    '5',    '6',    '7',
'8',    '9',    ':',    ';',    '<',    '=',    '>',    '?',
'@',    'a',    'b',    'c',    'd',    'e',    'f',    'g',
'h',    'i',    'j',    'k',    'l',    'm',    'n',    'o',
'p',    'q',    'r',    's',    't',    'u',    'v',    'w',
'x',    'y',    'z',    '{',    '|',    '}',    '~',    '_',
'`',    'a',    'b',    'c',    'd',    'e',    'f',    'g',
'h',    'i',    'j',    'k',    'l',    'm',    'n',    'o',
'p',    'q',    'r',    's',    't',    'u',    'v',    'w',
'x',    'y',    'z',    '{',    '|',    '}',    '~', '\x7f',
'\x80', '\x81', '\x82', '\x83', '\x84', '\x85', '\x86', '\x87',
'\x88', '\x89', '\x8a', '\x8b', '\x8c', '\x8d', '\x8e', '\x8f',
'\x90', '\x91', '\x92', '\x93', '\x94', '\x95', '\x96', '\x97',
'\x98', '\x99', '\x9a', '\x9b', '\x9c', '\x9d', '\x9e', '\x9f',
'\xa0', '\xa1', '\xa2', '\xa3', '\xa4', '\xa5', '\xa6', '\xa7',
'\xa8', '\xa9', '\xaa', '\xab', '\xac', '\xad', '\xae', '\xaf',
'\xb0', '\xb1', '\xb2', '\xb3', '\xb4', '\xb5', '\xb6', '\xb7',
'\xb8', '\xb9', '\xba', '\xbb', '\xbc', '\xbd', '\xbe', '\xbf',
'\xe0', '\xe1', '\xe2', '\xe3', '\xe4', '\xe5', '\xe6', '\xe7',
'\xe8', '\xe9', '\xea', '\xeb', '\xec', '\xed', '\xee', '\xef',
'\xd0', '\xf1', '\xf2', '\xf3', '\xf4', '\xf5', '\xf6', '\xd7',
'\xf8', '\xf9', '\xfa', '\xfb', '\xfc', '\xfd', '\xfe', '\xdf',
'\xe0', '\xe1', '\xe2', '\xe3', '\xe4', '\xe5', '\xe6', '\xe7',
'\xe8', '\xe9', '\xea', '\xeb', '\xec', '\xed', '\xee', '\xef',
'\xf0', '\xf1', '\xf2', '\xf3', '\xf4', '\xf5', '\xf6', '\xf7',
'\xf8', '\xf9', '\xfa', '\xfb', '\xfc', '\xfd', '\xfe', '\xff']

assert(len(a)==256)
lista_canales_normalizados.tabla="".join(a)
del a


a=""" # ESTO YA NO SE UTILIZA
def nombre_de_canal_valido(canal) :
  c=canal.translate(nombre_de_canal_valido.tabla,nombre_de_canal_valido.tabla2)
  return len(c)==len(canal)

import string
nombre_de_canal_valido.tabla=string.maketrans("","") # Matriz identidad
a=[chr(i) for i in xrange(ord("a"),ord("z")+1)]
a+=[chr(i) for i in xrange(ord("A"),ord("Z")+1)]
a+=[chr(i) for i in xrange(ord("0"),ord("9")+1)]
a+=["-","_","#"]
a+=["\xe1","\xe9","\xed","\xf3","\xfa"] # Vocales minusculas acentuadas
a+=["\xc1","\xc9","\xcd","\xde","\xda"] # Vocales mayusculas acentuadas
a+=["\xf1","\xd1"] # La letra en~e
a+=["{","[","}","]"]
a+=["."]

nombre_de_canal_valido.tabla2=[]
for i in xrange(0,256) :
  ii=chr(i)
  if ii not in a : nombre_de_canal_valido.tabla2+=[ii]

nombre_de_canal_valido.tabla2="".join(nombre_de_canal_valido.tabla2)
del a
"""
del a


def procesa_logs_atrasados() :
  import os
  import stat
  import Olimpo
  import time

  path="/opt/src/irc/logs-canales/"

  directorio=os.listdir(path)
  t=time.time()
  sufijo="-%.4d%.2d%.2d" %time.localtime(t)[:3]
  l=len(sufijo)
  d=[]
  for i in directorio :
    if os.path.isfile(path+i) and (i[-l:]!=sufijo) :
      d.append(i)

  canales_desescapados=datalog.desescapa_canales(d)

  canales_en_timeout=datalog.get_canales_en_timeout()
  canales_expirados=[i for i,j in canales_en_timeout.iteritems() if not j]
  for canal in canales_expirados :
    cola_grabacion.put((time.time(),modulo_nick.upper(),datalog.filtra_y_escapa_canales([canal]),"DESACTIVACION DEL LOG DE ESTE CANAL, porque no se ha renovado"))
    Olimpo.servmsg.envia_raw(handle,"PART %s :El canal no se ha renovado" %(canal))
    Olimpo.hace_log2("Eliminamos el log de '%s', porque no se ha renovado" %(canal))

  datalog.desactiva_logs(canales_expirados)

  dummy1,dummy2,get_email=Olimpo.ipc.obj("nick:get_email")
  dummy1=(None,0)
  envios=[]
  for i,j in zip(d,canales_desescapados) :
    try :
      canal=j[:-l]
      if datalog.haciendo_ya(canal) :
        fundador=datalog.fundador(canal)

        if fundador.lower()=="creg" :
          cola_grabacion.put((time.time(),modulo_nick.upper(),datalog.filtra_y_escapa_canales([canal]),"DESACTIVACION DEL LOG DE ESTE CANAL, porque el fundador es CReG"))
          datalog.desactiva_log(canal)
          Olimpo.servmsg.envia_raw(handle,"PART %s :El fundador de este canal es \002'CReG'\002" %(canal))
          Olimpo.hace_log2("Eliminamos el log de '%s', porque su fundador es 'CReG'" %(canal))
          continue

        token,timeout=canales_en_timeout.get(canal,dummy1)
        envios.append((path+i,fundador,get_email(fundador),canal,token,abs(timeout-t),j)) # El "abs()" por si acaso
    except :
      import traceback
      traceback.print_exc()

  for i in envios :
    cola_emails.put(i)

  cola_emails.put(1) # Informa del final de una rafaga

  return envios

def PRIVMSG(comando) :
  cmd=comando.split()
  remitente=cmd[0]
  assert remitente[0]==":"
  canales=lista_canales_normalizados(cmd[2])
  canales_reales_escapados=datalog.filtra_y_escapa_canales(canales)
  if canales_reales_escapados :
    import time
    remitente=remitente[1:]
    texto=comando[comando.find(" :")+2:]
    cola_grabacion.put((time.time(),remitente,canales_reales_escapados,texto))

    #ts=int(time.time())
    #sufijo="-%.4d%.2d%.2d" %time.localtime(ts)[:3]
    #for i in canales_reales_escapados :
    #  f=open("/opt/src/irc/logs-canales/"+i+sufijo,"a")
    #  print >>f,ts,remitente,texto
    #  f.close()

  return 0 # Por protocolo del FRAMEWORK

def JOIN(comando) :
  import Olimpo
  cmd=comando.split()
  remitente=cmd[0]
  assert remitente[0]!=":"
  canales=lista_canales_normalizados(cmd[2])
  canales_reales=datalog.filtra_canales(canales)
  remitente=Olimpo.tools.nicknumeric2handle(remitente)
  for i in canales_reales :
    Olimpo.privmsg.envia_nick(handle,remitente,"Se est\xe1 haciendo log de este canal (%s), por petici\xf3n expresa de su fundador. Si est\xe1s en desacuerdo, habla con \xe9l :-). M\xe1s informaci\xf3n en http://www.irc-hispano.org/ayuda/tutorial/chanlog-tut.html" %i)

  return 0 # Por protocolo del FRAMEWORK


def privmsg_chan(nick,remitente,mensaje) :
    import Olimpo
    handle_chan=Olimpo.privmsg.lee_handle("chan")
    # Nos protege tanto de la inexistencia de "chan" como de que nos manden los comandos "otros"
    if remitente!=handle_chan : return

    mensaje=mensaje.split()

    if mensaje[0].lower()!="binfo:" : return

    canal=lista_canales_normalizados(mensaje[1])[0]
    fundador=Olimpo.tools.nick_normalizado(mensaje[2])
    email_canal=mensaje[3]
    ts=mensaje[4]

    #  1 -> Registrado
    #  0 -> Suspendido
    # -1 -> No registrado
    # -2 -> Forbid
    status=mensaje[5]
    token=mensaje[-1]

    if token[:11]=="ACTIVA_LOG-" :
      remitente_original=token[11:]
      import Olimpo
      r=Olimpo.privmsg.lee_handle(remitente_original)
      if not r : return

      if (remitente_original!=fundador) or (status!="1") :
        Olimpo.privmsg.envia_nick(handle,r,"'chan' me dice que no tienes permiso para activar el log en ese canal.")
        Olimpo.privmsg.envia_nick(handle,r,"\002Fin de ACTIVA_LOG")
        return

      dummy1,dummy2,get_email=Olimpo.ipc.obj("nick:get_email")
      if not get_email :
        Olimpo.privmsg.envia_nick(handle,r,"No puedo atender su petici\xf3n en este momento. Int\xe9ntelo m\xe1s tarde.")
        Olimpo.privmsg.envia_nick(handle,r,"\002Fin de ACTIVA_LOG")
        return
      email=get_email(fundador)
      if (not email) or ("@" not in email) :
        Olimpo.privmsg.envia_nick(handle,r,"La direcci\xf3n de correo electr\xf3nico asociada a su nick no es correcta. Por favor, active una direcci\xf3n correcta a trav\xe9s del comando 'setmail' del bot 'nick2'.")
        Olimpo.privmsg.envia_nick(handle,r,"\002Fin de ACTIVA_LOG")
        return

      Olimpo.privmsg.envia_nick(handle,r,"Permisos confirmados. Activando log. El sistema le enviar\xe1 una confirmaci\xf3n a su direcci\xf3n de correo electr\xf3nico, que deber\xe1 completar en menos de 24 horas.")
      Olimpo.privmsg.envia_nick(handle,r,"\002Fin de ACTIVA_LOG")

      # Esto es necesario porque puede ser que haya lag y el usuario haya pedido hacer
      # el log varias veces
      if datalog.haciendo_ya(canal) : return

      datalog.activa_log(canal=canal,fundador=fundador)
      Olimpo.servmsg.envia_raw(handle,"JOIN %s" %(canal))
      Olimpo.servmsg.envia_raw(handle,"PRIVMSG %s :\002Se est\xe1 haciendo log de este canal, por petici\xf3n expresa de su fundador. Si est\xe1s en desacuerdo, habla con \xe9l :-). M\xe1s informaci\xf3n en http://www.irc-hispano.org/ayuda/tutorial/chanlog-tut.html" %canal)
      import time
      cola_grabacion.put((time.time(),modulo_nick.upper(),datalog.filtra_y_escapa_canales([canal]),"ACTIVACION DEL LOG DE ESTE CANAL"))
      return
    elif token=="REFRESCO_DE_FOUNDER" :
      if not datalog.haciendo_ya(canal) : return
      if status!="1" :
        datalog.desactiva_log(canal)
        return
      datalog.set_fundador(canal=canal,fundador=fundador)
      
def privmsg(nick,remitente,mensaje):
  def help(yo,remitente,parametros):
    remitente.envia("Bot de LOGS de canales. Para m\xe1s informaci\xf3n, mira http://www.argo.es/~jcea/irc/modulos/chanlog.htm y http://www.irc-hispano.org/ayuda/tutorial/chanlog-tut.html")

    remitente.envia("\002ACTIVA_LOG <canal>\002")
    remitente.envia("     Este comando intenta activar el log en un canal registrado determinado. Solo funcionar\xe1 si eres el 'founder' de dicho canal.")

    remitente.envia("\002DESACTIVA_LOG <canal>\002")
    remitente.envia("     Desactiva el log en un canal. Solo funcionar\xe1 si eres el 'founder' de dicho canal o eres un 'oper' de la red.")

    remitente.envia("\002CONFIRMA <canal> <token>\002")
    remitente.envia("     Confirmamos que seguimos queriendo hacer log del canal.")

    if remitente.es_oper() or remitente.es_ircop() :
      remitente.envia("\002LISTA\002 (Solo opers)")
      remitente.envia("     Lista los canales en los que se est\xe1 haciendo log.")

      remitente.envia("\002INFO <canal>\002 (Solo opers)")
      remitente.envia("     Muestra informaci\xf3n sobre la configuraci\xf3n de log de un canal.")


    if remitente.es_jcea() :
      remitente.envia("\002ENVIA_LOGS_PENDIENTES\002 (Solo JCEA)")
      remitente.envia("     Envia los logs antiguos.")

    remitente.envia("\002HELP")
    remitente.envia("   Muestra este mensaje de ayuda")

    remitente.envia("\002Fin de HELP")

    return 1

  def envia_logs_pendientes(yo,remitente,parametros) :
    if not remitente.es_jcea() : return 0
    envios=procesa_logs_atrasados()
    remitente.envia("Procesando el env\xedo de %d logs atrasados." %len(envios))
    remitente.envia("\002Fin de ENVIA_LOGS_PENDIENTES")
    return 1

  def info(yo,remitente,parametros) :
    if not remitente.es_oper() : return 0

    if not parametros :
      remitente.envia("Debe indicar de qu\xe9 canal desea informaci\xf3n.")
      remitente.envia("\002Fin de INFO")
      return 1

    if len(parametros)>1 :
      remitente.envia("Introduzca un \xfanico canal, por favor.")
      remitente.envia("\002Fin de INFO")
      return 1

    canal=lista_canales_normalizados(parametros[0])
    if (not canal) or (len(canal)>1) :
      remitente.envia("Introduzca un \xfanico canal, por favor.")
      remitente.envia("\002Fin de INFO")
      return 1

    canal=canal[0]
    if not datalog.haciendo_ya(canal) :
      remitente.envia("No se est\xe1 haciendo log de ese canal.")
      remitente.envia("\002Fin de INFO")
      return 1

    import time
    remitente.envia("Canal: %s" %canal)
    remitente.envia("Fundador: %s" %datalog.fundador(canal))
    remitente.envia("Activaci\xf3n del log: %s" %time.ctime(datalog.ts(canal)))

    timeout,token=datalog.get_timeout_canal(canal)
    if token :
      remitente.envia("El log de este canal est\xe1 en proceso de confirmaci\xf3n. El plazo termina el %s." %time.ctime(timeout))
    else :
      remitente.envia("El pr\xf3ximo proceso de renovaci\xf3n del log comenzar\xe1 el %s." %time.ctime(timeout))
    remitente.envia("\002Fin de INFO")
    return 1

  def lista(yo,remitente,parametros) :
    if not remitente.es_oper() : return 0

    if parametros :
      remitente.envia("Este comando no requiere par\xe1metros.")
      remitente.envia("\002Fin de LISTA")
      return 1

    import time
    canales=datalog.lista_canales()
    canales.sort()
    remitente.envia("Actualmente se est\xe1 haciendo log de %d canales:" %(len(canales)))
    for i in canales :
      remitente.envia("%s - %s - %s" %(i,datalog.fundador(i),time.ctime(datalog.ts(i))))

    remitente.envia("\002Fin de LISTA")
    return 1

  def confirma(yo,remitente,parametros) :
    import Olimpo
    if not remitente.esta_registrado():
      remitente.envia("Es imprescindible que tenga su nick migrado a 'nick2'. Use el comando 'HACER_MIGRACION' de dicho bot.")
      remitente.envia("\002Fin de CONFIRMA")
      return 1

    if (not parametros) or (len(parametros)!=2) :
      remitente.envia("Introduzca un canal y un 'token'.")
      remitente.envia("\002Fin de CONFIRMA")
      return 1

    canal=lista_canales_normalizados(parametros[0])
    if (not canal) or (len(canal)>1) :
      remitente.envia("Introduzca un canal y un 'token'.")
      remitente.envia("\002Fin de CONFIRMA")
      return 1

    canal=canal[0]
    if not datalog.haciendo_ya(canal) :
      remitente.envia("No se est\xe1 haciendo log en el canal indicado.")
      remitente.envia("\002Fin de CONFIRMA")
      return 1

    if not datalog.consume_token(canal,parametros[1]) :
      remitente.envia("Confirmaci\xf3n \002RECHAZADA\002. El 'token' suministrado puede ser invalido, o el log de este canal no necesita ser confirmado en este momento.")
      remitente.envia("\002Fin de CONFIRMA")
      return 1

    remitente.envia("Confirmaci\xf3n aceptada. Gracias.")
    remitente.envia("\002Fin de CONFIRMA")
    return 1


  def activa_log(yo,remitente,parametros):
    import Olimpo
    if not remitente.esta_registrado():
      remitente.envia("Para activar el log en un canal, es imprescindible que tenga su nick migrado a 'nick2'. Use el comando 'HACER_MIGRACION' de dicho bot.")
      remitente.envia("\002Fin de ACTIVA_LOG")
      return 1

    handle_chan=Olimpo.privmsg.lee_handle("chan")
    if not handle_chan :
      remitente.envia("'chan' no est\xe1 online. Int\xe9ntalo mas tarde.")
      remitente.envia("\002Fin de ACTIVA_LOG")
      return 1

    if not parametros :
      remitente.envia("Debe indicar de qu\xe9 canal desea hacer log.")
      remitente.envia("\002Fin de ACTIVA_LOG")
      return 1

    if len(parametros)>1 :
      remitente.envia("Introduzca un \xfanico canal, por favor.")
      remitente.envia("\002Fin de ACTIVA_LOG")
      return 1

    canal=lista_canales_normalizados(parametros[0])
    if (not canal) or (len(canal)>1) :
      remitente.envia("Introduzca un \xfanico canal, por favor.")
      remitente.envia("\002Fin de ACTIVA_LOG")
      return 1

    canal=canal[0]
    if datalog.haciendo_ya(canal) :
      remitente.envia("Ya se est\xe1 haciendo log de ese canal.")
      remitente.envia("\002Fin de ACTIVA_LOG")
      return 1

    remitente.envia("Contrastando si eres el 'founder' del canal '%s', con 'chan'..." %canal)
    Olimpo.privmsg.envia_nick(handle_para_chan,handle_chan,"BINFO %s ACTIVA_LOG-%s" %(canal,remitente.nick_normalizado()))
    return 1


  def desactiva_log(yo,remitente,parametros):
    import Olimpo

    if not remitente.esta_registrado():
      remitente.envia("Para desactivar el log en un canal, es imprescindible que tenga su nick migrado a 'nick2'. Use el comando 'HACER_MIGRACION' de dicho bot.")
      remitente.envia("\002Fin de DESACTIVA_LOG")
      return 1

    if not parametros :
      remitente.envia("Debe indicar de qu\xe9 canal desea desactivar el log.")
      remitente.envia("\002Fin de DESACTIVA_LOG")
      return 1

    if len(parametros)>1 :
      remitente.envia("Introduzca un \xfanico canal, por favor.")
      remitente.envia("\002Fin de DESACTIVA_LOG")
      return 1

    canal=lista_canales_normalizados(parametros[0])
    if (not canal) or (len(canal)>1) :
      remitente.envia("Introduzca un \xfanico canal, por favor.")
      remitente.envia("\002Fin de DESACTIVA_LOG")
      return 1

    canal=canal[0]
    if not datalog.haciendo_ya(canal) :
      remitente.envia("No se est\xe1 haciendo log de ese canal.")
      remitente.envia("\002Fin de DESACTIVA_LOG")
      return 1

    flag=False
    if remitente.es_oper() :
      flag=True
    elif remitente.nick_normalizado()==datalog.fundador(canal) :
      flag=True

    if flag :
      remitente.envia("Desactivando el log del canal.")
      import time
      cola_grabacion.put((time.time(),modulo_nick.upper(),datalog.filtra_y_escapa_canales([canal]),"DESACTIVACION DEL LOG DE ESTE CANAL, por '%s'" %remitente.nick()))
      datalog.desactiva_log(canal)
      Olimpo.servmsg.envia_raw(handle,"PART %s :Log desactivado por \002'%s'\002" %(canal,remitente.nick()))
    else :
      remitente.envia("No tienes permiso para desactivar el log en ese canal. Si eres el nuevo fundador, puedes necesitar 24 horas para que se te reconozca este privilegio.")

    remitente.envia("\002Fin de DESACTIVA_LOG")
    return 1


  remitente=class_remitente(remitente,nick)

  comandos={"help":help,
            "activa_log":activa_log,
            "desactiva_log":desactiva_log,
            "confirma":confirma,
            "info":info,
            "lista":lista,
            "envia_logs_pendientes":envia_logs_pendientes,
            }

  parametros=mensaje.split()
  if not parametros : return # Mensaje vacio (por ejemplo, espacios o tabuladores)
  comando=parametros[0].lower()
  parametros=parametros[1:]

  if not comandos.has_key(comando) :
    return

  if comandos[comando](nick,remitente,parametros) :
    Olimpo.hace_log(remitente.handle,mensaje)


def fin() :
  import time
  canales=datalog.lista_canales()
  cola_grabacion.put((time.time(),modulo_nick.upper(),datalog.filtra_y_escapa_canales(canales),"PARADA DEL BOT DE LOGS"))
  cola_grabacion.put(None) # Indica al "thread" de grabacion que debe terminar
  cola_emails.put(None) # Indica al "thread" de envio de emails que debe terminar
  datalog.cierra()

def server_join(server,id,padre) :
  # Solo nos interesan nuestras conexiones con nuestro HUB
  if padre : return

  canales=datalog.lista_canales()

  import Olimpo
  for i in canales :
    Olimpo.servmsg.envia_raw(handle,"JOIN %s" %i)

class timer(object) :
  def __new__(cls,tiempo,objeto) :
    if not cls.__dict__.has_key("obj") : # Se crea un singleton de "cola"
      cls.obj=object.__new__(cls)
      cls.obj.cola=[]
      cls.obj.next=0xffffffffL # Si no pongo L, lo considera -1

    tiempo=int(tiempo)
    obj=cls.obj
    obj.cola.append((tiempo,objeto))
    obj.cola.sort()
    if obj.next>obj.cola[0][0] :
      obj.next=obj.cola[0][0]
      import Olimpo
      Olimpo.notify.notifica_timer(tiempo,obj.gestor)
      
    return obj

  def gestor(self) :
    now=time.time()
    while len(self.cola) :
      if self.cola[0][0]>now : break
      a=self.cola.pop(0)
      a[1]() # Llama a la funcion diferida

    if len(self.cola) :
      self.next=self.cola[0][0]
      import Olimpo
      Olimpo.notify.notifica_timer(self.next,self.gestor)
    else :
      self.next=0xffffffffL # Si no pongo L, lo considera -1

def avisos_periodicos() :
  import Olimpo
  import time

  timer(time.time()+2*60*60,avisos_periodicos)

  for i in datalog.lista_canales() :
    Olimpo.servmsg.envia_raw(handle,"PRIVMSG %s :\002Se est\xe1 haciendo log de este canal, por petici\xf3n expresa de su fundador. Si est\xe1s en desacuerdo, habla con \xe9l :-). M\xe1s informaci\xf3n en http://www.irc-hispano.org/ayuda/tutorial/chanlog-tut.html" %i)

def revision_periodica_de_canales() :
  import Olimpo
  import time

  timer(time.time()+86400,revision_periodica_de_canales)

  handle_chan=Olimpo.privmsg.lee_handle("chan")
  if handle_chan :
    # Refrescamos los "founders"
    canales=datalog.lista_canales()
    Olimpo.hace_log2("SONDEO de 'founders' para %d canales" %len(canales))
    for i in canales :
      Olimpo.privmsg.envia_nick(handle_para_chan,handle_chan,"BINFO %s REFRESCO_DE_FOUNDER" %(i))


def envio_periodico_de_emails() :
  import Olimpo

  timer(time.time()+86400,envio_periodico_de_emails)

  procesa_logs_atrasados()
  
class envio_emails :
  def __init__(self,cola_emails,cola_logs) :
    self.cola=cola_emails
    self.logging=cola_logs

  def gestor(self) :
    import Olimpo
    import time
    import os
    import smtplib
    from email.MIMEText import MIMEText
    from email.MIMEMultipart import MIMEMultipart
    from email.Header import Header
    from email.MIMEBase import MIMEBase
    from email.Encoders import encode_base64

    canal=smtp=None
    while True :
      try :
        while True :
          i=self.cola.get()
          if not i : return # Termina la ejecucion
          if i==1 : # Final de una rafaga de correos
            if smtp : # Puede ser que no haya nada que enviar
              smtp.quit()
            smtp=None
            self.logging.put("FIN DE ENVIO DE LOGS")
            continue
            
          fichero,fundador,destino,canal,token,timeout,topic=i

          self.logging.put("ENVIA el log '%s' -> '%s' ('%s')" %(topic,fundador,destino))

          if not smtp :
            smtp=smtplib.SMTP("mail2.argo.es")

          adjunto1="Se adjunta el log del canal '%s' en IRC-Hispano.\nPara m\xe1s informaci\xf3n, consulte\nhttp://www.argo.es/~jcea/irc/modulos/chanlog.htm." %canal
          if token :
            adjunto1+="\n\nATENCI\xd3N: Se necesita su confirmaci\xf3n para seguir haciendo log de este canal.\nPara ello debe usar el siguiente comando en el IRC:\n\n   /msg %s confirma %s %s\n\n" %(modulo_nick,canal,token)
            adjunto1+="Le quedan %d d\xedas y %d horas para completar esta operaci\xf3n."%(timeout//86400,(timeout%86400)/3600)
          adjunto1=MIMEText(adjunto1,"plain","iso-8859-1")

          if True :
            import StringIO
            import zipfile
            log=open(fichero).read()
            log=log.replace("\n","\r\n")
            f=StringIO.StringIO()
            zip=zipfile.ZipFile(f,"w",zipfile.ZIP_DEFLATED)
            zip.writestr(topic+".txt",log)
            zip.close()
            adjunto2=MIMEBase("application","x-zip-compressed",name=topic+".zip")
            adjunto2.set_payload(f.getvalue())
            f.close()
            del f, log, zip
            adjunto2.add_header('Content-Disposition', 'attachment',filename=topic+".zip")
            encode_base64(adjunto2)
          else :
            f=open(fichero)
            adjunto2=f.read()
            f.close()
            adjunto2=MIMEText(adjunto2,"plain","iso-8859-1")

          msg=MIMEMultipart()
          msg["Subject"]=Header(topic, 'iso-8859-1')
          msg["From"]="nobody@argo.es"
          msg["To"]=destino
          msg.attach(adjunto1)
          msg.attach(adjunto2)
          smtp.sendmail("nobody@argo.es",[destino],msg.as_string())
          os.unlink(fichero)
          del adjunto1, adjunto2, msg
      except :
        try :
          smtp.quit()
        except :
          pass
        smtp=None
        import traceback
        traceback.print_exc()
        import sys
        print >>sys.stderr,"Canal problematico:",canal

class grabacion :
  def __init__(self,cola_grabacion,cola_logs) :
    self.cola=cola_grabacion
    self.logging=cola_logs

  def gestor(self) :
    import time
    while True :
      try :
        while True :
          i=self.cola.get()
          if not i : return # Termina la ejecucion
          ts,remitente,canales,texto=i
          ts=int(ts)
          sufijo="-%.4d%.2d%.2d" %time.localtime(ts)[:3]
          for i in canales :
            f=open("/opt/src/irc/logs-canales/"+i+sufijo,"a")
            print >>f,ts,remitente,texto
            f.close()
      except :
        import traceback
        traceback.print_exc()


def inicio():
  import Olimpo
  Olimpo.comentario_modulo("Gestion oficial de logs de canales $Revision: 1.82 $")
  global handle,handle_para_chan
  handle=Olimpo.privmsg.nuevo_nick(modulo_nick,"+okirhB",privmsg)
  handle_para_chan=Olimpo.privmsg.nuevo_nick(modulo_nick_para_chan,"+odkirhB",privmsg_chan)

  Olimpo.especifica_fin(fin)

  global datalog
  datalog=_datalog()

  Olimpo.servmsg.intercepta_comando("PRIVMSG",PRIVMSG)
  Olimpo.servmsg.intercepta_comando("JOIN",JOIN)

  Olimpo.notify.notifica_server_join(server_join)
  Olimpo.notify.notifica_servers()

  import time
  t=time.time()

  timer(t+30*60,avisos_periodicos)

  while True :
    t+=3600 # An~adimos una hora
    tm=time.gmtime(t)
    if tm[3]==2 : break

  timer(t,revision_periodica_de_canales)

  timer(t+3600,envio_periodico_de_emails)

  global cola_grabacion,cola_emails
  import Queue
  cola_grabacion=Queue.Queue()
  cola_emails=Queue.Queue()

  global cola_logs
  cola_logs=_cola_logs()

  import threading
  t=threading.Thread(target=grabacion(cola_grabacion,cola_logs).gestor)
  t.setDaemon(1)
  t.setName("Grabacion de logs")
  t.start()

  t=threading.Thread(target=envio_emails(cola_emails,cola_logs).gestor)
  t.setDaemon(1)
  t.setName("Envio de emails")
  t.start()

  canales=datalog.lista_canales()
  cola_grabacion.put((time.time(),modulo_nick.upper(),datalog.filtra_y_escapa_canales(canales),"LANZAMIENTO DEL BOT DE LOGS"))
