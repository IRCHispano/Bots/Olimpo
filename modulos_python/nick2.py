# $Id: nick2.py,v 1.467 2004/04/07 11:44:06 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import Olimpo

debug=Olimpo.debug() or 0

if not debug :
  modulo_nick="nick2"
  modulo_nick_para_nick="nick2n"
else :
  modulo_nick="nick2-2"
  modulo_nick_para_nick="nick2-2n"

handle=0
handle_para_nick=0

db=None
db2=None

verbose_db = 0

caracteres_correctos_clave="" # Esto se genera al cargar el modulo

nick_historico=None

admins=None
token=None
__db_nick=None
db_expiraciones=None
db_setemail=None

def db_nick(nick=None,txn=None) :
  return __db_nick(nick,db=db,txn=txn,db_expiraciones=db_expiraciones)

import re

pwxxxx=re.compile("^[pP][wW][0-9][0-9][0-9][0-9]$")

email_valido=re.compile("^[-_A-Za-z0-9.]+@([-A-Za-z0-9]+\.)+[A-Za-z]+$")


class ejecucion_diferida(object) :
  def __new__(cls,tiempo,objeto) :
    if not cls.__dict__.has_key("obj") : # Se crea el singleton de "obj"
      cls.obj=object.__new__(cls)
      cls.obj.cola=[]
      cls.obj.flag_do=0
    tiempo=int(tiempo)
    obj=cls.obj
    obj.cola.append([tiempo,objeto])
    if not obj.flag_do :
      obj._recalcula_diferidos()
    return obj

  def _recalcula_diferidos(self) :
      self.cola.sort()
      import Olimpo
      if len(self.cola) :
        Olimpo.notify.notifica_timer(self.cola[0][0],self.do)
      else :
        Olimpo.notify.notifica_timer(0,None)

  def do(self) :
    import time
    import Olimpo

    now=time.time()
    n=len(self.cola)
    for i in xrange(n) :
      if self.cola[i][0]>now : break
    else :
      i=n

    cola=self.cola[:i]      # Temporizaciones vencidas
    self.cola=self.cola[i:] # Temporizaciones pendientes

    self.flag_do=1
    for i,j in cola :
      j()
    self.flag_do=0
    self._recalcula_diferidos()

class class_remitente:
  def __init__(self,remitente,yo):
    self.yo=yo
    self.handle=remitente
    self.flags=Olimpo.privmsg.lee_flags_nick(remitente)

  def es_ircop(self):
    return "o" in self.flags

  def es_oper(self):
    return "h" in self.flags
  
  def es_jcea(self) :
    flag=self.es_ircop() and self.es_oper()
    if not flag : return 0
    if self.home()!="gaia.irc-hispano.org" : return 0
    if self.nick_normalizado()!="jcea" : return 0
    return 1
  
  def es_admin(self) :
    return admins.es_admin(self)
  
  def esta_registrado(self):
    return "r" in self.flags

  def esta_suspendido(self) :
    return "S" in self.flags
  
  def tiene_nivel(self):
    return self.es_ircop() or self.es_oper()
  
  def home(self) :
    return Olimpo.tools.which_server(self.handle)
  
  def nick(self) :
    return Olimpo.privmsg.lee_nick(self.handle)

  def nick_normalizado(self) :
    return Olimpo.privmsg.lee_nick_normalizado(self.handle)
  
  def envia(self,msg) :
    Olimpo.privmsg.envia_nick(self.yo,self.handle,msg)


def db_nickdrop(nick,num_serie) :
  txn=Olimpo.BerkeleyDB.txn()
  db.delete(txn,nick)
  db.put(txn," NICKDROPS",repr(num_serie))

  # La limpieza siguiente es necesaria, porque si el nick se
  # vuelve a registrar, "heredaria" esos atributos.

  # Si era un oper, nos lo cargamos
  Olimpo.bdd.envia_registro('o',nick,None)
  # Si tenia IP virtual, nos la cargamos
  Olimpo.bdd.envia_registro('v',nick,None)

  admins.vincula_txn(txn)
  admins.delete(nick)
  admins.desvincula_txn()

  db_expiraciones.delete(nick,txn)
  db_setemail.delete(nick,txn)

  txn.compromete()


def ipc_nick_get_email(nick) :
  import Olimpo
  txn=Olimpo.BerkeleyDB.txn(Olimpo.BerkeleyDB.txn.NO_SYNC)
  try :
    n=db_nick(nick,txn=txn)
    if n.found :
      return n.get_email()
  finally :
    txn.compromete()

  return None


def m_nickdrop(yo,r,msg_segmentado,msg) :
  if not r.es_oper() : return
  if not (r.es_jcea() or r.es_admin()) :
    r.envia("No eres JCEA ni un admin")
    return

  if len(msg_segmentado)<3 :
    r.envia("Indique un nick y un comentario")
    r.envia("\002Fin de NickDrop")
    return

  Olimpo.hace_log(r.handle,msg)

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])

  assert(nick[0]!='*')

  if len(nick) > 20 :
    r.envia("El nick especificado es demasiado largo")
    r.envia("\002Fin de NickDrop")
    return

  comentario="%s hace un 'Drop': '%s'" %(r.nick()," ".join(msg_segmentado[2:]))

  if len(comentario) > 400 :
    r.envia("El comentario especificado es demasiado largo")
    r.envia("\002Fin de NickDrop")
    return

  r.envia("Desregistrando nick \002%s" %nick)

  txn=Olimpo.BerkeleyDB.txn()
  n=db_nick(nick,txn=txn)
  if n.found :
    historico=nick_historico(nick,txn=txn)
    historico.add(comentario,operacion=historico.op_drop)
    historico.update() 
  n.update()
  txn.compromete()

  if verbose_db :
    r.envia("Propagando (des)registro en la red \002(%d)" %Olimpo.bdd.envia_registro('n',nick,None))
  else :
    r.envia("Comando ejecutado con exito")
    Olimpo.bdd.envia_registro('n',nick,None)

  r.envia("Informando del \002Drop\002 a los modulos dinamicos")
  r.envia("\002Fin de NickDrop")


# Pasa un entero largo de 64 bits a ASCII.
def long2ascii(valor) :
  b=""
  for i in xrange(8) :
    b+=chr((valor>>((7-i)*8))&255)
  return b

# Pasa una cadena ASCII a un entero largo de 64 bits.
def ascii2long(cadena) :
  b=0L
  for i in xrange(8) :
    b=(b<<8)+ord(cadena[i])
  return b

# Pasa un entero largo de 64 bits a 4 enteros de 16 bits.
def long2tuple(valor) :
  t=[0,0,0,0]
  for i in xrange(4) :
   t[3-i]=valor&65535
   valor>>=16
  return t

# Pasa una tupla de 4 enteros de 16 bits a un entero largo de 64 bits.
def tuple2long(tupla1,tupla2=None) :
  if not tupla2 : return (long(tupla1[0])<<16)+tupla1[1]
  return (long(tupla1[0])<<48)+(long(tupla1[1])<<32)+(long(tupla2[0])<<16)+tupla2[1]

# Pasa una cadena ASCII a una tupla de 4 valores de 16 bits.
def ascii2tuple(cadena) :
  return long2tuple(ascii2long(cadena))

# Pasa una tupla de 4 valores a 64 bits en base64
def ascii2base64(cadena) :
  t=ascii2tuple(cadena)
  a=Olimpo.tools.inttobase64(t[0:2])
  a+=Olimpo.tools.inttobase64(t[2:4])
  return a

# Pasa una cadena de 64 bits en base64 a cadena ASCII
def base642ascii(cadena) :
  return long2ascii(tuple2long(Olimpo.tools.base64toint(cadena[0:6]),Olimpo.tools.base64toint(cadena[6:12])))

def procesa_TEA(nick,clave=None) :
  def tobase64(a) :
    b=ord(a[2])*256+ord(a[3])
    a=ord(a[0])*256+ord(a[1])
    return Olimpo.tools.inttobase64((a,b))
  def tostring(a) :
    b=""
    for i in a :
      b+=chr(i>>8)
      b+=chr(i&255)
    return b

  nick=Olimpo.tools.nick_normalizado(nick)
  nick2=(nick+"\0"*16)[:16]
  if clave : # Normalizamos la clave
    clave=(clave+"A"*12)[:12]
    a=list(Olimpo.tools.base64toint(clave[:6]))
    a+=list(Olimpo.tools.base64toint(clave[6:]))
    a=tostring(a)
    clave=tobase64(a[:4])+tobase64(a[4:8])
  else :
    import random
    import md5
    import time
    a=repr(random.randrange(0,pow(2,30)))+repr(time.time())
    for i in xrange(0,10) :
      a+="dfgcweuctiusdjxflmkerw3c"+repr(random.randrange(0,pow(2,30)))
    a=md5.new(a).digest()
    clave=tobase64(a[:4])+tobase64(a[4:8])

  clave2=list(Olimpo.tools.base64toint(clave[0:6]))
  clave2+=list(Olimpo.tools.base64toint(clave[6:12]))
  clave2+=[0,0,0,0] # Hay 64 bits de la clave que son cero
  nick3=[(ord(nick2[0])<<8)+ord(nick2[1]),(ord(nick2[2])<<8)+ord(nick2[3]),
         (ord(nick2[4])<<8)+ord(nick2[5]),(ord(nick2[6])<<8)+ord(nick2[7]),
         (ord(nick2[8])<<8)+ord(nick2[9]),(ord(nick2[10])<<8)+ord(nick2[11]),
         (ord(nick2[12])<<8)+ord(nick2[13]),(ord(nick2[14])<<8)+ord(nick2[15])]
  r1=Olimpo.tools.tea_crypt(nick3[0:4],clave2)
  nick3[4]^=r1[0]
  nick3[5]^=r1[1]
  nick3[6]^=r1[2]
  nick3[7]^=r1[3]
  r2=Olimpo.tools.tea_crypt(nick3[4:8],clave2)
  result=Olimpo.tools.inttobase64(r2[0:2])
  result+=Olimpo.tools.inttobase64(r2[2:4])
  return (nick,clave,result)

def tiempo2ascii(tiempo) :
  if tiempo>=2*86400 :
    return "%.1f dias" %(tiempo/86400.0)
  if tiempo>=3600 :
    return "%.1f horas" %(tiempo/3600.0)
  if tiempo>=60 :
    return "%.1f minutos" %(tiempo/60.0)
  return "%.1f segundos" %tiempo # Conversion automatica a coma flotante

def m_getpass(yo,r,msg_segmentado,msg) :
  if not r.es_jcea() : return
  if len(msg_segmentado)!=2 : return

  Olimpo.hace_log(r.handle,msg)

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])
  n=db_nick(nick)
  if n.found :
    r.envia("La clave de \002%s\002 es: \002%s" %(nick,n.get_clave()))
  else :
    r.envia("El nick \002%s\002 no consta como registrado en la BDD" %nick)
  n.compromete_txn()
  r.envia("\002Fin de GetPass")

def m_listasetemail(yo,r,msg_segmentado,msg) :
  if not r.es_jcea() : return

  Olimpo.hace_log(r.handle,msg)

  lista=db_setemail.lista()
  r.envia("La lista de nicks con \002SetEMail\002 pendientes son (%d):" %len(lista))
  for i in lista :
    r.envia("  %s" %i)

  r.envia("\002Fin de ListaSetEMail")


def m_expiredpass(yo,r,msg_segmentado,msg) :
  if not r.es_jcea() : return

  Olimpo.hace_log(r.handle,msg)

  no_oper,hard,soft=db_expiraciones.claves_expiradas()
  soft=soft.items()
  soft.sort()
  hard=hard.items()
  hard.sort()
  no_oper=no_oper.items()
  no_oper.sort()
  import time
  r.envia("\002Usuarios cuya ultima conexion fue sin +h: %d" %len(no_oper))
  for i,j in no_oper : # Imprime el primer acceso sin +h
    r.envia("  %s: %s - %s" %(i,time.ctime(j[0]),time.ctime(j[1])))
  r.envia("\002Usuarios con expiraciones HARD: %d" %len(hard))
  for i,j in hard : # Imprime fecha de cambio de clave
    r.envia("  %s: %s" %(i,time.ctime(j[0])))
  r.envia("\002Usuarios con expiraciones SOFT: %d" %len(soft))
  for i,j in soft : # Inprime fecha de cambio de clave
    r.envia("  %s: %s" %(i,time.ctime(j[0])))
  r.envia("\002Fin de ExpiredPass")

def m_pesto(yo,r,msg_segmentado,msg) :
  raise "prueba"
  Olimpo.hace_log2(r.nick_normalizado())
  Olimpo.hace_log2("IRCop:",r.es_ircop())
  Olimpo.hace_log2("OPer:",r.es_oper())
  Olimpo.hace_log2("Home:",r.home())
  r.envia("\002Fin de Pesto")

def m_adminlist(yo,r,msg_segmentado,msg) :
  if not r.es_oper() : return
  if not (r.es_jcea() or r.es_admin()) :
    r.envia("No eres JCEA ni un admin")
    return

  Olimpo.hace_log(r.handle,msg)

  a=admins.list()
  a.sort()
  for i in a :
    r.envia(i)
  r.envia("\002Fin de AdminList")

def m_admindel(yo,r,msg_segmentado,msg) :
  if not r.es_oper() : return
  if not (r.es_jcea() or r.es_admin()) :
    r.envia("No eres JCEA ni un admin")
    return

  if len(msg_segmentado)!=2 : return

  Olimpo.hace_log(r.handle,msg)

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])
  if admins.delete(nick) :
    r.envia("\002%s\002 ya no es admin" %nick)
  else :
    r.envia("El nick indicado no es admin")
  r.envia("\002Fin de AdminDel")

def m_adminadd(yo,r,msg_segmentado,msg) :
  if not r.es_jcea() : return
  if len(msg_segmentado)!=2 : return

  Olimpo.hace_log(r.handle,msg)

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])
  if admins.add(nick) :
    r.envia("Ahora \002%s\002 ya es admin" %nick)
  else :
    r.envia("\002%s\002 no esta registrado en la Base de Datos Distribuida" %nick)
  r.envia("\002Fin de AdminAdd")

def m_nickinfo(yo,r,msg_segmentado,msg) :
  if len(msg_segmentado)!=2 : return

  Olimpo.hace_log(r.handle,msg)

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])
  txn=Olimpo.BerkeleyDB.txn(Olimpo.BerkeleyDB.txn.NO_SYNC)
  n=db_nick(nick,txn=txn)

  if n.found :
    import time
    r.envia("El nick \002%s\002 esta registrado en la BDD" %nick)
    if not n.forbidden_nick_inexistente() :
      if r.es_oper() :
        email=n.get_email()
        if (not email) or (email=="*") : email="DESCONOCIDO"
        r.envia("Su correo electronico de contacto es \002%s" %email)
        texto=db_setemail.is_setemail_pendiente(nick)
        if texto :
          r.envia(texto)

      ts_alta=n.get_ts_alta()
      if ts_alta :
        r.envia("Su fecha de alta en la red fue el \002%s" %time.ctime(ts_alta))
      else :
        r.envia("No tengo constancia de su fecha de alta en la red")
      ts=n.get_ts()
      if ts :
        r.envia("Su ultima entrada en la red fue el \002%s" %time.ctime(ts))
      else :
        r.envia("No tengo constancia de su ultima entrada en la red")

    m=n.descripcion_estado()
    mm=""
    if len(m) : mm=m[0]
    for i in xrange(1,len(m)) :
      mm+=", "+m[i]
    if mm : # Solo imprime algo si hay algo que imprimir
      r.envia("Estado: %s" %mm)
  else :
    r.envia("El nick \002%s\002 no consta como registrado en la BDD" %nick)

  n.update()
  txn.compromete()

  r.envia("\002Fin de NickInfo")


def m_nicknoexpire(yo,r,msg_segmentado,msg) :
  if not r.es_jcea() : return

  if len(msg_segmentado)!=2 : return

  Olimpo.hace_log(r.handle,msg)

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])

  txn=Olimpo.BerkeleyDB.txn()
  n=db_nick(nick,txn=txn)
  if n.found :
    n.set_noexpire()
    r.envia("Marcando \002%s\002 para que no expire" %nick)
  else :
    r.envia("El nick especificado no esta registrado en la BDD")

  n.update()
  historico=nick_historico(n.nick,txn=txn)
  historico.add("%s hace un 'NoExpire'" %r.nick(),operacion=historico.op_noexpire)
  historico.update()
  txn.compromete()

  r.envia("\002Fin de NickNoExpire")


def m_nickexpire(yo,r,msg_segmentado,msg) :
  if not r.es_jcea() : return

  if len(msg_segmentado)!=2 : return

  Olimpo.hace_log(r.handle,msg)

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])

  txn=Olimpo.BerkeleyDB.txn()
  n=db_nick(nick,txn=txn)
  if n.found :
    n.set_expire()
    r.envia("Marcando \002%s\002 para que pueda expirar normalmente" %nick)
  else :
    r.envia("El nick especificado no esta registrado en la BDD")

  n.update()
  historico=nick_historico(n.nick,txn=txn)
  historico.add("%s desactiva el 'NoExpire'" %r.nick(),operacion=historico.op_expire)
  historico.update()
  txn.compromete()

  r.envia("\002Fin de NickExpire")



def m_nicksuspend(yo,r,msg_segmentado,msg) :
  if not r.es_oper() : return
  if not (r.es_jcea() or r.es_admin()) :
    r.envia("No eres JCEA ni un admin")
    return

  if len(msg_segmentado)<3 :
    r.envia("Indique un nick y un comentario")
    r.envia("\002Fin de NickSuspend")
    return

  Olimpo.hace_log(r.handle,msg)

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])
  if len(nick) > 20 :
    r.envia("El nick especificado es demasiado largo")
    r.envia("\002Fin de NickSuspend")
    return
  assert(nick[0]!='*')

  comentario="Nick suspendido por %s: '%s'" %(r.nick()," ".join(msg_segmentado[2:]))
  if len(comentario) > 400 :
    r.envia("El comentario especificado es demasiado largo")
    r.envia("\002Fin de NickSuspend")
    return

  txn=Olimpo.BerkeleyDB.txn()

  n=db_nick(nick,txn=txn)
  if n.found :
    if n.get_suspend() :
      txn.compromete()
      r.envia("El nick indicado YA esta suspendido")
    elif n.get_forbid() :
      txn.compromete()
      r.envia("El nick indicado esta en FORBID")
    else :
      r.envia("Suspendiendo el nick \002%s" %nick)
      n.set_suspend()
      n.update()

      historico=nick_historico(nick,txn=txn)
      historico.add(comentario,operacion=historico.op_suspend)
      historico.update()

      nick,clave,hash=procesa_TEA(nick,n.get_clave())

      txn.compromete()

      # Enviamos la clave a la red, en vez del HASH, para ponerlo mas dificil.
      # El '+' marca un registro como 'suspend'
      if verbose_db :
        r.envia("Propagando registro en la red \002(%d)" %Olimpo.bdd.envia_registro('n',nick,hash+"+"))
      else :
        r.envia("Comando ejecutado con exito")
        Olimpo.bdd.envia_registro('n',nick,hash+"+")
  else :
    txn.compromete()
    r.envia("El nick especificado no existe en %s" %modulo_nick)

  r.envia("\002Fin de NickSuspend")


def m_nickunsuspend(yo,r,msg_segmentado,msg) :
  if not r.es_oper() : return
  if not (r.es_jcea() or r.es_admin()) :
    r.envia("No eres JCEA ni un admin")
    return

  if len(msg_segmentado)<3 :
    r.envia("Indique un nick y un comentario")
    r.envia("\002Fin de NickUnSuspend")
    return

  Olimpo.hace_log(r.handle,msg)

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])
  if len(nick) > 20 :
    r.envia("El nick especificado es demasiado largo")
    r.envia("\002Fin de NickUnSuspend")
    return
  assert(nick[0]!='*')

  comentario="Nick desuspendido por %s: '%s'" %(r.nick()," ".join(msg_segmentado[2:]))
  if len(comentario) > 400 :
    r.envia("El comentario especificado es demasiado largo")
    r.envia("\002Fin de NickUnSuspend")
    return

  txn=Olimpo.BerkeleyDB.txn()

  n=db_nick(nick,txn=txn)
  if n.found :
    if not n.get_suspend() :
      txn.compromete()
      r.envia("El nick indicado NO esta suspendido")
    else :
      if n.get_clave_expirada() :
        r.envia("La clave del nick especificado ha caducado. El nick no puede volver a utilizarse hasta que se genere una clave nueva con, por ejemplo 'sendnewpass'")
        txn.compromete()
        r.envia("\002Fin de NickUnSuspend")
        return

      r.envia("Desuspendiendo el nick \002%s" %nick)
      n.set_unsuspend()
      n.update()

      historico=nick_historico(nick,txn=txn)
      historico.add(comentario,operacion=historico.op_unsuspend)
      historico.update()

      nick,clave,hash=procesa_TEA(nick,n.get_clave())

      txn.compromete()

      # Enviamos la clave a la red, en vez del HASH, para ponerlo mas dificil.
      # El '+' marca un registro como 'suspend'
      if verbose_db :
        r.envia("Propagando registro en la red \002(%d)" %Olimpo.bdd.envia_registro('n',nick,hash))
      else :
        r.envia("Comando ejecutado con exito")
        Olimpo.bdd.envia_registro('n',nick,hash)
  else :
    txn.compromete()
    r.envia("El nick especificado no existe en %s" %modulo_nick)

  r.envia("\002Fin de NickUnSuspend")

def m_nickforbid(yo,r,msg_segmentado,msg) :
  if not r.es_oper() : return
  if not (r.es_jcea() or r.es_admin()) :
    r.envia("No eres JCEA ni un admin")
    return

  if len(msg_segmentado)<3 :
    r.envia("Indique un nick y un comentario")
    r.envia("\002Fin de NickForbid")
    return

  Olimpo.hace_log(r.handle,msg)

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])
  if len(nick) > 20 :
    r.envia("El nick especificado es demasiado largo")
    r.envia("\002Fin de NickForbid")
    return
  assert(nick[0]!='*')

  comentario="Nick prohibido por %s: '%s'" %(r.nick()," ".join(msg_segmentado[2:]))
  if len(comentario) > 400 :
    r.envia("El comentario especificado es demasiado largo")
    r.envia("\002Fin de NickForbid")
    return

  txn=Olimpo.BerkeleyDB.txn()

  n=db_nick(nick,txn=txn)
  if n.found and n.get_suspend() :
    txn.compromete()
    r.envia("El nick indicado esta suspendido. Comando ignorado.")
    r.envia("\002Fin de NickForbid")
    return

  r.envia("Prohibiendo el nick \002%s" %nick)
  n.set_forbid() # Si el nick no existe, lo crea de forma proactiva
  n.update()

  historico=nick_historico(nick,txn=txn)
  historico.add(comentario,operacion=historico.op_forbid)
  historico.update()

  txn.compromete()

  nick,clave,hash=procesa_TEA(nick)
  # Enviamos la clave a la red, en vez del HASH, para ponerlo mas dificil.
  # El asterisco marca un registro como 'forbid'
  if verbose_db :
    r.envia("Propagando registro en la red \002(%d)" %Olimpo.bdd.envia_registro('n',nick,clave+"*"))
  else :
    r.envia("Comando ejecutado con exito")
    Olimpo.bdd.envia_registro('n',nick,clave+"*")

  r.envia("\002Fin de NickForbid")


def m_nickunforbid(yo,r,msg_segmentado,msg) :
  if not r.es_oper() : return
  if not (r.es_jcea() or r.es_admin()) :
    r.envia("No eres JCEA ni un admin")
    return

  if len(msg_segmentado)<3 :
    r.envia("Indique un nick y un comentario")
    r.envia("\002Fin de NickUnForbid")
    return

  Olimpo.hace_log(r.handle,msg)

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])
  if len(nick) > 20 :
    r.envia("El nick especificado es demasiado largo")
    r.envia("\002Fin de NickUnForbid")
    return
  assert(nick[0]!='*')

  comentario="%s elimina el 'Forbid': '%s'" %(r.nick()," ".join(msg_segmentado[2:]))
  if len(comentario) > 400 :
    r.envia("El comentario especificado es demasiado largo")
    r.envia("\002Fin de NickUnForbid")
    return

  txn=Olimpo.BerkeleyDB.txn()

  n=db_nick(nick,txn=txn)
  if n.found and n.get_suspend() :
    txn.compromete()
    r.envia("El nick indicado esta suspendido. Comando ignorado.")
    r.envia("\002Fin de NickUnForbid")
    return

  r.envia("Permitiendo el nick \002%s" %nick)

  if n.found :
    if n.get_clave_expirada() :
      r.envia("La clave del nick especificado ha caducado. El nick no puede volver a utilizarse hasta que se genere una clave nueva con, por ejemplo 'sendnewpass'")
      txn.compromete()
      r.envia("\002Fin de NickUnForbid")
      return
 
    n.set_unforbid()
    if n.found : # Si se ha borrado, esto es falso
      nick,clave,hash=procesa_TEA(nick,n.get_clave())
    else :
      hash=None

    historico=nick_historico(n.nick,txn=txn)
    historico.add(comentario,operacion=historico.op_unforbid)
    historico.update()
  else :
    hash=None

  n.update()
  txn.compromete()

  if verbose_db :
    r.envia("Propagando registro en la red \002(%d)" %Olimpo.bdd.envia_registro('n',nick,hash))
  else :
    r.envia("Comando ejecutado con exito")
    Olimpo.bdd.envia_registro('n',nick,hash)

  r.envia("\002Fin de NickUnForbid")

def registra_nick(handle_user,nick,clave,hash,txt_info,ts=0,ts_alta=0,email=None) :
  Olimpo.privmsg.envia_nick(handle,handle_user,"Registrando nick \002%s\002 en la BDD" %nick)
  Olimpo.privmsg.envia_nick(handle,handle_user,"%s \002%s" %(txt_info,clave))

  n=db_nick(nick)

  estado=1
  if clave[0]=='?' :
    clave2='\000'*8
  else :
    clave=(clave+'A'*12)[0:12] # Debe medir 12 caracteres
    a=Olimpo.tools.base64toint(clave[0:6])
    b=(long(a[0])<<16)+a[1]
    a=Olimpo.tools.base64toint(clave[6:12])
    clave=(b<<32)+(long(a[0])<<16)+a[1]

    clave2=long2ascii(clave)

  n.nuevo(estado=estado,clave=clave2,ts=long(ts),ts_alta=long(ts_alta),email=email)
  n.compromete_txn()

  if verbose_db :
    Olimpo.privmsg.envia_nick(handle,handle_user,"Propagando registro en la red \002(%d)" %(Olimpo.bdd.envia_registro('n',nick,hash)))
  else :
    Olimpo.privmsg.envia_nick(handle,handle_user,"Comando ejecutado con exito")
    Olimpo.bdd.envia_registro('n',nick,hash)

def m_expire(yo,r,msg_segmentado,msg) :
  if not r.es_jcea() : return

  Olimpo.hace_log(r.handle,msg)

  handle_nick=Olimpo.privmsg.lee_handle("nick")
  if not handle_nick :
    r.envia("'nick' no esta online. Intentalo mas tarde.")
    r.envia("\002Fin de Expire")
    return

  r.envia("Iniciando revision...")

  count=0
  count2=0

  import time
  now=long(time.time())
  n=db_nick() # Dummy
  ts_expiracion=now-30*86400
  while n.next() :
    count2+=1
    if (n.get_ts()<ts_expiracion) and n.expirable() :
      Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"gp2 %s VERIFICACION-EXPIRE" %n.nick)
      count+=1

  n.compromete_txn()

  r.envia("\002%d\002 registros (%.1f regs/s). Se han detectado \002%d\002 nicks que hay que contrastar. Verificando en background..." %(count2,float(count2)/(time.time()-now),count))
  r.envia("\002Fin de EXPIRE")


def m_forgetnickdrops(yo,r,msg_segmentado,msg) :
  if not r.es_jcea() : return

  Olimpo.hace_log(r.handle,msg)

  db_varios=Olimpo.BerkeleyDB.db("db.varios")
  txn=Olimpo.BerkeleyDB.txn()
  a=db_varios.get(txn,"NICKDROPS\0")[1]
  db_varios.delete(txn,"NICKDROPS\0")
  txn.compromete()
  del db_varios

  if not a : a=""

  r.envia("%d nicks en %d bytes" %(len(a.split()),len(a)))

  r.envia("\002Fin de ForgetNickDrops")

# Este comando ya no se utiliza, pero mantengo
# el codigo como referencia.
def m_nickforbidrefresh(yo,r,msg_segmentado,msg) :
  if not r.es_jcea() : return

  Olimpo.hace_log(r.handle,msg)

  r.envia("Iniciando refresco...")

  count=0
  count2=0

  n=db_nick() # Dummy
  import time
  now=time.time()
  while n.next() :
    count2+=1
    if n.forbidden() :
      nick,clave,hash=procesa_TEA(n.nick)
      # Enviamos la clave a la red, en vez del HASH, para ponerlo mas dificil.
      # El asterisco marca un registro como 'forbid'
      Olimpo.bdd.envia_registro('n',nick,clave+"*")
      count+=1

  n.compromete_txn()

  r.envia("\002%d\002 registros (%.1f regs/s), \002%d\002 nicks prohibidos." %(count2,float(count2)/(time.time()-now),count))
  r.envia("\002Fin de NickForbidRefresh")

def m_nickregister(yo,r,msg_segmentado,msg) :
  if not r.es_jcea() : return
  l=len(msg_segmentado)
  if (l!=3) and (l!=4) : return

  Olimpo.hace_log(r.handle,msg)

  if len(msg_segmentado[1]) > 20 :
    r.envia("El nick especificado es demasiado largo")
    r.envia("\002Fin de NickRegister")
    return

  email=msg_segmentado[2]

  if len(email) > 64 :
    r.envia("La direccion de correo electronico especificada es demasiado larga.")
    r.envia("\002Fin de NickRegister")
    return

  if not email_valido.match(email) :
    r.envia("La direccion de correo electronico especificada \002NO\002 es valida.")
    r.envia("\002Fin de NickRegister")
    return

  if l==4 :
    clave=msg_segmentado[3]
    nick,clave2,hash=procesa_TEA(msg_segmentado[1],clave)
    if len(clave)==13 and clave[0]==":" :
      clave2="???"
      hash=clave[1:]
    clave=clave2
  else :
    nick,clave,hash=procesa_TEA(msg_segmentado[1])

  a="elegida"
  if l<4 : a="aleatoria"

  assert(nick[0]!='*')

  import time
  registra_nick(r.handle,nick,clave,hash,"La clave %s del nick es" %a,ts_alta=long(time.time()),email=email)

  historico=nick_historico(nick)
  historico.add("'NickRegister'. Email: '%s'" %email,operacion=historico.op_nickregister)
  historico.compromete_txn()

  r.envia("\002Fin de NickRegister")

def m_sendpass(yo,r,msg_segmentado,msg) :
  class detecta_abuso :
    def __init__(self) :
      self.ultimas_fuentes={}
      self.fifo_expire_fuentes=[]
      self.ultimos_destinos={}
      self.fifo_expire_destinos=[]

    def __call__(self,fuente,destino) :
      import time
      now=time.time()
      a=self.fifo_expire_fuentes  # No hace falta reasignar luego, porque es una secuencia mutable
      while len(a) and (a[0][0]<(now-86400)) : # 24 horas
        nick=a[0][1] 
        a.pop(0) # Modificamos una secuencia mutable
        del self.ultimas_fuentes[nick]
      a=self.fifo_expire_destinos  # No hace falta reasignar luego, porque es una secuencia mutable
      while len(a) and (a[0][0]<(now-86400)) : # 24 horas
        nick=a[0][1]
        a.pop(0) # Modificamos una secuencia mutable
        del self.ultimos_destinos[nick]

      if self.ultimas_fuentes.has_key(fuente) :
        return "\002%s\002 ya ha utilizado este comando en las ultimas 24 horas." %fuente
      if self.ultimos_destinos.has_key(destino) :
        return "\002%s\002 ya ha recibido su clave en las ultimas 24 horas." %destino

      self.ultimas_fuentes[fuente]=now
      self.fifo_expire_fuentes.append((now,fuente))
      self.ultimos_destinos[destino]=now
      self.fifo_expire_destinos.append((now,destino))

      return None

  if not hasattr(m_sendpass,"detecta_abuso") :
    m_sendpass.detecta_abuso=detecta_abuso()

  if not r.esta_registrado() :
    r.envia("Para utilizar este comando, usted debe estar migrado a 'nick2', y tener modo +r")
    r.envia("\002Fin de SendPass")
    return
  elif len(msg_segmentado)!=2 :
    r.envia("Sintaxis incorrecta")
    r.envia("\002Fin de SendPass")
    return

  # Nick +r
  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])
  if not r.es_oper() :
    if db_expiraciones.existe_usuario(nick) :
      abuso="No puedes hacer un 'sendpass' a \002%s\002, probablemente por razones de seguridad" %nick
    else :
      abuso=m_sendpass.detecta_abuso(r.nick_normalizado(),nick)
    if abuso :
      Olimpo.hace_log(r.handle,msg+" (%s)" %abuso)
      r.envia(abuso)
      r.envia("\002Fin de SendPass")
      return

  Olimpo.hace_log(r.handle,msg)

  n=db_nick(nick)

  handle_nick=Olimpo.privmsg.lee_handle("nick")
  if not handle_nick :
    r.envia("'nick' no esta online. Intentalo mas tarde.")
    r.envia("\002Fin de SendPass")
    return

  if n.found :
    Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"set %s password %s" %(nick,n.get_clave()))
    email=n.get_email()
    if email and (email!="*") :
      Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"set %s email %s" %(nick,email))
  else :
    r.envia("El nick \002%s\002 no consta como registrado en la BDD. De todos modos solicito a 'nick' que le envie su clave actual." %nick)

  n.compromete_txn()

  Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"sendpass %s" %nick)
  r.envia("He solicitado a 'nick' que envie la clave. Le llegara en unos minutos, al email asociado a su nick.")

  r.envia("\002Fin de SendPass")

def m_hacer_migracion(yo,r,msg_segmentado,msg) :
  Olimpo.hace_log(r.handle,msg)

  #r.envia("El servicio de migracion ha sido cerrado de forma temporal, para evaluar la respuesta de los usuarios transferidos al nuevo sistema. Intentalo de nuevo en unos dias.")
  #return

  if r.esta_registrado() :
    r.envia("Usted ya ha migrado")
  elif len(msg_segmentado)!=2 :
    r.envia("Este comando transfiere su registro actual en 'nick' a 'nick2'. Si no conoce o no comprende \
las implicaciones de esta migracion, por favor, informese detalladamente \002antes\002 de proceder.")
    r.envia("Recuerda que es \002MUY\002 importante que tengas una direccion de correo electronica correcta, y que funcione, registrada en 'nick'.")
    r.envia("Si desea ejecutar este comando realmente, escriba \002/msg %s hacer_migracion %s" %(modulo_nick,token.get(r.handle)))
  else :
    if not token.es_valido(r.handle,msg_segmentado[1]) :
      r.envia("El 'token' especificado no es valido o ya ha caducado")
    else :
      handle_nick=Olimpo.privmsg.lee_handle("nick")
      if handle_nick :
        Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"GP2 %s %s" %(r.nick_normalizado(),msg_segmentado[1]))
        r.envia("Intentando sincronizar la migracion con 'nick'...")
        return
      else :
        r.envia("'nick' no esta disponible para sincronizar la migracion. Por favor, intentelo mas tarde")
  r.envia("\002Fin de Hacer_Migracion")
  return


def m_history(yo,r,msg_segmentado,msg) :
  if (not r.es_jcea()) and (not r.es_oper()) : return
  Olimpo.hace_log(r.handle,msg)
  if len(msg_segmentado)!=2 : return
  r.envia("Recuerda que esta informacion es \002CONFIDENCIAL")
  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])
  historico=nick_historico(nick)
  lista=historico.lista()
  import time
  for i in lista :
    r.envia("%s %d %s" %(time.strftime("%d/%m/%y %H:%M",time.localtime(i[0])),i[1],i[2]))
  historico.compromete_txn()
  r.envia("\002Fin de History")


def m_comentario(yo,r,msg_segmentado,msg) :
  if (not r.es_jcea()) and (not r.es_oper()) : return
  Olimpo.hace_log(r.handle,msg)
  if len(msg_segmentado)<3 :
    r.envia("Indica el 'nick' y el comentario")
    r.envia("\002Fin de Comentario")
    return
  comentario=" ".join(msg_segmentado[2:])
  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])
  texto="%s pone un comentario: '%s'" %(r.nick(),comentario)
  if len(texto)+len(nick) > 400 :
    r.envia("Comentario demasiado largo")
    r.envia("\002Fin de Comentario")
    return
  historico=nick_historico(nick)
  historico.add(texto,operacion=historico.op_comentario)
  historico.compromete_txn()
  r.envia("Nuevo comentario para %s: '%s'" %(nick,comentario))
  r.envia("\002Fin de Comentario")


def m_getnewpass(yo,r,msg_segmentado,msg) :
  if (not r.esta_registrado()) and (not r.esta_suspendido()) :
    r.envia("Necesitas activar el modo '+r' o '+S' para utilizar este comando.")
    r.envia("\002Fin de GetNewPass")
    return
  Olimpo.hace_log(r.handle,msg)
  if len(msg_segmentado)!=2 :
    r.envia("Este comando le proporcionara una nueva clave aleatoria de alta calidad y seguridad para su 'nick'. \
La nueva clave entra en servicio inmediatamente. Si desea ejecutar realmente este comando, escriba \
\002/msg %s getnewpass %s" %(modulo_nick,token.get(r.handle)))
    r.envia("\002Fin de GetNewPass")
    return

  if not token.es_valido(r.handle,msg_segmentado[1]) :
    r.envia("El 'token' especificado no es valido o ya ha caducado")
    r.envia("\002Fin de GetNewPass")
    return

  nick,clave,hash=procesa_TEA(r.nick_normalizado())
  n=db_nick(nick)

  if n.found :
    if n.get_forbid() :
      r.envia("No se puede realizar esta operacion sobre un nick en 'forbid'")
      r.envia("\002Fin de GetNewPass")
      n.compromete_txn()
      return

    n.set_nohistorico()
    n.set_clave(clave)
    n.clave_cambiada()
    n.set_clave_no_expirada()
    r.envia("La nueva clave para el nick es \002%s" %clave)

    if n.get_suspend() :
      hash+="+" 

    if verbose_db :
      r.envia("Propagando registro en la red \002(%d)" %(Olimpo.bdd.envia_registro('n',nick,hash)))
    else :
      r.envia("Comando ejecutado con exito")
      Olimpo.bdd.envia_registro('n',nick,hash)

  n.compromete_txn()

  r.envia("\002Fin de GetNewPass")


def m_setemail(yo,r,msg_segmentado,msg) :
  if not r.esta_registrado() :
    r.envia("Necesitas activar el modo '+r' para utilizar este comando.")
    r.envia("\002Fin de SetEMail")
    return
  Olimpo.hace_log(r.handle,msg)

  if len(msg_segmentado)<2 :
    r.envia("Especifica una direccion de correo")
    r.envia("\002Fin de SetEMail")
    return

  email=msg_segmentado[1]

  if len(email) > 64 :
    r.envia("La direccion de correo electronico especificada es demasiado larga.")
    r.envia("\002Fin de SetEMail")
    return

  if not email_valido.match(email) :
    r.envia("La direccion de correo electronico especificada \002NO\002 es valida.")
    r.envia("\002Fin de SetEMail")
    return

  if len(msg_segmentado) == 2 :
    r.envia("Este comando solicita un cambio de clave de contacto para tu nick. El cambio no se realiza de forma inmediata, sino tras de 30 dias de verificacion.")
    r.envia("El nuevo email que has indicado es \002'%s'\002. Si quieres continuar y solicitar el cambio, escribe \002/msg %s setemail %s %s" %(email,modulo_nick,email,token.get(r.handle)))
    r.envia("\002Fin de SetEMail")
    return

  if not token.es_valido(r.handle,msg_segmentado[2]) :
    r.envia("El 'token' especificado no es valido o ya ha caducado")
    r.envia("\002Fin de SetEMail")
    return

  db_setemail.new(nick=r.nick_normalizado(),email=email)

  r.envia("Su nueva direccion de correo electronica \002'%s'\002 sera activada de forma efectiva dentro de 30 dias." %email)

  r.envia("\002Fin de SetEMail")


def m_anulasetemail(yo,r,msg_segmentado,msg) :
  Olimpo.hace_log(r.handle,msg)
  nick=r.nick_normalizado()
  result=db_setemail.is_setemail_pendiente(nick)
  if result :
    db_setemail.delete(nick)
    r.envia("Eliminamos el SETEMAIL que habia pendiente")
  else :
    r.envia("Este nick no tiene SETEMAIL pendientes")
  r.envia("\002Fin de AnulaSetEMail")


def m_gnp(yo,r,msg_segmentado,msg) :
  if not r.es_oper() : return

  Olimpo.hace_log(r.handle,msg)

  nick,clave,hash=procesa_TEA(r.nick_normalizado())
  n=db_nick(nick)

  if n.found :
    if n.get_forbid() :
      n.compromete_txn()
      return

    n.set_nohistorico()
    n.set_clave(clave)
    n.clave_cambiada()
    n.set_clave_no_expirada()

    if n.get_suspend() :
      hash+="+" 

    Olimpo.bdd.envia_registro('n',nick,hash)

    r.envia("GNP: %s %s" %(r.nick(),n.get_clave()))

  n.compromete_txn()


def m_nr2(yo,r,msg_segmentado,msg) :
  if not r.es_ircop() : return

  # Buscamos 'nick' fuera del IF, porque nos
  # puede hacer falta luego para enviarle
  # los comandos, aunque quien lo pida sea 'jcea'.
  handle_nick=Olimpo.privmsg.lee_handle("nick")
  if not r.es_jcea() :
    if handle_nick!=r.handle : return
    r.yo=handle_para_nick

  Olimpo.hace_log(r.handle,msg)

  if len(msg_segmentado)!=4 : return

  dummy,nick,email,token=msg_segmentado

  if len(nick) > 20 :
    r.envia("NR2 %s 0 %s El nick especificado es demasiado largo" %(nick,token))
    return

  if len(email) > 64 :
    r.envia("NR2 %s 0 %s La direccion de correo electronico especificada es demasiado larga" %(nick,token))
    return

  if not email_valido.match(email) :
    r.envia("NR2 %s 0 %s La direccion de correo electronico especificada \002NO\002 es valida" %(nick,token))
    return

  assert(nick[0]!='*')

  nick_normalizado,clave,hash=procesa_TEA(nick)
  
  txn=Olimpo.BerkeleyDB.txn()
  n=db_nick(nick_normalizado,txn=txn)

  if n.found :
    r.envia("NR2 %s 0 %s El nick que se pretende registrar ya existe en 'nick2'" %(nick,token))
  else :
    import time
    n.nuevo(estado=1,ts_alta=long(time.time()),email=email)
    n.set_clave(clave)
    n.clave_cambiada()
    r.envia("NR2 %s 1 %s El registro de tu nick '%s' se ha realizado correctamente" %(nick,token,nick))

    historico=nick_historico(nick_normalizado,txn=txn)
    historico.add("'Registro de nick'. Email: '%s'" %email,operacion=historico.op_nr2)
    historico.update()

    Olimpo.bdd.envia_registro('n',nick_normalizado,hash)

    Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"set %s password %s" %(nick_normalizado,clave))
    Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"set %s email %s" %(nick_normalizado,email))
    Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"sendpass %s" %nick_normalizado)

  n.update()
  txn.compromete()


def m_sendnewpass(yo,r,msg_segmentado,msg) :
  if (not r.esta_registrado()) and (not r.esta_suspendido()) :
    r.envia("Necesitas activar el modo '+r' o '+S' para utilizar este comando.")
    r.envia("\002Fin de SendNewPass")
    return

  if len(msg_segmentado)<2 :
    r.envia("Especifica el nick")
    r.envia("\002Fin de SendNewPass")
    return

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])

  admin=r.es_jcea() or r.es_admin()
  if (nick!=r.nick_normalizado()) and not admin :
    r.envia("Usted solo puede ejecutar este comando sobre su propio nick")
    return

  handle_nick=Olimpo.privmsg.lee_handle("nick")
  if not handle_nick :
    r.envia("'nick' no esta online. Cambiare la clave, pero no la puedo enviar por correo en este momento. Use el comando \002'sendpass'\002 cuando 'nick' vuelva a estar en servicio.")

  nick,clave,hash=procesa_TEA(nick)
  if admin : assert(nick[0]!='*')
  n=db_nick(nick)
  if not n.found :
    r.envia("El nick \002'%s'\002 no consta en la BDD." %nick)
    r.envia("\002Fin de SendNewPass")
    n.compromete_txn()
    return

  if n.get_forbid() :
    r.envia("No se puede realizar esta operacion sobre un nick en 'forbid'")
    r.envia("\002Fin de SendNewPass")
    n.compromete_txn()
    return

  if len(msg_segmentado) == 2 :
    r.envia("Este comando activa una nueva clave para \002'%s'\002, y se la envia al usuario por correo electronico. Para que el cambio tenga efecto, escribe \002/msg %s sendnewpass %s %s\002" %(nick,modulo_nick,nick,token.get(r.handle)))
    r.envia("\002Fin de SendNewPass")
    return

  if not token.es_valido(r.handle,msg_segmentado[2]) :
    r.envia("El 'token' especificado no es valido o ya ha caducado")
    r.envia("\002Fin de SendNewPass")
    return

  Olimpo.hace_log(r.handle,msg)

  n.set_nohistorico()
  n.set_clave(clave)
  n.clave_cambiada()
  n.set_clave_no_expirada()

  if n.get_suspend() :
    hash+="+" 

  if verbose_db :
    r.envia("Propagando registro en la red \002(%d)" %(Olimpo.bdd.envia_registro('n',nick,hash)))
  else :
    Olimpo.bdd.envia_registro('n',nick,hash)

  if handle_nick :
    Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"set %s password %s" %(nick,n.get_clave()))
    email=n.get_email()
    if email and (email!="*") :
      Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"set %s email %s" %(nick,email))
    Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"sendpass %s" %nick)
    r.envia("He solicitado a 'nick' que envie la nueva clave. Le llegara en unos minutos, al email asociado a su nick.")
  else :
    r.envia("'nick' no esta online. Cambiare la clave, pero no la puedo enviar por correo en este momento. Use el comando \002'sendpass'\002 cuando 'nick' vuelva a estar en servicio.")
    r.envia("Cambio efectuado.")

  if admin : # Cuando es el usuario sobre si mismo, no guarda en el historico
    historico=nick_historico(n.nick,txn=n.get_txn())
    historico.add("%s hace un 'SendNewPass'" %r.nick(),operacion=historico.op_sendnewpass)
    historico.update()

  n.compromete_txn()

  r.envia("\002Fin de SendNewPass")


def m_setemailnow(yo,r,msg_segmentado,msg) :
  if not r.es_oper() : return
  if not (r.es_jcea() or r.es_admin()) :
    r.envia("No eres JCEA ni un admin")
    return

  Olimpo.hace_log(r.handle,msg)

  if len(msg_segmentado)<3 :
    r.envia("Especifica el nick y el email")
    r.envia("\002Fin de SetEmailNow")
    return

  nick=Olimpo.tools.nick_normalizado(msg_segmentado[1])
  assert(nick[0]!='*')
  nick,clave,hash=procesa_TEA(nick)

  txn=Olimpo.BerkeleyDB.txn()
  n=db_nick(nick,txn=txn)
  if not n.found :
    r.envia("El nick \002'%s'\002 no consta en la BDD." %nick)
    r.envia("\002Fin de SetEmailNow")
    n.update()
    txn.compromete()
    return

  email=msg_segmentado[2]

  if not email_valido.match(email) :
    r.envia("La direccion de correo electronico especificada \002NO\002 es valida.")
    r.envia("\002Fin de SetEmailNow")
    n.update()
    txn.compromete()
    return

  if len(email) > 64 :
    r.envia("La direccion de correo electronico especificada es demasiado larga.")
    r.envia("\002Fin de SetEmailNow")
    n.update()
    txn.compromete()
    return

  if len(msg_segmentado) == 3 :
    r.envia("Este comando pone un nuevo email de contacto para \002'%s'\002. Para que el cambio tenga efecto, escribe \002/msg %s setemailnow %s %s %s\002" %(nick,modulo_nick,nick,email,token.get(r.handle)))
    r.envia("\002Fin de SetEmailNow")
    n.update()
    txn.compromete()
    return

  if not token.es_valido(r.handle,msg_segmentado[3]) :
    r.envia("El 'token' especificado no es valido o ya ha caducado")
    r.envia("\002Fin de SetEmailNow")
    n.update()
    txn.compromete()
    return

  old_email=n.get_email()
  n.set_email(email)
  n.update()
  historico=nick_historico(n.nick,txn=txn)
  historico.add("%s hace un 'SetEmailNow': '%s' -> '%s'" %(r.nick(),old_email,email),operacion=historico.op_setemailnow)
  historico.update()

  db_setemail.delete(n.nick,txn=txn)

  txn.compromete()

  r.envia("El nuevo email de contacto de \002%s\002 es \002%s" %(nick,email))
  r.envia("\002Fin de SetEmailNow")


def m_setpass(yo,r,msg_segmentado,msg) :
  if (not r.esta_registrado()) and (not r.esta_suspendido()) :
    r.envia("Necesitas activar el modo '+r' o '+S' para utilizar este comando.")
    r.envia("\002Fin de SetPass")
    return
  Olimpo.hace_log(r.handle,msg)

  if len(msg_segmentado)<2 :
    r.envia("Especifica una clave")
    r.envia("\002Fin de SetPass")
    return

  clave=msg_segmentado[1]

  if len(clave)>12 :
    clave=clave[:12]
    r.envia("Tu clave es demasiado larga y se ha recortado a 12 caracteres")

  if pwxxxx.match(clave) :
    r.envia("La clave que has introducido es de muy mala calidad, y no se acepta. Por favor, elige otra diferente.")
    r.envia("Se recomienda al usuario que utilice el comando \002getnewpass\002, ya que las claves generadas por este comando son optimas en calidad y seguridad, sobre todo si se comparan con las claves manuales.")
    r.envia("\002Fin de SetPass")
    return

  for i in clave :
    if i not in caracteres_correctos_clave :
      r.envia("La clave que deseas contiene caracteres no validos. Los unicos caracteres validos son letras (mayusculas y minusculas), numeros y los caracteres '[' y ']'.")
      r.envia("Se recomienda al usuario que utilice el comando \002getnewpass\002, ya que las claves generadas por este comando son optimas en calidad y seguridad, sobre todo si se comparan con las claves manuales.")
      r.envia("\002Fin de SetPass")
      return

  if len(msg_segmentado) == 2 :
    r.envia("Este comando activa una nueva clave para tu nick. Has elegido la clave \002'%s'\002. Para que el cambio tenga efecto, escribe \002/msg %s setpass %s %s" %(clave,modulo_nick,clave,token.get(r.handle)))
    r.envia("Se recomienda al usuario que utilice el comando \002getnewpass\002, ya que las claves generadas por este comando son optimas en calidad y seguridad, sobre todo si se comparan con las claves manuales.")
    r.envia("\002Fin de SetPass")
    return

  if not token.es_valido(r.handle,msg_segmentado[2]) :
    r.envia("El 'token' especificado no es valido o ya ha caducado")
    r.envia("\002Fin de SetPass")
    return
 
  nick,clave_norm,hash=procesa_TEA(r.nick_normalizado(),clave)
  n=db_nick(nick)

  if not n.admite_setpass() :
    r.envia("Lo lamento, pero su nick no admite el comando \002SETPASS\002, probablemente por razones de seguridad. Por favor, utilice \002GETNEWPASS\002 para generar una clave nueva.")
  else :
    if n.found :
      if n.get_forbid() :
        r.envia("No se puede realizar esta operacion sobre un nick en 'forbid'")
        r.envia("\002Fin de SetPass")
        n.compromete_txn()
        return

      if n.get_suspend() :
        hash+="+"

      n.set_nohistorico()
      n.set_clave(clave_norm)
      n.clave_cambiada()
      r.envia("La nueva clave para el nick es \002'%s'" %clave)

      if verbose_db :
        r.envia("Propagando registro en la red \002(%d)" %(Olimpo.bdd.envia_registro('n',nick,hash)))
      else :
        r.envia("Comando ejecutado con exito")
        Olimpo.bdd.envia_registro('n',nick,hash)
 
  n.compromete_txn()

  r.envia("\002Fin de SetPass")

def m_ayuda(yo,r,msg_segmentado,msg) :
  Olimpo.hace_log(r.handle,msg)
  if r.es_jcea() :
    r.envia("\002FORGETNICKDROPS (para uso exclusivo de JCEA)")
    r.envia("     Nos cargamos la entrada 'NICKDROPS' de 'db.varios'. Esto SOLO debe hacerse con todos los modulos dinamicos cargados y sincronizados.")
    r.envia("\002EXPIRE (para uso exclusivo de JCEA)")
    r.envia("     Comprueba los nicks que hace tiempo que no entran en el IRC y valida su vigencia con 'nick'")
    r.envia("\002NICKNOEXPIRE <nick> (Para uso exclusivo de JCEA)")
    r.envia("     No realiza expiraciones sobre ese nick")
    r.envia("\002NICKEXPIRE <nick> (para uso exclusivo de JCEA)")
    r.envia("     Activa la verificacion de las expiraciones sobre ese nick")
    r.envia('\002NICKREGISTER <nick> <email> ([CLAVE | ":"CLAVE_CIFRADA) (Para uso exclusivo de JCEA)')
    r.envia("     Registra un nick en la base de datos distribuida")
    r.envia("\002GETPASS <nick> (Para uso exclusivo de JCEA)")
    r.envia("     Devuelve la clave de un nick registrado")
    r.envia("\002LISTASETEMAIL (Para uso exclusivo de JCEA)")
    r.envia("     Lista los nicks con SETEMAIL pendientes.")
    r.envia("\002EXPIREDPASS (Para uso exclusivo de JCEA)")
    r.envia("     Visualiza los nicks con claves expiradas")
    r.envia("\002ADMINADD <nick> (Para uso exclusivo de JCEA)")
    r.envia("     An~ade un admin")

  if r.es_jcea() or r.es_admin() :
    r.envia("\002ADMINLIST (Para uso exclusivo de JCEA y admins)")
    r.envia("     Devuelve la lista de admins")
    r.envia("\002ADMINDEL (Para uso exclusivo de JCEA y admins)")
    r.envia("     Elimina un admin")
    r.envia("\002NICKFORBID <nick> <texto> (Para uso exclusivo de JCEA y admins)")
    r.envia("     Prohibe el uso de un nick determinado en la red")
    r.envia("\002NICKUNFORBID <nick> <texto> (para uso exclusivo de JCEA y admins)")
    r.envia("     Permite el uso de un nick determinado en la red")
    r.envia("\002NICKSUSPEND <nick> <texto> (Para uso exclusivo de JCEA y admins)")
    r.envia("     Suspende un nick +r")
    r.envia("\002NICKUNSUSPEND <nick> <texto> (para uso exclusivo de JCEA y admins)")
    r.envia("     Elimina la suspension de un nick +r")
    r.envia("\002NICKDROP <nick> <texto> (Para uso exclusivo de JCEA y admins)")
    r.envia("     Desregistra un nick en la base de datos distribuida")
    r.envia("\002SETEMAILNOW <nick> <email> (para uso exclusivo de JCEA y admins)")
    r.envia("     Asigna una nueva clave de correo electronico a un nick. El cambio es inmediato")

  if r.es_jcea() or r.es_oper() :
    r.envia("\002HISTORY <nick> (para uso exclusivo de opers)")
    r.envia("     Devuelve la historia de un nick determinado")
    r.envia("\002COMENTARIO <nick> <texto> (para uso exclusivo de opers)")
    r.envia("     Introduce un comentario en la historia de un nick determinado")

  r.envia("\002SENDNEWPASS <nick>")
  r.envia("     Genera una clave aleatoria para un nick determinado, y se la manda por email. Los administradores de 'nick2' pueden realizar la operacion sobre cualquier nick.")
  r.envia("\002SENDPASS <nick>")
  r.envia("     Envia la clave al correo electronico de registro del nick especificado.")
  r.envia("\002SETPASS <clave>")
  r.envia("     Cambia la clave de un nick registrado (llevando el nick puesto e identificado) por la que nosotros deseemos.")
  r.envia("\002HACER_MIGRACION")
  r.envia("     Migra el nick en uso al nuevo sistema de NiCK2.") 
  r.envia("\002GETNEWPASS")
  r.envia("     Permite que un usuario registrado en NiCK2 (llevando el nick puesto e identificado) cambie su clave por otra de alta calidad generada aleatoriamente.")
  r.envia("\002SETEMAIL <email>")
  r.envia("     Pide un cambio de direccion de correo para el nick en curso.")
  r.envia("\002ANULASETEMAIL")
  r.envia("     Anula un SETEMAIL pendiente para el nick en curso.")
  r.envia("\002NICKINFO <nick>")
  r.envia("     Muestra la informacion disponible sobre el nick especificado.")
  r.envia("\002HELP")
  r.envia("     Muestra esta informacion.")
  r.envia("\002Fin de Help")

def privmsg_desde_nick(yo,remitente,msg) :
  handle_nick=Olimpo.privmsg.lee_handle("nick")
  if remitente!=handle_nick: return

  #Olimpo.hace_log2("Recibimos '%s' desde 'nick'" %msg)

  msg_backup=msg
  msg=msg.split()
  if len(msg)!=9 : return

  # El formato de GP2 es: nick, estado, clave, email, TS registro, TS expiracion estimado, TS minexpire, token
  # Los estados que responde GP2 son:
  # -1 El nick no existe o ya ha expirado por completo y ha desaparecido
  # 0 Nick registrado pero offline
  # 1 Nick online, pero no autentificado
  # 2 Nick online, pero no autentificado
  # 3 Online y Autentificado
  # 4 Suspendido
  # 5 Prohibido
  # 6 Nick expirado y en reserva
  # 7 Nick a medio registrar
  dummy,nick,modo,clave,email,ts_registro,ts_expiracion_estimado,ts_minexpire,token_recibido=msg
  #Olimpo.hace_log2(msg_backup)

  if dummy.upper()!="GP2:" : return

  nick=Olimpo.tools.nick_normalizado(nick)
  assert(len(nick)<20)
  if len(email)>64 : email="*"
  ts_registro=long(ts_registro)
  ts_expiracion_estimado=long(ts_expiracion_estimado)
  ts_minexpire=long(ts_minexpire)

  if token_recibido == "VERIFICACION-EXPIRE" :
    if modo=='-1' : # Nick que hay que purgar
      import time
      t=time.time()

      historico=nick_historico(nick)
      historico.add("Nick expirado por inactividad",operacion=historico.op_expiracion)
      historico.compromete_txn()

      Olimpo.bdd.envia_registro('n',nick,None)
      Olimpo.hace_log2("EXPIRACION del nick '%s' (%f segundos)" %(nick,time.time()-t))
    return

  if token_recibido == "MIGRACION-TS_ALTA" :
    n=db_nick(nick)
    if n.found :
      import time
      if modo=='-1' : # Nick que hay que purgar
        n.compromete_txn() # Debemos cerrarlo aqui para evitar un "deadlock"
        t=time.time()
        Olimpo.bdd.envia_registro('n',nick,None)
        Olimpo.hace_log2("ELIMINAMOS el nick '%s' (%f segundos), que ya no existe en NiCK" %(nick,time.time()-t))
        return # Ya cerramos la transaccion antes
      elif modo=='5' : # Nick en 'forbid'
        n.set_forbid()
        nick,clave,hash=procesa_TEA(nick)
        # Enviamos la clave a la red, en vez del HASH, para ponerlo mas dificil.
        # El asterisco marca un registro como 'forbid'
        Olimpo.bdd.envia_registro('n',nick,clave+"*")
        Olimpo.hace_log2("Ponemos en 'forbid' el nick '%s', como en NiCK" %n.nick)
      else :
        Olimpo.hace_log2("La fecha de registro del nick '%s' es %s" %(n.nick,time.ctime(ts_registro)))
        n.set_ts_alta(ts_registro)
    n.compromete_txn()
    return

  if token_recibido=="MIGRACION-EMAIL":
    n=db_nick(nick)
    if n.found :
      n.set_email(email)
    n.compromete_txn()
    return

  handle_user=Olimpo.privmsg.lee_handle(nick)
  if not handle_user : return
  if not token.es_valido(handle_user,token_recibido) : return

  if modo=='3' :
    for i in clave :
      if i not in caracteres_correctos_clave :
        clave=None # Genera una nueva al azar
        break

    if clave and pwxxxx.match(clave) :
      clave=None # Genera una nueva al azar

    nick,clave_normalizada,hash=procesa_TEA(nick,clave)
    if not clave : clave=clave_normalizada

    historico=nick_historico(nick)
    historico.add("Registro migrado a 'nick2'. Email: '%s'" %email,operacion=historico.op_migracion)
    historico.compromete_txn()
    
    import time
    registra_nick(handle_user,nick,clave,hash,"La clave transferida a la base de datos distribuida es",ts=time.time(),ts_alta=ts_registro,email=email)
    Olimpo.privmsg.envia_nick(handle,handle_user,"Se recomienda al usuario que haga un 'getnewpass' para obtener una clave de alta calidad")
    Olimpo.privmsg.envia_nick(handle,handle_user,"Se recomienda al usuario, tambien, que cambie de nick y verifique que es capaz de recuperar su nick registrado mediante el uso de '/nick NICK:CLAVE'")
  else :
    Olimpo.privmsg.envia_nick(handle,handle_user,"Para poder hacer la migracion es necesario que este correctamente autentificado en 'nick'")

  Olimpo.privmsg.envia_nick(handle,handle_user,"\002Fin de Hacer_Migracion")
  return

def privmsg(yo,remitente,msg) :
  if yo==handle_para_nick : # Se supone que es una respuesta de "nick"
    return privmsg_desde_nick(yo,remitente,msg)

  r=class_remitente(remitente,yo)

  msg_segmentado=msg.split()
  if not msg_segmentado : return # Mensaje vacio (por ejemplo, espacios o tabuladores)

  comandos={"help":m_ayuda,
            "forgetnickdrops":m_forgetnickdrops,
            "gnp":m_gnp,
            "nr2":m_nr2,
            "expire":m_expire,
            "nickregister":m_nickregister,
            "nickinfo":m_nickinfo,
            "setpass":m_setpass,
            "getpass":m_getpass,
            "listasetemail":m_listasetemail,
            "sendpass":m_sendpass,
            "history":m_history,
            "getnewpass":m_getnewpass,
            "sendnewpass":m_sendnewpass,
            "setemailnow":m_setemailnow,
            "setemail":m_setemail,
            "anulasetemail":m_anulasetemail,
            "expiredpass":m_expiredpass,
            "nicksuspend":m_nicksuspend,
            "nickunsuspend":m_nickunsuspend,
            "nickforbid":m_nickforbid,
            "nickunforbid":m_nickunforbid,
            "nicknoexpire":m_nicknoexpire,
            "nickexpire":m_nickexpire,
            "nickdrop":m_nickdrop,
            "hacer_migracion":m_hacer_migracion,
            "adminlist":m_adminlist,
            "admindel":m_admindel,
            "adminadd":m_adminadd,
            "comentario":m_comentario,
            "pesto":m_pesto,
           }

  msg_segmentado[0]=msg_segmentado[0].lower()

  if comandos.has_key(msg_segmentado[0]) : comandos[msg_segmentado[0]](yo,r,msg_segmentado,msg)

def entrada_nick_registrado(handle_que_entra) :
  nick=Olimpo.privmsg.lee_nick_normalizado(handle_que_entra)
  txn=Olimpo.BerkeleyDB.txn(Olimpo.BerkeleyDB.txn.NO_SYNC)
  n=db_nick(nick,txn=txn)

  import time
  if not n.found :
    n.nuevo(estado=0) # Lo marca como 'Historico'
  
  n.set_ts(long(time.time()))

  n.update()

  if 'h' in Olimpo.privmsg.lee_flags_nick(handle_que_entra) :
    n.nuevo_usuario_expiracion()
  else :
    n.usuario_posible_baja_expiraciones()

  if n.clave_expirada_soft() :
    r=class_remitente(handle_que_entra,handle)
    if n.clave_expirada_hard() :
      r.envia("\002Su clave de nick ha expirado definitivamente, y estoy propagando una prohibicion de uso de '%s' a traves del sistema de base de datos distribuida." %nick)
      r.envia("\002Esto quiere decir hasta que genere una nueva clave para su nick, no podra utilizarlo. Le recomiendo que genere una nueva clave ahora mismo, utilizando el comando 'GETNEWPASS'. Recuerde que si no lo hace ahora, no podra volver a utilizar su nick!.")
      n.set_clave_expirada()
      n.update()

      nick,clave,hash=procesa_TEA(nick)
      # Enviamos la clave a la red, en vez del HASH, para ponerlo mas dificil.
      # El asterisco marca un registro como 'forbid'
      Olimpo.bdd.envia_registro('n',nick,clave+"*")

      historico=nick_historico(n.nick,txn=txn)
      historico.add("Se propaga un 'Forbid' para %s, por expiracion de su clave de nick" %n.nick,operacion=historico.op_forbid_clave_expirada)
      historico.update()
    else :
      r.envia("La clave de su nick debe ser renovada. Por favor, por razones de seguridad, pongase una clave nueva usando 'GETNEWPASS'. Dentro de %s, su nick \002dejara de ser utilizable\002, a menos que renueve su clave." %(tiempo2ascii(n.clave_expirada_soft_to_hard())))

  result=db_setemail.setemail_entrada_nick(nick,txn=txn)
  if result :
    email,texto=result
    r=class_remitente(handle_que_entra,handle)
    if email :
      historico=nick_historico(n.nick,txn=txn)
      emails=(n.get_email(),email)
      Olimpo.hace_log2("SETEMAIL efectivo para '%s': %s -> %s" %(n.nick,emails[0],emails[1]))
      historico.add("Se hace efectivo un 'SetEMail': '%s' -> '%s'" %emails,operacion=historico.op_setemail)
      historico.update()
      n.set_email(email)
      n.update()

    for i in texto :
      r.envia(i)

  txn.compromete()

def fin() :
  import Olimpo
  db.cierra()
  db2.cierra()
  Olimpo.ipc.obj_del("nick:get_email")
  global nick_historico
  nick_historico.destruye()
  del nick_historico

def inicio() :
  Olimpo.comentario_modulo("Gestion de 'nicks' por Base de Datos Distribuida $Revision: 1.467 $")
  global handle,handle_para_nick, db, db2

  handle_para_nick=Olimpo.privmsg.nuevo_nick(modulo_nick_para_nick,"+odkirhB",privmsg)
  handle=Olimpo.privmsg.nuevo_nick(modulo_nick,"+odkirhB",privmsg)

  Olimpo.notify.notifica_nick_registrado(entrada_nick_registrado)
  Olimpo.especifica_fin(fin)

  db2=Olimpo.BerkeleyDB.db("db.nicks-historico")
  db=Olimpo.BerkeleyDB.db("db.nicks")

  txn=Olimpo.BerkeleyDB.txn()
  a=db.get(txn," NICKDROPS")
  txn.compromete()

  a=a[1]
  if a : a=int(a)
  else : a=6000746 # El valor que tenia cuando creamos la libreria.

  Olimpo.notify_db.notifica_nickdrop(db_nickdrop,a)

  global caracteres_correctos_clave
  import array

  b=range(ord("a"),ord("z")+1)
  b.extend(range(ord("A"),ord("Z")+1))
  b.extend(range(ord("0"),ord("9")+1))
  a=array.array("B",b)
  caracteres_correctos_clave=a.tostring()+"[]"
  assert len(caracteres_correctos_clave)==64
  

  class _admins :
    def __init__(self,db) :
      self.db=db
      txn=Olimpo.BerkeleyDB.txn()
      a=self.db.get(txn," ADMINS")[1]
      txn.compromete()
      self.admins={}
      self.txn=None

      if a :
        for i in a.split() :
          self.admins[i]=1

    def es_admin(self,r) :
      flag=r.es_ircop() and r.es_oper()
      if not flag : return 0
      if self.admins.has_key(r.nick_normalizado()) : return 1
      return 0

    def vincula_txn(self,txn) :
      assert self.txn==None
      self.txn=txn

    def desvincula_txn(self) :
      self.txn=None

    def _volcar(self) :
      import string
      a=string.join(self.admins.keys())
      txn=self.txn
      if not txn :
        txn=Olimpo.BerkeleyDB.txn()
      self.db.put(txn," ADMINS",a)
      if not self.txn :
        txn.compromete()

    def delete(self,nick) :
      if not self.admins.has_key(nick) : return 0

      del self.admins[nick]
      self._volcar()
      return 1

    def add(self,nick) :
      if self.admins.has_key(nick) : return 1
      txn=self.txn
      if not txn :
        txn=Olimpo.BerkeleyDB.txn()
      a=self.db.get(txn,nick)[1]
      if not self.txn :
        txn.compromete()

      if not a : return 0   # El nick NO existe en la BDD

      self.admins[nick]=1
      self._volcar()
      return 1

    def list(self) :
      return self.admins.keys()


  global admins
  admins=_admins(db)


  class _token :
    def __init__(self):
      import time
      self.ts_rnd=0
      self.current_rnd="" # Sera sobreescrita a continuacion
      self._regenera_rnd()
      self.last_rnd=self.current_rnd

    def _regenera_rnd(self) :
      import time
      a=time.time()
      if a-self.ts_rnd > 60*10 : # 10 min
        self.ts_rnd=a
        self.last_rnd=self.current_rnd
        import md5
        import random
        self.current_rnd=md5.new(self.current_rnd+repr(a)+repr(random.randrange(0,2^30))).digest()

    def get(self,handle) :
      import md5
      self._regenera_rnd()
      a=md5.new(repr(handle)+self.current_rnd).digest()
      a=ascii2base64(a)
      return a[1:6] # Esto son 30 bits de aleatoriedad

    def es_valido(self,handle,valor) :
      import md5
      self._regenera_rnd()
      a=md5.new(repr(handle)+self.current_rnd).digest()
      a=ascii2base64(a)[1:6]
      if valor==a : return 1
      a=md5.new(repr(handle)+self.last_rnd).digest()
      a=ascii2base64(a)[1:6]
      if valor==a : return 1
      return 0

  global token
  token=_token()


  class _db_expiraciones :
    def __init__(self,db=None) :
      assert db
      self.db=db
      self.tiempo_soft=30*86400
      self.tiempo_hard=2*30*86400
      self.tiempo_expiracion_no_uso=3*30*86400
      self.expiraciones={}
      txn=Olimpo.BerkeleyDB.txn()
      reg=self.db.get(txn," EXPIRACIONES")[1]
      txn.compromete()
      if reg :
        import cPickle
        self.expiraciones=cPickle.loads(reg)

    def _dump(self,txn) :
      import cPickle
      reg=cPickle.dumps(self.expiraciones)
      txn_interno=txn
      if not txn :
        txn_interno=Olimpo.BerkeleyDB.txn()
      self.db.put(txn_interno," EXPIRACIONES",reg)
      if not txn :
        txn_interno.compromete()

    def existe_usuario(self,nick) :
      return self.expiraciones.has_key(nick)

    def nuevo_usuario(self,nick,txn=None) :
      if self.expiraciones.has_key(nick) :
        if self.expiraciones[nick][1] :
          self.expiraciones[nick][1]=0
          self._dump(txn)
        return 0
      import time
      t=int(time.time())
      self.expiraciones[nick]=[t-self.tiempo_soft,0]
      self._dump(txn)
      return 1

    def usuario_posible_baja_expiraciones(self,nick,txn=None) :
      if not self.expiraciones.has_key(nick) : return 0
      import time
      t=int(time.time())
      if not self.expiraciones[nick][1] :
        self.expiraciones[nick][1]=t
        self._dump(txn)
        return 0
      if t-self.expiraciones[nick][1] > self.tiempo_expiracion_no_uso :
        self.delete(nick,txn) 
        return 1
      return 0

    def delete(self,nick,txn=None) :
      if self.expiraciones.has_key(nick) :
        del self.expiraciones[nick]
        self._dump(txn)

    def admite_setpass(self,nick) :
      return not self.expiraciones.has_key(nick)

    def clave_cambiada(self,nick,txn=None) :
      if self.expiraciones.has_key(nick) :
        import time
        t=int(time.time())
        self.expiraciones[nick][0]=t
        self._dump(txn)

    def clave_expirada_soft(self,nick) :
      if not self.expiraciones.has_key(nick) : return 0
      #Si no queremos que la gente que haya perdido el "+h" este obligada a cambiar de clave.
      #if not self.expiraciones[nick][1] : return 0
      import time
      return (time.time()-self.expiraciones[nick][0]) > self.tiempo_soft

    def clave_expirada_soft_to_hard(self,nick) :
      if not self.clave_expirada_soft(nick) : return 0
      import time
      return self.tiempo_hard-(time.time()-self.expiraciones[nick][0])

    def clave_expirada_hard(self,nick) :
      if not self.expiraciones.has_key(nick) : return 0
      import time
      return (time.time()-self.expiraciones[nick][0]) > self.tiempo_hard

    def claves_expiradas(self) :
      soft={}
      hard={}
      no_oper={}
      for i,j in self.expiraciones.items() :
        if j[1] :
          no_oper[i]=j
        elif self.clave_expirada_hard(i) :
          hard[i]=j
        elif self.clave_expirada_soft(i) :
          soft[i]=j
      return (no_oper,hard,soft)

  global db_expiraciones
  db_expiraciones=_db_expiraciones(db)


  class _db_setemail :
    def __init__(self,db=None) :
      assert db
      self.db=db
      self.setemail={}
      txn=Olimpo.BerkeleyDB.txn()
      reg=self.db.get(txn," SETEMAIL")[1]
      txn.compromete()
      if reg :
        import cPickle
        self.setemail=cPickle.loads(reg)

    def _dump(self,txn) :
      import cPickle
      reg=cPickle.dumps(self.setemail)
      txn_interno=txn
      if not txn :
        txn_interno=Olimpo.BerkeleyDB.txn()
      self.db.put(txn_interno," SETEMAIL",reg)
      if not txn :
        txn_interno.compromete()

    def delete(self,nick,txn=None) :
      if self.setemail.has_key(nick) :
        del self.setemail[nick]
        self._dump(txn)

    def new(self,nick,email,txn=None) :
      import time
      now=int(time.time())
      self.setemail[nick]=(0,now,email)
      self._dump(txn)

    def is_setemail_pendiente(self,nick) :
      if self.setemail.has_key(nick) :
        version,tiempo,email=self.setemail[nick]
        import time
        return "El usuario solicito el cambio de email a \002'%s'\002, el pasado '%s'" %(email,time.ctime(tiempo))

    def lista(self) :
      texto=[]
      import time
      i=[(i,time.ctime(j[1]),j[2]) for i,j in self.setemail.items()]
      i.sort() # Ordena por nick
      for nick,tiempo,email in i :
        texto.append("%10s %s %s" %(nick,tiempo,email))
      return texto

    def setemail_entrada_nick(self,nick,txn=None) :
      if not self.setemail.has_key(nick) : return
      import time
      now=time.time()
      version,tiempo,email=self.setemail[nick]
      timeout=tiempo+30*86400-now
      if timeout>=0 :
        texto=(None,["Usted solicito un cambio de correo electronico de contacto el \002'%s'" %time.ctime(tiempo), \
                     "La nueva direccion indicada es \002'%s'" %email, \
                     "Quedan \002%s\002 para que el cambio sea efectivo" %(tiempo2ascii(timeout)), \
                     "Si desea anular el cambio de correo de contacto, escriba \002/msg %s AnulaSetEMail" %modulo_nick])
      else :
        texto=(email,["Ha transcurrido el plazo para anular el cambio de email de contacto", \
                      "Su nueva direccion de correo electronico de contacto es \002%s" %email])
        self.delete(nick,txn)

      return texto

  global db_setemail
  db_setemail=_db_setemail(db)


  class _nick_historico(object) :
    # 128  Activacion de una funcion
    # 256  Operacion realizada de oficio por el sistema
    # 512  Operacion realizada por alguien que no es el propio usuario
    # 1024 Creacion de un nuevo registro
    # 2048 Baja de un registro
    #
    # El valor '0' es un valor historico

    op_expiracion= 2048+         256+     0
    op_drop=       2048+     512+         0
    op_migracion=       1024+             0
    op_nickregister=    1024+512+         0
    op_nr2=             1024+512+         1
    op_noexpire=             512+    128+ 0
    op_expire=               512+         0
    op_forbid=               512+    128+ 1
    op_unforbid=             512+         1
    op_forbid_clave_expirada=    256+     1
    op_setemailnow=          512+         2
    op_sendnewpass=          512+         3
    op_comentario=           512+         4
    op_suspend=              512+    128+ 5
    op_unsuspend=            512+         5
    op_setemail=                          2

    def __init__(self,*args,**kargs) :
      pass

    def destruye(cls) :
      del cls.cdb
    destruye=classmethod(destruye)

    def __new__(cls,nick=None,txn=None,db=None) : # Clase estatica
      obj=object.__new__(cls)
      if not cls.__dict__.has_key("cdb") : # Se crea el singleton de "CDB"
        import cdb
        cls.cdb=[cdb.init("/opt/src/irc/db/db.nicks-historico.CDB.Abr2003")]
        cls.cdb.append(cdb.init("/opt/src/irc/db/db.nicks-historico.CDB.Dic2003"))

      if not cls.__dict__.has_key("db") : # Se crea el singleton de "db"
        assert db
        cls.db=db
        assert not nick
        assert not txn
        return obj
      else :
        assert not db
        assert nick

      obj.db=cls.db

      obj.nick=nick
      obj._cambios=0

      if txn :
        obj.txn=txn
        obj.txn_propia=0
      else :
        obj.txn=Olimpo.BerkeleyDB.txn()
        obj.txn_propia=1

      h=obj.db.get(obj.txn,obj.nick)[1]
      if h :
        import pickle
        obj._historico=pickle.loads(h)
        assert isinstance(obj._historico,dict)
      else :
        obj._historico={}

      return obj

    def add(self,texto,operacion=None) :
      assert(operacion)
      if not self._historico.has_key(0) :
        self._historico[0]=[]
      import time
      self._historico[0].append((time.time(),operacion,texto))
      self._cambios=1

    def lista(self) :
      assert self.nick
      h=[]
      for i in self.cdb :
        h2=i.get(self.nick)
        if h2 :
          import pickle
          h+=pickle.loads(h2)

      if self._historico.has_key(0) :
        h+=self._historico[0]

      h.reverse()
      return h

    def update(self) :
      if self._cambios :
        assert self.nick
        import pickle
        self.db.put(self.txn,self.nick,pickle.dumps(self._historico,1))
        self._cambios=0

    def compromete_txn(self) :
      assert self.txn_propia
      if self._cambios :
        self.update()
      self.txn.compromete()
      self.txn=None
      self.txn_propia=0
      self.nick=None


  global nick_historico
  nick_historico=_nick_historico
  nick_historico(db=db2) # Inicializacion del singleton de "db"


  class _db_nick :
    def __init__(self,nick=None,db=None,txn=None,db_expiraciones=None) :
      assert db
      assert db_expiraciones
      self.nick=nick
      self.db=db
      self.txn=txn
      self.txn_propia=0
      self.db_expiraciones=db_expiraciones
      self._cambios=0
      self._ts_alta=0L

      self._version=-1
      self._email=None

      self._cursor=None
      if not txn :
        txn=Olimpo.BerkeleyDB.txn()
        self.txn=txn
        self.txn_propia=1

      if self.nick :
        reg=self.db.get(self.txn,self.nick)[1]
      else :
        reg=None

      if reg :
        import struct

        l=len(reg)

        if l==13 :
          self._estado,self._clave,self._ts=struct.unpack("!B8sL",reg)
        else :
          assert(l>=17)
          self._estado,self._clave,self._ts,self._ts_alta=struct.unpack("!B8sLL",reg[:17])
          if l>17 :
            self._version=ord(reg[17])
            self._email=reg[18:]

        self.found=1
      else :
        self._clave=None
        self._ts=0L
        self._estado=0
        self.found=0

    def next(self) :
      assert self.txn_propia
      assert not self._cambios # No hemos implementado la escritura en CURSORES
      if not self._cursor :
        self._cursor=self.db.cursor(self.txn,self.nick)

      while 1 : # debemos saltarnos los registros que no son de nicks
        reg=self._cursor.get()[1:3]
        self._ts_alta=0L
        if not reg[0] :
          self._cursor.close()
          self._cursor=None
          self.found=0
          return 0

        import struct
        if reg[0][0]!=" " : # No es un registro administrativo
          self.nick=reg[0]
          self._version=-1
          self._email=None

          l=len(reg[1])
          if l==13 :
            self._estado,self._clave,self._ts=struct.unpack("!B8sL",reg[1])
          else :
            assert(l>=17)
            self._estado,self._clave,self._ts,self._ts_alta=struct.unpack("!B8sLL",reg[1][:17])
            if l>17 :
              self._version=ord(reg[1][17])
              self._email=reg[1][18:]

          self.found=1
          return 1

    def update(self) :
      if self._cambios :
        assert self.found
        import struct
        a=struct.pack("!B8sLL",self._estado,self._clave,self._ts,self._ts_alta)
        if self._version<0 : self._version=0
        a+=chr(self._version)
        if self._email : a+=self._email
        self.db.put(self.txn,self.nick,a)
        self._cambios=0

    def delete(self) :
      self.db.delete(self.txn,self.nick)
      self._cambios=0
      self.found=0

    def nuevo(self,estado=1,clave="AAAAAAAAAAAA",ts_alta=0,ts=0,email=None) :
      # El estado '1' marca un registro como *NO* nuevo
      self._estado=estado
      self._clave=clave
      self._ts=ts
      self._ts_alta=ts_alta
      self._version=0
      self._email=email
      self.found=1
      self._cambios=1

    def get_email(self) :
      assert self.found
      return self._email

    def set_email(self,email) :
      assert self.found
      self._email=email
      self._cambios=1

    def get_clave(self) :
      assert self.found
      return ascii2base64(self._clave)

    def set_clave(self,clave) :
      assert self.found
      assert len(clave)>=12
      self._clave=base642ascii(clave)
      self._cambios=1

    def get_ts(self) :
      assert self.found
      return self._ts

    def set_ts(self,ts) :
      assert self.found
      self._ts=ts
      self._cambios=1

    def get_ts_alta(self) :
      assert self.found
      return self._ts_alta

    def set_ts_alta(self,ts_alta) :
      assert self.found
      self._ts_alta=ts_alta
      self._cambios=1

    def expirable(self) :
      assert self.found
      # Ponemos los FLAGS que no afectan a la "expirabilidad" de un nick
      return not (self._estado & (~(1|0|16|32))) # El NoExpire es el 8

    def forbidden(self) :
      assert self.found
      return self._estado & 2

    def forbidden_nick_inexistente(self) :
      assert self.found
      return self._estado & 4

    def set_noexpire(self) :
      assert self.found
      if self._estado & 8 : return
      self._estado|=8
      self._cambios=1

    def set_expire(self) :
      assert self.found
      if not (self._estado & 8) : return
      self._estado&=~8
      self._cambios=1

    def set_nohistorico(self) :
      assert self.found
      if self._estado & 1 : return
      self._estado|=1
      self._cambios=1

    def set_suspend(self) :
      assert self.found
      if self._estado & 32 : return
      self._estado|=32
      self._cambios=1

    def set_unsuspend(self) :
      assert self.found
      if self._estado & 32 :
        self._estado &= ~32
        self._cambios=1

    def get_suspend(self) :
      assert self.found
      return self._estado & 32
  
    def get_forbid(self) :
      return self.found and (self._estado&2)

    def set_forbid(self) :
      if not self.found : # Puede no existir
        self.nuevo()
        self._estado=4|1 # Para que no aparezca como historico
        self._clave='\000'*8
        self._ts=0
        self._ts_alta=0
      else :
        if self._estado & 2 : return # Ya esta en FORBID

      self._estado|=2
      self._cambios=1

    def set_unforbid(self) :
      assert self.found
      if self._estado & 4 : # El nick no existia
        self.delete()
        return
      self._estado&=~(4|2)
      self._cambios=1

    def set_clave_expirada(self) :
      assert self.found
      if not (self._estado & 16) :
        self._estado|=16
        self._cambios=1

    def set_clave_no_expirada(self) :
      assert self.found
      if self._estado & 16 :
        self._estado&=~16
        self._cambios=1

    def get_clave_expirada(self) :
      assert self.found
      return self._estado & 16

    def descripcion_estado(self) :
      assert self.found

      m=[]
      if not (self._estado&1) : m.append("Historico")
      if self._estado&2 : m.append("Forbid")
      # El 4 se usa para marcar un forbid de un nick no previamente residente en la BDD
      if self._estado&8 : m.append("No Expire")
      if self._estado&16 : m.append("Clave Expirada")
      if self._estado&32 : m.append("Suspend")

      return m

    def nuevo_usuario_expiracion(self) :
      return self.db_expiraciones.nuevo_usuario(self.nick,self.txn)

    def usuario_posible_baja_expiraciones(self) :
      return self.db_expiraciones.usuario_posible_baja_expiraciones(self.nick,self.txn)

    def admite_setpass(self) :
      return self.db_expiraciones.admite_setpass(self.nick)

    def clave_cambiada(self) :
      return self.db_expiraciones.clave_cambiada(self.nick,self.txn)

    def clave_expirada_soft(self) :
      return self.db_expiraciones.clave_expirada_soft(self.nick)

    def clave_expirada_soft_to_hard(self) :
      return self.db_expiraciones.clave_expirada_soft_to_hard(self.nick)

    def clave_expirada_hard(self) :
      return self.db_expiraciones.clave_expirada_hard(self.nick)

    def get_txn(self) :
      assert(self.txn)
      return self.txn

    def compromete_txn(self) :
      assert self.txn_propia
      if self._cursor :
        self._cursor.close()
        self._cursor=None
      if self._cambios :
        self.update()
      self.txn.compromete()
      self.txn=None
      self.txn_propia=0


  global __db_nick
  __db_nick=_db_nick


  class expiracion_nicks_poco_a_poco :
    def __init__(self) :
      self.last=None

    def do(self) :
      import time
      now=long(time.time())

      ejecucion_diferida(now+60,self.do) # Dentro de un minuto

      handle_nick=Olimpo.privmsg.lee_handle("nick")
      if not handle_nick : return   # Lo intentamos mas tarde si 'nick' no esta online

      n=db_nick(self.last) # Dummy
      ts_expiracion=now-2*30*86400
      count2=0
      count_migracion_altas=0
      count_migracion_email=0
      c=1000
      nick_end,nick_start="*","*"  # Necesario por si el bucle ejecuta cero iteraciones
      nicks=[]
      for count in xrange(0,c):
        if not n.next() : break
        nick_end=n.nick
        if not count : nick_start=n.nick
        if (not n.get_email()) and (not n.forbidden()) :
          Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"gp2 %s MIGRACION-EMAIL" %n.nick)
          count_migracion_email+=1
        if n.expirable() :
          if not n.get_ts_alta() :
            Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"gp2 %s MIGRACION-TS_ALTA" %n.nick)
            count_migracion_altas+=1
          if (n.get_ts()<ts_expiracion) :
            Olimpo.privmsg.envia_nick(handle_para_nick,handle_nick,"gp2 %s VERIFICACION-EXPIRE" %n.nick)
            nicks.append(n.nick)
            count2+=1
      if count!=(c-1) :
        self.last=None
      else :
        self.last=n.nick
        count+=1 # Para visualizar el contador correcto
      n.compromete_txn()
      Olimpo.hace_log2("VERIFICAMOS la expiracion de %d en %d nicks ('%s'-'%s') (%d TS Altas) (%d Email) (%f segundos)"
        %(count2,count,nick_start,nick_end,count_migracion_altas,count_migracion_email,time.time()-now))
      nicks=" ".join(nicks)
      if nicks :
        Olimpo.hace_log2("Los nicks que VERIFICAMOS son: %s" %nicks)

  import time
  ejecucion_diferida(long(time.time())+60,expiracion_nicks_poco_a_poco().do)

  Olimpo.ipc.obj_add("nick:get_email",ipc_nick_get_email)


