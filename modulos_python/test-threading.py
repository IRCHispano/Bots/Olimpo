# $Id: test-threading.py,v 1.20 2002/01/08 23:24:40 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import Olimpo

nombre=Olimpo.ipc.nombre_modulo()

def handler() :
  import Olimpo
  Olimpo.hace_log2("handler '%s' ejecutado..." %nombre)

  import sys
  print >>sys.stderr,Olimpo.ipc.get_objects()

  def obj() :
    pass

  Olimpo.ipc.add_obj("prueba.%d" %hash(obj),obj)
  

def inicio():
  global t
  Olimpo.comentario_modulo("Modulo PYTHON de prueba de threads $Revision: 1.20 $")
  Olimpo.especifica_fin(fin)
  Olimpo.ipc.IRQ_handler(handler)
  
  import sys
  print >>sys.stderr,Olimpo.ipc.purge_objects()

  import threading
  t=threading.Thread(target=tarea)
  t.setDaemon(1)
  t.start()


def tarea() :
  import time
  while not 0 :
    time.sleep(5)
    if tarea.must_die : return
    import sys
    import time
    print >>sys.stderr,"tarea activada... %s" %time.ctime()
    try :
      Olimpo.ipc.IRQ_nombre(nombre)
    except :
      pass

tarea.must_die=0

def fin() :
  tarea.must_die=not 0
  import time
  tiempo=time.time()
  t.join()
  Olimpo.hace_log2("Esperamos la muerte de los 'threads' hijos (%f segundos)"%(time.time()-tiempo))
  import sys
  #print >>sys.stderr,Olimpo.ipc.del_id_objects(Olimpo.ipc.id_modulo(nombre))
  

