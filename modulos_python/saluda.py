# $Id: saluda.py,v 1.4 2002/09/17 14:03:39 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import Olimpo

def privmsg(nick,remitente,mensaje):
  flags=Olimpo.privmsg.lee_flags_nick(remitente)
  nick_u=Olimpo.privmsg.lee_nick(remitente)
  ip=Olimpo.privmsg.lee_ip_nick(remitente)
  if not ip: ip=(0,0,0,0)
  host=Olimpo.privmsg.lee_host_nick(remitente)

  Olimpo.privmsg.envia_nick(nick,remitente,"Hola, %s, ?Como estas?. Veo que tus flags son '%s'" %(nick_u,flags))
  Olimpo.privmsg.envia_nick(nick,remitente,"Tu IP es %d.%d.%d.%d, y tu host es '%s'" %(ip[0],ip[1],ip[2],ip[3],host))

def inicio():
  Olimpo.comentario_modulo("Modulo PYTHON de ejemplo $Revision: 1.4 $")
  Olimpo.privmsg.nuevo_nick("saluda","+odkirhB",privmsg)
  
