# $Id: dxcluster.py,v 1.160 2003/07/31 18:34:25 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


class _main :
  indicativo=None
  nick=None
  handle_nick=None
  canales_dx_cluster=None
  canales_anuncios=None
  tiempo_entre_anuncios=None
  id_modulo=None
  cola_a_procesar=None
  objeto_arranque_thread=None
  admins={}
  usuarios={}
  db=None
  pc_directo=None
  envia_cluster=None

  def server_join(self,server,id,padre) :
    # Solo nos interesan nuestras conexiones con nuestro HUB
    if padre : return

    import Olimpo
    canales={}
    for i in self.canales_dx_cluster+self.canales_anuncios :
      if i not in canales :
        Olimpo.servmsg.envia_raw(self.handle_nick,"JOIN %s" %i)
        canales[i]=1


class class_remitente:
  def __init__(self,main,remitente,yo):
    self.main=main
    self.yo=yo
    self.handle=remitente
    import Olimpo
    self.flags=Olimpo.privmsg.lee_flags_nick(remitente)

  def es_ircop(self):
    return "o" in self.flags

  def es_oper(self):
    return "h" in self.flags
  
  def es_jcea(self) :
    flag=self.es_ircop() and self.es_oper()
    if not flag : return 0
    if self.home()!="gaia.irc-hispano.org" : return 0
    if self.nick_normalizado()!="jcea" : return 0
    return 1
  
  def es_admin(self) :
    return self.esta_registrado() and self.main.admins.has_key(self.nick_normalizado())

  def es_usuario(self) :
    return self.esta_registrado() and self.main.usuarios.has_key(self.nick_normalizado())
  
  def esta_registrado(self):
    return "r" in self.flags
  
  def home(self) :
    import Olimpo
    return Olimpo.tools.which_server(self.handle)
  
  def nick(self) :
    import Olimpo
    return Olimpo.privmsg.lee_nick(self.handle)

  def nick_normalizado(self) :
    import Olimpo
    return Olimpo.privmsg.lee_nick_normalizado(self.handle)
  
  def envia(self,msg) :
    import Olimpo
    Olimpo.privmsg.envia_nick(self.yo,self.handle,msg)


class conexion :
  def __init__(self,host,puerto,gestor) :
    self.socket=None

    self.gestor=gestor
    self.bufout=""
    import threading
    self.bufout_lock=threading.Lock()
    self.tupla_conexion=(host,puerto)

  def poll(self,timeout=30) :
    import socket
    import select
    import time

    if not self.socket :
      # Hacemos esto aqui y no en el constructor, porque
      # el intento de conexion bloquea el "thread" primario
      # cuando el DNS no funciona bien.
      self.socket=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
      self.socket.setsockopt(socket.SOL_SOCKET,socket.SO_KEEPALIVE,1)
      self.socket.setblocking(0)

      # Si hay un fallo en la conexion, nos enteraremos cuando
      # se intente hacer read/write.
      try :
        self.socket.connect_ex(self.tupla_conexion)
      except :
        self.socket=None

    assert self.socket,"Conexion no establecida" # Ojo, sin parentesis

    t=time.time()
    if self.bufout : w=[self.socket]
    else : w=[]
    r,w,e=select.select([self.socket],w,[],timeout)
    t=time.time()-t

    if w :
      self.bufout_lock.acquire()
      try :
        #import sys
        #print >>sys.stderr,"= = ="

        a=self.socket.send(self.bufout)
        self.bufout=self.bufout[a:]
      finally :
        self.bufout_lock.release()

    if r :
      a=self.socket.recv(1024)
      if not a : raise "El otro extremo nos desconecta"
      self.gestor.dump(a,self)

    return timeout-t

  def envia(self,linea,loopback=0) :
    self.bufout_lock.acquire()
    try :
      #import sys
      #print >>sys.stderr,"=====",linea

      self.bufout+=linea+"\r\n"
      if loopback :
        self.gestor.loopback(linea,self)
    finally :
      self.bufout_lock.release()

  def nuevo_gestor(self,gestor) :
    self.gestor=gestor

  def gestor_timeout(self) :
    self.gestor.timeout(self)

  def close(self):
    if self.socket: self.socket.close()

class gestor_sencillo :
  # DXSpider Gateway
  def __init__(self,main,callsign,callback) :
    self.main=main
    self.callsign=callsign
    self.callback=callback
    self.estamos_dentro=0
    self.buf=""
    self.inicial=1

  def dump(self,texto,callback) :
    texto=self.buf+texto
    i=texto.find("\n")
    while i!=-1 :
      t=texto[:i] # Eliminamos '\n'
      texto=texto[i+1:]
      if t[-1]=='\r' : t=t[:-1]
      while len(t) and (t[-1]=='\x07') : t=t[:-1]
      if t[:len(self.callsign)+1]==self.callsign+" " :
        self.estamos_dentro=1
      elif self.estamos_dentro :
        self.callback(self.main,t)
      elif self.inicial:
          callback.envia(self.callsign)
          self.inicial=0

      i=texto.find("\n")

    self.buf=texto
    assert(len(texto)<1024)

  def timeout(self,callback) :
    callback.envia("")

class gestor_packet_cluster :
  # Packet Cluster
  def __init__(self,main,callsign,callback) :
    self.main=main
    self.callsign=callsign
    self.callback=callback
    self.estamos_dentro=0
    self.inicial=1
    self.buf=""
    self.peer=None

  def PC11(self,cmd,callback) : # DX
    self.callback(self.main,"DX de %s: %s %s %s %s" \
      %(cmd[6].rjust(8),  \
        cmd[1].rjust(12), \
        cmd[2].rjust(8),  \
        cmd[5].ljust(30), \
        cmd[4]))

  def PC12(self,cmd,callback) : # Anuncio
    if cmd[2]=='*' : cmd[2]='ALL'
    self.callback(self.main,"De %s para %s: %s" \
      %(cmd[1].rjust(8), \
        cmd[2].rjust(8), \
        cmd[3]))

  def PC15(self,cmd,callback) : # ????????
    pass

  def PC16(self,cmd,callback) : # NODE PC ADD
    if not self.peer :
      self.peer=cmd[1] # Nos dice el nombre del otro extremo

  def PC17(self,cmd,callback) : # NODE PC DEL
    pass

  def PC19(self,cmd,callback) : # NODE ADD
    pass

  def PC21(self,cmd,callback) : # NODE DEL
    pass

  def PC22(self,cmd,callback) : # PC DONE
    pass

  def PC23(self,cmd,callback) : # ?informacion metereologica/propagacion?
    pass

  def PC24(self,cmd,callback) : # Here Status Info
    pass

  def PC41(self,cmd,callback) : # Informacion de un usuario
    pass

  def PC50(self,cmd,callback) : # Contador usuarios locales
    pass

  def PC51(self,cmd,callback) : # PING
    if cmd[-2]=='1' :
      callback.envia("PC51^%s^%s^%d^" %(cmd[2],cmd[1],0))

  def dump(self,texto,callback) :
    texto=self.buf+texto
    i=texto.find("\n")
    while i!=-1 :
      t=texto[:i] # Eliminamos '\n'
      texto=texto[i+1:]
      if t[-1]=='\r' : t=t[:-1]

      #import sys
      #print >>sys.stderr,"=-=-=",repr(t)

      if not self.estamos_dentro :
        if t[:13]=="PC18^DXSpider" : # Saludo de entrada
          self.estamos_dentro=1
          callback.envia("PC19^0^%s^0^0001^H25^" %self.callsign) # Entrada en la red
          callback.envia("PC20^")
          callback.envia("PC21^")
        elif self.inicial :
          self.inicial=0
          callback.envia(self.callsign)
      else :
        cmd=t.split("^")
        assert(len(cmd[0])==4)
        assert(cmd[0][:2]=="PC")
        try :
          n=int(cmd[0][2:])
        except :
          n=-1
        assert(n>=0)

        func=None
        try:
          func = getattr(self, cmd[0])
        except AttributeError:
          import sys
          print >>sys.stderr,t

        if func : func(cmd,callback)

      i=texto.find("\n")

    self.buf=texto
    assert(len(texto)<1024)

  def loopback(self,texto,callback) :
    cmd=texto.split("^")
    assert(len(cmd[0])==4)
    assert(cmd[0][:2]=="PC")
    try :
      n=int(cmd[0][2:])
    except :
      n=-1
    assert(n>=0)

    func=None
    try:
      func = getattr(self, cmd[0])
    except AttributeError:
      import sys
      print >>sys.stderr,t

    if func : func(cmd,callback)
    
  def timeout(self,callback) :
    assert(self.peer)
    callback.envia("PC51^%s^%s^%d^" %(self.peer,self.callsign,1))

class gestor_thread :
  def __init__(self,main,conexion) :
    self.main=main
    self.conexion=conexion
    import Olimpo
    self.must_die=0

  def gestor(self) :
    import time
    time.sleep(30) # Para evitar consumir recursos si ocurre una rapida sucesion de sucesos
    try :
      ping=0
      while 1 :
        import Olimpo
        if not Olimpo.ipc.existe_id_modulo(self.main.id_modulo) :
          self.conexion.close()
          return # Terminamos

        t=self.conexion.poll()
        if t>0 :
          ping=0
        else : # Ha ocurrido un timeout
          if ping :
           self.conexion.close()
           raise "Ha ocurrido un 'ping timeout'"
          else :
            ping=1
            self.conexion.gestor_timeout()
    finally :
      import Olimpo
      self.main.cola_a_procesar.put_nowait(self.main.objeto_arranque_thread) # 'bootstrap' de la multitarea
      Olimpo.ipc.IRQ_id(self.main.id_modulo) # 'bootstrap' de la multitarea
      

class envia_a_canal :
  def __init__(self,main,texto) :
    self.main=main
    if texto=="" : return
    self.texto=texto
    self.main.cola_a_procesar.put_nowait(self) # Nos metemos en la cola
    import Olimpo
    Olimpo.ipc.IRQ_id(self.main.id_modulo)

  def procesa(self) :
    import Olimpo
    for i in self.main.canales_dx_cluster :
      Olimpo.servmsg.envia_raw(self.main.handle_nick,"PRIVMSG %s :\00312%s" %(i,self.texto))

class privmsg :
  def __init__(self,main) :
    self.main=main

  def procesa(self,nick,remitente,mensaje):
    comandos={"help":self.m_help,
      "adminlist":self.m_adminlist,"adminadd":self.m_adminadd,"admindel":self.m_admindel,
      "userlist":self.m_userlist,"useradd":self.m_useradd,"userdel":self.m_userdel}

    if self.main.pc_directo :
      comandos["sendnotice"]=self.m_sendnotice
      comandos["senddx"]=self.m_senddx

    msg=mensaje.split()
    if not msg : return # Mensaje vacio (por ejemplo, espacios o tabuladores)
    cmd=msg[0].lower()

    remitente=class_remitente(self.main,remitente,nick)

    if comandos.has_key(cmd) :
      comandos[cmd](remitente,mensaje,cmd,msg)

  def nick_valido(self,nick) :
    import Olimpo
    handle=Olimpo.privmsg.lee_handle(nick)
    if not handle :
      return "El nick especificado debe estar 'online' para realizar esta operacion"
    flags=Olimpo.privmsg.lee_flags_nick(handle)
    if "r" not in flags :
      return "El nick especificado debe tener modo +r (registrado en 'nick2') para realizar esta operacion"

  def m_adminlist(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    Olimpo.hace_log(remitente.handle,mensaje)
    remitente.envia("Listado de administradores de \002%s" %self.main.nick)
    a=self.main.admins.keys()
    a.sort() # In-Place
    for i in a :
      remitente.envia(i)
    remitente.envia("\002Fin de AdminList") 

  def m_userlist(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    Olimpo.hace_log(remitente.handle,mensaje)
    remitente.envia("Listado de usuarios registrados de \002%s" %self.main.nick)
    a=self.main.usuarios.keys()
    a.sort() # In-Place
    for i in a :
      remitente.envia(i)
    remitente.envia("\002Fin de UserList")

  def m_adminadd(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    if remitente.es_jcea() :
      if len(msg)==2 :
        Olimpo.hace_log(remitente.handle,mensaje)
        nick=Olimpo.tools.nick_normalizado(msg[1])
        msg=self.nick_valido(nick)
        if not msg :
          self.main.admins[nick]=1
          txn=Olimpo.BerkeleyDB.txn()
          self.main.db.put(txn,"admins"," ".join(self.main.admins.keys()))
          txn.compromete()
          remitente.envia("Ahora \002%s\002 ya es admin en \002%s" %(nick,self.main.nick))
        else :
          remitente.envia(msg)
      else :
        remitente.envia("Numero de parametros incorrecto")
    else :
      remitente.envia("No tienes permiso para realizar esta operacion")

    remitente.envia("\002Fin de AdminAdd")

  def m_useradd(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    if remitente.es_jcea() or remitente.es_admin() :
      if len(msg)==2 :
        Olimpo.hace_log(remitente.handle,mensaje)
        nick=Olimpo.tools.nick_normalizado(msg[1])
        msg=self.nick_valido(nick)
        if not msg :
          self.main.usuarios[nick]=1
          txn=Olimpo.BerkeleyDB.txn()
          self.main.db.put(txn,"usuarios"," ".join(self.main.usuarios.keys()))
          txn.compromete()
          remitente.envia("Ahora \002%s\002 ya es un usuario registrado en \002%s" %(nick,self.main.nick))
        else :
          remitente.envia(msg)
      else :
        remitente.envia("Numero de parametros incorrecto")
    else :
      remitente.envia("No tienes permiso para realizar esta operacion")

    remitente.envia("\002Fin de UserAdd")

  def m_admindel(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    if remitente.es_jcea() or remitente.es_admin() :
      if len(msg)==2 :
        Olimpo.hace_log(remitente.handle,mensaje)
        nick=Olimpo.tools.nick_normalizado(msg[1])
        if self.main.admins.has_key(nick) :
          del self.main.admins[nick]
          txn=Olimpo.BerkeleyDB.txn()
          self.main.db.put(txn,"admins"," ".join(self.main.admins.keys()))
          txn.compromete()
          remitente.envia("El admin \002%s\002 ha sido eliminado de \002%s" %(nick,self.main.nick))
        else :
          remitente.envia("\002%s\002 no es admin en \002%s" %(nick,self.main.nick))
      else :
        remitente.envia("Numero de parametros incorrecto")
    else :
      remitente.envia("No tienes permiso para realizar esta operacion")
  
    remitente.envia("\002Fin de AdminDel")

  def m_userdel(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    if remitente.es_jcea() or remitente.es_admin() :
      if len(msg)==2 :
        Olimpo.hace_log(remitente.handle,mensaje)
        nick=Olimpo.tools.nick_normalizado(msg[1])
        if self.main.usuarios.has_key(nick) :
          del self.main.usuarios[nick]
          txn=Olimpo.BerkeleyDB.txn()
          self.main.db.put(txn,"usuarios"," ".join(self.main.usuarios.keys()))
          txn.compromete()
          remitente.envia("El usuario \002%s\002 ha sido eliminado de \002%s" %(nick,self.main.nick))
        else :
          remitente.envia("\002%s\002 no es usuario registrado de \002%s" %(nick,self.main.nick))
      else :
        remitente.envia("Numero de parametros incorrecto")
    else :
      remitente.envia("No tienes permiso para realizar esta operacion")
 
    remitente.envia("\002Fin de UserDel")

  def m_sendnotice(self,remitente,mensaje,cmd,msg) :
    if len(msg)<2 :
      return

    if not (remitente.es_jcea() or remitente.es_admin() or remitente.es_usuario()) :
      return

    import Olimpo
    Olimpo.hace_log(remitente.handle,mensaje)

    mensaje=mensaje[mensaje.find(" ")+1:]

    self.main.envia_cluster("PC12^%s^*^%s^ ^%s^0^H25^~" %(remitente.nick().upper(),mensaje,self.main.indicativo),loopback=1)

    remitente.envia("Texto enviado")
    remitente.envia("\002Fin de SendNotice")


  def m_senddx(self,remitente,mensaje,cmd,msg) :
    if len(msg)<4 :
      return

    if not (remitente.es_jcea() or remitente.es_admin() or remitente.es_usuario()) :
      return

    import Olimpo
    Olimpo.hace_log(remitente.handle,mensaje)

    mensaje=mensaje[mensaje.find(" ")+1:] # Eliminamos el comando
    mensaje=mensaje[mensaje.find(" ")+1:] # Eliminamos la frecuencia
    mensaje=mensaje[mensaje.find(" ")+1:] # Eliminamos el indicativo

    if len(mensaje)>30 :
      remitente.envia("Texto demasiado largo")
      remitente.envia("\002Fin de SendNotice")
      return

    import time
    now_gm=time.gmtime()
    self.main.envia_cluster("PC11^%s^%s^%s^%sZ^%s^%s^%s^H25^~" \
      %(msg[1], \
        msg[2].upper(), \
        time.strftime("%e-%b-%Y",now_gm), \
        time.strftime("%H%M",now_gm), \
        mensaje, \
        remitente.nick().upper(), \
        self.main.indicativo.upper()),loopback=1)

    remitente.envia("Texto enviado")
    remitente.envia("\002Fin de SendDX")
    
  def m_help(self,remitente,mensaje,cmd,msg) :
      import Olimpo
      Olimpo.hace_log(remitente.handle,mensaje)
      remitente.envia("DX-Cluster $Revision: 1.160 $")
      remitente.envia("Este bot envia la informacion DX-Cluster al canal %s de IRC-Hispano. Mas informacion en \002http://www.argo.es/~jcea/irc/modulos/dxcluster.htm" %self.main.canales_dx_cluster[0]) # El primero es el oficial
      remitente.envia("\002ADMINLIST")
      remitente.envia("     Proporciona la lista de administradores de %s" %self.main.nick)
      remitente.envia("\002ADMINADD\002 (para uso exclusivo de JCEA)")
      remitente.envia("     An~ade un admin de %s" %self.main.nick)
      remitente.envia("\002ADMINDEL\002 (para uso de admins de %s)" %self.main.nick)
      remitente.envia("     Elimina un administrador de  %s" %self.main.nick)
      remitente.envia("\002USERLIST")
      remitente.envia("     Proporciona la lista de usuarios registrados en %s" %self.main.nick)
      remitente.envia("\002USERADD\002 (para uso de admins de %s)" %self.main.nick)
      remitente.envia("     Registra un usuario en %s" %self.main.nick)
      remitente.envia("\002USERDEL\002 (para uso de admins de %s)" %self.main.nick)
      remitente.envia("     Elimina un usuario registrado en %s" %self.main.nick)

      if self.main.pc_directo :
        remitente.envia("\002SENDNOTICE <texto>\002 (para usuarios registrados en %s)" %self.main.nick)
        remitente.envia("     Envia un texto a toda la red")
        remitente.envia("\002SENDDX <freq> <indicativo> <msg>\002 (para usuarios registrados en %s)" %self.main.nick)
        remitente.envia("     Comunica un DX a la red")

      remitente.envia("\002HELP")
      remitente.envia("     Muestra esta ayuda")
      remitente.envia("Para cualquier duda o consulta, buscar el nick 'jcea' o enviar un mensaje a 'jcea@argo.es'")
      remitente.envia("\002Fin de HELP")
    

class lanza_thread :
  def __init__(self,main,conexion,gestor,host,puerto,callsign) :
    self.conexion=conexion
    self.gestor=gestor
    self.host=host
    self.puerto=puerto
    self.callsign=callsign

    import weakref
    self.main=weakref.ref(main) # Con esto intentamos eliminar un bucle de referencias

  def procesa(self) :
    main=self.main() # Weakref
    assert(main)
    gestor=self.gestor(main,self.callsign,envia_a_canal)
    a=self.conexion(self.host,self.puerto,gestor)
    import weakref
    main.envia_cluster=a.envia
    import threading
    t=threading.Thread(target=gestor_thread(main,a).gestor)
    t.setDaemon(1)
    t.setName("DX-Cluster '%s:%d'" %(self.host,self.puerto))
    t.start()
   
class IRQ_handler :
  def __init__(self,main) :
    self.main=main

  def procesa(self) :
    import Olimpo

    # No hay "race condition" porque si damos la cola como vacia,
    # volvemos a entrar con el nuevo IRQ.
    while not self.main.cola_a_procesar.empty() :
      try :
        self.main.cola_a_procesar.get_nowait().procesa()
      except :
        Olimpo.ipc.IRQ_id(self.main.id_modulo) # Si ocurre un problema, debemos procesar el resto de la cola
        raise

class timer :
  def __init__(self,main) :
    self.main=main

  def procesa(self) :
    import Olimpo
    import time
    cluster=self.main.canales_dx_cluster[0] # El primero es el oficial
    for i in self.main.canales_anuncios :
      Olimpo.servmsg.envia_raw(self.main.handle_nick,"PRIVMSG %s :\0037\002%s\002, canal oficial de difusion de informacion DX-Cluster. Entra escribiendo \002/join %s" %(i,cluster,cluster))
    Olimpo.notify.notifica_timer(int(time.time())+self.main.tiempo_entre_anuncios,self.procesa)


class nickdrop :
  def __init__(self,main) :
    self.main=main

  def procesa(self,nick,num_serie) :
    flag=0
    try :
      del self.main.admins[nick]
      flag|=1
    except :
      pass

    try :
      del self.main.usuarios[nick]
      flag|=2
    except :
      pass

    # Nos vale con transaccion ACI, en vez de ACID, porque si la perdemos,
    # sincronizaremos cuando recarguemos el modulo.
    import Olimpo
    txn=Olimpo.BerkeleyDB.txn(Olimpo.BerkeleyDB.txn.NO_SYNC)
    self.main.db.put(txn," NICKDROPS",repr(num_serie))
    if flag & 1 : self.main.db.put(txn,"admins"," ".join(self.main.admins.keys()))
    if flag & 2 : self.main.db.put(txn,"usuarios"," ".join(self.main.usuarios.keys()))
    txn.compromete()

class fin :
  def __init__ (self,main) :
    self.main=main

  def procesa(self) :
    self.main.db.cierra()

def inicio() :
  import Olimpo
  Olimpo.comentario_modulo("DX-Cluster $Revision: 1.160 $")
  main=_main()

  main.db=Olimpo.BerkeleyDB.db("db.dxcluster")
  txn=Olimpo.BerkeleyDB.txn()
  a=main.db.get(txn,"admins")[1]
  b=main.db.get(txn,"usuarios")[1]
  num_reg_nickdrop=main.db.get(txn," NICKDROPS")[1]
  txn.compromete()

  if num_reg_nickdrop : num_reg_nickdrop=int(num_reg_nickdrop)
  else : num_reg_nickdrop=6214152 # El valor que tenia cuando creamos el modulo

  if a : main.admins=dict([(i,1) for i in a.split()])
  if b : main.usuarios=dict([(i,1) for i in b.split()])

  main.pc_directo=1

  if not main.pc_directo : # DX-Spider
    main.indicativo="INDICATIVO-8"
    gestor=gestor_sencillo
  else : # Protocolo Packet Cluster directo
    main.indicativo="INDICATIVO-6"
    gestor=gestor_packet_cluster

  main.nick="dxcluster"
  main.canales_dx_cluster=["#DX-Cluster","#radioaficionados"] # El primero es el mas oficial
  main.canales_anuncios=["#radioaficionados"]
  #main.tiempo_entre_anuncios=5*60 # Tiempo entre anuncios

  main.id_modulo=Olimpo.ipc.id_modulo(Olimpo.ipc.nombre_modulo())

  import threading
  import Queue
  main.cola_a_procesar=Queue.Queue(0) # Sin limite de taman~o

  main.handle_nick=Olimpo.privmsg.nuevo_nick(main.nick,"+odkirhB",privmsg(main).procesa)
  Olimpo.especifica_fin(fin(main).procesa)

  Olimpo.notify.notifica_server_join(main.server_join)
  Olimpo.notify.notifica_servers()

  # Varios accesos DX-Cluster

  if not main.pc_directo : # Spider
    #main.objeto_arranque_thread=lanza_thread(main,conexion,gestor,"ea7urc.alcavia.net",41112,main.indicativo)

    # Este acceso no funciona haciendo un telnet desde Unix, aunque si desde Windows
    main.objeto_arranque_thread=lanza_thread(main,conexion,gestor,"eadx.net",23,main.indicativo)
  else :
    main.objeto_arranque_thread=lanza_thread(main,conexion,gestor,"eadx.net",23,main.indicativo)

  Olimpo.ipc.IRQ_handler(IRQ_handler(main).procesa)
  main.cola_a_procesar.put_nowait(main.objeto_arranque_thread) # 'bootstrap' de la multitarea
  Olimpo.ipc.IRQ_id(main.id_modulo) # 'bootstrap' de la multitarea

  if main.tiempo_entre_anuncios : # Si queremos anuncios...
    timer(main).procesa() # 'bootstrap' de publicacion de publicidad

  Olimpo.notify_db.notifica_nickdrop(nickdrop(main).procesa,num_reg_nickdrop)

