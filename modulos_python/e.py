# $Id: e.py,v 1.8 2002/09/05 15:18:53 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


# Este modulo accede directamente a objetos internos de NICK2

def inicio() :
  import Olimpo
  import nick2
  import time

  claves=[]

  for i in xrange(1,501) :
    nick="lgq%.3d" %i
    t=long(time.time())
    nick,clave,hash=nick2.procesa_TEA(nick)
    n=nick2.db_nick(nick)
    try :
      n.nuevo(ts=0,ts_alta=t,email="%s@danielcp.net" %nick)
      n.set_noexpire()
      n.set_clave(clave)

      historico=nick2.nick_historico(nick,txn=n.get_txn())
      historico.add("'NickRegister'. Email: '%s'" %n.get_email(),operacion=historico.op_nickregister)
      historico.update()

      Olimpo.bdd.envia_registro('n',nick,hash)
      claves.append((nick,clave))
    finally :
      n.compromete_txn()

  f=open("/export/home/irc/olimpo/zzzrety434537","w")
  for i in claves:
    print >>f,"%s,%s" %i
  f.close()
