# $Id: libolimpodapi.py,v 1.36 2002/09/18 17:31:25 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


__version__="0.0.1"

class ConexionCliente(object) :
  def __init__(self,sock,olimpodapi,ping_timeout=30) :
    import time
    self.sock=sock
    self.olimpodapi=olimpodapi
    self.buf_out=""
    self.actividad_reciente=time.time()
    self.tiempo_ping_timeout=ping_timeout
    self.ping_enviado=0

  def procesa(self,timeout) :
    import select
    import time
    r_i=[self.sock]
    vacio_i=[]
    t=time.time()
    t_timeout=t+timeout
    while t<=t_timeout :
      salida=self.olimpodapi.envia(len(self.buf_out))
      if salida :
        self.buf_out+=salida
      if self.buf_out :
        w_i=r_i
      else :
        w_i=vacio_i
      r,w,e=select.select(r_i,w_i,vacio_i,t_timeout-t)
      if w :
        c=self.sock.send(self.buf_out)
        self.buf_out=self.buf_out[c:]
      if r :
        self.actividad_reciente=-1
        self.ping_enviado=0
        r=self.sock.recv(16384)
        if not r :
          raise "Conexion Cerrada"

        self.olimpodapi.recibe(r)

      if self.olimpodapi.bye :
        raise self.olimpodapi.bye

      t=time.time()

    if self.actividad_reciente<0 :
      self.actividad_reciente=t
    else :
      if t-self.actividad_reciente>=self.tiempo_ping_timeout :
        if self.ping_enviado :
          raise "PING timeout"
        else :
          self.ping_enviado=1
          self.actividad_reciente=t
          self.buf_out+="PING :timeout\r\n"
    
  
class OlimpoDistributedAPI(object) :
  def __init__(self) :
    self.buf_in=self.buf_out=""
    self.comandos={"ping":self.m_ping,"bye":self.m_bye,}
    self.bye=None

  def recibe(self,texto) :
    self.buf_in+=texto

    while True :
      t=t2=self.buf_in.find("\n") 
      if t<0 : return
      if self.buf_in[t-1]=='\r' : t-=1
      linea=self.buf_in[:t]
      self.buf_in=self.buf_in[t2+1:]

      t=linea.find(" :") 
      if t>=0 :
        bloque=linea[t+2:]
        linea=linea[:t]

      comando=linea.split()
      if t>=0 :
         comando.append(bloque)

      cmd=comando[0].lower()

      salida=self.comandos[cmd](comando)
      if salida :
        for i in salida :
          self.buf_out+=i+"\r\n"
    
  def envia(self,len_cola) :
    a=self.buf_out
    self.buf_out=""
    return a

  def m_bye(self,comando) :
    assert(len(comando)==2)
    self.bye=" ".join(comando)

  def m_ping(self,comando) :
    assert(len(comando)==2)
    return ["PONG :%s" %comando[1]]


class ServerOlimpoDistributedAPI(OlimpoDistributedAPI) :
  def __init__(self,reto=None) :
    OlimpoDistributedAPI.__init__(self)
    self.comandos.update({"challenge-result":self.m_challenge_result,})
    self.nick=None
    self.level=None
    self.lversion=None
    if reto :
      self._reto=reto
    else :
      import random
      import sha
      r=random.Random()
      i=[chr(r.randrange(0,256)) for x in xrange(256)]
      i="".join(i)
      self._reto=sha.new(i).hexdigest()

      self.buf_out+="CHALLENGE HMAC-SHA1 HMAC-MD5 :%s\r\n" %self._reto

  def clave(self,nick) :
    raise "hay que suplantar esta rutina"

  def m_challenge_result(self,comando) :
    assert(len(comando)==6)

    assert(comando[-4]=="0")

    self.level=int(comando[-4])
    self.lversion=int(comando[-3])
    assert(self.lversion>=1)

    if comando[1]=="HMAC-MD5" :
      import md5 as HASH
    elif comando[1]=="HMAC-SHA1" :
      import sha as HASH
    else :
      raise "El cliente envia un HASH desconocido"

    nick=comando[-2]
    import hmac
    digest=hmac.new(self.clave(nick),self._reto,HASH).hexdigest()
    if digest!=comando[-1] : raise "Clave incorrecta"
    self.nick=nick

    return ["VERSION 1"]


class ClientOlimpoDistributedAPI(OlimpoDistributedAPI) :
  def __init__(self,nick,clave) :
    OlimpoDistributedAPI.__init__(self)
    self.comandos.update({"challenge":self.m_challenge,
      "version":self.m_version,
      "pong":self.m_pong,"privmsg":self.m_privmsg,
      "notice":self.m_notice,"csession":self.m_csession})
    self._nick=nick
    self._clave=clave
    self.version=None
    self.bye=None

  def m_pong(self,comando) :
    pass

  def m_privmsg(self,comando) :
    pass

  def m_notice(self,comando) :
    pass

  def m_csession(self,comando) :
    pass

  def m_version(self,comando) :
    assert(len(comando)==2)
    version=int(comando[1])
    assert((version>=1) and (version<=999999))
    assert(not self.version)
    self.version=version

  def m_challenge(self,comando) :
    assert(len(comando)>=3)
    for tipo in comando[1:-1] :
      if tipo=="HMAC-MD5" :
        import md5 as HASH
        break
      elif tipo=="HMAC-SHA1" :
        import sha as HASH
        break
    else :
      HASH=None

    if HASH :
      import hmac
      digest=hmac.new(self._clave,comando[-1],HASH).hexdigest()
      return ["CHALLENGE-RESULT %s 0 1 %s :%s" %(tipo,self._nick,digest)]
    else :
      raise "El servidor solicita mecanismos de HASH desconocidos: %s" %comando[1:-2]


import unittest
import exceptions

class TestSuiteOlimpoDistributedAPI(unittest.TestCase) :
  def setUp(self) :
    self.conn=OlimpoDistributedAPI()

  def tearDown(self) :
    self.conn=None

  def test001NullInput(self) :
    "Cierre del canal de entrada"
    salida=self.conn.recibe("")
    self.failUnlessEqual(salida,None)

  def test002LineasFragmentadas(self) :
    "Fragmentacion de lineas en entrada"
    txt="PING :12345678\r\n"
    for i in txt:
      salida=self.conn.envia(0)
      self.failUnlessEqual(salida,"")
      salida=self.conn.recibe(i)
      self.failUnlessEqual(salida,None)

    salida=self.conn.envia(0)
    self.failUnlessEqual(salida.upper(),"PONG :12345678\r\n".upper())

  def test100ComandoDesconocido(self) :
    "Gestion de un comando desconocido"
    salida=0
    try :
      self.conn.recibe("ComandoDesconocido\r\n")
    except exceptions.KeyError :
      salida=1
    self.failUnless(salida)

  def test110PING(self) :
    "PING"
    salida=self.conn.recibe("PING :12345678\r\n")
    self.failUnlessEqual(salida,None)
    salida=self.conn.envia(0)
    self.failUnlessEqual(salida.upper(),"PONG :12345678\r\n".upper())

  def test200Bye(self) :
    "'Bye'"
    self.failUnless(not self.conn.bye)
    salida=self.conn.recibe("BYE :Prueba\n")
    self.failUnless(self.conn.bye)


class TestSuiteServerOlimpoDistributedAPI(TestSuiteOlimpoDistributedAPI) :
  def setUp(self) :
    class LocalServerOlimpoDistributedAPI(ServerOlimpoDistributedAPI) :
      def clave(self,nick) :
        return "Jefe"

    self.conn=LocalServerOlimpoDistributedAPI(reto="what do ya want for nothing?")

  def test130ChallengeResultInvalido(self) :
    "'Challenge-Result' con mecanismos desconocidos"
    salida=0
    try :
      self.conn.recibe("CHALLENGE-RESULT HMAC-DESCONOCIDO 0 1 HOLA :digest\n")
    except :
      salida=1

    self.failUnless(salida)

  def test131ChallengeResultInvalido(self) :
    "'Challenge-Result' con resultado incorrecto"
    salida=0
    try :
      self.conn.recibe("CHALLENGE-RESULT HMAC-MD5 0 1 HOLA :digest\n")
    except :
      salida=1

    self.failUnless(salida)

  def test132ChallengeResultMD5(self) :
    "'Challenge-Result' con MD5"
    salida=self.conn.recibe("CHALLENGE-RESULT HMAC-MD5 0 1 Jefe :750c783e6ab0b503eaa86e310a5db738\n")
    self.failUnlessEqual(self.conn.nick,"Jefe")

  def test133ChallengeResultSHA1(self) :
    "'Challenge-Result' con SHA-1"
    salida=self.conn.recibe("CHALLENGE-RESULT HMAC-SHA1 0 1 Jefe :effcdf6ae5eb2fa2d27416d5f184df9c259a7c79\n")
    self.failUnlessEqual(self.conn.nick,"Jefe")

  def test134ChallengeResultVersion(self) :
    "'Challenge-Result' con una version no soportada"
    salida=self.conn.recibe("CHALLENGE-RESULT HMAC-SHA1 0 65535 Jefe :effcdf6ae5eb2fa2d27416d5f184df9c259a7c79\n")
    self.failUnlessEqual(self.conn.nick,"Jefe")
    salida=self.conn.envia(0)
    self.failUnlessEqual(salida.upper(),"VERSION 1\r\n".upper())


class TestSuiteClientOlimpoDistributedAPI(TestSuiteOlimpoDistributedAPI) :
  def setUp(self) :
    self.conn=ClientOlimpoDistributedAPI(nick="nick_de_prueba",clave="Jefe")

  def test120ChallengeInvalido(self) :
    "'Challenge' con mecanismos desconocidos"
    salida=0
    try :
      self.conn.recibe("CHALLENGE HMAC-DESCONOCIDO :reto\r\n")
    except :
      salida=1

    self.failUnless(salida)

  def test121ChallengeMD5(self) :
    "'Challenge' con MD5"
    salida=self.conn.recibe("CHALLENGE HMAC-DESCONOCIDO HMAC-MD5 :what do ya want for nothing?\n")
    salida=self.conn.envia(0)
    self.failUnlessEqual(salida.lower(),"CHALLENGE-RESULT HMAC-MD5 0 1 nick_de_prueba :750c783e6ab0b503eaa86e310a5db738\r\n".lower())

  def test122ChallengeSHA1(self) :
    "'Challenge' con SHA-1"
    salida=self.conn.recibe("CHALLENGE HMAC-DESCONOCIDO HMAC-SHA1 :what do ya want for nothing?\n")
    salida=self.conn.envia(0)
    self.failUnlessEqual(salida.lower(),"CHALLENGE-RESULT HMAC-SHA1 0 1 nick_de_prueba :effcdf6ae5eb2fa2d27416d5f184df9c259a7c79\r\n".lower())


class TestSuiteBothOlimpoDistributedAPI(unittest.TestCase) :

  verbose=1

  def setUp(self) :
    class LocalServerOlimpoDistributedAPI(ServerOlimpoDistributedAPI) :
      def __init__(self,reto=None) :
        ServerOlimpoDistributedAPI.__init__(self,reto)
        self.comandos.update({"completo":self.m_completo,})
        self.COMPLETADO=0

      def clave(self,nick) :
        return "LaClave"

      def m_completo(self,comando) :
        self.COMPLETADO=1

    class LocalClientOlimpoDistributedAPI(ClientOlimpoDistributedAPI) :
      def __init__(self,nick,clave) :
        ClientOlimpoDistributedAPI.__init__(self,nick,clave)
        self.comandos.update({"completo":self.m_completo,})
        self.COMPLETADO=0

      def m_completo(self,comando) :
        self.COMPLETADO=1

    self.conn_server=LocalServerOlimpoDistributedAPI()
    self.conn_client=LocalClientOlimpoDistributedAPI(nick="ElNick", clave="LaClave")

  def tearDown(self) :
    self.conn_server=self.conn_client=None

  def test900ComunicacionMutua(self) :
    "Comunicacion mutua Cliente<->Servidor"
    comandos=[(0,"completo"),(1,"completo")]
    while True: 
      c2s=s2c="*" # Dummy
      while c2s or s2c :
        c2s=self.conn_client.envia(0)
        if c2s :
          if self.verbose : print "<<<",c2s,
          self.conn_server.recibe(c2s)
        s2c=self.conn_server.envia(0)
        if s2c :
          if self.verbose : print ">>>",s2c,
          self.conn_client.recibe(s2c)

      if not comandos : break
      i,msg=comandos.pop(0)
      if i :
        if self.verbose : print ">>*>>",msg
        self.conn_client.recibe(msg+"\n")
      else :
        if self.verbose : print "<<*<<",msg
        self.conn_server.recibe(msg+"\n") 

    self.failUnless(self.conn_server.COMPLETADO and self.conn_server.COMPLETADO)



class TestSuite(object,unittest.TestSuite) :
  def __new__(cls,*a,**b) :
    if not cls.__dict__.has_key("_tests") : # Se crea un singleton
      cls._tests=[]

    return object.__new__(cls)

  def __init__(self,tests=(),nombre="") :
    if nombre :
      assert(type(nombre)==type(""))
      self._tests.append(nombre)

    if tests :
      self.addTests(tests)

  def __call__(self,result) :
    t=type("")
    for test in self._tests:
      if type(test)==t :
        print "\n*** Comprobando '%s':" %test
        continue
      if result.shouldStop:
        break
      test(result)
    return result


class DemoConnectionClientOlimpoDistributedAPI :
  def __init__(self) :
    class DemoClientOlimpoDistributedAPI(ClientOlimpoDistributedAPI) :
      def m_privmsg(self,comando) :
        assert(len(comando)==3)
        return ["PRIVMSG %s :Hola, usuario. Me has escrito '%s'. Tu identificador de sesion conmigo es '%s'" %(comando[1],comando[2],comando[1]),"CSESSION closed %s " %comando[1]]

    import socket
    sock=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET,socket.SO_KEEPALIVE,1)
    sock.setblocking(0)

    from errno import EALREADY, EINPROGRESS, EWOULDBLOCK, ECONNRESET, \
      ENOTCONN, ESHUTDOWN, EINTR, EISCONN
    st=sock.connect_ex(("127.0.0.1",6776))
    if st not in (EINPROGRESS, EALREADY, EWOULDBLOCK): raise "Error al intentar conectar"

    self.procesa=ConexionCliente(sock,DemoClientOlimpoDistributedAPI(nick="MiNick",clave="MiClave")).procesa



  
if __name__=="__main__" :
  t=unittest.TestLoader()
  t.suiteClass=TestSuite

  TestSuite(nombre="OlimpoDistributedAPI");
  u=t.loadTestsFromTestCase(TestSuiteOlimpoDistributedAPI)
  TestSuite(nombre="ClientOlimpoDistributedAPI")
  u=t.loadTestsFromTestCase(TestSuiteClientOlimpoDistributedAPI)
  TestSuite(nombre="ServerOlimpoDistributedAPI")
  u=t.loadTestsFromTestCase(TestSuiteServerOlimpoDistributedAPI)
  TestSuite(nombre="BothOlimpoDistributedAPI")
  u=t.loadTestsFromTestCase(TestSuiteBothOlimpoDistributedAPI)

  unittest.TextTestRunner(verbosity=2).run(u)


