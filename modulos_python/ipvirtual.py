# $Id: ipvirtual.py,v 1.252 2003/09/16 13:18:14 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import Olimpo
import time
import cPickle

modulo_nick="ipvirtual"
handle=None

db_varios=None
db_tokens=None
db_nicks=None

filtros=[]

class _usuarios_generadores(object) :
  def __new__(cls,txn=None) :
    assert txn
    if not cls.__dict__.has_key("u") : # Se crea un singleton
      u=db_varios.get(txn,"USUARIOS_GENERADORES")[1]
      if u :
        u=cPickle.loads(u)
      else :
        u={}
      cls.u=u

    return object.__new__(cls)

  def __init__(self,txn=None) :
    pass

  def _vuelca(self,txn) :
    assert txn

    u=cPickle.dumps(self.u)
    db_varios.put(txn,"USUARIOS_GENERADORES",u)

  def nuevo(self,nick,miembro,nodo,remitente,email=None,txn=None) :
    assert txn
    self.u[nick]=(miembro,nodo,email,remitente,int(time.time()))
    self._vuelca(txn)

  def eliminar(self,nick,txn=None) :
    assert txn
    if self.u.has_key(nick) :
      del self.u[nick]
      self._vuelca(txn)
      return 1
    return 0
    
  def validacion(self,handle) :
    flags=Olimpo.privmsg.lee_flags_nick(handle)
    if not flags : return 0
    if "r" not in flags : return 0
    nick=Olimpo.privmsg.lee_nick_normalizado(handle)
    if not self.u.has_key(nick) : return 0
    return 1

  def get_miembro(self,nick) :
    miembro=self.u.get(nick,None)
    if miembro :
      miembro=miembro[0]
    return miembro

  def get_email(self,nick) :
    email=self.u.get(nick,None)
    if email :
      email=email[2]
    return email

  def lista_formateada(self) :
    a=[]
    for nick,j in self.u.items() :
      miembro,nodo,email,remite,ts=j
      if email :
        b="%s - %s - %s - %s (%s - %s)" %(nick,miembro,nodo,email,remite,time.ctime(ts))
      else :
        b="%s - %s - %s (%s - %s)" %(nick,miembro,nodo,remite,time.ctime(ts))
      a.append(b)
    a.sort()
    return a


usuarios_generadores=None


class handle_db(object) :
  def __init__(self,clave=None,db=None,txn=None) :
    assert db
    self.clave=clave
    self.db=db
    self.cursor=None

    if txn :
      self.txn_propia=0
    else :
      txn=Olimpo.BerkeleyDB.txn()
      self.txn_propia=1

    self.txn=txn
    self.cambios=0

    if clave :
      reg=db.get(txn,clave)[1]
    else :
      reg=None

    if reg :
      self.found=1
      self.valor=self.loads(reg)
    else :
      self.found=0
      self.valor=None

  def loads(self,valor) :
    return cPickle.loads(valor)

  def dumps(self,valor) :
    return cPickle.dumps(valor)

  def next(self) :
    # Si la transaccion no es propia,
    # debemos terminar este objeto con
    # un "next_end()", antes de cerrar
    # la transaccion.

    # assert self.txn_propia
    assert not self.cambios # No hemos implementado la escritura en CURSORES

    if not self.cursor :
      self.cursor=self.db.cursor(self.txn,self.clave)

    while 1 : # Debemos saltarnos los registros especiales
      reg=self.cursor.get()[1:3]
      if not reg[0] :
        self.cursor.close()
        self.cursor=None
        self.found=0
        return 0

      if reg[0][0]==' ' : continue # Nos saltamos los registros especiales

      self.clave=reg[0]
      self.valor=self.loads(reg[1])
      self.found=1
      return 1

  def next_end(self) :
    # Esta funcion debe invocarse
    # cuando queremos cerrar un cursor
    # y la transaccion asociada es
    # externa a este objeto.
    assert not self.cambios # No hemos implementado la escritura en CURSORES
    if self.cursor :
      self.cursor.close()
      self.cursor=None

  def delete(self) :
    self.db.delete(self.txn,self.clave)
    self.found=0
    self.cambios=0

  def update(self) :
    if self.cambios :
      assert self.found
      self.db.put(self.txn,self.clave,self.dumps(self.valor))
      self.cambios=0

  def get_txn(self) :
    assert self.txn
    return self.txn

  def compromete_txn(self) :
    assert self.txn_propia
    if self.cursor :
      self.cursor.close()
      self.cursor=None
    if self.cambios :
      self.update()
    self.txn.compromete()
    self.txn=None

class handle_nick(handle_db) :
  def __init__(self,nick=None,txn=None,token=None,peticionario=None,ip=None) :
    assert nick

    handle_db.__init__(self,clave=nick,txn=txn,db=db_nicks)

    if token :
      assert peticionario
      assert ip

      self.valor=(int(time.time()),token,peticionario,ip)
      self.found=1
      self.cambios=1

  def get_ts(self) :
    assert self.found
    return self.valor[0]

  def ip_virtual(self) :
    assert self.found
    return self.valor[3]

  def nick(self) :
    assert self.found
    return self.clave


class handle_token(handle_db) :
  def __init__(self,token=None,txn=None,peticionario=None) :
    if token :
      assert not peticionario
      handle_db.__init__(self,clave=token,txn=txn,db=db_tokens)
    else :
      assert peticionario
      while 1 :
        b=aleatorio("ponemos un valor aleatorio wert2346u101vewrkcgrewjxfow").digest()

        if 0 : # Formato tokens de 64 bits
          a=(ord(b[0])*256+ord(b[1]),ord(b[2])*256+ord(b[3]))
          b=(ord(b[4])*256+ord(b[5]),ord(b[6])*256+ord(b[7]))
          a=Olimpo.tools.inttobase64(a)+Olimpo.tools.inttobase64(b)
        else : # Formato tokens de 15 digitos
          n=0L
          for i in b :
            n=n*256+ord(i)
          n%=pow(10,15)
          a=("0"*15+repr(n))
          if a[-1]=='L' : a=a[-16:-1]
          else : a=a[-15:]

        handle_db.__init__(self,clave=a,txn=txn,db=db_tokens)
        if not self.found : break # Hemos encontrado un TOKEN que no existe, lo tomamos.

      self.valor=(int(time.time()),peticionario)
      self.found=1
      self.cambios=1

  def id(self) :
    return self.clave

  def peticionario(self) :
    assert self.found
    return self.valor[1]

  def get_ts(self) :
    assert self.found
    return self.valor[0]


class handle_estado_cuentas(handle_db) :
  def __init__(self,fecha=None,txn=None) :
    handle_db.__init__(self,clave=fecha,txn=txn,db=db_varios)

  def valores(self) :
    if not self.valor : return []
    a=self.valor.items()
    a.sort() # In place
    return a

  def actualizar(self,peticionario,consumidos=0,generados=0,expirados=0) :
    assert (consumidos>0) or (generados>0) or (expirados>0)
    if not self.valor:
      self.valor={}
      self.found=1
    a=self.valor.get(peticionario,(0,0,0))
    a=(a[0]+generados,a[1]+consumidos,a[2]+expirados)
    self.valor[peticionario]=a

    self.cambios=1


class verificador(object):
  def __new__(cls) :
    if not cls.__dict__.has_key("obj") : # Se crea un singleton
      cls.obj=obj=object.__new__(cls)
      obj.ts_rnd=0
      obj.current_rnd="" # Sera sobreescrita a continuacion
      obj._regenera_rnd()
      obj.last_rnd=obj.current_rnd

    return cls.obj

  def _regenera_rnd(self) :
    import time
    a=time.time()
    if a-self.ts_rnd > 60*10 : # 10 min
      self.ts_rnd=a
      self.last_rnd=self.current_rnd
      import md5
      import random
      self.current_rnd=md5.new(self.current_rnd+repr(a)+repr(random.randrange(0,2^30))).digest()

  def ascii2base64(self,cadena) :
    a=((ord(cadena[0])<<8)+(ord(cadena[1])),(ord(cadena[2])<<8)+(ord(cadena[3])))
    return Olimpo.tools.inttobase64(a)

  def get(self,handle) :
    import md5
    self._regenera_rnd()
    a=md5.new(repr(handle)+self.current_rnd).digest()
    a=self.ascii2base64(a)
    return a[1:6] # Esto son 30 bits de aleatoriedad

  def es_valido(self,handle,valor) :
    import md5
    self._regenera_rnd()
    a=md5.new(repr(handle)+self.current_rnd).digest()
    a=self.ascii2base64(a)[1:6]
    if valor==a : return 1
    a=md5.new(repr(handle)+self.last_rnd).digest()
    a=self.ascii2base64(a)[1:6]
    if valor==a : return 1
    return 0


class timer(object) :
  def __new__(cls,tiempo,objeto) :
    if not cls.__dict__.has_key("obj") : # Se crea un singleton de "cola"
      cls.obj=object.__new__(cls)
      cls.obj.cola=[]
      cls.obj.next=0xffffffffL # Si no pongo L, lo considera -1

    tiempo=int(tiempo)
    obj=cls.obj
    obj.cola.append((tiempo,objeto))
    obj.cola.sort()
    if obj.next>obj.cola[0][0] :
      obj.next=obj.cola[0][0]
      Olimpo.notify.notifica_timer(tiempo,obj.gestor)
      
    return obj

  def gestor(self) :
    now=time.time()
    while len(self.cola) :
      if self.cola[0][0]>now : break
      a=self.cola.pop(0)
      a[1]() # Llama a la funcion diferida

    if len(self.cola) :
      self.next=self.cola[0][0]
      Olimpo.notify.notifica_timer(self.next,self.gestor)
    else :
      self.next=0xffffffffL # Si no pongo L, lo considera -1

class aleatorio(object) :
  def __new__(cls,seed=None) :
    if not cls.__dict__.has_key("obj") : # Se crea el singleton de "obj"
      cls.obj=obj=object.__new__(cls)
      import sha,random
      obj.hash=sha.new()
      obj.rnd=random.Random(obj.hash)
    else :
      obj=cls.obj

    if seed :
      obj.hash.update(str(obj.rnd.randrange(0,2147483647))+str(time.time())+str(Olimpo.tools.get_entropia())+str(seed))

    return obj

  def digest(self) :
    a=self.hash.copy()
    return a.digest()

class class_remitente:
  def __init__(self,remitente,yo):
    self.yo=yo
    self.handle=remitente
    self.flags=Olimpo.privmsg.lee_flags_nick(remitente)

  def es_ircop(self):
    return "o" in self.flags

  def es_oper(self):
    return "h" in self.flags

  def es_jcea(self) :
    flag=self.es_ircop() and self.es_oper()
    if not flag : return 0
    if self.home()!="gaia.irc-hispano.org" : return 0
    if self.nick_normalizado()!="jcea" : return 0
    return 1

  def es_sisco(self) :
    if not (self.es_ircop() and self.es_oper()) : return 0
    if self.nick_normalizado()!="sisco": return 0
    return 1

  def es_admin(self) :
    return admins.es_admin(self)

  def esta_registrado(self):
    return "r" in self.flags

  def tiene_nivel(self):
    return self.es_ircop() or self.es_oper()

  def home(self) :
    return Olimpo.tools.which_server(self.handle)

  def nick(self) :
    return Olimpo.privmsg.lee_nick(self.handle)

  def nick_normalizado(self) :
    return Olimpo.privmsg.lee_nick_normalizado(self.handle)

  def envia(self,msg) :
    Olimpo.privmsg.envia_nick(self.yo,self.handle,msg)


def creacion_tokens(num_tokens,peticionario) :
  tokens=[]
  txn=Olimpo.BerkeleyDB.txn()
  try :
    for i in xrange(num_tokens) :
      t=handle_token(txn=txn,peticionario=peticionario)
      t.update() # La graba en disco
      tokens.append(t.id())

    t=time.localtime()
    fecha="%.4d%.2d" %(t[0],t[1])
    cuentas=handle_estado_cuentas(fecha=fecha,txn=txn)
    cuentas.actualizar(peticionario=peticionario,generados=num_tokens)
    cuentas.update() # La graba en disco
  except :
    txn.aborta()
    raise

  txn.compromete()

  return tokens


class timer_expiracion_usuarios :
  def __init__(self) :
    self.last=" DUMMY"
    self.cache_expiraciones_proximas=[]
    self.modo_expira_cache=0

  def gestor(self) :
    now=time.time()
    if not Olimpo.bdd.flujo_abierto('w') :
      timer(now+60,self.gestor)
      return

    ts_expiracion=now-30*86400 # 30 dias
    ts_expiracion_proxima=ts_expiracion+4*3600 # 4 horas

    if self.modo_expira_cache :
      # No necesitamos ACID. Si casca, ya nos repasaremos al
      # colega en la siguiente pasada.
      txn=Olimpo.BerkeleyDB.txn(Olimpo.BerkeleyDB.txn.NO_SYNC)
      try :
        while len(self.cache_expiraciones_proximas) :
          if self.cache_expiraciones_proximas[0][0]>now : break
          t,nick=self.cache_expiraciones_proximas.pop(0)
          n2=handle_nick(nick,txn=txn)
          if not n2.found : continue
          if n2.get_ts()<ts_expiracion :
            Olimpo.bdd.envia_registro("w",nick,None)
            Olimpo.hace_log2("EXPIRACION de la IP Virtual Personalizada '%s': '%s' (Cache %d)" %(nick,n2.ip_virtual(),len(self.cache_expiraciones_proximas)))
            n2.delete()
        txn.compromete()
      except :
        txn.aborta()
        raise

      if len(self.cache_expiraciones_proximas) :
        timer(self.cache_expiraciones_proximas[0][0],self.gestor)
        return
      self.modo_expira_cache=0

    # Modo revision de base de datos

    # No necesitamos ACID. Si casca, ya nos repasaremos al
    # colega en la siguiente pasada.
    txn=Olimpo.BerkeleyDB.txn(Olimpo.BerkeleyDB.txn.NO_SYNC)
    len_expiraciones_cache=len(self.cache_expiraciones_proximas)
    try :
      n=handle_nick(self.last,txn=txn) # Dummy
      c=100
      nick_end,nick_start="*","*"  # Necesario por si el bucle ejecuta cero iteraciones
      nicks=[]
      for_completado=0
      for count in xrange(0,c):
        if not n.next() : break
        nick_end=n.nick()
        if not count : nick_start=n.nick()
        ts=n.get_ts()
        if ts<ts_expiracion :
          nicks.append(n.nick())
        elif ts<ts_expiracion_proxima :
          self.cache_expiraciones_proximas.append((ts+30*86400,n.nick())) # 30 dias
      else : # Se ejecuta cuando el FOR llega al final
        for_completado=1

      if not for_completado :
        self.last=" DUMMY"
        if not len(self.cache_expiraciones_proximas) : # No hay nada que expire pronto
          timer(now+4*3600,self.gestor) # 4 horas
        else :
          self.modo_expira_cache=1
          self.cache_expiraciones_proximas.sort() # Ordenado por expiracion
          timer(self.cache_expiraciones_proximas[0][0],self.gestor)
      else :
        self.last=n.nick()
        count+=1 # Para visualizar el contador correcto
        timer(now+60,self.gestor)

      i=len(self.cache_expiraciones_proximas)
      Olimpo.hace_log2("VERIFICAMOS la expiracion de %d IPs Virtuales Personalizadas ('%s'-'%s') (%f segundos) (Cache %d, %d)"
          %(count,nick_start,nick_end,time.time()-now,i,i-len_expiraciones_cache))

      # No usamos 'n' por el 'except'
      for i in nicks :
        Olimpo.bdd.envia_registro("w",i,None)
        n2=handle_nick(i,txn=txn)
        Olimpo.hace_log2("EXPIRACION de la IP Virtual Personalizada '%s': '%s'" %(i,n2.ip_virtual()))
        n2.delete()

      # Es necesario cerrar el cursor ANTES
      # de cerrar la transaccion asociada.
      n.next_end()
      txn.compromete()

    except :
      # Es necesario cerrar el cursor ANTES
      # de cerrar la transaccion asociada.
      n.next_end()
      txn.aborta()
      raise


class timer_expiracion_tokens :
  def __init__(self) :
    self.last=" DUMMY"

  def gestor(self) :
    now=time.time()

    ts_expiracion=now-6*30*86400 # 6 meses
    #ts_expiracion=now-180

    # No necesitamos ACID. Si casca, ya nos repasaremos el
    # TOKEN en la siguiente pasada.
    txn=Olimpo.BerkeleyDB.txn(Olimpo.BerkeleyDB.txn.NO_SYNC)
    try :
      t=handle_token(self.last,txn=txn) # Dummy
      c=100
      tokens=[]
      for_completado=0
      for count in xrange(0,c):
        if not t.next() : break
        # if t.peticionario()=="promocion2" : tokens.append(t.id())
        if t.get_ts()<ts_expiracion :
          tokens.append(t.id())
      else : # Se ejecuta cuando el FOR llega al final
        for_completado=1

      if not for_completado :
        self.last=" DUMMY"
        timer(now+22*3600+17*60+12,self.gestor) # Esperamos un tiempo alto y raro para evitar resonancias
      else :
        self.last=t.id()
        count+=1 # Para visualizar el contador correcto
        timer(now+60,self.gestor)

      Olimpo.hace_log2("VERIFICAMOS la expiracion de %d TOKENS (%f segundos)"
          %(count,time.time()-now))

      tiempo=time.localtime()
      fecha="%.4d%.2d" %(tiempo[0],tiempo[1])
      cuentas=handle_estado_cuentas(fecha=fecha,txn=txn)

      # No usamos 't' por el 'except'
      for i in tokens :
        t2=handle_token(i,txn=txn)
        peticionario=t2.peticionario()
        Olimpo.hace_log2("EXPIRACION del TOKEN '%s' de '%s'" %(i,peticionario))
        t2.delete()
        cuentas.actualizar(peticionario=peticionario,expirados=1)

      cuentas.update() # La graba en disco

      # Es necesario cerrar el cursor ANTES
      # de cerrar la transaccion asociada.
      t.next_end()
      txn.compromete()

    except :
      # Es necesario cerrar el cursor ANTES
      # de cerrar la transaccion asociada.
      t.next_end()
      txn.aborta()
      raise


def timer_regeneracion_clave_IP() :
  path="/export/home/irc/olimpo/claves_de_cifrado"
  now=time.time()
  timer(now+86400,timer_regeneracion_clave_IP) # Se ejecuta de nuevo en 24 horas

  if not Olimpo.bdd.flujo_abierto('z') : return

  rnd=[ord(i) for i in aleatorio(str(now)).digest()[:8]]
  clave_hi=(rnd[0]*256+rnd[1],rnd[2]*256+rnd[3])
  clave_lo=(rnd[4]*256+rnd[5],rnd[6]*256+rnd[7])
  clave=Olimpo.tools.inttobase64(clave_hi)+Olimpo.tools.inttobase64(clave_lo)
  f=open(path,"a")
  print >>f,"%s: %s" %(time.ctime(),clave)
  f.close()
  Olimpo.bdd.envia_registro("z","clave.de.cifrado.de.ips",clave)

  Olimpo.hace_log2("Generamos una nueva clave de proteccion de IPs")

  # No podemos permitir el lujo de que lo que sigue falle,
  # sin mayores consecuencias.

  import smtplib
  mail=smtplib.SMTP("mail2.argo.es")
  msg=open(path).read()
  msg="Subject: Claves de cifrado de IPs virtuales\n\n"+msg
  mail.sendmail("jcea@argo.es",["jcea@argo.es","sisco@lleida.net"],msg)
  mail.quit()
  Olimpo.hace_log2("Enviamos las claves por email")

  # Esto tambien puede fallar, y no pasa nada

  now=time.localtime()
  if now[2]%7 !=1 : return 

  this_month="%.4d%.2d" %(now[0],now[1])
  last_month="%.4d%.2d" %(now[0],now[1]-1)
  if now[1]==1 :
    last_month="%.4d%.2d" %(now[0]-1,12)

  def lista_cuentas(cuentas) :
    columnas=[0,15,30,45]
    a=[("Miembro","Generados","Consumidos","Expirados")]
    for i in cuentas :
      a.append((i[0],i[1][0],i[1][1],i[1][2]))

    b=[]
    for i in a :
      c=""
      for j in xrange(4) :
        w=columnas[j]-len(c)
        assert(c>=0)
        c+=" "*w+str(i[j])
      b.append(c)

    return b

  cuentas=handle_estado_cuentas(fecha=this_month)
  try :
    cuentas_este_mes=lista_cuentas(cuentas.valores())
  finally :
    cuentas.compromete_txn()

  cuentas=handle_estado_cuentas(fecha=last_month)
  try :
    cuentas_mes_pasado=lista_cuentas(cuentas.valores())
  finally :
    cuentas.compromete_txn()

  mail=smtplib.SMTP("mail2.argo.es")
  msg="Subject: Estadisticas de uso de 'Tokens' de IPs Virtuales Personalizadas\n"
  msg+="From: jcea@argo.es\n"
  msg+="To: administracion@lleida.net\n\n"

  msg+="Fecha: %s \n\n" %this_month
  for i in cuentas_este_mes :
    msg+=i+"\n"

  msg+="\n\nFecha: %s \n\n" %last_month
  for i in cuentas_mes_pasado :
    msg+=i+"\n"

  mail.sendmail("jcea@argo.es",["jcea@argo.es","sisco@lleida.net","administracion@lleida.net"],msg)
  mail.quit()

  Olimpo.hace_log2("Enviamos las estadisticas de 'TOKENS' por email")


def privmsg(nick,remitente,mensaje):
  def help(yo,remitente,parametros):
    remitente.envia("Para mas informacion de como obtener un token y poder activarte " 
      "tu IP Virtual, visita http://www.irc-hispano.org/servicios/ipvirtuales.html")
    remitente.envia(" ")

    if remitente.es_jcea() :
      remitente.envia("\002GENERA_TOKENS <numero> <peticionario>\002 (JCEA)")
      remitente.envia("   Proporciona el numero de tokens solicitado")

    if usuarios_generadores.validacion(remitente.handle) :
      remitente.envia("\002GENERA <num_tokens>\002 (nicks generadores)")
      remitente.envia("   Proporciona el numero de tokens solicitado")

    if remitente.es_jcea() or remitente.es_sisco() :
      remitente.envia("\002LISTGENERADOR\002 (JCEA o Sisco)")
      remitente.envia("   Lista los usuarios generadores actuales")
      remitente.envia("\002ADDGENERADOR <nick> <miembro> <nodo> [email]\002 (JCEA o Sisco)")
      remitente.envia("   Da de alta un nuevo usuario generador")
      remitente.envia("\002DELGENERADOR <nick>\002 (JCEA o Sisco)")
      remitente.envia("   Elimina un usuario generador")

      remitente.envia("\002LISTFILTRO\002 (JCEA o Sisco)")
      remitente.envia("   Lista los filtros activos")
      remitente.envia("\002ADDFILTRO <expresion regular>\002 (JCEA o Sisco)")
      remitente.envia("   Activa un filtro nuevo")
      remitente.envia("\002DELFILTRO <numero>\002 (JCEA o Sisco)")
      remitente.envia("   Elimina un filtro. El numero se sabe por el LISTFILTRO")

    if remitente.es_oper() and remitente.es_ircop() :
      remitente.envia("\002STATS <fecha>\002 (IRCops que sean OPERS tambien)")
      remitente.envia("   Le proporcionamos una fecha YYYYMM y nos da estadisticas de generacion y uso de 'tokens'")

    if remitente.es_oper() or remitente.es_ircop() :
      remitente.envia("\002DESCIFRA <clave> <ip virtual>\002 (Opers o IRCops)")
      remitente.envia("   Descifra una IP Virtual")

    remitente.envia("\002DESACTIVACION")
    remitente.envia("   Elimina una IP Virtual Personalizada")

    remitente.envia("\002ACTIVACION <token> <IP Virtual Personalizada>")
    remitente.envia("   Activa una IP Virtual Personalizada")

    remitente.envia("\002INFO <nick>")
    remitente.envia("   Muestra informacion sobre la IP Virtual Personalizada de un usuario")

    remitente.envia("\002HELP")
    remitente.envia("   Muestra este mensaje de ayuda")

    remitente.envia("\002Fin de HELP")

    return 1

  def desactivacion(yo,remitente,parametros) :
    if not Olimpo.bdd.flujo_abierto('w') : return

    if len(parametros)>2 :
      remitente.envia("Numero de parametros incorrecto")
      remitente.envia("\002Fin de DESACTIVACION")
      return 1 # Hace log

    if len(parametros)==0 :
      remitente.envia("La ejecucion de este comando supone la baja inmediata")
      remitente.envia("de la IP Virtual Personalizada del usuario. Si posteriormente")
      remitente.envia("desea reactivar la misma u otra IP Virtual Personalizada, debera")
      remitente.envia("conseguir otro \002TOKEN\002.")
      remitente.envia("Para que la baja de su IP Virtual Personalizada sea \002EFECTIVA\002,")
      remitente.envia('escriba "\002/msg %s DESACTIVACION %s".' %(modulo_nick,verificador().get(remitente.handle)))
      remitente.envia("\002Fin de DESACTIVACION")
      return 1 # Hace log

    if not verificador().es_valido(remitente.handle,parametros[0]) :
      remitente.envia("El \002verificador\002 que indica no es correcto. Comando ignorado.")
      remitente.envia("\002Fin de DESACTIVACION")
      return 1 # Hace log

    nick=remitente.nick_normalizado()
    n=handle_nick(nick)
    try :
      if n.found :
        remitente.envia("Su IP Virtual Personalizada '%s' ha sido \002ELIMINADA\002" %(n.ip_virtual()))
        Olimpo.bdd.envia_registro('w',nick,None)
        n.delete() # Primero imprimimos y luego borramos, por razones evidentes.
      else :
        remitente.envia("Este nick no tiene ninguna IP Virtual Personalizada asignada. Operacion ignorada.")

      remitente.envia("\002Fin de DESACTIVACION")
      n.compromete_txn()
      return 1 # Hace Log
    except :
      txn=n.get_txn()
      if txn:
        txn.aborta()
      raise

    return 1 # Hace Log


  def activacion(yo,remitente,parametros) :
    def verifica_filtros(remitente,ip) :
      import re
      # Ignoro entre minusculas y mayusculas.
      ip=ip.lower()
      for i in filtros :
        if not i[3] : # La cache
          i[3]=re.compile(i[0].lower())
        if i[3].search(ip) :
          remitente.envia("La IP introducida no es valida. El filtro '%s' no la permite" %i[0])
          return 1
      return 0

    if not remitente.esta_registrado() :
      remitente.envia("Para usar este comando debes tener el nick registrado en 'nick2'")
      remitente.envia("\002Fin de ACTIVACION")
      return 0 # No hace log

    if len(parametros)!=2 :
      remitente.envia("Debes indicar un \002TOKEN\002 y una IP Virtual personalizada")
      remitente.envia("\002Fin de ACTIVACION")
      return 1 # Hace log

    t=handle_token(parametros[0]) # Busca el Token en disco
    try :
      if t.found :
        if not hasattr(privmsg,"activacion_expresion_regular") :
          import re
          privmsg.activacion_expresion_regular= \
            re.compile("^([a-zA-Z0-9](|[-a-zA-Z0-9]*[a-zA-Z0-9])\.)*[a-zA-Z0-9](|[-a-zA-Z0-9]*[a-zA-Z0-9])$")

        flag=0
        ip=parametros[1]
        if len(ip)>35 :
          remitente.envia("La IP Virtual que solicitas es demasiado larga")
          flag=1
        elif not privmsg.activacion_expresion_regular.match(ip) :
          remitente.envia("La IP Virtual que solicitas contiene caracteres o un formato no validos")
          flag=1
        elif verifica_filtros(remitente,ip) :
          flag=1

        if not flag :
          nick=remitente.nick_normalizado()
          txn=t.get_txn()
          peticionario=t.peticionario()
          now=time.localtime()
          fecha="%.4d%.2d" %(now[0],now[1])
          cuentas=handle_estado_cuentas(fecha=fecha,txn=txn)
          cuentas.actualizar(peticionario=peticionario,consumidos=1)
          cuentas.update() # La graba en disco
          a=handle_nick(nick,txn=txn,token=t.id(),peticionario=peticionario,ip=ip)
          a.update() # La graba en disco
          Olimpo.hace_log2("TOKEN CONSUMIDO: %s %s %s %s" %(t.id(),peticionario,nick,ip))
          t.delete()
          remitente.envia("IP Virtual activada")
          Olimpo.bdd.envia_registro('w',nick,ip)
      else :
        remitente.envia("El \002TOKEN\002 que proporcionas no es valido. Ya se ha usado, no existe o ha caducado.")
    finally :
      t.compromete_txn()

    remitente.envia("\002Fin de ACTIVACION")
    return 1 # Hace log

  def info(yo,remitente,parametros) :
    if len(parametros)!=1 :
      remitente.envia("Debes indicar un nick")
      remitente.envia("\002Fin de INFO")
      return 1 # Log

    nick=Olimpo.tools.nick_normalizado(parametros[0])
    n=handle_nick(nick)
    if n.found :
      remitente.envia("La IP Virtual Personalizada asociada al nick especificado es '%s'" %n.ip_virtual())
      remitente.envia("Su fecha de activacion fue %s" %time.ctime(n.get_ts()))
    else :
      remitente.envia("El nick especificado no tiene una IP Virtual Personalizada asociada")
    n.compromete_txn()
    remitente.envia("\002Fin de INFO")
    return 1 # Log

  def listgenerador(yo,remitente,parametros) :
    if not (remitente.es_jcea() or remitente.es_sisco()) :
      return # No hace log

    for i in usuarios_generadores.lista_formateada() :
      remitente.envia(i)

    remitente.envia("\002Fin de LISTGENERADOR")
    return 1 # Hace log

  def addgenerador(yo,remitente,parametros) :
    if not (remitente.es_jcea() or remitente.es_sisco()) :
      return # No hace log

    l=len(parametros)

    if (l!=3) and (l!=4) :
      remitente.envia("Parametros incorrectos")
      remitente.envia("\002Fin de ADDGENERADOR")
      return 1 # Log

    nick=Olimpo.tools.nick_normalizado(parametros[0])
    miembro=parametros[1]
    nodo=parametros[2]
    if l==4 :
      email=parametros[3]
      import re
      email_valido=re.compile("^[-_A-Za-z0-9.]+@([-A-Za-z0-9]+\.)+[A-Za-z]+$")
      if not email_valido.search(email) :
        remitente.envia("La direccion de correo electronico que se indica no es valida")
        remitente.envia("\002Fin de ADDGENERADOR")
        return 1 # Log
    else :
      email=None

    txn=Olimpo.BerkeleyDB.txn()
    try :
      usuarios_generadores.nuevo(nick,miembro,nodo,remitente.nick(),email=email,txn=txn)
      txn.compromete()
    except :
      txn.aborta()
      raise
   
    remitente.envia("Doy de alta el nick especificado como generador")
    remitente.envia("\002Fin de ADDGENERADOR")
    return 1 # Log
    

  def delgenerador(yo,remitente,parametros) :
    if not (remitente.es_jcea() or remitente.es_sisco()) :
      return # No hace log

    if len(parametros)!=1 :
      remitente.envia("Debe indicar un nick")
      remitente.envia("\002Fin de DELGENERADOR")
      return 1 # Log

    nick=Olimpo.tools.nick_normalizado(parametros[0])
    txn=Olimpo.BerkeleyDB.txn()
    try :
      if usuarios_generadores.eliminar(nick,txn=txn) :
        remitente.envia("Nick generador eliminado")
      else :
        remitente.envia("El nick indicado no esta definido como generador")
      txn.compromete()
    except :
      txn.aborta()
      raise

    remitente.envia("\002Fin de DELGENERADOR")
    return 1 # Log 
  
  def listfiltro(yo,remitente,parametros) :
    if not (remitente.es_jcea() or remitente.es_sisco()) :
      return # No hace log

    j=0
    import time
    for f,nick,fecha,dummy in filtros :
      j+=1
      remitente.envia("%d. %s (%s - %s)" %(j,f,nick,time.ctime(fecha)))

    remitente.envia("\002Fin de LISTFILTRO")
    return 1 # Hace log

  def addfiltro(yo,remitente,parametros) :
    if not (remitente.es_jcea() or remitente.es_sisco()) :
      return # No hace log

    if len(parametros)!=1 :
      remitente.envia("Debes especificar una espresion regular, sin espacios")
      remitente.envia("\002Fin de ADDFILTRO")
      return 1 # Hace log

    exp=parametros[0]

    import re
    try : # Esto es para ver si la expresion regular es sintacticamente correcta.
      a=re.compile(exp.lower())
    except :
      a=None

    if not a :
      remitente.envia("La expresion regular que especificas es ilegal")
      remitente.envia("\002Fin de ADDFILTRO")
      return 1 # Hace log

    remitente.envia("Creamos un nuevo filtro cuya expresion regular es '%s'" %exp)

    for i in filtros :
      i[3]=None # Elimino la cache

    import time
    filtros.append([exp,remitente.nick(),int(time.time()),None])

    import cPickle
    f=cPickle.dumps(filtros)

    txn=Olimpo.BerkeleyDB.txn()
    try :
      db_varios.put(txn,"FILTROS",f)
    finally:
      txn.compromete()

    remitente.envia("\002Fin de ADDFILTRO")

    return 1 # Log


  def delfiltro(yo,remitente,parametros) :
    if not (remitente.es_jcea() or remitente.es_sisco()) :
      return # No hace log

    if len(parametros)!=1 :
      remitente.envia("Debes especificar un numero de filtro a eliminar")
      remitente.envia("\002Fin de ADDFILTRO")
      return 1 # Hace log

    try :
      n=int(parametros[0])
    except :
      n=0

    if (n<1) or (n>len(filtros)) :
      remitente.envia("El numero de filtro especificado no existe")
      remitente.envia("\002Fin de DELFILTRO")
      return 1 # Hace log

    f=filtros[n-1]
    filtros.pop(n-1)
    remitente.envia("Eliminamos el filtro '%s'" %f[0])

    for i in filtros :
      i[3]=None # Elimino la cache

    import cPickle
    f=cPickle.dumps(filtros)

    txn=Olimpo.BerkeleyDB.txn()
    try :
      db_varios.put(txn,"FILTROS",f)
    finally:
      txn.compromete()

    remitente.envia("\002Fin de DELFILTRO")

    return 1 # Log


  def stats(yo,remitente,parametros) :
    if not (remitente.es_oper() and remitente.es_ircop()) :
      return # No hace log

    if len(parametros)!=1 :
      remitente.envia("La sintasis es YYYYMM")
      remitente.envia("\002Fin de STATS")
      return 1 # Log

    cuentas=handle_estado_cuentas(fecha=parametros[0])
    c=cuentas.valores()
    cuentas.compromete_txn()
    for i in c :
      remitente.envia("'%s' - Generados: %d - Consumidos: %d - Expirados: %d" %(i[0],i[1][0],i[1][1],i[1][2]))

    remitente.envia("\002Fin de STATS")
    return 1 # Log

  def promocion(yo,remitente,parametros) :
    remitente.envia("La promocion de lanzamiento del servicio de \002IPs Virtuales Personalizadas\002 ya ha concluido, con un gran exito.")
    remitente.envia("Si quieres contratar el servicio, consulta los detalles en la siguiente direccion web: \002\0034http://www.irc-hispano.org/servicios/ipvirtuales.html")
    remitente.envia("\002Fin de PROMOCION")
    return 1 # Log

  def promocion_old(yo,remitente,parametros) :
    if not remitente.esta_registrado() :
      remitente.envia("Para usar este comando debes tener el nick registrado en 'nick2'")
      remitente.envia("\002Fin de PROMOCION")
      return 0 # No hace log

    if not hasattr(privmsg,"promocion_ultimo_TS") :
      privmsg.promocion_ultimo_TS=0

    if not hasattr(privmsg,"promocion_abusones") :
      privmsg.promocion_abusones={}

    nick=remitente.nick_normalizado()
    if privmsg.promocion_abusones.has_key(nick) :
      remitente.envia("Ya te he dado un 'token'. !!No abuses!!. Deja algo para los demas...")
      remitente.envia("\002Fin de PROMOCION")
      return 1 # LOG

    now=time.time()
    if now-privmsg.promocion_ultimo_TS<1 :
      remitente.envia("Lo siento, pero esta promocion SOLO otorga un nuevo 'token' gratuito por minuto, " \
                      "y ya se te ha adelantado otro usuario.")
      remitente.envia("Intentalo mas tarde, a ver si hay mas suerte };-)")
      remitente.envia("\002Fin de PROMOCION")
      return 1 # Log

    privmsg.promocion_abusones[nick]=1
    privmsg.promocion_ultimo_TS=now
    tokens=creacion_tokens(num_tokens=1,peticionario="promocion2")

    remitente.envia("Tu TOKEN es: \002%s" %tokens[0])
    remitente.envia("Utilizalo cuanto antes :-)")
  
    remitente.envia("\002Fin de PROMOCION")
    return 1 # Log


  def genera_tokens(yo,remitente,parametros) :
    if not remitente.es_jcea() :
      return # No hace log

    if len(parametros)!=2 :
      remitente.envia("Error de sintaxis")
      remitente.envia("\002Fin de GENERA_TOKENS")
      return 1 # Log

    try :
      num_tokens=int(parametros[0])
    except :
      num_tokens=-1

    if (num_tokens<1) or (num_tokens>1000) :
      remitente.envia("El numero de 'tokens' solicitado es ilegal")
      remitente.envia("\002Fin de GENERA_TOKENS")
      return 1 # Log

    peticionario=parametros[1]
    tokens=creacion_tokens(num_tokens,peticionario)

    for i in tokens :
      remitente.envia("TOKEN: %s" %i)

    remitente.envia("\002Fin de GENERA_TOKENS")
    return 1 # Log


  def genera(yo,remitente,parametros) :
    if not usuarios_generadores.validacion(remitente.handle) :
      return # No hace log

    if len(parametros)!=1 :
      remitente.envia("Error de sintaxis")
      remitente.envia("\002Fin de GENERA")
      return 1 # Log

    try :
      num_tokens=int(parametros[0])
    except :
      num_tokens=-1

    if (num_tokens<1) or (num_tokens>1000) :
      remitente.envia("El numero de 'tokens' solicitado es ilegal")
      remitente.envia("\002Fin de GENERA")
      return 1 # Log

    nick=remitente.nick_normalizado()
    peticionario=usuarios_generadores.get_miembro(nick)
    email=usuarios_generadores.get_email(nick)
    tokens=creacion_tokens(num_tokens,peticionario)

    if email :
      import smtplib
      mail=smtplib.SMTP("mail2.argo.es")
      msg="Subject: TOKENS %s\n" %peticionario
      msg+="From: jcea@argo.es\n"
      msg+="To: %s\n" %email
      msg+="\n"
      for i in tokens :
        msg+="TOKEN: %s\n" %i
      mail.sendmail("jcea@argo.es",email,msg)
      mail.quit()
    else :
      for i in tokens :
        remitente.envia("TOKEN: %s" %i)

    remitente.envia("Tokens enviados por correo electronico")
    remitente.envia("\002Fin de GENERA")
    return 1 # Log

  def descifra(yo,remitente,parametros) :
    if (not remitente.es_oper()) and (not remitente.es_ircop()) :
      return # No hace log

    if len(parametros)!=2 :
      remitente.envia("Sintaxis: DESCIFRA <clave> <IP virtual>")
      remitente.envia("\002Fin de DECIFRA")
      return

    clave,ipvirtual=parametros

    if len(clave)==12 :
      ipv=ipvirtual.split(".")
      if (ipvirtual[-8:]!=".virtual") or (len(ipv)<3) or (len(ipv[0])!=6) or (len(ipv[1])!=6) :
        remitente.envia("La IP Virtual especificada no es correcta")
      else :
        clave_hi=Olimpo.tools.base64toint(clave[:6])
        clave=clave_hi+Olimpo.tools.base64toint(clave[6:])
        ipv=Olimpo.tools.base64toint(ipv[0])+Olimpo.tools.base64toint(ipv[1])
        clave+=(0,0,0,0) # La clave solo usa 64 bits
        ipvirtual=Olimpo.tools.tea_decrypt(ipv,clave)
        if ipvirtual[0]==clave_hi[0] :
          remitente.envia("IP descifrada: %d.%d.%d.%d" %(ipvirtual[2]>>8,ipvirtual[2]&255,ipvirtual[3]>>8, ipvirtual[3]&255))
        else :
          remitente.envia("La IP virtual y la clave que se indican no coinciden")
    else :
      remitente.envia("La clave indicada no es correcta")

    remitente.envia("\002Fin de DECIFRA")

    return 1

  remitente=class_remitente(remitente,nick)

  comandos={"help":help,
            "genera":genera,
            "listgenerador":listgenerador,
            "addgenerador":addgenerador,
            "delgenerador":delgenerador,
            "listfiltro":listfiltro,
            "addfiltro":addfiltro,
            "delfiltro":delfiltro,
            "descifra":descifra,
            "genera_tokens":genera_tokens,
            "stats":stats,
            "promocion":promocion,
            "info":info,
            "desactivacion":desactivacion,
            "activacion":activacion,}

  parametros=mensaje.split()
  if not parametros : return # Mensaje vacio (por ejemplo, espacios o tabuladores)
  comando=parametros[0].lower()
  parametros=parametros[1:]

  if not comandos.has_key(comando) :
    return

  if comandos[comando](nick,remitente,parametros) :
    Olimpo.hace_log(remitente.handle,mensaje)

def fin() :
  global db_varios, db_tokens, db_nicks

  if db_varios :
    db_varios.cierra()
    db_varios=None

  if db_tokens :
    db_tokens.cierra()
    db_tokens=None

  if db_nicks :
    db_nicks.cierra()
    db_nicks=None


class nickdrop :
  def __init__(self,num_serie_inicial) :
    self.pendientes=[]
    Olimpo.notify_db.notifica_nickdrop(self.gestor,num_serie_inicial)

  def gestor(self,nick,num_serie):
    self.pendientes.append(nick)
    if not Olimpo.bdd.flujo_abierto('w') : return

    # Lo primero seria cepillarselos de cualquier tabla en
    # memoria, disco, etc.
    for i in self.pendientes :
      Olimpo.bdd.envia_registro('w',i,None)

    # Nos vale con transaccion ACI, en vez de ACID, porque si la perdemos,
    # sincronizaremos cuando recarguemos el modulo.
    txn=Olimpo.BerkeleyDB.txn(Olimpo.BerkeleyDB.txn.NO_SYNC)
    db_varios.put(txn, " NICKDROPS",repr(num_serie))
    for i in self.pendientes :
      usuarios_generadores.eliminar(i,txn=txn) 
      a=handle_nick(i,txn=txn)
      if a.found :
        a.delete()

    txn.compromete()

    self.pendientes=[]

def inicio():
  Olimpo.comentario_modulo("Gestion de IPs Virtuales $Revision: 1.252 $")
  global handle
  handle=Olimpo.privmsg.nuevo_nick(modulo_nick,"+odkirhB",privmsg)

  Olimpo.especifica_fin(fin)

  aleatorio("Aqui metemos cualquier cosa mas o menos poco predecible, como semilla inicial del ")
  aleatorio("generador de numeros aleatorios usados para generar las claves de cifrado de IPs Virtuales.")

  t=time.time()

  timer(t+9,timer_expiracion_usuarios().gestor)

  timer(t+27,timer_expiracion_tokens().gestor)

  while not None :
   t+=3600 # An~adimos una hora
   tm=time.gmtime(t)
   if tm[3]==4 : break

  timer(t,timer_regeneracion_clave_IP)

  global db_varios
  db_varios=Olimpo.BerkeleyDB.db("db.ipvirtual")
  txn=Olimpo.BerkeleyDB.txn(Olimpo.BerkeleyDB.txn.NO_SYNC)
  num_reg_nickdrop=db_varios.get(txn," NICKDROPS")[1]
  global filtros
  filtros=db_varios.get(txn,"FILTROS")[1]
  global usuarios_generadores
  usuarios_generadores=_usuarios_generadores(txn)
  txn.compromete()

  if filtros :
    import cPickle
    filtros=cPickle.loads(filtros)
  else :
    filtros=[]

  if num_reg_nickdrop : num_reg_nickdrop=int(num_reg_nickdrop)
  else : num_reg_nickdrop=0

  nickdrop(num_reg_nickdrop)

  global db_tokens
  db_tokens=Olimpo.BerkeleyDB.db("db.ipvirtual-tokens")

  global db_nicks
  db_nicks=Olimpo.BerkeleyDB.db("db.ipvirtual-nicks")


