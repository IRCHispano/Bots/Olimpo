# $Id: h.py,v 1.1 2003/07/22 17:33:23 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import Olimpo
import time
import cPickle

def grabacion() :
  import time
  for i in xrange(100) :
    sufijo="-%.4d%.2d%.2d" %time.localtime()[:3]


def inicio():
  import threading
  t=threading.Thread(target=grabacion)
  t.setDaemon(1)
  t.setName("Grabacion de logs")
  t.start()

  grabacion()
