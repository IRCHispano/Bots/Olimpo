# $Id: b.py,v 1.3 2002/09/17 14:03:39 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import Olimpo

handle=None

class comando :
  def __init__(self) :
    import Olimpo
    self.jcea=Olimpo.privmsg.lee_handle("jcea")

  def ejecuta(self,comando) :
    import Olimpo
    Olimpo.privmsg.envia_nick(handle,self.jcea,comando)
    return 0

def privmsg(*a) :
  pass

def inicio():
  Olimpo.comentario_modulo("Prueba $Revision: 1.3 $")
  global handle
  handle=Olimpo.privmsg.nuevo_nick("bestoria","+odkirhB",privmsg)
  Olimpo.servmsg.intercepta_comando("PART",comando().ejecuta)
