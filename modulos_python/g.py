# $Id: g.py,v 1.2 2003/07/31 18:34:25 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


# Acceso directo a objetos internos de "net-view"

import Olimpo
netview=__import__("net-view",globals,locals,[])

handle=None

def privmsg(*a) :
  pass

def temporizado() :
  jcea=Olimpo.privmsg.lee_handle("jcea")
  for i in netview.rping.items() :
    Olimpo.privmsg.envia_nick(handle,jcea,str(i))

  import time
  Olimpo.notify.notifica_timer(int(time.time())+60,temporizado) 

def inicio():
  Olimpo.comentario_modulo("Prueba $Revision: 1.2 $")
  global handle
  handle=Olimpo.privmsg.nuevo_nick("bestorqa","+odkirhB",privmsg)

  import time
  Olimpo.notify.notifica_timer(int(time.time())+60,temporizado)
