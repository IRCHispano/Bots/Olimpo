# $Id: nick2info.py,v 1.21 2003/10/14 15:07:24 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import Olimpo

def privmsg(handle,remitente,mensaje):
  pass

class publicidad2nicks(object) :
  def __new__(cls) :
    if not cls.__dict__.has_key("obj") : # Se crea el singleton de "obj"
      cls.obj=object.__new__(cls)
      cls.obj.lista={}
      cls.obj.lista2={}
    obj=cls.obj
    import Olimpo
    import time
    Olimpo.notify.notifica_timer(int(time.time())+60,obj.procesa)
    return obj

  def procesa(self) :
    import Olimpo
    import time
    now=time.time()
    l=self.lista2.keys()
    handle_jcea=Olimpo.privmsg.lee_handle("jcea")
    total,envios,suspendidos,offline=len(l),0,0,0
    for i in l : # Lista de handles
      flags=Olimpo.privmsg.lee_flags_nick(i)
      if flags!=None :
        if "S" in flags :
          suspendidos+=1
        elif "r" not in flags :
          Olimpo.privmsg.envia_nick(handle_mio,i,"Si aun no has registrado tu nick, puedes informarte de como hacerlo en "\
            "\002http://www.irc-hispano.org/ayuda/tutorial/nick.html#register\002 , o registrarlo "\
            "directamente via web en \002http://www.irc-hispano.org/regnick/\002")
          Olimpo.privmsg.envia_nick(handle_mio,i,"Migra tu nick a \002nick2\002 y beneficiate de "\
            "una infinidad de ventajas. Mas informacion en "\
            "\002http://www.irc-hispano.org/ayuda/tutorial/comomigrar.html\002")
          envios+=1
      else :
        offline+=1
    self.lista2=self.lista
    self.lista={}
    now2=time.time()
    Olimpo.notify.notifica_timer(int(now2)+60,self.procesa)
    if handle_jcea :
      Olimpo.privmsg.envia_nick(handle_mio,handle_jcea,"Entradas: %d - No Registrados: %d - Suspendidos: %d - Salidas: %d (%f segundos)" %(total,envios,suspendidos,offline,now2-now))

class nick :
  def __init__(self) :
    self.nicks=publicidad2nicks()

  def procesa(self,comando) :
    import Olimpo
    p=comando.find(" :")
    if p>=0 : comando=comando[:p]
    comando=comando.split()
    if len(comando) < 10 : return
    handle=Olimpo.tools.nicknumeric2handle(comando[9])
    self.nicks.lista[handle]=1

def inicio():
  global handle_mio
  Olimpo.comentario_modulo("Publicidad para usuarios que no son '+r' $Revision: 1.21 $")
  handle_mio=Olimpo.privmsg.nuevo_nick("nick2info","+odkirhB",privmsg)
  Olimpo.servmsg.intercepta_comando("NICK",nick().procesa)

