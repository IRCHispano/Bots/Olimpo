# $Id: olimpodapi.py,v 1.101 2003/04/11 15:40:43 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


nick=None
handle_nick=None
cola2thread=None
cola2main=None
objeto_arranque_thread=None

from Queue import Queue
import os
class _cola2main(Queue) :
  def __init__(self,id) :
    self.id=id
    Queue.__init__(self)

  def put_nowait(self,obj) :
    a=Queue.put_nowait(self,obj)
    import Olimpo
    Olimpo.ipc.IRQ_id(self.id)
    return a

  def put(self,obj,timeout) :
    a=Queue.put(self,obj,timeout)
    import Olimpo
    Olimpo.ipc.IRQ_id(self.id)
    return a

class _cola2thread(Queue) :
  def __init__(self,fd) :
    import os
    self.senalizacion=fd
    Queue.__init__(self)

  def __del__(self) :
    # Hace falta borrar el descriptor a saco, porque
    # sino no se cierra.
    import os
    os.close(self.senalizacion)

  def put_nowait(self,obj) :
    a=Queue.put_nowait(self,obj)
    os.write(self.senalizacion,"*") # Sen~alizacion
    return a

  def put(self,obj,timeout) :
    a=Queue.put(self,obj,timeout)
    os.write(self.senalizacion,"*") # Sen~alizacion
    return a
     
class class_remitente:
  def __init__(selfremitente,yo):
    self.yo=yo
    self.handle=remitente
    import Olimpo
    self.flags=Olimpo.privmsg.lee_flags_nick(remitente)

  def es_ircop(self):
    return "o" in self.flags

  def es_oper(self):
    return "h" in self.flags
  
  def es_jcea(self) :
    flag=self.es_ircop() and self.es_oper()
    if not flag : return 0
    if self.home()!="gaia.irc-hispano.org" : return 0
    if self.nick_normalizado()!="jcea" : return 0
    return 1
  
  def es_admin(self) :
    return self.esta_registrado() and self.main.admins.has_key(self.nick_normalizado())

  def es_usuario(self) :
    return self.esta_registrado() and self.main.usuarios.has_key(self.nick_normalizado())
  
  def esta_registrado(self):
    return "r" in self.flags
  
  def home(self) :
    import Olimpo
    return Olimpo.tools.which_server(self.handle)
  
  def nick(self) :
    import Olimpo
    return Olimpo.privmsg.lee_nick(self.handle)

  def nick_normalizado(self) :
    import Olimpo
    return Olimpo.privmsg.lee_nick_normalizado(self.handle)
  
  def envia(self,msg) :
    import Olimpo
    Olimpo.privmsg.envia_nick(self.yo,self.handle,msg)


class gestor_thread :
  def __init__(self,reservorio,fd,id_modulo,cola2main,cola2thread) :
    self.reservorio=reservorio # No queremos que se eliminen ciertas referencias
    self.senalizacion=fd
    self.id_modulo=id_modulo
    self.cola2main=cola2main
    self.cola2thread=cola2thread

  def gestor(self) :
    import os
    import Olimpo
    from select import select
    aA=Olimpo.ipc.existe_id_modulo
    bB=self.id_modulo

    try :
      import socket
      listen=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
      listen.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

      listen.bind(("127.0.0.1",7776))
      listen.listen(5)
      listen.setblocking(0)

      while 1 :
        r,w,e=select([self.senalizacion,listen],[],[],5)

        if not aA(bB) :
          return # Terminamos

        if not len(r) : continue

        for i in r :
          if i==self.senalizacion :
            while 1 : # Intenta procesar una rafaga
              try :
                os.read(self.senalizacion,1) # Eliminamos la senalizacion
                q=self.cola2thread.get_nowait()
              except:
                break

              q.procesa()
          elif i==listen :
            w,dummy=listen.accept()
            w.send("hasta luego\n")
            w.close()
          else :
            pass

    finally :
      del self.reservorio
      if Olimpo.ipc.existe_id_modulo(self.id_modulo) :
        import time
        time.sleep(1)
        self.cola2main.put_nowait(objeto_arranque_thread) # 'bootstrap' de la multitarea
      

class privmsg :
  def procesa(self,nick,remitente,mensaje):
    comandos={"help":self.m_help,
      "adminlist":self.m_adminlist,"adminadd":self.m_adminadd,"admindel":self.m_admindel,
      "userlist":self.m_userlist,"useradd":self.m_useradd,"userdel":self.m_userdel}

    if pc_directo :
      comandos["sendnotice"]=self.m_sendnotice
      comandos["senddx"]=self.m_senddx

    msg=mensaje.split()
    if not msg : return # Mensaje vacio (por ejemplo, espacios o tabuladores)
    cmd=msg[0].lower()

    remitente=class_remitente(self.main,remitente,nick)

    if comandos.has_key(cmd) :
      comandos[cmd](remitente,mensaje,cmd,msg)

  def nick_valido(self,nick) :
    import Olimpo
    handle=Olimpo.privmsg.lee_handle(nick)
    if not handle :
      return "El nick especificado debe estar 'online' para realizar esta operacion"
    flags=Olimpo.privmsg.lee_flags_nick(handle)
    if "r" not in flags :
      return "El nick especificado debe tener modo +r (registrado en 'nick2') para realizar esta operacion"

  def m_adminlist(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    Olimpo.hace_log(remitente.handle,mensaje)
    remitente.envia("Listado de administradores de \002%s" %self.main.nick)
    a=self.main.admins.keys()
    a.sort() # In-Place
    for i in a :
      remitente.envia(i)
    remitente.envia("\002Fin de AdminList") 

  def m_userlist(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    Olimpo.hace_log(remitente.handle,mensaje)
    remitente.envia("Listado de usuarios registrados de \002%s" %self.main.nick)
    a=self.main.usuarios.keys()
    a.sort() # In-Place
    for i in a :
      remitente.envia(i)
    remitente.envia("\002Fin de UserList")

  def m_adminadd(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    if remitente.es_jcea() :
      if len(msg)==2 :
        Olimpo.hace_log(remitente.handle,mensaje)
        nick=Olimpo.tools.nick_normalizado(msg[1])
        msg=self.nick_valido(nick)
        if not msg :
          self.main.admins[nick]=1
          txn=Olimpo.BerkeleyDB.txn()
          self.main.db.put(txn,"admins"," ".join(self.main.admins.keys()))
          txn.compromete()
          remitente.envia("Ahora \002%s\002 ya es admin en \002%s" %(nick,self.main.nick))
        else :
          remitente.envia(msg)
      else :
        remitente.envia("Numero de parametros incorrecto")
    else :
      remitente.envia("No tienes permiso para realizar esta operacion")

    remitente.envia("\002Fin de AdminAdd")

  def m_useradd(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    if remitente.es_jcea() or remitente.es_admin() :
      if len(msg)==2 :
        Olimpo.hace_log(remitente.handle,mensaje)
        nick=Olimpo.tools.nick_normalizado(msg[1])
        msg=self.nick_valido(nick)
        if not msg :
          self.main.usuarios[nick]=1
          txn=Olimpo.BerkeleyDB.txn()
          self.main.db.put(txn,"usuarios"," ".join(self.main.usuarios.keys()))
          txn.compromete()
          remitente.envia("Ahora \002%s\002 ya es un usuario registrado en \002%s" %(nick,self.main.nick))
        else :
          remitente.envia(msg)
      else :
        remitente.envia("Numero de parametros incorrecto")
    else :
      remitente.envia("No tienes permiso para realizar esta operacion")

    remitente.envia("\002Fin de UserAdd")

  def m_admindel(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    if remitente.es_jcea() or remitente.es_admin() :
      if len(msg)==2 :
        Olimpo.hace_log(remitente.handle,mensaje)
        nick=Olimpo.tools.nick_normalizado(msg[1])
        if self.main.admins.has_key(nick) :
          del self.main.admins[nick]
          txn=Olimpo.BerkeleyDB.txn()
          self.main.db.put(txn,"admins"," ".join(self.main.admins.keys()))
          txn.compromete()
          remitente.envia("El admin \002%s\002 ha sido eliminado de \002%s" %(nick,self.main.nick))
        else :
          remitente.envia("\002%s\002 no es admin en \002%s" %(nick,self.main.nick))
      else :
        remitente.envia("Numero de parametros incorrecto")
    else :
      remitente.envia("No tienes permiso para realizar esta operacion")
  
    remitente.envia("\002Fin de AdminDel")

  def m_userdel(self,remitente,mensaje,cmd,msg) :
    import Olimpo
    if remitente.es_jcea() or remitente.es_admin() :
      if len(msg)==2 :
        Olimpo.hace_log(remitente.handle,mensaje)
        nick=Olimpo.tools.nick_normalizado(msg[1])
        if self.main.usuarios.has_key(nick) :
          del self.main.usuarios[nick]
          txn=Olimpo.BerkeleyDB.txn()
          self.main.db.put(txn,"usuarios"," ".join(self.main.usuarios.keys()))
          txn.compromete()
          remitente.envia("El usuario \002%s\002 ha sido eliminado de \002%s" %(nick,self.main.nick))
        else :
          remitente.envia("\002%s\002 no es usuario registrado de \002%s" %(nick,self.main.nick))
      else :
        remitente.envia("Numero de parametros incorrecto")
    else :
      remitente.envia("No tienes permiso para realizar esta operacion")
 
    remitente.envia("\002Fin de UserDel")

  def m_sendnotice(self,remitente,mensaje,cmd,msg) :
    if len(msg)<2 :
      return

    if not (remitente.es_jcea() or remitente.es_admin() or remitente.es_usuario()) :
      return

    import Olimpo
    Olimpo.hace_log(remitente.handle,mensaje)

    mensaje=mensaje[mensaje.find(" ")+1:]

    self.main.envia_cluster("PC12^%s^*^%s^ ^%s^0^H25^~" %(remitente.nick().upper(),mensaje,self.main.indicativo),loopback=1)

    remitente.envia("Texto enviado")
    remitente.envia("\002Fin de SendNotice")


  def m_senddx(self,remitente,mensaje,cmd,msg) :
    if len(msg)<4 :
      return

    if not (remitente.es_jcea() or remitente.es_admin() or remitente.es_usuario()) :
      return

    import Olimpo
    Olimpo.hace_log(remitente.handle,mensaje)

    mensaje=mensaje[mensaje.find(" ")+1:] # Eliminamos el comando
    mensaje=mensaje[mensaje.find(" ")+1:] # Eliminamos la frecuencia
    mensaje=mensaje[mensaje.find(" ")+1:] # Eliminamos el indicativo

    if len(mensaje)>20 :
      remitente.envia("Texto demasiado largo")
      remitente.envia("\002Fin de SendNotice")
      return

    import time
    now_gm=time.gmtime()
    self.main.envia_cluster("PC11^%s^%s^%s^%sZ^%s^%s^%s^H25^~" \
      %(msg[1], \
        msg[2].upper(), \
        time.strftime("%e-%b-%Y",now_gm), \
        time.strftime("%H%M",now_gm), \
        mensaje, \
        remitente.nick().upper(), \
        self.main.indicativo.upper()),loopback=1)

    remitente.envia("Texto enviado")
    remitente.envia("\002Fin de SendDX")
    
  def m_help(self,remitente,mensaje,cmd,msg) :
      import Olimpo
      Olimpo.hace_log(remitente.handle,mensaje)
      remitente.envia("DX-Cluster $Revision: 1.101 $")
      remitente.envia("Este bot envia la informacion DX-Cluster al canal %s de IRC-Hispano. Mas informacion en \002http://www.argo.es/~jcea/irc/modulos/dxcluster.htm" %self.main.canales_dx_cluster[0]) # El primero es el oficial
      remitente.envia("\002ADMINLIST")
      remitente.envia("     Proporciona la lista de administradores de %s" %self.main.nick)
      remitente.envia("\002ADMINADD\002 (para uso exclusivo de JCEA)")
      remitente.envia("     An~ade un admin de %s" %self.main.nick)
      remitente.envia("\002ADMINDEL\002 (para uso de admins de %s)" %self.main.nick)
      remitente.envia("     Elimina un administrador de  %s" %self.main.nick)
      remitente.envia("\002USERLIST")
      remitente.envia("     Proporciona la lista de usuarios registrados en %s" %self.main.nick)
      remitente.envia("\002USERADD\002 (para uso de admins de %s)" %self.main.nick)
      remitente.envia("     Registra un usuario en %s" %self.main.nick)
      remitente.envia("\002USERDEL\002 (para uso de admins de %s)" %self.main.nick)
      remitente.envia("     Elimina un usuario registrado en %s" %self.main.nick)

      if self.main.pc_directo :
        remitente.envia("\002SENDNOTICE <texto>\002 (para usuarios registrados en %s)" %self.main.nick)
        remitente.envia("     Envia un texto a toda la red")
        remitente.envia("\002SENDDX <freq> <indicativo> <msg>\002 (para usuarios registrados en %s)" %self.main.nick)
        remitente.envia("     Comunica un DX a la red")

      remitente.envia("\002HELP")
      remitente.envia("     Muestra esta ayuda")
      remitente.envia("Para cualquier duda o consulta, buscar el nick 'jcea' o enviar un mensaje a 'jcea@argo.es'")
      remitente.envia("\002Fin de HELP")
    

class lanza_thread :
  def __init__(self,fd) :
    import os
    self.fd=fd

  def __del__(self) :
    # Hace falta borrar el descriptor a saco, porque
    # sino no se cierra.
    import os
    os.close(self.fd)

  def procesa(self) :
    import threading
    t=threading.Thread(target=gestor_thread(self,self.fd,id_modulo,cola2main,cola2thread).gestor)
    t.setDaemon(1)
    t.setName("Olimpo Distributed API")
    t.start()
   
class IRQ_handler :
  def procesa(self) :
    import Olimpo

    # No hay "race condition" porque si damos la cola como vacia,
    # volvemos a entrar con el nuevo IRQ.
    while not cola2main.empty() :
      try :
        cola2main.get_nowait().procesa()
      except :
        Olimpo.ipc.IRQ_id(id_modulo) # Si ocurre un problema, debemos procesar el resto de la cola
        raise

def fin() :
  # Eliminamos la cache, por si se modifica la libreria
  import libolimpodapi
  import sys
  del sys.modules["libolimpodapi"]

def inicio() :
  global nick
  global handle_nick
  global id_modulo
  global cola2thread
  global cola2main
  global objeto_arranque_thread

  import Olimpo
  Olimpo.comentario_modulo("Olimpo Distributed API $Revision: 1.101 $")

  nick="dapi"

  id_modulo=Olimpo.ipc.id_modulo(Olimpo.ipc.nombre_modulo())

  import os
  fds=os.pipe()
  cola2thread=_cola2thread(fds[1])
  cola2main=_cola2main(id_modulo)

  handle_nick=Olimpo.privmsg.nuevo_nick(nick,"+odkirhB",privmsg().procesa)
  Olimpo.especifica_fin(fin)

  Olimpo.ipc.IRQ_handler(IRQ_handler().procesa)

  objeto_arranque_thread=lanza_thread(fds[0])
  cola2main.put_nowait(objeto_arranque_thread) # 'bootstrap' de la multitarea

  #Olimpo.notify_db.notifica_nickdrop(nickdrop().procesa,num_reg_nickdrop)
