# $Id: chanfollower.py,v 1.72 2002/10/04 18:48:56 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import Olimpo
import time

canales={}  # Canal -> diccionario de usuarios -> '1'
usuarios={} # Usuario -> diccionario de canales -> '1'
singletons={} # Objeto -> objeto (el mismo)
topics={} # Canal -> TS+" "+Usuario+":"+texto
modos={} # Canal -> (flags,limite,clave)

# Modos -> modos (las letras ordenadas por orden alfabetico)
singletons_modos={
  "+":"+",
  "+nt":"+nt",
}

# Modos mas usuales
canal_sin_modos=("+",None,None)
canal_modo_nt=("+nt",None,None)
singletons_modos2={
  canal_sin_modos:canal_sin_modos,
  canal_modo_nt:canal_modo_nt,
}
  



def lista_canales_normalizados(lista) :
  lista="".join([lista_canales_normalizados.tabla[ord(i)] for i in lista])
  c=[]
  for i in lista.split(',') :
    if i=='0' :
      c=[None]
    elif i[0]=='#' :
      c.append(i)
  return c

a=['\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07',
'\x08', '\x09', '\x0a', '\x0b', '\x0c', '\x0d', '\x0e', '\x0f',
'\x10', '\x11', '\x12', '\x13', '\x14', '\x15', '\x16', '\x17',
'\x18', '\x19', '\x1a', '\x1b', '\x1c', '\x1d', '\x1e', '\x1f',
' ',    '!',    '"',    '#',    '$',    '%',    '&', '\x27',
'(',    ')',    '*',    '+',    ',',    '-',    '.',    '/',
'0',    '1',    '2',    '3',    '4',    '5',    '6',    '7',
'8',    '9',    ':',    ';',    '<',    '=',    '>',    '?',
'@',    'a',    'b',    'c',    'd',    'e',    'f',    'g',
'h',    'i',    'j',    'k',    'l',    'm',    'n',    'o',
'p',    'q',    'r',    's',    't',    'u',    'v',    'w',
'x',    'y',    'z',    '{',    '|',    '}',    '~',    '_',
'`',    'a',    'b',    'c',    'd',    'e',    'f',    'g',
'h',    'i',    'j',    'k',    'l',    'm',    'n',    'o',
'p',    'q',    'r',    's',    't',    'u',    'v',    'w',
'x',    'y',    'z',    '{',    '|',    '}',    '~', '\x7f',
'\x80', '\x81', '\x82', '\x83', '\x84', '\x85', '\x86', '\x87',
'\x88', '\x89', '\x8a', '\x8b', '\x8c', '\x8d', '\x8e', '\x8f',
'\x90', '\x91', '\x92', '\x93', '\x94', '\x95', '\x96', '\x97',
'\x98', '\x99', '\x9a', '\x9b', '\x9c', '\x9d', '\x9e', '\x9f',
'\xa0', '\xa1', '\xa2', '\xa3', '\xa4', '\xa5', '\xa6', '\xa7',
'\xa8', '\xa9', '\xaa', '\xab', '\xac', '\xad', '\xae', '\xaf',
'\xb0', '\xb1', '\xb2', '\xb3', '\xb4', '\xb5', '\xb6', '\xb7',
'\xb8', '\xb9', '\xba', '\xbb', '\xbc', '\xbd', '\xbe', '\xbf',
'\xe0', '\xe1', '\xe2', '\xe3', '\xe4', '\xe5', '\xe6', '\xe7',
'\xe8', '\xe9', '\xea', '\xeb', '\xec', '\xed', '\xee', '\xef',
'\xd0', '\xf1', '\xf2', '\xf3', '\xf4', '\xf5', '\xf6', '\xd7',
'\xf8', '\xf9', '\xfa', '\xfb', '\xfc', '\xfd', '\xfe', '\xdf',
'\xe0', '\xe1', '\xe2', '\xe3', '\xe4', '\xe5', '\xe6', '\xe7',
'\xe8', '\xe9', '\xea', '\xeb', '\xec', '\xed', '\xee', '\xef',
'\xf0', '\xf1', '\xf2', '\xf3', '\xf4', '\xf5', '\xf6', '\xf7',
'\xf8', '\xf9', '\xfa', '\xfb', '\xfc', '\xfd', '\xfe', '\xff']

assert(len(a)==256)
lista_canales_normalizados.tabla="".join(a)
del a

def imprime_error(valor) :
  import sys
  print >>sys.stderr,str(valor)

def salida(handle) :
  try :
    u=usuarios.get(handle)
    if not u : # No sabemos nada de ese usuario
      # import Olimpo
      # imprime_error("%s (%d): %s " %(Olimpo.privmsg.lee_nick(handle),handle,Olimpo.tools.which_server(handle)))
      return
    for i in u.keys() : # Recorremos sus canales
      if len(canales[i])==1 : # El ultimo que quedaba
        del canales[i]
        del topics[i]
        del modos[i]
        del singletons[i]
      else :
        del canales[i][handle]
    del usuarios[handle]
    del singletons[handle]
  except :
    imprime_error(handle)
    raise

def KICK(comando) :
  # numeric KICK #canal numeric [:razon]
  try :
    assert(comando[0]!=':')
    cmd=comando.split()
    handle=cmd[3]
    l=len(handle)
    assert((l==3) or (l==5))
    handle=Olimpo.tools.nicknumeric2handle(handle)
    u=usuarios.get(handle)
    if not u : return 0 # No sabemos nada de ese usuario
    lista_canales=lista_canales_normalizados(cmd[2])
    for i in lista_canales : # Recorremos los canales de los que se echa al usuario
      if u.has_key(i) :
        del u[i]
        if len(canales[i]) == 1 : # El ultimo que quedaba
          del canales[i]
          del topics[i]
          del modos[i]
          del singletons[i]
        else :
          del canales[i][handle]
    if not len(u) : # Se ha salido de todos sus canales
      del usuarios[handle]
      del singletons[handle]
  except :
    imprime_error(comando)
    raise

  return 0 # Para futuras ampliaciones

def PART(comando) :
  # numeric PART #canal[,#canal] [:razon]
  try :
    assert(comando[0]!=':')
    cmd=comando.split()
    handle=cmd[0]
    l=len(handle)
    assert((l==3) or (l==5))
    handle=Olimpo.tools.nicknumeric2handle(handle)
    u=usuarios.get(handle)
    if not u : return 0 # No sabemos nada de ese usuario
    lista_canales=lista_canales_normalizados(cmd[2])
    for i in lista_canales : # Recorremos los canales de los que se echa al usuario
      if u.has_key(i) :
        del u[i]
        if len(canales[i]) == 1 : # El ultimo que quedaba
          del canales[i]
          del topics[i]
          del modos[i]
          del singletons[i]
        else :
          del canales[i][handle]
    if not len(u) : # Se ha salido de todos sus canales
      del usuarios[handle]
      del singletons[handle]
  except :
    imprime_error(comando)
    raise

  return 0 # Para futuras ampliaciones

def JOIN(comando) :
  # numeric JOIN #canal[,#canal] [clave o TS[,clave o TS]]
  try :
    assert(comando[0]!=':')
    cmd=comando.split()
    handle=cmd[0]
    l=len(handle)
    assert((l==3) or (l==5))

    lista_canales=lista_canales_normalizados(cmd[2])
    if not len(lista_canales) : return 0  # Puede entrar, por ejemplo, en un canal '+'

    handle=Olimpo.tools.nicknumeric2handle(handle)

    u=usuarios.get(handle)
    if not u :
      singletons[handle]=handle
      u={}  # Queremos trabajar con el mismo diccionario, porque es mutable
      usuarios[handle]=u # La primera noticia que tenemos del usuario
    else :
      handle=singletons[handle]

    if not lista_canales[0] : # JOIN 0
      for i in u.keys() : # Recorremos sus canales
        if len(canales[i])==1 : # El ultimo que quedaba
          del canales[i]
          del topics[i]
          del modos[i]
          del singletons[i]
        else :
          del canales[i][handle]
      lista_canales=lista_canales[1:]
      u={} # Queremos trabajar con el mismo diccionario, porque es mutable
      usuarios[handle]=u
      if not len(lista_canales) : # Canales adicionales ????
        del usuarios[handle] # Se pira de todos los canales
        del singletons[handle]

    for i2 in lista_canales :
      i=singletons.get(i2)
      if i==None: i=singletons[i2]=i2

      u[i]=1
      c=canales.get(i)
      if c :
        c[handle]=1
      else : # La primera noticia que tenemos de ese canal
        canales[i]={handle:1}
        topics[i]=None
        modos[i]=canal_sin_modos

  except :
    imprime_error(comando)
    raise

  return 0 # Para futuras ampliaciones

def BURST(comando) :
  try :
    assert(comando[0]!=':')
    c=comando
    l=c.find(" :") # De momento ignoramos los BANEOS
    if l>=0 : c=c[:l]
    cmd=c.split()
    if len(cmd) < 5 : return 0 # Para futuras ampliaciones
    canal=lista_canales_normalizados(cmd[2])
    l=len(canal)
    if l!=1 :
      assert(not l)
      return 0  # Puede entrar, por ejemplo, en un canal '+'

    canal2=canal[0]
    canal=singletons.get(canal2)
    if canal==None : canal=singletons[canal2]=canal2

    t=4
    m=cmd[4] # ?Modos?
    if m[0]=='+' :
      limite=clave=None
      t+=1
      if 'l' in m :
        limite=int(cmd[t])
        t+=1
      if 'k' in m :
        clave=cmd[t]
        t+=1

      m=list(m)
      m.sort()
      m="".join(m)
      try :
        m=singletons_modos[m]
      except : # Esto ocurre muy de vez en cuando
        singletons_modos[m]=m
      m=(m,limite,clave)
      m2=singletons_modos2.get(m) # Modos mas habituales
      if m2 : m=m2
    else :
      m=canal_sin_modos

    u=cmd[t]
    c=canales.get(canal) # El diccionario es mutable
    for i in u.split(",") :
      i=Olimpo.tools.nicknumeric2handle(i.split(":")[0]) # Ignoramos modos

      j=usuarios.get(i) # El diccionario es mutable
      if j :
        j[canal]=1
        i=singletons[i]
      else :
        usuarios[i]={canal:1}
        singletons[i]=i

      if c :
        c[i]=1
      else :
        c={i:1} # El diccionario se puede mutar en la siguiente iteracion
        canales[canal]=c
        topics[canal]=None
        modos[canal]=m
        

  except :
    imprime_error(comando)
    raise

  return 0 # Para futuras ampliaciones

def CREATE(comando) :
  # numeric CREATE #canal[,#canal] TS
  try :
    assert(comando[0]!=':')
  except :
    imprime_error(comando)
    raise

  return JOIN(comando)

  return 0 # Para futuras ampliaciones

def TOPIC(comando) :
  # :nick TOPIC #canal :TEXTO
  try :
    assert(comando[0]==':')
    cmd=comando.split()
    canal=lista_canales_normalizados(cmd[2])
    l=len(canal)
    if l!=1 :
      assert(not l)
      return 0  # Puede entrar, por ejemplo, en un canal '+'
    c=singletons.get(canal[0])
    if c==None:
      return 0 # No sabemos nada de ese canal
    # Usamos %d para que trunque y el resultado sea mas corto
    topics[c]="%d %s:%s" %(time.time(),cmd[0][1:],comando[comando[1:].find(':')+2:]) # El +2 compensa el [1:]
  except :
    imprime_error(comando)
    raise

def MODE(comando) :
  # :nick TOPIC #canal modos
  try :
    assert(comando[0]==':')
    cmd=comando.split()
    if cmd[2][0]!='#' : return 0 # De usuario o de canales sin modo
    canal=lista_canales_normalizados(cmd[2])
    l=len(canal)
    if l!=1 :
      assert(not l)
      return 0  # Puede entrar, por ejemplo, en un canal '+'
    c=singletons.get(canal[0])
    if c==None:
      return 0 # No sabemos nada de ese canal
    m,limite,clave=modos[c]
    m=list(m)
    add=1
    t=4
    for i in cmd[3] :
      if i=='+' :
        add=1
        continue
      elif i=='-' :
        add=0
        continue

      if add :
        if i in 'ovblk' : # Modos "largos"
          if i=='o' :
            t+=1
          elif i=='v' :
            t+=1
          elif i=='b' :
            t+=1
          elif i=='l' :
            limite=int(cmd[t])
            if i not in m :
              m.append(i)
            t+=1
          elif i=='k' :
            clave=cmd[t]
            m.append(i)
            t+=1
        else : # Modos "cortos"
          m.append(i)
      else : # Borrado de modos
        if i in 'ovblk' : # Modos "largos"
          if i=='o' :
            t+=1
          elif i=='v' :
            t+=1
          elif i=='b' :
            t+=1
          elif i=='l' :
            limite=None # No incrementamos el puntero
            m.remove(i)
          elif i=='k' :
            clave=None  # No incrementamos el puntero
            m.remove(i)
        else : # Modos "cortos"
          m.remove(i)

    m.sort()
    m="".join(m)
    m2=singletons_modos.get(m)
    if m2 :
      m=m2
    else :
      singletons_modos[m]=m

    m=(m,limite,clave)

    m2=singletons_modos2.get(m)
    if m2 : m=m2

    modos[c]=m
  except :
    imprime_error(comando)
    raise


def privmsg(handle,remitente,mensaje):
  Olimpo.privmsg.envia_nick(handle,remitente,"%d usuarios en %d canales" %(len(usuarios),len(canales)))
  if mensaje[0]=="#" :
    canal=lista_canales_normalizados(mensaje)[0]
    l=canales.get(canal,0)
    if l : l=len(l)
    Olimpo.privmsg.envia_nick(handle,remitente,"%d usuarios en '%s'" %(l,canal))
    if l :
      Olimpo.privmsg.envia_nick(handle,remitente,"Topic: '%s'" %topics[canal])
      Olimpo.privmsg.envia_nick(handle,remitente,"Modos: %s" %repr(modos[canal]))
  elif mensaje!="." :
    l=Olimpo.privmsg.lee_handle(mensaje)
    l=usuarios.get(l,0)
    if l : l=len(l)
    Olimpo.privmsg.envia_nick(handle,remitente,"El usuario '%s' esta en %d canales" %(mensaje,l))

    try :
      i=int(mensaje)
    except :
      i=None

    if i!=None :
      Olimpo.privmsg.envia_nick(handle,remitente,"%d: %s" %(i,Olimpo.tools.handle2nicknumeric(i)))
  else :
    for i in usuarios.keys() :
      if not Olimpo.privmsg.lee_nick(i) :
        Olimpo.privmsg.envia_nick(handle,remitente,"%d: %s" %(i,str(usuarios[i])))

def inicio():
  Olimpo.comentario_modulo("Seguidor del estado de canales $Revision: 1.72 $")
  Olimpo.servmsg.intercepta_comando("BURST",BURST)
  Olimpo.servmsg.intercepta_comando("CREATE",CREATE)
  Olimpo.servmsg.intercepta_comando("JOIN",JOIN)
  Olimpo.servmsg.intercepta_comando("PART",PART)
  Olimpo.servmsg.intercepta_comando("KICK",KICK)
  Olimpo.servmsg.intercepta_comando("TOPIC",TOPIC)
  Olimpo.servmsg.intercepta_comando("MODE",MODE)
  Olimpo.notify.notifica_nick_salida(salida)

  Olimpo.privmsg.nuevo_nick("xdrqn3a","+odkirhB",privmsg)

