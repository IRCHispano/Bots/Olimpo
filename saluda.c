/*
** SALUDA_C
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

/*
    $Id: saluda.c,v 1.13 2001/03/19 14:31:49 jcea Exp $
*/

#include <stdio.h>

#include "module.h"
#include "mod_privmsg.h"


static int handle;

static void privmsg(int nick, int remitente, char *mensaje)
{
  char flags[1024];
  char nick_u[1024];
  char buf[1024];

  flags[0] = '\0';
  lee_flags_nick(remitente, flags);

  nick_u[0] = '\0';
  lee_nick(remitente, nick_u);

  sprintf(buf, "Hola, %s, ?Como estas?. Veo que tus flags son %s", nick_u,
          flags);
  envia_nick(nick, remitente, buf);
}

static void fin(void)
{
}

int inicio(void)
{
  especifica_fin(fin);
  comentario_modulo("Modulo 'privmsg' de ejemplo $Revision: 1.13 $");
  handle = nuevo_nick("saluda", "+odkirh", privmsg);
  if (handle < 0)
    return -1;
  return 0;
}
