/*
** PRIVMSG_C
**
** $Id: privmsg.c,v 1.102 2003/07/31 17:59:15 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#include "shared.h"
#include "privmsg.h"
#include "m_db.h"
#include "config.h"
#include "db.h"
#include "berkeleydb.h"
#include "modulos.h"
#include "dbuf.h"

#include "mod_bdd.h"

#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <sys/times.h>
#include <limits.h>
#include <netinet/in.h>

#include <assert.h>


char buf2[2 * BUF_LEN];

clock_t tiempo_inicial;

/*
** channick
** Este comando construye una cadena basandose en la sintaxis #canal nick
** La conversi\163n de nick a comprimido se realiza aqu\155
** Si los par\141metros de entrada son ilegales, devuelve NULL
*/
static char *channick(char *cn, char *buf, char *cmd, char *cmd2)
{
  char *p;

  if (strlen(cn) + strlen(cmd) + strlen(cmd2) > BUF_LEN / 2)
    return NULL;
  if ((*cn != '#') && (*cn != '&') && (*cn != '+'))
    return NULL;                /* No es un canal */
  strcpy(buf, ":" NOMBRE_NODO " ");
  strcat(buf, cmd);
  strcat(buf, " ");
  p = strchr(cn, ' ');          /* Canal */
  if (p == NULL)
    return NULL;
  *p++ = '\0';
  strcat(buf, cn);
  strcat(buf, " ");
  strcat(buf, cmd2);
  cn = strchr(p, ' ');
  if (cn != NULL) {             /* Si tiene algo detr\141s, nos lo cargamos */
    *cn = '\0';
  }
  strcat(buf, " ");

/*
** P apunta al nick, pero el comando necesita la versi\163n
** comprimida, por lo que tenemos que hacer una b\172squeda aqu\155.
*/

  p = encuentra_usuario_nick(p);
  if (p == NULL)
    return NULL;                /* No existe -> Ignoramos */
  p = strchr(p, ' ') + 1;       /* Nick */
  p = strchr(p, ' ') + 1;       /* Comprimido */
  cn = strchr(p, ' ');          /* Final Comprimido */
  *cn = '\0';
  strcat(buf, p);
  *cn = ' ';
  strcat(buf, "\n");
  return buf;
}

int respuesta_nick(char *buf)
{
  char *p, *p2;

  p = encuentra_usuario_nick(buf + 1);
  if (p == NULL)
    return 0;
  p = strchr(p, ' ');
  p = strchr(p + 1, ' ');
  strcpy(buf2, ":" NICK " PRIVMSG ");
  p2 = strchr(p + 1, ' ');
  *p2 = '\0';
  strcat(buf2, p + 1);
  *p2 = ' ';
  strcat(buf2, " :");
  return strlen(buf2);
}

void envia_respuesta(char *buf, char *texto)
{
  int len;

  len = respuesta_nick(buf);
  if (!len)
    return;
  strcpy(buf2 + len, texto);
  dbuf_write(buf2);
}

static void privmsg_meminfo(char *p, char *buf)
{
  struct mallinfo m;
  int len;

  len = respuesta_nick(buf);
  if (!len)
    return;
  m = mallinfo();
  sprintf(buf2 + len, "arena: %d\n", m.arena);
  dbuf_write(buf2);
  sprintf(buf2 + len, "ordblks: %d\n", m.ordblks);
  dbuf_write(buf2);
  sprintf(buf2 + len, "smblks: %d\n", m.smblks);
  dbuf_write(buf2);
  sprintf(buf2 + len, "usmblks: %d\n", m.usmblks);
  dbuf_write(buf2);
  sprintf(buf2 + len, "fsmblks: %d\n", m.fsmblks);
  dbuf_write(buf2);
  sprintf(buf2 + len, "uordblks: %d\n", m.uordblks);
  dbuf_write(buf2);
  sprintf(buf2 + len, "fordblks: %d\n", m.fordblks);
  dbuf_write(buf2);
  sprintf(buf2 + len, "keepcost: %d\n", m.keepcost);
  dbuf_write(buf2);
  envia_respuesta(buf, "\002MEMINFO ejecutado\n");
}

static void privmsg_help(char *p, char *buf)
{
  int len;

  len = respuesta_nick(buf);
  if (!len)
    return;
  strcpy(buf2 + len, "\002HELP:\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002OP <#canal> <nick>\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "     Pone OP en el canal especificado, mediante HACK(4)\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002DEOP <#canal> <nick>\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "     Quita OP en el canal especificado, mediante HACK(4)\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002DIE\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "     Mata a " NICK ". Util si da algun tipo de problema\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002PROFILE\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "     Almacena en disco informacion de \"profiling\". Util exclusivamente para el programador\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002MEMINFO \0034(Para uso exclusivo de JCEA)\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "     Muestra estadisticas de uso de memoria\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002DUMP\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "     Almacena en disco el estado de la base de datos. Es una \"foto\" de la red\n");
  dbuf_write(buf2);

#ifdef ALLOW_ILINE_REGISTRATION
  strcpy(buf2 + len,
         "\002ILINEADD <host> <clones> <expiracion> <usuario> [email] <nonce> \0034(Para uso exclusivo de JCEA)\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "     An~ade una I-Line a la base de datos distribuida\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "\002ILINEDEL <host> <nonce> \0034(Para uso exclusivo de JCEA)\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "     Elimina una I-Line de la base de datos distribuida\n");
  dbuf_write(buf2);
#endif

  strcpy(buf2 + len, "\002STATS\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "     Muestra informacion variada sobre el funcionamiento de " NICK
         "\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002HISTORY\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "     Estadisticas historicas de conexiones a la red\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002USERS\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "     Estadisticas de usuarios\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002SETTIME\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "     Resincroniza el reloj de la red\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002JUPE <servidor>\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "     Expulsa un servidor de la red. El servidor debe estar conectado\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002RELOAD\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "     Actualiza la base de datos de I-Lines, etc., y las BDD\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002DBQ <bdd> \0034(Para uso exclusivo de JCEA)\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "      Sondea la version de BDD de los nodos de la red\n");
  strcpy(buf2 + len,
         "\002DBDEL <bdd> <nodo> \0034(Para uso exclusivo de JCEA)\n");
  dbuf_write(buf2);
  strcpy(buf2 + len,
         "\002DBADD <bdd> <clave> [:contenido] \0034(Para uso exclusivo de JCEA)\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "      Borra una BDD de un nodo de la red\n");
  dbuf_write(buf2);

  strcpy(buf2 + len,
         "\002DLLOAD <libreria> \0034(Para uso exclusivo de JCEA)\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "     Carga una libreria dinamica.\n");
  dbuf_write(buf2);

  strcpy(buf2 + len,
         "\002DLUNLOAD <libreria> \0034(Para uso exclusivo de JCEA)\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "     Descarga una libreria dinamica.\n");
  dbuf_write(buf2);

  strcpy(buf2 + len, "\002DLLIST\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "     Lista las librerias dinamicas cargadas.\n");
  dbuf_write(buf2);

  strcpy(buf2 + len, "\002HELP\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "     Muestra esta informacion\n");
  dbuf_write(buf2);
  strcpy(buf2 + len, "\002Fin de Help\n");
  dbuf_write(buf2);
}

static void privmsg_dbq(char *p, char *buf)
{
  if (((*p != '*') && ((*p < 'a') || (*p > 'z'))) || (*(p + 1) != '\0')) {
    envia_respuesta(buf, "\002Base de datos incorrecta\n");
    return;
  }
  sprintf(buf2, "%s DB * 0 Q %s %c\n", NUM_NODO, buf + 1, *p);
  dbuf_write(buf2);
  envia_respuesta(buf, "\002Sondeo iniciado\n");
}

static void privmsg_dbadd(char *p, char *buf)
{
  segmentos seg;
  int i;
  unsigned char que_bdd;

  i = segmentacion(&seg, p, NULL, 3);
  if ((i < 2) || ((que_bdd = *(seg.segm[0])) < 'a') || (que_bdd > 'z') ||
      (*(seg.segm[0] + 1) != '\0')) {
    envia_respuesta(buf, "\002Parametros incorrectos\n");
    return;
  }

  if (!strchr(BDD_AUTHORITATIVE, que_bdd)) {
    envia_respuesta(buf,
                    "\002No somos autoritativos para la BDD especificada\n");
    return;
  }

  if ((i == 3) && (*(seg.segm[2]) == ':'))
    seg.segm[2]++;              /* Nos saltamos un posible ':' */

  bdd_envia_registro(que_bdd, seg.segm[1], (i == 3) ? seg.segm[2] : NULL);

  envia_respuesta(buf, "\002DBADD ejecutado\n");
}

static void privmsg_dbdel(char *p, char *buf)
{
  char *p2;

  if ((*p < 'a') || (*p > 'z') || (*(p + 1) != ' ')) {
    envia_respuesta(buf, "\002Parametros incorrectos\n");
    return;
  }

  if (!strchr(BDD_AUTHORITATIVE, *p)) {
    envia_respuesta(buf,
                    "\002No somos autoritativos para la BDD especificada\n");
    return;
  }

  p2 = strchr(p + 2, ' ');
  if (p2)
    *p2 = '\0';
  sprintf(buf2, "%s DB %s 0 D %s %c\n", NUM_NODO, p + 2, buf + 1, *p);
  dbuf_write(buf2);
  envia_respuesta(buf, "\002Orden de borrado enviada\n");
}

static void privmsg_dlload(char *p, char *buf)
{
  envia_respuesta(buf, dlload(p));
  envia_respuesta(buf, "\002DLLOAD ejecutado\n");
}

static void privmsg_dlunload(char *p, char *buf)
{
  envia_respuesta(buf, dlunload(p));
  envia_respuesta(buf, "\002DLUNLOAD ejecutado\n");
}

static void privmsg_dllist(char *p, char *buf)
{
  int len;

  len = respuesta_nick(buf);
  if (!len)
    return;
  dllist(buf2, buf2 + len);
  envia_respuesta(buf, "\002DLLIST ejecutado\n");
}

static void privmsg_op(char *p, char *buf)
{
  if (channick(p, buf2, "MODE", "+o") != NULL) {
    dbuf_write(buf2);
  }
  envia_respuesta(buf, "\002OP ejecutado\n");
}

static void privmsg_deop(char *p, char *buf)
{
  if (channick(p, buf2, "MODE", "-o") != NULL) {
    dbuf_write(buf2);
  }
  envia_respuesta(buf, "\002DEOP ejecutado\n");
}

static void privmsg_die(char *p, char *buf)
{
  envia_respuesta(buf, "\002" NICK " muere\n");
  descarga_modulos();
  cierra_berkeley_db();
  dbuf_envia();                 /* Intentamos que llegue la respuesta */
  exit(0);
}

static void privmsg_profile(char *p, char *buf)
{
  pid_t pid;

  pid = fork();
  if (pid == (pid_t) - 1)
    return;                     /* El fork falla */
  if (!pid)
    exit(0);                    /* El hijo */
/* El padre */
  while (wait(NULL) == (pid_t) - 1) /* Recoge el hijo */
    if (errno == ECHILD)
      break;                    /* No hay hijo que recoger */
  envia_respuesta(buf, "\002Fork&Die ejecutado con exito\n");
}

static void privmsg_dump(char *p, char *buf)
{
  int i;

  i = open("olimpo.dump", O_WRONLY | O_CREAT | O_TRUNC, 0600);
  dump_db(i);
  close(i);
  envia_respuesta(buf, "\002Base de Datos volcada a disco\n");
}

static void privmsg_reload(char *p, char *buf)
{
  envia_m_db();
  envia_respuesta(buf, "\002RELOAD ejecutado\n");
}

static void privmsg_history(char *p, char *buf)
{
  int len;
  int len2;
  long max_usr;
  long conexiones, conexiones_dia;
  time_t tiempo, min_tiempo, max_tiempo, max_usr_tiempo;
  long punt, punt2, c, c2;
  char buf_time[100];

  len = respuesta_nick(buf);
  if (!len)
    return;

  punt = puntero_historico + 1;
  if (punt >= LONG_HISTORICO)
    punt = 0;
  punt2 = punt;

  c2 = 0;
  buf2[len] = '\0';
  conexiones_dia = 0;
  do {
    conexiones = 0;
    for (c = 3600 / HISTORICO_MUESTRAS; c--; punt++) {
      if (punt >= LONG_HISTORICO)
        punt = 0;
      conexiones += num_conexiones_hist[punt];
    }
    conexiones_dia += conexiones;
    if (c2)
      strcat(buf2, " ");
    sprintf(buf2 + strlen(buf2), "%lu", conexiones);
    if (++c2 == 24) {
      sprintf(buf2 + strlen(buf2), " - Suma 24h: %lu\n", conexiones_dia);
      dbuf_write(buf2);
      c2 = 0;
      buf2[len] = '\0';
      conexiones_dia = 0;
    }
  } while (punt != punt2);

  punt = punt2;

  do {
    max_usr = max_usr_tiempo = 0;
    min_tiempo = tiempo_hist[punt];
    for (c = 0; c < HISTORICO_DIA; c++, punt++) {
      if (punt >= LONG_HISTORICO)
        punt = 0;
      tiempo = tiempo_hist[punt];
      max_tiempo = tiempo;
      if (num_usuarios_hist[punt] >= max_usr) {
        max_usr = num_usuarios_hist[punt];
        max_usr_tiempo = tiempo;
      }
    }
    sprintf(buf2 + len, "\002%ld\002 Usuarios de Pico (%s",
            max_usr, ctime_sincronizado(&max_usr_tiempo, buf_time));
    *strchr(buf2, '\n') = ')';
    len2 = len + strlen(buf2 + len);
    sprintf(buf2 + len2, " (%s", ctime_sincronizado(&min_tiempo, buf_time));
    *strchr(buf2, '\n') = ' ';
    len2 = len + strlen(buf2 + len);
    sprintf(buf2 + len2, "- %s", ctime_sincronizado(&max_tiempo, buf_time));
    *strchr(buf2, '\n') = ')';
    strcat(buf2 + len2, "\n");
    dbuf_write(buf2);
  } while (punt != punt2);

  strcpy(buf2 + len, "\002Fin de history\n");
  dbuf_write(buf2);
}

static void privmsg_stats(char *p, char *buf)
{
  struct tms tiempos;
  int i, j, k, len;
  unsigned long long t, u, s;
  time_t tiempo;
  char buf_time[100];

  t = (long long) (times(&tiempos) - tiempo_inicial) / CLK_TCK;
  u = (long long) (tiempos.tms_utime + tiempos.tms_cutime) / CLK_TCK;
  s = (long long) (tiempos.tms_stime + tiempos.tms_cstime) / CLK_TCK;

  tiempo = time(NULL) - t;

  len = respuesta_nick(buf);
  if (!len)
    return;

#if 0
  sprintf(buf2 + len, "entropia: %llx\n", entropia);
  dbuf_write(buf2);
#endif

  sprintf(buf2 + len, "\002%lld\002 segundos de ejecucion (%s", t,
          ctime_sincronizado(&tiempo, buf_time));
  *(buf2 + strlen(buf2) - 1) = '\0';
  strcat(buf2 + len, ")\n");
  dbuf_write(buf2);
  sprintf(buf2 + len, "\002%lld/%lld\002 segundos Usuario/Sistema\n", u, s);
  dbuf_write(buf2);
  i = usersstats_db(&j, &k);
  sprintf(buf2 + len, "\002%llu\002 Conexiones\n", num_conexiones);
  dbuf_write(buf2);
  sprintf(buf2 + len, "\002%ld\002 Usuarios de Pico (%s",
          num_usuarios_max, ctime_sincronizado(&tiempo_num_usuarios_max,
                                               buf_time));
  *(buf2 + strlen(buf2) - 1) = '\0';
  strcat(buf2 + len, ")\n");
  dbuf_write(buf2);
  sprintf(buf2 + len, "\002%ld\002 Usuarios Online\n", num_usuarios);
  dbuf_write(buf2);
  sprintf(buf2 + len, "\002%d\002 Usuarios. Hash_Num(%d,%d)\n", i, j, k);
  dbuf_write(buf2);
  i = nicksstats_db(&j, &k);
  sprintf(buf2 + len, "\002%d\002 Usuarios. Hash_Nick(%d,%d)\n", i, j, k);
  dbuf_write(buf2);
  i = ipsstats_db(&j, &k);
  sprintf(buf2 + len, "\002%d\002 Usuarios. Hash_IP(%d,%d)\n", i, j, k);
  dbuf_write(buf2);
  i = serversstats_db(&j, &k);
  sprintf(buf2 + len, "\002%d\002 Servidores. Hash(%d,%d)\n", i, j, k);
  dbuf_write(buf2);
  i = dinamic_ilines_stats_db(&j, &k);
  sprintf(buf2 + len, "\002%d\002 I-Lines dinamicas. Hash_IP(%d,%d)\n", i, j,
          k);
  dbuf_write(buf2);
  i = dinamic_ilines_nicks_stats_db(&j, &k);
  sprintf(buf2 + len, "\002%d\002 I-Lines dinamicas. Hash_Nicks(%d,%d)\n", i,
          j, k);
  dbuf_write(buf2);

  sprintf(buf2 + len, "DB 'b': \002%ld\002   DB 'i': \002%ld\002\n", db[DB_B],
          db[DB_I]);
  dbuf_write(buf2);
  sprintf(buf2 + len, "DB 'n': \002%ld\002   DB 'o': \002%ld\002\n", db[DB_N],
          db[DB_O]);
  dbuf_write(buf2);
  sprintf(buf2 + len, "DB 'v': \002%ld\002   DB 'w': \002%ld\002\n", db[DB_V],
          db[DB_W]);
  dbuf_write(buf2);

  strcpy(buf2 + len, "\002Fin de stats\n");
  dbuf_write(buf2);
}

static void privmsg_jupe(char *p, char *buf)
{
  char numeric[16];
  char *p2, *p3;
  int l;

  p2 = busca_servidor(p);
  if (p2 == NULL)
    return;
  p3 = strchr(p2, ' ') + 1;
  p2 = strrchr(p2, ' ') + 1;
  l = p2 - p3;;
  assert((l == 2) || (l == 3));
  strncpy(numeric, p3, l);      /* No pone el '\0' al final, pero se pone en el strcpy siguiente */
  if (l == 2) {                 /* Numeric corto */
    strcpy(numeric + 1, "D]");  /* 255 */
  }
  else {                        /* Numeric largo */
    strcpy(numeric + 2, "D]]"); /* 16383 */
  }
  sprintf(buf2, NUM_NODO " SQUIT %s 0 :Servidor *JUPE* por %s\n"
          NUM_NODO " SERVER %s 2 880000000 880000000 "
          "J10 %s :JUPE\n", p2, buf + 1, p2, numeric);
  dbuf_write(buf2);
  borra_servidor(p, buf);
  envia_respuesta(buf, "\002JUPE ejecutado\n");
}

static void privmsg_settime(char *p, char *buf)
{
  sprintf(buf2, NUM_NODO " SETTIME %ld *\n", time(NULL));
  dbuf_write(buf2);
  envia_respuesta(buf, "\002SETTIME ejecutado\n");
}

static void privmsg_users(char *param, char *buf)
{
  int i;

  i = respuesta_nick(buf);
  if (!i)
    return;
  distribucion_users(buf2, i);
  strcpy(buf2 + i, "\002Fin de Users\n");
  dbuf_write(buf2);
}

#ifdef ALLOW_ILINE_REGISTRATION
static void privmsg_ilineadd(char *param, char *buf)
{
  dbt clave;
  dbt contenido;
  void *transaccion;
  int i;
  char buf3[BUF_LEN];
  unsigned long *p;
  char *p2;
  unsigned char *p3, *p4;
  unsigned char buf4[BUF_LEN];
  int st;
  int cont;
  segmentos seg;
  time_t expiracion;
  char buf_time[100];

  if (!bdd_flujo_abierto2('i'))
    return;

  i = segmentacion(&seg, param, NULL, MAX_NUM_SEGMENTOS);
  if ((i < 4) || (i == MAX_NUM_SEGMENTOS))
    return;

  p2 = encuentra_usuario_nick(buf + 1); /* +1 por el ':' */

  i = respuesta_nick(buf);
  if ((!i) || (!p2))
    return;

  p4 = seg.segm[0];
  p3 = buf4;

  while ((*p3++ = tolowertab[*p4++]));

  clave.datos = buf4;
  clave.len = strlen(clave.datos) + 1;

  p = (unsigned long *) buf3;

  *p++ = htonl(time(NULL));

  expiracion = atol(seg.segm[2]);

  if (*(seg.segm[2] + strlen(seg.segm[2]) - 1) == 'd')
    expiracion = time(NULL) + expiracion * 60 * 60 * 24;
  if (*(seg.segm[2] + strlen(seg.segm[2]) - 1) == 'm')
    expiracion = time(NULL) + expiracion * 60;

  *p++ = htonl(expiracion);

  *p = htonl(0);

  for (cont = 0; cont < seg.num_segm; cont++)
    seg.segm[cont] = seg.segm[cont + 1];
  seg.segm[seg.num_segm - 1] = p2;
  dessegmentar(&seg, buf3 + 12);

  contenido.datos = buf3;
  contenido.len = 12 + 1 + strlen(buf3 + 12); /* Los timestamp y el '\0' final */

  transaccion = inicia_txn_db();
  st = put_db(db_ilines, transaccion, &clave, &contenido);
  compromete_txn_db(transaccion);

  if (!st) {
    bdd_envia_registro('i', (char *) (clave.datos), seg.segm[0]);
    if (expiracion > 0)
      sprintf(buf2 + i, "Expiracion el %s",
              ctime_sincronizado(&expiracion, buf_time));
    else
      sprintf(buf2 + i, "Sin Expiracion\n");
    dbuf_write(buf2);
    strcpy(buf2 + i, "\002I-Line activada\n");
  }
  else {
    strcpy(buf2 + i, "\002Problemas con la base de datos\n");
  }
  dbuf_write(buf2);

  sprintf(buf2 + i, "\002Fin de IlineAdd\002 (%s)\n",
          seg.segm[seg.num_segm - 2]);
  dbuf_write(buf2);
}

#endif /* ALLOW_ILINE_REGISTRATION */

#ifdef ALLOW_ILINE_REGISTRATION
static void privmsg_ilinedel(char *param, char *buf)
{
  dbt clave;
  void *transaccion;
  int i;
  int st;
  segmentos seg;
  unsigned char buf4[BUF_LEN];
  unsigned char *p3, *p4;

  if (!bdd_flujo_abierto2('i'))
    return;

  i = segmentacion(&seg, param, NULL, MAX_NUM_SEGMENTOS);
  if ((i < 2) || (i == MAX_NUM_SEGMENTOS))
    return;

  i = respuesta_nick(buf);
  if (!i)
    return;

  p4 = seg.segm[0];
  p3 = buf4;

  while ((*p3++ = tolowertab[*p4++]));

  clave.datos = buf4;
  clave.len = strlen(clave.datos) + 1;

  transaccion = inicia_txn_db();
  st = del_db(db_ilines, transaccion, &clave);
  compromete_txn_db(transaccion);

  if (!st) {
    bdd_envia_registro('i', (char *) (clave.datos), NULL);
    strcpy(buf2 + i, "\002I-Line borrada\n");
  }
  else if (st == BERKELEY_DB_NOTFOUND) {
    strcpy(buf2 + i, "\002I-Line inexistente\n");
  }
  else {
    strcpy(buf2 + i, "\002Problemas con la base de datos\n");
  }
  dbuf_write(buf2);

  sprintf(buf2 + i, "\002Fin de IlineDel\002 (%s)\n",
          seg.segm[seg.num_segm - 1]);
  dbuf_write(buf2);
}

#endif /* ALLOW_ILINE_REGISTRATION */

void m_privmsg(char *p, char *buf)
{
  int privil;
  int i, j;
  char *p2;

  typedef struct {
    char *comando;
    void (*rutina) (char *p, char *buf);
    int privil;
    int param;
  } comando;

  comando tabla_comandos[] = {
    {"op", privmsg_op, PRIVIL_IRCOP, !0},
    {"deop", privmsg_deop, PRIVIL_IRCOP, !0},
    {"die", privmsg_die, PRIVIL_IRCOP, 0},
    {"profile", privmsg_profile, PRIVIL_IRCOP, 0},
    {"dump", privmsg_dump, PRIVIL_IRCOP, 0},
#ifdef ALLOW_ILINE_REGISTRATION
    {"ilineadd", privmsg_ilineadd, PRIVIL_JCEA | PRIVIL_BREI, !0},
    {"ilinedel", privmsg_ilinedel, PRIVIL_JCEA | PRIVIL_BREI, !0},
#endif
    {"stats", privmsg_stats, PRIVIL_IRCOP, 0},
    {"history", privmsg_history, PRIVIL_IRCOP, 0},
    {"users", privmsg_users, PRIVIL_IRCOP, 0},
    {"jupe", privmsg_jupe, PRIVIL_IRCOP, !0},
    {"settime", privmsg_settime, PRIVIL_IRCOP, 0},
    {"reload", privmsg_reload, PRIVIL_IRCOP, 0},
    {"dbq", privmsg_dbq, PRIVIL_JCEA, !0},
    {"dbdel", privmsg_dbdel, PRIVIL_JCEA, !0},
    {"dbadd", privmsg_dbadd, PRIVIL_JCEA, !0},
    {"dlload", privmsg_dlload, PRIVIL_JCEA, !0},
    {"dlunload", privmsg_dlunload, PRIVIL_JCEA, !0},
    {"dllist", privmsg_dllist, PRIVIL_IRCOP, 0},
    {"help", privmsg_help, PRIVIL_IRCOP, 0},
    {"meminfo", privmsg_meminfo, PRIVIL_JCEA, 0},
    {NULL, NULL, 0, 0}
  };

  *strchr(buf, ' ') = '\0';     /* Remitente */
  if (*buf != ':')              /* Ignora comandos comprimidos */
    return;

  p2 = strchr(p, ' ') + 2;      /* Se salta el numeric, el espacio y los ":" */

/* No es para nosotros */
  if ((p[0] != *NUM_NODO) && (p[1] != 'A'))
    return;
  if (p[2] != 'A') {            /* ?nick del modulo? */
    envia_privmsg_modulo(p, buf, p2);
    return;
  }

  p = p2;

  for (i = 0; tabla_comandos[i].comando != NULL; i++) {
    j = strlen(tabla_comandos[i].comando);
    if (!strncmp(p, tabla_comandos[i].comando, j)) {
      if ((*(p + j) == ' ')
          || ((*(p + j) == '\0') && (tabla_comandos[i].param == 0))) {
        p += j;
        if (*p == ' ')
          p++;
        privil = privilegiado(buf + 1, p2, tabla_comandos[i].privil);
        if (privil & tabla_comandos[i].privil) { /* +1 por ":" */
          tabla_comandos[i].rutina(p, buf);
        }
        else if (privil & (PRIVIL_IRCOP | PRIVIL_HELPER)) {
          envia_respuesta(buf, "\002Acceso Denegado\n");
        }
        return;
      }
    }
  }
/*
** Se forma un bucle con NICK
*/
/*
  envia_respuesta(buf,"\002Comando desconocido. Escribe \"help\" para obtener ayuda\n");
*/
}
