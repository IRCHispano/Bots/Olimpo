/*
**  $Id: nicks_lista.c,v 1.29 2003/07/31 17:59:15 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <signal.h>

#include "berkeleydb.h"
#include "mod_tools.h"
#include "modulos.h"

static int senal = 0;

void senal_handler(int s)
{
  senal = s;
}

void errores(int st)
{
  fprintf(stderr, "DB: %s\n", strerror_db(st));
  exit(-1);
}

int main(void)
{
  void *db_nicks;
  void *db_cursor;
  dbt clave;
  dbt contenido;
  void *transaccion;
  unsigned char *p, *p2;
  int st = 0;

  //signal(SIGPIPE, SIG_IGN);

  sigset(SIGHUP, senal_handler);
  sigset(SIGINT, senal_handler);
  sigset(SIGQUIT, senal_handler);
  sigset(SIGKILL, senal_handler);
  sigset(SIGPIPE, senal_handler);
  sigset(SIGALRM, senal_handler);
  sigset(SIGTERM, senal_handler);
  sigset(SIGUSR1, senal_handler);
  sigset(SIGUSR2, senal_handler);
  sigset(SIGPOLL, senal_handler);

  sigset(SIGVTALRM, senal_handler);
  sigset(SIGPROF, senal_handler);
  sigset(SIGXCPU, senal_handler);
  sigset(SIGXFSZ, senal_handler);
  /*
   ** De momento vamos a obviar
   ** las senales en tiempo real.
   */

  printf("%s\n\n", version_db());

  abre_berkeley_db();
  db_nicks = abre_db("db.nicks");
  if (st)
    errores(st);

  transaccion = inicia_txn_db_no_wait();

  db_cursor = cursor_db(db_nicks, transaccion, NULL, 1);
  if (!db_cursor) {
    aborta_txn_db(transaccion);
    errores(st);
  }

  while (!0) {
    if (senal) {
      printf("\n\nLlega la sen~al %d\n", senal);
      break;
    }
    st = cursor_get_db(db_cursor, &clave, &contenido);
    if (st == BERKELEY_DB_NOTFOUND)
      break;
    if (st) {
      cursor_close_db(db_cursor);
      aborta_txn_db(transaccion);
      errores(st);
      break;
    }

    p = (unsigned char *) clave.datos;
    if (*p != ' ') {
      char buf[256];
      char buf2[16];
      char buf3[16];
      char buf4[64];
      char buf5[64];
      time_t ts;
      time_t ts2;
      char email[1024];
      char buf_time[100];

      strncpy(buf, p, clave.len);
      buf[clave.len] = '\0';
      p2 = (unsigned char *) contenido.datos;
      strcpy(buf2,
             inttobase64((*(p2 + 1) << 24) + (*(p2 + 2) << 16) +
                         (*(p2 + 3) << 8) + (*(p2 + 4))));
      strcpy(buf3,
             inttobase64((*(p2 + 5) << 24) + (*(p2 + 6) << 16) +
                         (*(p2 + 7) << 8) + (*(p2 + 8))));

      ts =
        (*(p2 + 9) << 24) + (*(p2 + 10) << 16) + (*(p2 + 11) << 8) +
        (*(p2 + 12));
      strcpy(buf4, ctime_sincronizado(&ts, buf_time));
      *(buf4 + strlen(buf4) - 1) = '\0'; /* Nos cargamos el '\n' final */

      email[0] = '\0';
      if (contenido.len >= 17) {
        ts2 =
          (*(p2 + 13) << 24) + (*(p2 + 14) << 16) + (*(p2 + 15) << 8) +
          (*(p2 + 16));
        strcpy(buf5, ctime_sincronizado(&ts, buf_time));
        *(buf5 + strlen(buf5) - 1) = '\0'; /* Nos cargamos el '\n' final */
        if (contenido.len > 18) {
          strncpy(email, contenido.datos + 18, contenido.len - 18);
          email[contenido.len - 18] = '\0';
        }
      }
      else {
        buf5[0] = '\0';
        ts2 = 0;
      }
      //printf("%s %d %s %s %s %s\n", buf, *p2, buf2, buf3, buf4, buf5);
      printf("%s %d %s%s %ld %ld %s\n", buf, *p2, buf2, buf3, ts, ts2, email);
    }
/*
** Esto no lo necesitamos en modo BULK
**
**    free(contenido.datos);
**    free(clave.datos);
*/
  }

  st = cursor_close_db(db_cursor);
  if (st) {
    aborta_txn_db(transaccion);
    errores(st);
  }

  compromete_txn_db(transaccion);

  cierra_db(db_nicks);
  if (st)
    errores(st);

  cierra_berkeley_db();

  return 0;
}
