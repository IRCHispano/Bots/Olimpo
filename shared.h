/*
** SHARED_H
**
** $Id: shared.h,v 1.37 2004/04/05 17:09:37 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#ifndef SHARED_H
#define SHARED_H

#include <time.h>

#include "config.h"

#include "mod_tools.h"

extern int soc;
extern char uplink[3];

#if defined(ALLOW_SETTIME)
extern time_t siguiente_settime;
#endif

extern void *db_ilines;
extern void *db_varios;

#define DB_INICIO	'a'
#define DB_FINAL	'z'
#define DB_B		('b'-DB_INICIO)
#define DB_I            ('i'-DB_INICIO)
#define DB_N		('n'-DB_INICIO)
#define DB_O            ('o'-DB_INICIO)
#define DB_V            ('v'-DB_INICIO)
#define DB_W		('w'-DB_INICIO)

extern long db[DB_FINAL - DB_INICIO + 1];

extern unsigned long long num_conexiones;
extern long num_usuarios;
extern long num_usuarios_max;
extern time_t tiempo_num_usuarios_max;

#define HISTORICO_DIAS	10
#define HISTORICO_MUESTRAS	60   /* Cada cuanto toma una muestra */
#define HISTORICO_DIA	(86400/HISTORICO_MUESTRAS)
#define LONG_HISTORICO	(HISTORICO_DIAS*HISTORICO_DIA)

extern long num_usuarios_hist[LONG_HISTORICO];
extern long num_conexiones_hist[LONG_HISTORICO];
extern time_t tiempo_hist[LONG_HISTORICO];
extern long puntero_historico;

extern unsigned long long entropia;
extern char convert2y[];
extern unsigned char convert2n[];
extern unsigned char tolowertab[];

#define mas_entropia(x)  entropia=((entropia<<1)|(entropia>>63))^(x)

#define MAX_NUM_SEGMENTOS       16

typedef struct {
  int num_segm;
  char *segm[MAX_NUM_SEGMENTOS];
} segmentos;

/* segmentacion
** Esta rutina toma una cadena separada por espacios/tabuladores y la segmenta
** en palabras. Si una de las palabras intermedias (no la primera)
** empieza por ":", lo que sigue se considera todo un \172nico segmento.
** La segmentaci\163n se hace sobre la cadena original si el buffer es NULL.
** La rutina se salta los espacios en blanco/tabuladores m\172ltiples.
** Si demasiados segmentos, la cadena se trunca.
** Devuelve n\172mero de segmentos.
*/
int segmentacion(segmentos * seg, char *cadena, char *buf, int max_segm);

/*
** Al ultimo segmento le mete un : delante si tiene
** algun espacio o tabulador
*/
char *dessegmentar(segmentos * seg, char *buf);

extern char convert2y[];
extern unsigned char convert2n[];

void guarda_log(char *linea, char *modulo);
void guarda_log_nick(char *linea, char *modulo, char *nick);
void guarda_log_nick_compress(char *linea, char *modulo, char *nick_compress);

void borra_nodos_conectados(void);
int nodo_esta_conectado(char *numeric);
void nodo_conectado(char *numeric);
void nodo_desconectado(char *numeric);

#endif
