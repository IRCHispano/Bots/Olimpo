/*
** BERKELEYDB.C
**
** $Id: mod_berkeleydb.c,v 1.8 2002/02/21 20:08:54 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#include "berkeleydb.h"
#include "mod_berkeleydb.h"

char *mod_strerror_db(void)
{
  return strerror_db();
}

void *mod_abre_db(char *nombre)
{
  return abre_db(nombre);
}

void mod_cierra_db(void *db)
{
  cierra_db(db);
}

void *mod_inicia_txn_db(void)
{
  return inicia_txn_db();
}

void *mod_inicia_txn_db_no_sync(void)
{
  return inicia_txn_db_no_sync();
}

void *mod_inicia_txn_db_no_wait(void)
{
  return inicia_txn_db_no_wait();
}

void mod_compromete_txn_db(void *txn)
{
  compromete_txn_db(txn);
}

void mod_aborta_txn_db(void *txn)
{
  aborta_txn_db(txn);
}

int mod_get_db(void *db, void *transaccion, dbt * clave, dbt * contenido)
{
  return get_db(db, transaccion, clave, contenido);
}

int mod_put_db(void *db, void *transaccion, dbt * clave, dbt * contenido)
{
  return put_db(db, transaccion, clave, contenido);
}

int mod_del_db(void *db, void *transaccion, dbt * clave)
{
  return del_db(db, transaccion, clave);
}

char *mod_version_db(void)
{
  return version_db();
}

c_db *mod_cursor_db(void *db, void *transaccion, dbt * clave, int bulk)
{
  return cursor_db(db, transaccion, clave, bulk);
}

int mod_cursor_get_db(c_db * db_cursor, dbt * clave, dbt * contenido)
{
  return cursor_get_db(db_cursor, clave, contenido);
}

int mod_cursor_close_db(c_db * db_cursor)
{
  return cursor_close_db(db_cursor);
}
