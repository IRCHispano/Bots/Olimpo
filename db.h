/*
** DB.H
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

/*
    $Id: db.h,v 1.24 2002/05/29 15:58:34 jcea Exp $
*/

#ifndef DB_H
#define DB_H

#define BUF_LEN 1024

void borra_db(void);
/*
void borra_registro(char *cadena);
void guarda_registro(char *cadena);
*/
void guarda_iline(char *iline);
void iline_dinamica(char *nick);
void guarda_servidor(char *servidor);

char *encuentra_usuario_compress(char *usuario);
char *encuentra_usuario_nick(char *usu);
int buscar_iline(char *ip, char *usuario);
char *encuentra_ip(void **p, char *cadena);
void borra_usuario_compress(char *usu, int notify);
void borra_usuario_nick(char *usu, int notify);
void guarda_usuario(char *usu, int notify);
void incrementa_num_conexiones(void);
char *busca_servidor(char *serv);
char *busca_servidor_compress(char *serv);
void borra_servidor(char *serv, char *causa);

#define PRIVIL_IRCOP	1
#define PRIVIL_HELPER	2
#define PRIVIL_NICK	4
#define PRIVIL_JCEA	8
#define PRIVIL_BREI	16

int privilegiado(char *usu, char *comando, int privil);

void dump_db(int i);
int ilinesstats_db(int *min, int *max);
int serversstats_db(int *min, int *max);
int usersstats_db(int *min, int *max);
int nicksstats_db(int *min, int *max);
int ipsstats_db(int *min, int *max);
int dinamic_ilines_stats_db(int *min, int *max);
int dinamic_ilines_nicks_stats_db(int *min, int *max);
void dump_ilines(char *param, int i, char *buf2);
void reload_db(void);
void distribucion_users(char *buf2, int i);

void lee_historico(void);

void db_notifica_nicks_registrados(void);
void db_notifica_nicks(void);

#endif
