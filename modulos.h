/*
** MODULOS_H
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

/*
    $Id: modulos.h,v 1.33 2003/07/31 17:59:15 jcea Exp $
*/

#ifndef MODULOS_H
#define MODULOS_H

void inicializa_sistema_de_modulos(void);

void carga_modulos(void);
void envia_modulos_nicks(void);
void descarga_modulos(void);

void envia_privmsg_modulo(char *destino, char *remitente, char *mensaje);
void envia_servmsg_modulo(char *destino, char *remitente, char *comando);
void intercepta_comando_modulo(char *comando);

int notifica_posible_kill_modulo(char *nick_compress, char *comando);
void notifica_nick_entrada_modulo(char *nick_compress);
void notifica_nick_salida_modulo(char *nick_compress);
void notifica_nick_registrado_modulo(char *nick_compress);
void notifica_nicks_registrados2(char *nick_compress);
void notifica_nicks2(char *nick_compress);
void notifica_server_join_modulo(char *server, char *id, char *padre);
void notifica_server_split_modulo(char *server, char *causa);
void notifica_servers_split_end_modulo(void);
void notifica_timer_modulo(int tiempo);
void notifica_IRQ_modulo(void);

void notifica_db_nickdrop_modulo(char *nick, long num_serie);

char *dlload(char *lib);
char *dlunload(char *lib);
void dllist(char *buf_send, char *buf_write);

extern int tiempo_timeout_modulos;

extern volatile int IRQ;

/*
** Estas rutinas son necesarias por un bug en Solaris 2.5.1.
** Por conveniencia se definen en el modulo PYTHON
*/
char *ctime_sincronizado(const time_t * clock, char *buf);
struct tm *localtime_sincronizado(const time_t * clock, struct tm *res);

#endif
