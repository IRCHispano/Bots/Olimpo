/*
    $Id: dbuf.c,v 1.9 2003/07/31 17:59:15 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#include "shared.h"
#include "dbuf.h"
#include "malloc.h"

#include <string.h>
#include <assert.h>
#include <unistd.h>

struct dbuf {
  struct dbuf *next;
  char *buf;
  int len;
  char *p_read;
  char *p_write;
};

struct dbuf *cola_inicio = NULL;
struct dbuf *cola_fin = NULL;

#define DBUF_LEN	8192
//#define DBUF_LEN 16 /* Para comprobar que funciona bien */


void dbuf_purga(void)
{
  struct dbuf *p, *p2;

  p = cola_inicio;
  cola_inicio = cola_fin = NULL;

  while (p) {
    p2 = p->next;
    free(p->buf);
    free(p);
    p = p2;
  }
}

static struct dbuf *nuevo_dbuf(int len)
{
  struct dbuf *p;

  p = malloc(sizeof(struct dbuf));
  assert(p);
  p->buf = malloc(len);
  assert(p->buf);

  p->next = NULL;
  p->len = DBUF_LEN;
  p->p_read = p->buf;
  p->p_write = p->buf;

  return p;
}

void dbuf_write(char *buf)
{
  struct dbuf *p;
  int len, len2;

  assert(soc > 0);

  len = strlen(buf);
  assert(len);

  if (!cola_inicio) {
    cola_inicio = cola_fin = nuevo_dbuf(DBUF_LEN);
  }

  if ((cola_fin->p_write - cola_fin->buf + len) <= (cola_fin->len - 10)) { /* Cabe entero */
    strcpy(cola_fin->p_write, buf);
    cola_fin->p_write += len;
  }
  else {                        /* No cabe... */
    len2 = len + 1;             /* Por el '\0' del final */
    p = nuevo_dbuf(len2 < DBUF_LEN ? DBUF_LEN : len2);
    cola_fin->next = p;
    cola_fin = p;
    strcpy(p->buf, buf);
    p->p_write += len;
  }
}

void dbuf_envia(void)
{
  struct dbuf *p;
  int len;

  while (1) {
    if (!cola_inicio)
      return;
    if (cola_inicio->p_read == cola_inicio->p_write) {
      p = cola_inicio->next;
      if (!p)
        return;                 /* No hay nada que hacer */
      free(cola_inicio->buf);
      free(cola_inicio);
      cola_inicio = p;
    }
    p = cola_inicio;
    assert(p->p_write != p->p_read);

    len = write(soc, p->p_read, p->p_write - p->p_read);

    if (len <= 0)
      return;

    p->p_read += len;
    if (p->p_read == p->p_write) {
      if (p->next) {
        cola_inicio = p->next;
        free(p->buf);
        free(p);
        continue;
      }
      p->p_read = p->p_write = p->buf;
      break;
    }
  }
}
