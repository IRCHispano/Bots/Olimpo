/*
** BERKELEYDB.H
*/

/*
    $Id: berkeleydb.h,v 1.20 2002/02/21 20:08:54 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#ifndef BERKELEYDB_H
#define BERKELEYDB_H

#include "berkeleydb2.h"

char *version_db(void);

void abre_berkeley_db(void);
void cierra_berkeley_db(void);

p_db abre_db(char *nombre);
void cierra_db(p_db);

txn inicia_txn_db(void);
txn inicia_txn_db_no_sync(void);
txn inicia_txn_db_no_wait(void);
void compromete_txn_db(txn txn);
void aborta_txn_db(txn txn);


int get_db(p_db db, txn transaccion, dbt * clave, dbt * contenido);
int put_db(p_db db, txn transaccion, dbt * clave, dbt * contenido);
int del_db(p_db db, txn transaccion, dbt * clave);

c_db *cursor_db(p_db db, txn transaccion, dbt * clave, int bulk);
int cursor_get_db(c_db * db_cursor, dbt * clave, dbt * contenido);
int cursor_close_db(c_db * db_cursor);

char *strerror_db();

#endif
