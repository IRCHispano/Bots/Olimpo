/*
** DB.C
**
** $Id: db.c,v 1.182 2003/07/31 17:59:15 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>

#include <assert.h>

#include "db.h"
#include "berkeleydb.h"
#include "config.h"
#include "shared.h"
#include "modulos.h"
#include "mod_bdd.h"
#include "m_db.h"
#include "dbuf.h"


static int borra_usuario_bucle = 0;

struct lista {
  struct lista *siguiente;
  struct lista **anterior;
  char registro;                /* En realidad mide mas de 1 byte... El taman~o exacto se ve peticion a peticion */
};


#define NO_CANONICE	0
#define CANONICE	(!NO_CANONICE)

#define SERVERS_HASH_LEN 67
static struct lista *servers_hash[SERVERS_HASH_LEN];

#define NICKS_HASH_LEN 32771
static struct lista *nicks_hash[NICKS_HASH_LEN];

#define IPS_HASH_LEN 32771
static struct lista *ips_hash[IPS_HASH_LEN];

#define NICKSCOMP_HASH_LEN 32771
static struct lista *nickscomp_hash[NICKSCOMP_HASH_LEN];

#define CLONES_DINAMICOS_HASH_LEN 31
static struct lista *clones_dinamicos_hash[CLONES_DINAMICOS_HASH_LEN];

#define NICKS_CLONES_DINAMICOS_HASH_LEN 31
static struct lista
     *nicks_clones_dinamicos_hash[NICKS_CLONES_DINAMICOS_HASH_LEN];



/******
static int hash(char *p,int campo,int max)
{
int i=0;
char c;

  while(--campo) p=strchr(p,' ')+1;
  while(((c=*p++)!='\0')&&(c!=' ')) {
    i<<=1;
    if(i>255) {i-=256; i^=1;}
    i^=c;
  } 
  return i%max; 
}
*****/

static int hash(char *p, int campo, int max, int canonice)
{
  unsigned long h = 0, g;
  unsigned char c;

  while (--campo)
    p = strchr(p, ' ') + 1;     /* Nos vamos al campo demandado */
  while (((c = *p++) != '\0') && (c != ' ')) { /* La funcion HASH del formato ELF */
    if (canonice)
      c = tolowertab[c];
    h = (h << 4) + c;
    g = h & 0xF0000000;
    if (g)
      h ^= g >> 24;
    h &= ~g;
  }
  return h % max;
}

static struct lista *busca_lista(char *texto, int campo, int canonice,
                                 struct lista *tabla_hash[],
                                 int tabla_hash_len)
{
  struct lista *p;
  unsigned char *p2, *p3;
  int i, len;

  p = tabla_hash[hash(texto, 1, tabla_hash_len, canonice)];
  len = strlen(texto);
  while (p != NULL) {
    p2 = &(p->registro);
    i = campo;
    while (--i)
      p2 = strchr(p2 + 1, ' ');
    if (campo > 1)
      p2++;
    if ((*(p2 + len) == ' ') || (*(p2 + len) == '\0')) {
      if (canonice) {
        for (i = len, p3 = texto; i; i--) {
          if (tolowertab[*p2++] != tolowertab[*p3++])
            break;
        }
        if (!i)
          return p;
      }
      else if (!strncmp(p2, texto, len))
        return p;
    }
    p = p->siguiente;
  }
  return NULL;
}

static void guarda_lista(char *texto, int campo, int canonice,
                         struct lista *tabla_hash[], int tabla_hash_len)
{
  struct lista *p, *p2;
  int i;

  i = hash(texto, campo, tabla_hash_len, canonice);

  p = malloc(sizeof(struct lista) + strlen(texto)); /* El '\0' ya se contabilizo en la estructura */
  if (!p) {
    descarga_modulos();
    cierra_berkeley_db();
    exit(1);
  }

  p2 = tabla_hash[i];
  p->siguiente = p2;
  if (p2 != NULL)
    p2->anterior = &(p->siguiente);
  p->anterior = tabla_hash + i;
  tabla_hash[i] = p;
  strcpy(&(p->registro), texto);
}

static void borra_lista(struct lista *registro)
{
  struct lista *p;

  p = registro->siguiente;
  *(registro->anterior) = p;
  if (p != NULL)
    p->anterior = registro->anterior;
  free(registro);
}

void lee_historico(void)
{
  char buf[BUF_LEN];
  char buf_write[1000];
  FILE *handle;
  char *p;
  time_t tiempo = 0, tiempo2;

  sprintf(buf_write, "Cargando archivo historico de conexiones\n");
  write(1, buf_write, strlen(buf_write)); /* STDOUT */

  puntero_historico = 0;
  num_usuarios_max = 0;
  handle = fopen("historico.log", "r");
  if (handle == NULL)
    goto lee_db;

  while (fgets(buf, BUF_LEN / 2, handle) != NULL) {

    // printf("%s",buf);

    p = strrchr(buf, ' ');
    num_conexiones_hist[puntero_historico] = atol(p + 1);
    *p = '\0';
    p = strrchr(buf, ' ');
    num_usuarios_hist[puntero_historico] = atol(p + 1);
    *p = '\0';
    p = strrchr(buf, ' ');
    tiempo_hist[puntero_historico] = tiempo2 = atol(p + 1);

    if ((tiempo2 / HISTORICO_MUESTRAS) == (tiempo / HISTORICO_MUESTRAS)) {
/*
** Encontramos varios historicos para el mismo minuto.
**
** No deberia ocurrir a menos que el programe se lance y se pare
** varias veces en un intervalo de tiempo muy corto.
**
** Nos quedamos con el primer registro leido (los nuevos seran
** sobreeescritos al leer el siguiente registro).
*/
      tiempo -= HISTORICO_MUESTRAS;
      if (--puntero_historico < 0)
        puntero_historico = LONG_HISTORICO - 1;
    }

    if (!tiempo)
      tiempo = tiempo2;         // El primero
    else
      tiempo += HISTORICO_MUESTRAS;

    while ((tiempo2 / HISTORICO_MUESTRAS) != (tiempo / HISTORICO_MUESTRAS)) {
      num_conexiones_hist[puntero_historico] =
        num_usuarios_hist[puntero_historico] = 0;
      tiempo_hist[puntero_historico] = tiempo;
      tiempo += HISTORICO_MUESTRAS;
      if (++puntero_historico >= LONG_HISTORICO)
        puntero_historico = 0;
    }

    tiempo = tiempo2;

    if (num_usuarios_hist[puntero_historico] >= num_usuarios_max) {
      num_usuarios_max = num_usuarios_hist[puntero_historico];
      tiempo_num_usuarios_max = tiempo_hist[puntero_historico];
    }

    if (++puntero_historico >= LONG_HISTORICO)
      puntero_historico = 0;
  }
  fclose(handle);

  num_conexiones_hist[puntero_historico] = 0;
  num_usuarios_hist[puntero_historico] = 0;
  tiempo_hist[puntero_historico] = time(NULL);

lee_db:
/*
** Actualizamos la base de datos para que refleje el maximo
** de usuarios, para poder rotar los logs sin perderlo.
*/
  {
    dbt clave, contenido;
    void *transaccion;
    int st;
    long num_usuarios_max2;
    int flag = 0;

    clave.datos = "RECORD_USUARIOS";
    clave.len = strlen(clave.datos) + 1; /* Incluye el '\0' */
    transaccion = inicia_txn_db();
    st = get_db(db_varios, transaccion, &clave, &contenido);
    if (contenido.len != 0) {
      assert(contenido.len == 8);
      num_usuarios_max2 = *((long *) (contenido.datos));
      if (num_usuarios_max2 > num_usuarios_max) {
        num_usuarios_max = num_usuarios_max2;
        tiempo_num_usuarios_max =
          *((time_t *) (((long *) (contenido.datos)) + 1));
      }
      else {
        flag = !0;
      }
    }
    else {
      contenido.datos = malloc(8);
      contenido.len = 8;
      flag = !0;
    }

    if (flag) {
      *((long *) (contenido.datos)) = num_usuarios_max;
      *((time_t *) (((long *) (contenido.datos)) + 1)) =
        tiempo_num_usuarios_max;
      st = put_db(db_varios, transaccion, &clave, &contenido);
    }
    if (contenido.len)
      free(contenido.datos);
    compromete_txn_db(transaccion);
  }
}

void guarda_servidor(char *servidor)
{
  char numeric[10];
  char *server;
  char *padre;

  guarda_lista(servidor, 3, NO_CANONICE, servers_hash, SERVERS_HASH_LEN);

  server = strchr(servidor, ' ') + 1;
  numeric[0] = *server;
  numeric[1] = *(server + 1);
  numeric[2] = '\0';
  if (numeric[1] == ' ')
    numeric[1] = '\0';

  nodo_conectado(numeric);

  server = strrchr(servidor, ' ') + 1;

  padre = busca_servidor_compress(servidor);
  if (padre) {
    padre = strrchr(padre, ' ') + 1;
  }
  notifica_server_join_modulo(server, numeric, padre);
}

static void borrar_hash(struct lista *tabla_hash[], int tabla_hash_len)
{
  int i;
  struct lista *p, *p2;

  for (i = 0; i < tabla_hash_len; i++) {
    p = tabla_hash[i];
    while (p != NULL) {
      p2 = p->siguiente;
      free(p);
      p = p2;
    }
    tabla_hash[i] = NULL;
  }
}

void borra_db(void)
{
  static int ya_ejecutado = 0;

  num_usuarios = 0;

  borrar_hash(servers_hash, SERVERS_HASH_LEN);
  borrar_hash(nicks_hash, NICKS_HASH_LEN);
  borrar_hash(ips_hash, IPS_HASH_LEN);
  borrar_hash(nickscomp_hash, NICKSCOMP_HASH_LEN);
  if (!0 || !ya_ejecutado) {    /* Util para debug */
    ya_ejecutado = !0;
    borrar_hash(clones_dinamicos_hash, CLONES_DINAMICOS_HASH_LEN);
    borrar_hash(nicks_clones_dinamicos_hash, NICKS_CLONES_DINAMICOS_HASH_LEN);
  }

#ifdef VERBOSE_DB
  {
    char buf_write[1000];

    sprintf(buf_write, "***Borrado de toda la base de datos\n");
    write(1, buf_write, strlen(buf_write)); /* STDOUT */
  }
#endif

  borra_nodos_conectados();

  notifica_server_split_modulo(NULL, NULL); /* Indica que se caen todos */
}

/*
** encuentra_usuario_compress
** Busca un usuario comprimido en el heap.
** Devuelve el puntero al registro "U" o NULL
*/

char *encuentra_usuario_compress(char *usuario)
{
  struct lista *p;
  p =
    busca_lista(usuario, 3, NO_CANONICE, nickscomp_hash, NICKSCOMP_HASH_LEN);
  if (p != NULL)
    return &(p->registro);
  return NULL;
}

/*
** encuentra_usuario_nick
** Encuentra un usuario, dado su nick (completo)
** La b\172squeda es case insensible
** Si ese usuario no existe, devuelve NULL
*/
char *encuentra_usuario_nick(char *usu)
{
  struct lista *p;

  p = busca_lista(usu, 1, CANONICE, nicks_hash, NICKS_HASH_LEN);
  if (p == NULL)
    return NULL;
  return encuentra_usuario_compress(strchr(&(p->registro), ' ') + 1);
}

/*
** buscar_iline
** Devuelve el numero de clones permitidos para esa IP/HOST.
*/
int buscar_iline(char *ip, char *usuario)
{
/*
** Si es negativo, se trata del valor por defecto
*/
#define CLONES_DEFECTO	-2
  int clones = CLONES_DEFECTO;  /* Clones por defecto, si no hay GDBM */
  dbt clave;
  dbt contenido;
  void *transaccion;
  time_t tiempo, now = time(NULL);
  unsigned char *p;
  char buf[BUF_LEN];
  struct lista *clones_dinamicos;
  unsigned char ip2[100];
  char *ip3 = ip;

  assert(strlen(ip) < 95);

  p = ip2;

  while ((*p++ = tolowertab[*(unsigned char *) ip3++]));

/*
** Clones dinamicos
*/
  clones_dinamicos =
    busca_lista(ip2, 1, NO_CANONICE, clones_dinamicos_hash,
                CLONES_DINAMICOS_HASH_LEN);
  if (clones_dinamicos != NULL)
    return atoi(strchr(&(clones_dinamicos->registro), ' ') + 1);

/*
** Clones estaticos
*/
  transaccion = inicia_txn_db_no_sync();

  clave.datos = ip2;
  clave.len = strlen(ip2) + 1;  /* El '\0' final */
  get_db(db_ilines, transaccion, &clave, &contenido);
  p = contenido.datos;
  if (p != NULL) {              /* Existe ILINE */
    *((unsigned long *) p + 2) = htonl(now);
    put_db(db_ilines, transaccion, &clave, &contenido);

    tiempo = ntohl(*((unsigned long *) p + 1));

    if ((tiempo <= 0) || (tiempo >= now)) {
      p += 12;                  /* Nos saltamos los timestamps */
      if (*p != '\0')
        clones = atoi(p);
    }
    if ((tiempo > 0) && (tiempo < now)) { /* Ha caducado */
#ifndef DEVELOP
      if (bdd_flujo_abierto2('i')) {
/*
** Cuando conectamos Olimpo a una red, puede ser que
** nos lleguen usuarios cuyos clones ya han caducado, pero
** ANTES de que Olimpo haya tenido ocasion de sincronizar sus
** numeros de serie de las BDD con su HUB.
** En casos asi, y tratandose de clones, la solucion mas
** sencilla consiste en ignorar la entrada del usuario y no
** dar de baja los clones... de momento. Ya se daran de baja
** en su segunda conexion, o via mi script "z4".
*/
        del_db(db_ilines, transaccion, &clave);

        bdd_envia_registro('i', (char *) (clave.datos), NULL);
/* Con 12 se salta los TIMESTAMPS */
        sprintf(buf, "EXPIRACION %s %s", (char *) clave.datos, p + 12);
        guarda_log(buf, "olimpo");
      }
#else
      strcpy(buf, "Ha caducado"); /* Para ver algo con el GDB */
#endif
    }
    else if ((tiempo > 0) && (tiempo - now < 60 * 60 * 24 * 15)) { /* A punto de caducar */
      char buf_time[100];

      p = strchr(usuario, ' ') + 1; /* Envia el NOTICE */
      p = strchr(p, ' ') + 1;
      sprintf(buf, "%s NOTICE %s :EXPIRE\n", NUM_NODO, p);
      p = strchr(buf, ' ') + 1;
      p = strchr(p, ' ') + 1;
      p = strchr(p, ' ') + 1;
      sprintf(p, ":\002\0034Su permiso de %d clones para '%s' caduca el %s",
              abs(clones), ip, ctime_sincronizado(&tiempo, buf_time));
      p = strchr(p, '\n');
      sprintf(p,
              ". Siga las instrucciones en 'http://www.irc-hispano.org/servicios/compraclones.html' "
              "para proceder a su renovacion. Tal vez le interese tambien la nueva tecnologia de "
              "'IRC sobre Tuneles IP', cuya informacion puede encontrar en 'http://www.argo.es/clones/clones2.html'\n");
#ifndef DEVELOP
      dbuf_write(buf);
#endif
    }
    free(contenido.datos);
  }
  if (clones == CLONES_DEFECTO) { /* No hay ILINE */
    clave.datos = ".";
    clave.len = 2;
    get_db(db_ilines, transaccion, &clave, &contenido);
    p = contenido.datos;
    if (p != NULL) {
      p += 12;
/*
** Si es negativo, se trata del valor por defecto
*/
      if (*p != '\0')
        clones = -atoi(p);
      free(contenido.datos);
    }
  }

  compromete_txn_db(transaccion);

  return clones;
}

/*
** encuentra_ip
** Busca una IP o HOST determinado en el heap (empezando donde se le diga).
** Solo busca en los registros "U". La cadena debe medir menos que BUF_LEN.
** Devuelve un puntero a donde lo encontr\163 o NULL.
*/
char *encuentra_ip(void **p, char *cadena)
{
  struct lista *ips;

  ips = *p;
  if (ips == NULL) {
    ips = busca_lista(cadena, 1, NO_CANONICE, ips_hash, IPS_HASH_LEN);
  }
  else {
    ips = ips->siguiente;
  }

  while (ips != NULL) {
    if (!strcmp(&(ips->registro), cadena)) {
      *p = ips;
      return &(ips->registro);
    }
    ips = ips->siguiente;
  }
  *p = NULL;
  return NULL;
}

/*
** borra_usuario_ip
** Borra una de las IP especificada. Puede haber mas.
*/

static void borra_usuario_ip(char *ip)
{
  struct lista *p;

  p = busca_lista(ip, 1, NO_CANONICE, ips_hash, IPS_HASH_LEN);
  if (p != NULL)
    borra_lista(p);
}

/*
** borra_usuario_compress
** Borra un usuario, dado su nick comprimido
** Si ese usuario no existe, no hace nada
*/
void borra_usuario_compress(char *usu, int notify)
{
  struct lista *p;
  char *p2;
  char buf[BUF_LEN];

  p = busca_lista(usu, 3, NO_CANONICE, nickscomp_hash, NICKSCOMP_HASH_LEN);
  if (p == NULL)
    return;

/*
** Ponemos la notificacion al principio
** para que el usuario todavia exista en las
** estructuras internas, y los modulos
** puedan hacer consultas adicionales sobre el
*/
  if (notify)
    notifica_nick_salida_modulo(usu);

  p2 = &(p->registro);
  p2 = strchr(p2, ' ');
  *p2++ = '\0';
  *strchr(p2, ' ') = '\0';
  borra_usuario_ip(&(p->registro));
  strcpy(buf, p2);              /* Necesario porque entre las dos rutinas de borrado hay un bucle */
  borra_lista(p);
  if (!borra_usuario_bucle) {
    borra_usuario_bucle = !0;
    num_usuarios--;
    borra_usuario_nick(buf, 0);
    borra_usuario_bucle = 0;
  }
}

/*
** borra_usuario_nick
** Borra un usuario, dado su nick (completo)
** Si ese usuario no existe, no hace nada
*/
void borra_usuario_nick(char *usu, int notify)
{
  struct lista *p;
  char buf[BUF_LEN];

  p = busca_lista(usu, 1, CANONICE, nicks_hash, NICKS_HASH_LEN);
  if (p == NULL)
    return;

  strcpy(buf, strchr(&(p->registro), ' ') + 1); /* Esto es porque al borrar el nick comprimido */

/*
** Ponemos la notificacion al principio
** para que el usuario todavia exista en las
** estructuras internas, y los modulos
** puedan hacer consultas adicionales sobre el
*/
  if (notify)
    notifica_nick_salida_modulo(buf);

  borra_lista(p);               /* se vuelve a invocar esta funcion */

  if (!borra_usuario_bucle) {
    borra_usuario_bucle = !0;
    num_usuarios--;
    borra_usuario_compress(buf, 0);
    borra_usuario_bucle = 0;
  }
}

/*
** busca_servidor
** Busca un servidor determinado, por nombre.
** Devuelve un puntero a su registro o NULL
*/
char *busca_servidor(char *serv)
{
  struct lista *p;

  p = busca_lista(serv, 3, NO_CANONICE, servers_hash, SERVERS_HASH_LEN);
  while (p != NULL) {
    if (!strcmp(strrchr(&(p->registro), ' ') + 1, serv)) {
      return &(p->registro);
    }
  }
  return NULL;
}

/*
** busca_servidor_compress
** Busca un servidor por su letra.
** Devuelve un puntero a su registro o NULL
*/
char *busca_servidor_compress(char *serv)
{
  int i, flag;
  char *p2, *p3;
  struct lista *p;
  char servidor[1024];

  p2 = strchr(serv, ' ');
  assert(p2);
  memcpy(servidor, serv, p2 - serv);
  servidor[p2 - serv] = '\0';

  for (i = 0; i < SERVERS_HASH_LEN; i++) {
    p = servers_hash[i];
    while (p != NULL) {
      p2 = strchr(&(p->registro), ' ') + 1;
      p3 = strchr(p2, ' ');
      *p3 = '\0';
      flag = !strcmp(p2, servidor);
      *p3 = ' ';
      if (flag)
        return p2;
      p = p->siguiente;
    }
  }
  return NULL;
}


/*
** borra_servidor
** Borra un servidor y sus usuarios, y hace lo mismo con los
** dem\141s servidores que cuelgan de \151l.
** El parametro es un puntero al registro a borrar.
*/
void borra_servidor(char *serv, char *causa)
{
  int i;
  struct lista *serv_borrar[64 * 64]; /* Array de punteros a servidores a borrar */
  struct lista *p;
  char *p2, *p3;
  char *nodo;
  int num_nodo_padre, num_nodo;
  char *nombre_servidor;
  char *troceado = NULL;
  int troceado_len = 0;

  for (i = 0; i < 64 * 64; i++)
    serv_borrar[i] = NULL;

  p = busca_lista(serv, 3, NO_CANONICE, servers_hash, SERVERS_HASH_LEN);

  nodo = strchr(&(p->registro), ' ') + 1;
  num_nodo = convert2n[(unsigned char) *(nodo)];
  if (*(nodo + 1) != ' ') {     /* numeric largo */
    num_nodo = num_nodo * 64 + convert2n[(unsigned char) *(nodo + 1)];
  }

  nodo_desconectado(nodo);
  serv_borrar[num_nodo] = p;

  nombre_servidor = strrchr(&(p->registro), ' ') + 1;
  notifica_server_split_modulo(nombre_servidor, causa);

/*
** Lo borramos todo al final
*/
  //borra_lista(p);

  for (i = 0; i < SERVERS_HASH_LEN; i++) {
    p = servers_hash[i];
    while (p != NULL) {
      nodo = &(p->registro);
      num_nodo_padre = convert2n[(unsigned char) *(nodo)];
      if (*(nodo + 1) != ' ') { /* numeric largo */
        num_nodo_padre =
          num_nodo_padre * 64 + convert2n[(unsigned char) *(nodo + 1)];
      }
      if (serv_borrar[num_nodo_padre]) { /* Otro servidor a borrar */
        nodo = strchr(&(p->registro), ' ') + 1;
        num_nodo = convert2n[(unsigned char) *(nodo)];
        if (*(nodo + 1) != ' ') { /* numeric largo */
          num_nodo = num_nodo * 64 + convert2n[(unsigned char) *(nodo + 1)];
        }

        if (!serv_borrar[num_nodo]) { /* Es un nodo que aun no hemos marcado como borrado */
          nodo_desconectado(nodo);
          serv_borrar[num_nodo] = p;
          nombre_servidor = strrchr(&(p->registro), ' ') + 1;
          notifica_server_split_modulo(nombre_servidor, NULL);

/*
** Lo borramos todo al final 
*/
          //borra_lista(p);
          i = -1;               /* Empieza desde el principio */
          break;
        }
      }
      p = p->siguiente;
    }
  }

/*
** Llegado a este punto, tenemos en "serv_borrar"
** la lista de todos los servidores a dar
** de baja. Ahora hay que borrar los usuarios asociados.
*/

  for (i = 0; i < NICKSCOMP_HASH_LEN; i++) {
    char buf[BUF_LEN];
    struct lista *pbackup;

    p = nickscomp_hash[i];
    while (p != NULL) {
/*
** El follon con el 'troceado'
** es necesario para poder enviar
** las notificaciones mientras
** estamos recorriendo los hashes y
** modificandolos al vuelo.
*/
      if (strlen(&(p->registro)) > troceado_len) {
        troceado_len = strlen(&(p->registro));
        if (troceado)
          free(troceado);
        troceado = malloc(troceado_len + 1); /* Por el '\0' final */
        assert(troceado);
      }
      strcpy(troceado, &(p->registro));

      p2 = troceado;
      p3 = strrchr(p2, ' ');
      *p3 = '\0';
      nodo = strrchr(p2, ' ');
      *nodo = '\0';
      *p3 = ' ';
      nodo = strrchr(p2, ' ') + 1;
      num_nodo = convert2n[(unsigned char) *(nodo)];
      if (*(nodo + 1) != '\0') { /* numeric largo */
        num_nodo = num_nodo * 64 + convert2n[(unsigned char) *(nodo + 1)];
      }
      *(nodo + strlen(nodo)) = ' ';

      if (serv_borrar[num_nodo]) { /* Es nuestro */
        p3 = strchr(p2, ' ') + 1;
        *strchr(p3, ' ') = '\0';
        strcpy(buf, p3);        /* Para evitar un bucle */
        pbackup = p->siguiente;

/*
** Ponemos la notificacion al principio
** para que el usuario todavia exista en las
** estructuras internas, y los modulos
** puedan hacer consultas adicionales sobre el
** anterior.
*/
        borra_usuario_nick(buf, 1); /* Notifica */
/*
** Hacemos esto en segundo lugar para no
** interferir con las notificaciones.
**
** No hace falta borrar nada, ya que ya lo ha hecho
** la rutina anterior.
*/
        //borra_lista(p);

/*
** Atencion, es importante que cuando se borra
** un registro, el resto de registros no cambien
** de posicion, para que la linea siguiente sea valida.
*/
        p = pbackup;
      }
      else {
        p = p->siguiente;
      }
    }
  }
  if (troceado)
    free(troceado);

/*
** No borramos de forma efectiva los nodos
** hasta el final, por si un modulo pide
** informacion del nodo raiz de un usuario
** que sale por SPLIT.
*/
  for (i = 0; i < 64 * 64; i++) {
    p = serv_borrar[i];
    if (p)
      borra_lista(p);
  }

  notifica_servers_split_end_modulo();
}

/*
** privilegiado
*/
int privilegiado(char *usu, char *comando, int privil)
{
  int privil2 = 0;
  char *p, *p2, *p3, *p4;
  int num_nodo;

  p = encuentra_usuario_nick(usu);
  if (p == NULL)
    return 0;                   /* Ign\163ralo */

  p2 = p;
  p = strchr(p, ' ') + 1;       /* Nick */
  p3 = p;
  p = strchr(p, ' ') + 1;       /* Comprimido */
  p4 = p;
  num_nodo = convert2n[(unsigned char) *p4];
  if (*(p4 + 3) != ' ') {
    num_nodo = num_nodo * 64 + convert2n[(unsigned char) *(p4 + 1)];
    assert(*(p4 + 5) == ' ');
  }

  p = strchr(p, ' ') + 1;       /* Modos */
  while (*p != ' ') {
    switch (*p++) {
    case 'o':
      privil2 |= PRIVIL_IRCOP;
      break;
    case 'h':
      privil2 |= PRIVIL_HELPER;
      break;
    case 'r':
      privil2 |= PRIVIL_NICK;
      break;
    }
  }

  if ((privil2 & (PRIVIL_NICK | PRIVIL_IRCOP)) ==
      (PRIVIL_NICK | PRIVIL_IRCOP)) {
    if (!strncmp(p3, "jcea ", 5) &&
#ifdef IRC_HISPANO
        (num_nodo == 1)
#endif
#ifdef ESNET
        (num_nodo == 2)
#endif
      )
      privil2 |= PRIVIL_JCEA;
/*
    if (!strncmp(p2, "banana.lleida.net ", 18) && !strncmp(p3, "BReI ", 5) &&
        (num_nodo == 30))
*/
    if (!strncmp(p2, "luna.irc-hispano.org ", 21) && !strncmp(p3, "BReI ", 5)
        && (num_nodo == 30))

      privil2 |= PRIVIL_BREI;
  }

  if (privil & privil2) {
    guarda_log_nick(comando, "olimpo", usu);
  }
  return privil2;
}

static void dump_hash(int i, char *prefijo, struct lista *tabla_hash[],
                      int tabla_hash_len)
{
  int j;
  struct lista *p;

  for (j = 0; j < tabla_hash_len; j++) {
    p = tabla_hash[j];
    while (p != NULL) {
      write(i, prefijo, strlen(prefijo));
      write(i, " ", 1);
      write(i, &(p->registro), strlen(&(p->registro)));
      write(i, "\n", 1);
      p = p->siguiente;
    }
  }
}

void dump_db(int i)
{
  dump_hash(i, "S", servers_hash, SERVERS_HASH_LEN);
  dump_hash(i, "U", nickscomp_hash, NICKSCOMP_HASH_LEN);
}

void db_notifica_nicks_registrados(void)
{
  int j;
  struct lista *p;
  char *p1, *p2, *p3;

  for (j = 0; j < NICKSCOMP_HASH_LEN; j++) {
    p = nickscomp_hash[j];
    while (p != NULL) {
      p1 = strchr(&(p->registro), ' ') + 1; /* Nick */
      p1 = strchr(p1, ' ') + 1; /* Numeric */
      p2 = strchr(p1, ' ') + 1; /* Principio de modos */
      p3 = strchr(p2, ' ');     /* Fin de modos */
      p2 = strchr(p2, 'r');     /* Busca el modo "+r" */
      if (p2 && (p2 < p3))
        notifica_nicks_registrados2(p1);
      p = p->siguiente;
    }
  }
}

void db_notifica_nicks(void)
{
  int j;
  struct lista *p;
  char *p1, *p2, *p3;

  for (j = 0; j < NICKSCOMP_HASH_LEN; j++) {
    p = nickscomp_hash[j];
    while (p != NULL) {
      p1 = strchr(&(p->registro), ' ') + 1; /* Nick */
      p1 = strchr(p1, ' ') + 1; /* Numeric */
      p2 = strchr(p1, ' ') + 1; /* Principio de modos */
      p3 = strchr(p2, ' ');     /* Fin de modos */
      notifica_nicks2(p1);
      p = p->siguiente;
    }
  }
}

void distribucion_users(char *buf2, int i)
{
  long usr = 0;
  int j, k, k2;
  struct lista *p;
  long usuarios[64 * 64];
  char *orden_alfabetico[64 * 64];
  char *nodo;
  int num_nodo;

  for (j = 0; j < 64 * 64; j++)
    usuarios[j] = 0;

  for (j = 0; j < NICKS_HASH_LEN; j++) {
    p = nicks_hash[j];
    while (p != NULL) {         /* Cuenta usuarios por nodo */
      usr++;
      nodo = strchr(&(p->registro), ' ') + 1;
      num_nodo = convert2n[(unsigned char) *(nodo)];
      if (strlen(nodo) == 5) {  /* numeric largo */
        num_nodo = num_nodo * 64 + convert2n[(unsigned char) *(nodo + 1)];
      }
      usuarios[num_nodo]++;
      p = p->siguiente;
    }
  }
  sprintf(buf2 + i, "Numero de usuarios en la red: %ld\n", usr);
  dbuf_write(buf2);

  if (!usr)
    usr = 1;                    /* Para evitar divisiones por cero */

  for (k = j = 0; j < SERVERS_HASH_LEN; j++) {
    p = servers_hash[j];
    while (p != NULL) {
      orden_alfabetico[k++] = &(p->registro);
      p = p->siguiente;
    }
  }
  k2 = k;
  while (k) {
    char *temp;

    for (j = 0; j < k - 1; j++) {
      if (strcmp
          (strrchr(orden_alfabetico[j], ' ') + 1,
           strrchr(orden_alfabetico[j + 1], ' ') + 1) > 0) {
        temp = orden_alfabetico[j];
        orden_alfabetico[j] = orden_alfabetico[j + 1];
        orden_alfabetico[j + 1] = temp;
      }
    }
    k--;
  }

  for (j = 0; j < k2; j++) {
    nodo = strchr(orden_alfabetico[j], ' ') + 1;
    num_nodo = convert2n[(unsigned char) *(nodo)];
    if (*(nodo + 1) != ' ') {   /* numeric largo */
      num_nodo = num_nodo * 64 + convert2n[(unsigned char) *(nodo + 1)];
    }
    nodo = strchr(nodo, ' ') + 1;

    sprintf(buf2 + i, "%s (%d): %ld (%.1f%%)\n", nodo, num_nodo,
            usuarios[num_nodo], ((100.0 * usuarios[num_nodo]) / usr));
    dbuf_write(buf2);
  }
}


static int hash_stats(int *min, int *max, struct lista *tabla_hash[],
                      int tabla_hash_len)
{
  struct lista *p;
  int i, cont, num;

  *min = 32767;
  *max = -1;
  num = 0;
  for (i = 0; i < tabla_hash_len; i++) {
    p = tabla_hash[i];
    cont = 0;
    while (p != NULL) {
      p = p->siguiente;
      cont++;
      num++;
    }
    if (cont < *min)
      *min = cont;
    if (cont > *max)
      *max = cont;
  }
  return num;
}

int serversstats_db(int *min, int *max)
{
  return hash_stats(min, max, servers_hash, SERVERS_HASH_LEN);
}

int usersstats_db(int *min, int *max)
{
  return hash_stats(min, max, nickscomp_hash, NICKSCOMP_HASH_LEN);
}

int nicksstats_db(int *min, int *max)
{
  return hash_stats(min, max, nicks_hash, NICKS_HASH_LEN);
}

int ipsstats_db(int *min, int *max)
{
  return hash_stats(min, max, ips_hash, IPS_HASH_LEN);
}

int dinamic_ilines_stats_db(int *min, int *max)
{
  return hash_stats(min, max, clones_dinamicos_hash,
                    CLONES_DINAMICOS_HASH_LEN);
}

int dinamic_ilines_nicks_stats_db(int *min, int *max)
{
  return hash_stats(min, max, nicks_clones_dinamicos_hash,
                    NICKS_CLONES_DINAMICOS_HASH_LEN);
}

static void guarda_ip(char *ip)
{
  guarda_lista(ip, 1, NO_CANONICE, ips_hash, IPS_HASH_LEN);
}

static void guarda_iline_dinamica(char *nick, int num_clones)
{
  char buf[BUF_LEN];
  char *p;

  strcpy(buf, nick);
  p = strchr(buf, ' ') + 1;
  *strchr(p, ' ') = '\0';
  guarda_lista(buf, 2, CANONICE, nicks_clones_dinamicos_hash,
               NICKS_CLONES_DINAMICOS_HASH_LEN);
  sprintf(p, "%d", num_clones);
  guarda_lista(buf, 1, NO_CANONICE, clones_dinamicos_hash,
               CLONES_DINAMICOS_HASH_LEN);
}

static void guarda_nick(char *texto)
{
  guarda_lista(texto, 1, CANONICE, nicks_hash, NICKS_HASH_LEN);
}

void guarda_usuario(char *usu, int notify)
{
  char buf[BUF_LEN];
  char *p;
  time_t tiempo = time(NULL);

/*
** Actualizamos la base de datos para que refleje el maximo
** de usuarios, para poder rotar los logs sin perderlo.
*/
  num_usuarios++;
  if (num_usuarios >= num_usuarios_max) {
    char buf[16];
    dbt clave, contenido;
    void *transaccion;
    int st;

    num_usuarios_max = num_usuarios;
    tiempo_num_usuarios_max = tiempo;

    clave.datos = "RECORD_USUARIOS";
    clave.len = strlen(clave.datos) + 1; /* Incluye el '\0' */
    transaccion = inicia_txn_db_no_sync();
    contenido.datos = buf;
    contenido.len = 8;
    *((long *) (contenido.datos)) = num_usuarios_max;
    *((time_t *) (((long *) (contenido.datos)) + 1)) =
      tiempo_num_usuarios_max;
    st = put_db(db_varios, transaccion, &clave, &contenido);
    compromete_txn_db(transaccion);
  }

  strcpy(buf, usu);
  p = strchr(buf, ' ');
  *p++ = '\0';
  guarda_ip(buf);
  *strchr(strchr(p, ' ') + 1, ' ') = '\0';
  guarda_nick(p);

  guarda_lista(usu, 3, NO_CANONICE, nickscomp_hash, NICKSCOMP_HASH_LEN);

/*
** Ponemos la notificacion al final
** para que el usuario ya exista en las
** estructuras internas, y los modulos
** puedan hacer consultas adicionales sobre el
*/
  if (notify)
    notifica_nick_entrada_modulo(strchr(p, ' ') + 1);
}

void incrementa_num_conexiones(void)
{
#if defined(LOG_HISTORICO) && (!defined(DEVELOP))
  char buf[BUF_LEN];
  int handle;
#endif
  time_t tiempo = time(NULL);
  static time_t ultimo_historico = 0;

  num_conexiones++;

  if (!ultimo_historico)
    ultimo_historico = tiempo;

  if ((tiempo / HISTORICO_MUESTRAS) != (ultimo_historico / HISTORICO_MUESTRAS)) { /* No son el mismo minuto */
    char buf_time[100];

#if defined(LOG_HISTORICO) && (!defined(DEVELOP))
    handle = open("historico.log", O_APPEND | O_WRONLY | O_CREAT, 0644);
    strcpy(buf,
           ctime_sincronizado(&tiempo_hist[puntero_historico], buf_time));
    sprintf(buf + 24, " %lu %lu %lu\n", tiempo_hist[puntero_historico],
            num_usuarios_hist[puntero_historico],
            num_conexiones_hist[puntero_historico]);
    write(handle, buf, strlen(buf));
    close(handle);
#endif

    if (++puntero_historico >= LONG_HISTORICO)
      puntero_historico = 0;
    num_usuarios_hist[puntero_historico] = 0;
    num_conexiones_hist[puntero_historico] = 0;
    ultimo_historico = tiempo;
  }
  num_conexiones_hist[puntero_historico]++;

  if (num_usuarios_hist[puntero_historico] <= num_usuarios) {
    num_usuarios_hist[puntero_historico] = num_usuarios;
    tiempo_hist[puntero_historico] = tiempo;
  }
}

void mass_kill(char *ip)
{
}

void iline_dinamica(char *nick)
{
  int num_clones;
  char buf[BUF_LEN];
  struct lista *iline_nick;
  struct lista *iline_ip;
  char *p;

  strcpy(buf, strchr(nick, ' ') + 1);
  *strchr(buf, ' ') = '\0';
  iline_ip =
    busca_lista(nick, 1, NO_CANONICE, clones_dinamicos_hash,
                CLONES_DINAMICOS_HASH_LEN);
  iline_nick =
    busca_lista(nick, 2, CANONICE, clones_dinamicos_hash,
                CLONES_DINAMICOS_HASH_LEN);
  num_clones = buscar_iline(buf, nick);
  if (num_clones < 0) {         /* No dinamico o ya expirado */
    if (iline_ip) {             /* Tenemos conexiones */
      borra_lista(iline_ip);
      borra_lista(iline_nick);
      mass_kill(nick);
    }
  }
  else {                        /* Iline dinamica */
    if (!iline_ip) {            /* Nuevo */
      guarda_iline_dinamica(nick, num_clones);
    }
  }
}
