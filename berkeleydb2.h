/*
** BERKELEYDB2.H
*/

/*
    $Id: berkeleydb2.h,v 1.14 2002/02/21 20:46:12 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#ifndef BERKELEYDB2_H
#define BERKELEYDB2_H

#include <db.h>

typedef struct {
  void *datos;
  unsigned long len;
} dbt;

typedef struct {
  DBC *cursor;
  void *mem;
  unsigned long mem_len;
  DBT data;
  void *estado;
} c_db;

typedef void *p_db;
typedef void *txn;

#define BERKELEY_DB_NOTFOUND 2346

#endif
