/*
** Opciones GNU
*/

          -nbad -bap -nbc -bbo -bl -bli2 -bls -ncdb -nce -cp1 -cs -di2
          -ndj -nfc1 -nfca -hnl -i2 -ip5 -lp -pcs -psl -nsc -nsob

/*
** Mi estilo
*/
--tab-size1
//--preserve-mtime
--indent-level2
--braces-on-struct-decl-line
--braces-on-if-line
--dont-break-procedure-type
--break-after-boolean-operator
--declaration-indentation0
--dont-cuddle-else
--line-comments-indentation0
--case-indentation0
--no-space-after-function-call-names
--dont-format-first-column-comments
