/*
** M_DB.C
**
** $Id: m_db.c,v 1.51 2003/10/20 18:19:14 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#include "m_db.h"
#include "config.h"
#include "privmsg.h"
#include "db.h"
#include "shared.h"
#include "modulos.h"
#include "berkeleydb.h"
#include "dbuf.h"

#include <assert.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <sys/mman.h>


static unsigned char flujos[DB_FINAL - DB_INICIO + 1];

void bdd_offline(void)
{
  int i;

  for (i = 0; i <= (DB_FINAL - DB_INICIO); i++)
    flujos[i] = 0;
}

static void bdd_online(unsigned char bd)
{
  assert((bd >= 'a') && (bd <= 'z'));
  flujos[bd - DB_INICIO] = !0;
}

/*
** Esta rutina nos dice si ya sabemos los numeros de serie
** de las BDD.
*/
int bdd_flujo_abierto2(unsigned char bd)
{
  assert((bd >= 'a') && (bd <= 'z'));
  return flujos[bd - DB_INICIO] && (db[bd - DB_INICIO] >= 0);
}

void m_db(char *p, char *buf)
{
  char buf_write[1000];
  long db2;
  long db3;
  char que_bdd;
  char *p2, *p3, *p4;
  char buf2[1000];

  sprintf(buf_write, "%s\n", buf);
  write(1, buf_write, strlen(buf_write)); /* STDOUT */

  p = strchr(p, ' ') + 1;
  db2 = atol(p);
  p = strchr(p, ' ') + 1;
  switch (*p) {
  case 'R':
    p += 2;
    p2 = strrchr(p, '-');
    *p2++ = '\0';
    p3 = strchr(p2, ' ');
    *p3 = '\0';
    p4 = busca_servidor_compress(buf);
    if (p4 == NULL)
      return;
    sprintf(buf2, "%s - DB '%c': %s\n", strchr(p4, ' ') + 1, *(p3 + 1), p);
    envia_respuesta(p2 - 1, buf2);
    return;
    break;
  case 'E':
    p += 2;
    p2 = strchr(p, ' ');
    *p2++ = '\0';
    p4 = busca_servidor_compress(buf);
    if (p4 == NULL)
      return;
    sprintf(buf2, "%s: DB '%c' borrada\n", strchr(p4, ' ') + 1, *p2);
    envia_respuesta(p - 1, buf2);
    return;
    break;
  case 'B':
  case 'Q':
  case 'q':
    return;
    break;
  case 'J':
    db3 = atol(strchr(p, ' ') + 1);
    p = strchr(buf, ' ');
    *buf = *NUM_NODO;
    strcpy(buf + 1, p);
    strcat(buf, "\n");
    que_bdd = *(p + strlen(p) - 2); /* ojo con el \n */
    if (que_bdd == '2')
      que_bdd = 'n';

/*
** Esto hace falta cuando
** hay problemas de desincronizacion
** de la BDD por caidas del servicio.
*/
    //db[que_bdd - DB_INICIO]=db3;

    if (strchr(BDD_AUTHORITATIVE, que_bdd)) {
      assert((db[que_bdd - DB_INICIO] == -1) ||
             (db[que_bdd - DB_INICIO] == db3));
    }
    bdd_online(que_bdd);
    dbuf_write(buf);
    break;
  default:
    que_bdd = *p;
    db3 = db2;
    assert(!strchr(BDD_AUTHORITATIVE, que_bdd));
    if ((que_bdd == 'n') || (que_bdd == 'N')) {
      p = strchr(p, ' ');       /* registro */
      assert(p);
      p++;
      if (!strchr(p, ' ')) {    /* Se trata de un borrado */
        notifica_db_nickdrop_modulo(p, db3);
      }
    }
  }

  if (que_bdd == 'N')
    que_bdd = 'n';

  if ((que_bdd >= 'a') && (que_bdd <= 'z')) {
    db[que_bdd - DB_INICIO] = db3;
    vuelca_contadores_bdd();
  }
}

void vuelca_contadores_bdd(void)
{
  int i;
  dbt clave, contenido;
  void *transaccion;
  int st;
  unsigned long db_normalizado[DB_FINAL - DB_INICIO + 1];

  for (i = 0; i <= (DB_FINAL - DB_INICIO); i++)
    db_normalizado[i] = htonl(db[i]);

  clave.datos = "BDD-Counters";
  clave.len = strlen(clave.datos) + 1; /* Incluye el '\0' */
  contenido.datos = db_normalizado;
  contenido.len = sizeof(db_normalizado);
  assert(sizeof(db_normalizado) == 4 * 26);
  transaccion = inicia_txn_db();
  st = put_db(db_varios, transaccion, &clave, &contenido);
  compromete_txn_db(transaccion);
}

void lee_contadores_bdd(void)
{
  int i;
  dbt clave, contenido;
  void *transaccion;
  int st;
  unsigned long db_normalizado[DB_FINAL - DB_INICIO + 1];

  clave.datos = "BDD-Counters";
  clave.len = strlen(clave.datos) + 1; /* Incluye el '\0' */

  assert(sizeof(db_normalizado) == 4 * 26);

  transaccion = inicia_txn_db();
  st = get_db(db_varios, transaccion, &clave, &contenido);
  compromete_txn_db(transaccion);
  if (contenido.len) {
    assert(contenido.len == 4 * 26);
    memcpy(db_normalizado, contenido.datos, contenido.len); /* Alineamos valores */
    for (i = 0; i <= (DB_FINAL - DB_INICIO); i++)
      db[i] = ntohl(db_normalizado[i]);
    free(contenido.datos);
  }
  else {
    for (i = 0; i <= (DB_FINAL - DB_INICIO); i++)
      db[i] = -1;
  }
}

void envia_m_db(void)
{
  char c;
  char *map, *p, *p2;
  unsigned long len;
  int fd;
  char buf[1024], buf2[1024];
  struct stat st;
  segmentos seg;
  long num_db;

  for (c = 'a'; c <= 'z'; c++) {
    sprintf(buf, "database/tabla.%c", c);
    fd = open(buf, O_RDONLY);
    if (fd == -1)
      continue;
    fstat(fd, &st);
    len = st.st_size;
    p = map = mmap(NULL, len, PROT_READ, MAP_SHARED | MAP_NORESERVE, fd, 0);
    close(fd);
    if (map == MAP_FAILED)
      continue;
    while (p < map + len) {
      p2 = strchr(p, '\n');
      memcpy(buf2, p, p2 - p);
      buf2[p2 - p] = '\0';
      p = ++p2;
      segmentacion(&seg, buf2, NULL, 5);
      switch (seg.num_segm) {
      case 5:                  /* Nuevo registro */
      case 4:                  /* Borrado */
        seg.segm[6] = seg.segm[4]; /* Aunque sea un borrado, es igual */
        seg.segm[5] = seg.segm[3];
        seg.segm[4] = seg.segm[2];
        seg.segm[3] = seg.segm[0];
        seg.segm[2] = seg.segm[1];
        break;
      default:
        continue;
        break;
      }
      seg.num_segm += 2;
      seg.segm[0] = NUM_NODO;
      seg.segm[1] = "DB";
      num_db = atol(seg.segm[3]);
      *(seg.segm[4]) = c;
      *(seg.segm[4] + 1) = '\0';
      if (num_db > db[c - DB_INICIO])
        db[c - DB_INICIO] = num_db;
      dessegmentar(&seg, buf);
      strcat(buf, "\n");
      dbuf_write(buf);
    }
    munmap(map, len);
  }
}
