/*
** MOD_IPC_H
**
** $Id: mod_ipc.h,v 1.1 2002/01/08 20:22:55 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#ifndef MOD_IPC_H
#define MOD_IPC_H

char *nombre_modulo(void);
int existe_nombre_modulo(char *nombre);
int id_modulo(char *nombre);
int existe_id_modulo(int id);

void IRQ_handler(void (*func) (void));
int IRQ_nombre(char *modulo);
int IRQ_id(int id);

#endif
