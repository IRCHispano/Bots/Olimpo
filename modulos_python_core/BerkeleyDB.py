# $Id: BerkeleyDB.py,v 1.18 2002/06/03 16:18:42 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import _BerkeleyDB

def version():
  return _BerkeleyDB.version()

def error():
  return _BerkeleyDB.error()

class txn:
  NORMAL=_BerkeleyDB.inicia_txn
  NO_SYNC=_BerkeleyDB.inicia_txn_no_sync
  NO_WAIT=_BerkeleyDB.inicia_txn_no_wait

  def __init__(self,modo=NORMAL):
    self.txn=modo()

  def _fin_txn(self,call):
    if not self.txn : raise "La transaccion ya esta cerrada"
    call(self.txn)
    self.txn=None

  def compromete(self):
    if self.txn :
      self._fin_txn(_BerkeleyDB.compromete_txn)
    self.txn=None

  def aborta(self):
    if self.txn :
      self._fin_txn(_BerkeleyDB.aborta_txn)
    self.txn=None

  def __del__(self):
    self.aborta()

class db:
  class c:
    def __init__(self,p_db,transaccion,clave=None,bulk=1):
      if not isinstance(transaccion,txn) : raise "Debemos indicar un objeto transaccion valido"
      self.cursor=_BerkeleyDB.cursor(p_db,transaccion.txn,clave,bulk)

    def get(self):
      return _BerkeleyDB.cursor_get(self.cursor)

    def close(self):
      if self.cursor :
        _BerkeleyDB.cursor_close(self.cursor)
        self.cursor=None

    def __del__(self):
      if self.cursor :
        self.close()

  def cursor(self,transaccion,clave=None,bulk=1):
    return self.c(self.p_db,transaccion,clave,bulk)

  def __init__(self,nombre):
    a=_BerkeleyDB.abre_db(nombre)
    if not a : raise "Error al abrir la base de datos '%s': %s" %(nombre,_BerkeleyDB.error())
    self.p_db=a

  def cierra(self):
    a=self.p_db
    if a :
      _BerkeleyDB.cierra_db(a)
      self.p_db=None

  def __del__(self):
    self.cierra()

  def get(self,transaccion,clave):
    if transaccion :
      if not isinstance(transaccion,txn) : raise "Debemos indicar un objeto transaccion valido"
      return _BerkeleyDB.get(self.p_db,transaccion.txn,clave)
    return _BerkeleyDB.get(self.p_db,None,clave)

  def put(self,transaccion,clave,valor):
    if transaccion :
      if not isinstance(transaccion,txn) : raise "Debemos indicar un objeto transaccion valido"
      return _BerkeleyDB.put(self.p_db,transaccion.txn,clave,valor)
    return _BerkeleyDB.put(self.p_db,None,clave,valor)

  def delete(self,transaccion,clave):
    if transaccion :
      if not isinstance(transaccion,txn) : raise "Debemos indicar un objeto transaccion valido"
      return _BerkeleyDB.delete(self.p_db,transaccion.txn,clave)
    return _BerkeleyDB.delete(self.p_db,None,clave)


print version()
print error()

transaccion=txn()
bdatos=db("db.varios")
c=bdatos.cursor(transaccion)
v=(0,)
while not v[0]:
  v=c.get()
  print v
print error()
c.close()
transaccion.compromete()

