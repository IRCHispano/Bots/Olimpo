# $Id: olimpo_init.py,v 1.5 2003/07/15 00:34:26 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import sys

class log:
  def __init__(self,old_err):
    self.old_err=old_err
    self.ultimo_time=0

  def write(self,msg):
    import time
    import os
    self.old_err.write(msg)
    mask=os.umask(0022)
    f=open('olimpo_py_err.log','a+')
    os.umask(mask)
    t=time.time()
    # Imprime las marcas temporales si hace tiempo desde la ultima entrada
    if t-self.ultimo_time>60 :
      f.write('\n*** '+time.ctime(time.time())+'\n')
    f.write(msg)
    f.close()

    self.ultimo_time=t

sys.stderr=log(sys.stderr)

def load_core():
  import imp
  import sys

  core=['BerkeleyDB','_ipc']

  for i in core :
    f,path,desc=imp.find_module(i)
    m=imp.load_module("Olimpo.%s" %i,f,path,desc)
    f.close()

    sys.modules['Olimpo'].__dict__[i]=m
