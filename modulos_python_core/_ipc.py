# $Id: _ipc.py,v 1.13 2003/07/18 00:08:02 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


class ipc :
  def __init__(self) :
    import threading
    self.lock=threading.Lock()
    self.modulos={}
    self.objects={}

  def __obj_add(self,nombre,id,key,obj) :
    self.__obj_del(key)
    obj=(nombre,id,obj)
    self.objects[key]=obj
    if id in self.modulos : self.modulos[id][key]=obj
    else : self.modulos[id]={key:obj}

  def obj_add(self,key,obj) :
    import Olimpo.ipc
    self.lock.acquire()
    try :
      nombre=Olimpo.ipc.nombre_modulo()
      id=Olimpo.ipc.id_modulo(nombre)
      self.__obj_add(nombre,id,key,obj)
    finally:
      self.lock.release()   

  def obj_add2(self,nombre,id,key,obj) :
    self.lock.acquire()
    try :
      self.__obj_add(nombre,id,key,obj)
    finally:
      self.lock.release()

  def __obj_del(self,key) :
    try :
      id=self.objects[key][1]
      del self.modulos[id][key]
      del self.objects[key]
    except :
      id=-1
    return id!=-1

  def obj_del(self,key) :
    self.lock.acquire()
    try :
      return self.__obj_del(key)
    finally:
      self.lock.release()

  def obj(self,key) :
    import Olimpo.ipc
    self.lock.acquire()
    try :
      try :
        nombre,id,obj=self.objects[key]
        if not Olimpo.ipc.existe_id_modulo(id) :
          self.__del_id_objects(id)
          raise "Modulo descargado" # Interceptado justo a continuacion
      except :
        nombre=obj=None
        id=-1
    finally:
      self.lock.release()

    return nombre,id,obj

  def __del_id_objects(self,id) :
    try :
      w=self.modulos[id]
      del self.modulos[id]
      for key in w :
        del self.objects[key]
    except :
      w={}

    return len(w)

  def del_id_objects(self,id) :
    self.lock.acquire()
    try :
      return self.__del_id_objects(id)
    finally :
      self.lock.release()

  def get_objects(self) :
    self.lock.acquire()
    try :
      import copy # Un diccionario es mutable, asi que devolvemos una copia
      return copy.copy(self.objects)
    finally :
      self.lock.release()

  def purge_objects(self) :
    self.lock.acquire()
    purgados={}
    l=0
    try :
      w=self.modulos.keys()
      for id in w :
        if not Olimpo.ipc.existe_id_modulo(id) :
          l+=self.__del_id_objects(id)
    finally:
      self.lock.release()

    return l


import Olimpo.ipc

o=ipc()

Olimpo.ipc.obj_add=o.obj_add
Olimpo.ipc.obj_del=o.obj_del
Olimpo.ipc.obj=o.obj
Olimpo.ipc.del_id_objects=o.del_id_objects
Olimpo.ipc.get_objects=o.get_objects
Olimpo.ipc.purge_objects=o.purge_objects

