# $Id: agenda_sms.py,v 1.5 2000/04/25 22:01:32 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


import urllib , httplib , time

class sms:
  host=httplib.HTTP()
  
  def cadena_escape(self,cadena):
    return urllib.quote_plus(cadena)

  def formatea_cadena(self,numero,nick,dia,texto):
    pass
  
  def envia_sms(self,numero,nick,dia,texto):
    pass

  def envio_efectivo_get(self,cabeceras,cadena):
    self.host.putrequest("GET",cadena)
    for i in cabeceras:
      self.host.putheader(i[0],i[1])
    self.host.endheaders()
    return [self.host.getreply(),self.host.getfile()]

  def envio_efectivo_post(self,cabeceras,cadena,cuerpo):
    self.host.putrequest("POST",cadena)
    for i in cabeceras:
      self.host.putheader(i[0],i[1])
    self.host.putheader("Content-Length",repr(len(cuerpo)))
    self.host.endheaders()
    self.host.send(cuerpo)
    return [self.host.getreply(),self.host.getfile()]

class sms_amena(sms):
  def formatea_cadena(self,numero,nick,dia,texto):
    return "/sms-cgi/sendsms.pl?number="+numero+"&msg="+self.cadena_escape(texto)

  def envia_sms(self,numero,nick,dia,texto):
    self.host.connect("62.81.225.3")
    return self.envio_efectivo_get([],self.formatea_cadena(numero,nick,dia,texto))

class sms_airtel(sms):
  def formatea_cadena(self,numero,nick,dia,texto):
    return "/cgi-bin/cgi-nav?tNumber="+numero+"mBody="+self.cadena_escape(texto)+"&nAddress="
  
  def envia_sms(self,numero,nick,dia,texto):
    cabeceras=[("Referer","http://www.navegalia.com/portal/ofiweb/sms/c_01.htm"),
               ("User-Agent","Mozilla/4.72 (sortof :-)")]
    self.host.connect("x25.airtelmovil.net")
    return self.envio_efectivo_get(cabeceras,self.formatea_cadena(numero,nick,dia,texto))

class sms_terra(sms):
  def formatea_cadena(self,numero,nick,dia,texto):
    return time.strftime("telefono="+numero+"&fecha="
           "%%7Bts+%%27%Y%%2D%m%%2D%d+%H%%3A%M%%3A%S%%27%%7D&mensaje="+
           self.cadena_escape(texto),time.localtime(time.time()))

  def envia_sms(self,numero,nick,dia,texto):
    c=time.strftime("%%7Bts+%%27%Y%%2D%m%%2D%d+%H%%3A%M%%3A%S%%27%%7D",time.gmtime(time.time()))
    cookie="TERRASMS_MINUTO="+c+"; " \
           "TERRASMS_HORA="+c+"%%7C0"
    cabeceras=[("User-Agent","Mozilla/4.72 (sortof :-)"),
               ("Referer","http://www.terra.es/sms/"),
               ("Cookie",cookie),
               ("Content-Type","application/x-www-form-urlencoded")]
    self.host.connect("www.terra.es")
    return self.envio_efectivo_post(cabeceras,"/sms/envio.cfm",self.formatea_cadena(numero,nick,dia,texto))


# a=sms_airtel()
# print a.envia_sms("TELEFONO","jcea","25 Oct","prueba de SMS")

