#    $Id: Makefile,v 1.167 2003/11/26 14:28:46 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.


RED = IRC_HISPANO
#DEVELOP = -DDEVELOP

ifeq ($(RED), IRC_HISPANO)
  EXEC_PATH=/export/home/irc/olimpo
else
  EXEC_PATH=/export/home/irc/irc10/olimpo
endif

BERKELEYDB=/usr/local/BerkeleyDB.4.2

PYTHON=python2.3
PYTHON_INCLUDE=/usr/local/include/$(PYTHON)

# Ultra Sparc
MAQUINA=-mv8plus -mcpu=ultrasparc -mtune=ultrasparc -D_REENTRANT -D_POSIX_PTHREAD_SEMANTICS # Lo ultimo es por "ctime_r"
# Pentium III
#MAQUINA=

CC = ccache gcc
INCLUDE = -I$(BERKELEYDB)/include -I$(PYTHON_INCLUDE)
CFLAGS2 = -pipe -Wall -ggdb $(MAQUINA)  $(INCLUDE) -D$(RED) $(DEVELOP) -fPIC -DUSE_MALLOC_LOCK # Anadir -pg para hacer profiling
LDFLAGS2 = -Wl,--rpath -Wl,$(BERKELEYDB)/lib #-Wl,--rpath -Wl,/usr/local/lib
LIBS2 = -L$(BERKELEYDB)/lib -lsocket -lnsl -ldb -l$(PYTHON) -lpthread -lolimpo -lm -lposix4 # -L/usr/local/lib


ifdef DEVELOP
  PROFILING= -O #-pg
  CFLAGS = $(PROFILING) $(CFLAGS2)
  LDFLAGS = $(PROFILING) $(LDFLAGS2) -Wl,--rpath -Wl,$(EXEC_PATH)
  LIBS = -L . $(LIBS2)
else
  CFLAGS = $(CFLAGS2) -O3
  LDFLAGS = $(LDFLAGS2) -Wl,--rpath -Wl,$(EXEC_PATH)/estable
  LIBS = -L . $(LIBS2)
endif

TOTAL = olimpo ilines_lista agenda_lista nicks_lista libolimpo.so saluda.so agenda.so # ipvirtual.so

all : $(TOTAL)

python :
	cp -rfp modulos_python_core estable/
	cp -rfp modulos_python estable/

install : $(TOTAL) python
	cp -fp *.so estable
	rm -f estable/saluda.so # Colision con "saluda.py"
	cp -fp olimpo olimpo.estable
	cp -fp ilines_lista ilines_lista.estable
	cp -fp agenda_lista agenda_lista.estable
	cp -fp nicks_lista nicks_lista.estable

olimpo : olimpo.o libolimpo.so
	$(CC) $(CFLAGS) $(LDFLAGS) $^ $(LIBS) -o $@
 
libolimpo.so : malloc.o berkeleydb.o mod_berkeleydb.o db.o comandos.o privmsg.o shared.o m_db.o dbuf.o modulos.o modulos_python.o
	$(CC) $(CFLAGS) -shared $(LDFLAGS) $^ -o $@

saluda.so : saluda.o
	$(CC) $(CFLAGS) $(LDFALGS) -shared $^ $(LIBS) -o $@

agenda.so : agenda.o
	$(CC) $(CFLAGS) $(LDFALGS) -shared $^ $(LIBS) -o $@

#ipvirtual.so : ipvirtual.c
#	$(CC) $(CFLAGS) $(LDFALGS) -shared $^ $(LIBS) -o $@

ilines_lista : ilines_lista.o berkeleydb.o
	$(CC) $(CFLAGS) $(LDFLAGS) $^ $(LIBS) -o $@ -l$(PYTHON)

agenda_lista : agenda_lista.o berkeleydb.o
	$(CC) $(CFLAGS) $(LDFLAGS) $^ $(LIBS) -o $@ -l$(PYTHON)

nicks_lista : nicks_lista.o berkeleydb.o
	$(CC) $(CFLAGS) $(LDFLAGS) $^ $(LIBS) -o $@ -l$(PYTHON)

clean :
	rm -f olimpo *.o *.so *~

fullclean : clean
	rm -f olimpo olimpo.old olimpo.estable
	rm -fr estable/*
	rm -f depend
	rm -f ilines_lista ilines_lista.estable
	rm -f agenda_lista agenda_lista.estable
	rm -f nicks_lista nicks_lista.estable

list :
	nm -a *.o

indent :
	indent *.h *.c

depend :
	@echo "Generando dependencias..."
	@$(CC) $(CFLAGS) -MM $(shell echo *.c) > depend


-include depend

