/*
** MOD_NOTIFY_H
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

/*
    $Id: mod_notify.h,v 1.18 2002/05/30 10:11:09 jcea Exp $
*/

#ifndef MOD_NOTIFY_H
#define MOD_NOTIFY_H

void notifica_nick_entrada(void (*func) (int nick));
void notifica_nick_salida(void (*func) (int nick));
void notifica_nicks(void);

void notifica_nick_registrado(void (*func) (int nick));
void notifica_nicks_registrados(void);

void notifica_server_join(void (*func) (char *server, char *id, char *padre));
void notifica_server_split(void (*func) (char *server, char *causa));
void notifica_servers_split_end(void (*func) (void));
void notifica_servers();

void notifica_timer(int tiempo, void (*func) (void));

#endif
