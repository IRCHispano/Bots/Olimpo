/*
** MODULOS_C
**  $Id: modulos.c,v 1.210 2003/10/02 19:10:11 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <ctype.h>

#include <assert.h>

#include "dbuf.h"
#include "db.h"
#include "m_db.h"
#include "berkeleydb.h"
#include "shared.h"
#include "config.h"

#include "modulos.h"
#include "modulos_python.h"

#include "module.h"
#include "mod_privmsg.h"
#include "mod_servmsg.h"
#include "mod_notify.h"
#include "mod_notify_db.h"
#include "mod_berkeleydb.h"
#include "mod_bdd.h"
#include "mod_tools.h"
#include "mod_ipc.h"


int tiempo_timeout_modulos = 0;

static int id_modulo_unico = 0;

struct nick {
  char *nick;
  char *modos;
  int handle;
  void (*privmsg) (int handle, int remitente, char *mensaje);
  void (*servmsg) (int handle, char *nodo, char *mensaje);
  int id;
  struct nick *siguiente;
  struct nick **anterior;
};

#ifdef _0
struct conf_modulo {
  char *path;
  char *params;
  struct conf_modulo *siguiente;
};

#endif

struct interceptacion_comandos {
  char *comando;
  int (*intercepta_comando) (char *comando);
  struct interceptacion_comandos **anterior_hash;
  struct interceptacion_comandos *siguiente_hash;
  struct interceptacion_comandos **anterior_modulo;
  struct interceptacion_comandos *siguiente_modulo;
  struct modulo *modulo;
};

#define HASH_INTERCEPTACION_COMANDOS	511 /* Debe ser un (2^n)-1 */

struct interceptacion_comandos
  *hashes_interceptacion_comandos[HASH_INTERCEPTACION_COMANDOS + 1];

static int hash_interceptacion_comandos(unsigned char *comando)
{
  unsigned long h = 0, g;
  unsigned char c;

  while ((c = *comando++)) {    /* La funcion HASH del formato ELF */
    h = (h << 4) + c;
    g = h & 0xF0000000;
    if (g)
      h ^= g >> 24;
    h &= ~g;
  }
  return h & HASH_INTERCEPTACION_COMANDOS;
}

pthread_mutex_t mutex_cambios_modulos = PTHREAD_MUTEX_INITIALIZER;
volatile int IRQ = 0;

struct modulo {
  char *nombre;
  char *comentario;
  int id_modulo;
  void *python;
  void *dl;
  int (*inicio) (void);
  void (*fin) (void);
// API NOTIFY
  void (*notifica_nick_entrada) (int nick);
  void (*notifica_nick_salida) (int nick);
  void (*notifica_nick_registrado) (int nick);
  void (*notifica_server_join) (char *server, char *id, char *padre);
  void (*notifica_server_split) (char *server, char *causa);
  void (*notifica_servers_split_end) (void);
  int tiempo;
  void (*notifica_timer) (void);
  volatile int IRQ;
  void (*notifica_IRQ) (void);
// API NOTIFY_DB
  void (*notifica_db_nickdrop) (char *nick, long num_serie);
  long notifica_db_nickdrop_num_serie_actual;
// API PRIVMSG
  struct nick *nicks;
//API SERVMSG
  struct interceptacion_comandos *interceptacion_comandos;
  struct modulo **anterior;
  struct modulo *siguiente;
};

static void liberar_modulo_todos_interceptacion_comandos(struct modulo *m);

struct servidor {
  char *nombre;
  char *id;
  char *padre;
  struct servidor **anterior;
  struct servidor *siguiente;
};

static struct modulo *modulos = NULL;
static struct modulo *modulo_actual = NULL;

static struct servidor *servidores = NULL;

#ifdef _0
static struct conf_modulo *conf_modulos = NULL;
#endif

static unsigned long long nicks_en_uso = 1; /* Nick del sistema (Olimpo/SuX) */

static char *dl_error = NULL;

static void notifica_viejos_drops(struct modulo *m)
{
  char buf_write[1000];

  dbt clave, contenido;
  void *transaccion;
  int st;
  char *p;
  long num_serie;

  if (!m->notifica_db_nickdrop)
    return;

  sprintf(buf_write, "Notificando 'nickdrops' al modulo %s\n", m->nombre);
  write(1, buf_write, strlen(buf_write)); /* STDOUT */

  clave.datos = "NICKDROPS";
  clave.len = strlen(clave.datos) + 1; /* Incluye el '\0' */
  transaccion = inicia_txn_db();
  st = get_db(db_varios, transaccion, &clave, &contenido);
  if (contenido.len != 0) {
    p = contenido.datos;
    while (p - (char *) contenido.datos < contenido.len) {
      num_serie = atol(p);
      if (num_serie > m->notifica_db_nickdrop_num_serie_actual) {
        p = strchr(p, ' ') + 1;
        if (m->python) {
          modulo_python_notifica_db_nickdrop(m->python,
                                             m->notifica_db_nickdrop, p,
                                             num_serie);
        }
        else {
          m->notifica_db_nickdrop(p, num_serie);
        }
        m->notifica_db_nickdrop_num_serie_actual = num_serie;
      }
      p = p + strlen(p) + 1;
    }
    free(contenido.datos);
  }
  compromete_txn_db(transaccion);
}

static void inicializa_modulo(struct modulo *modulo)
{
  struct modulo *backup_modulo_actual;

  backup_modulo_actual = modulo_actual;
  modulo_actual = modulo;
  if (modulo->python) {
    modulo_python_inicio(modulo->python);
  }
  else {
    modulo_actual->inicio();
  }
  modulo_actual = backup_modulo_actual;
}

static struct modulo *nuevo_modulo(char *nombre)
{
  struct modulo *modulo_actual_backup;
  struct modulo *modulo;
  char *error = NULL;
  int python = 0;
  int t;

  dl_error = NULL;

  modulo_actual_backup = modulo_actual;

  modulo_actual = modulo = malloc(sizeof(struct modulo));
  if (modulo == NULL) {
    error = "No hay memoria";
    goto error;
  }
  modulo->nombre = malloc(strlen(nombre) + 1);
  if (modulo->nombre == NULL) {
    error = "No hay memoria";
    goto error;
  }

  strcpy(modulo->nombre, nombre);

  modulo->python = NULL;
  modulo->comentario = NULL;
  modulo->id_modulo = id_modulo_unico++;
  modulo->nicks = NULL;
  modulo->notifica_nick_entrada = NULL;
  modulo->notifica_nick_salida = NULL;
  modulo->notifica_nick_registrado = NULL;
  modulo->notifica_server_join = NULL;
  modulo->notifica_server_split = NULL;
  modulo->notifica_servers_split_end = NULL;
  modulo->notifica_db_nickdrop = NULL;
  modulo->interceptacion_comandos = NULL;
  modulo->tiempo = 0;
  modulo->notifica_timer = NULL;
  modulo->IRQ = 0;
  modulo->notifica_IRQ = NULL;
  modulo->fin = NULL;

  python = es_modulo_python(nombre);
  if (!python) {                /* Libreria compartida */
    char *path;

    path = malloc(strlen(LIB_PATH) + strlen(nombre) + 50);
    if (path == NULL) {
      error = "No hay memoria";
      goto error;
    }
    sprintf(path, LIB_PATH "/%s", nombre);
    modulo->dl = dlopen(path, RTLD_NOW | RTLD_GLOBAL);
    free(path);

    if (!modulo->dl) {
      error = dlerror();
      goto error;
    }

    modulo->inicio = dlsym(modulo->dl, "inicio");
    if (!modulo->inicio) {
      error = "No encuentro la funcion 'inicio()'";
      goto error;
    }
  }
  else {                        /* Modulo Python */
    modulo->dl = NULL;
    error = nuevo_modulo_python(nombre, &(modulo->python));
    if (error)
      goto error;
  }

  t = pthread_mutex_lock(&mutex_cambios_modulos);
  assert(!t);

  modulo->siguiente = modulos;
  modulo->anterior = &modulos;
  if (modulo->siguiente)
    modulo->siguiente->anterior = &(modulo->siguiente);
  modulos = modulo;

  t = pthread_mutex_unlock(&mutex_cambios_modulos);
  assert(!t);

  inicializa_modulo(modulo);
  notifica_viejos_drops(modulo);

  modulo_actual = modulo_actual_backup;

  return modulo;

error:
  dl_error = error;
  if (modulo) {
    if (modulo->nombre)
      free(modulo->nombre);
    if (modulo->dl)
      dlclose(modulo->dl);
    free(modulo);
  }
  if (error) {
    char buf_write[1000];

    sprintf(buf_write,"%s: %s\n", nombre, error);
    write(2, buf_write, strlen(buf_write)); /* STDERR */
  }

  modulo_actual = modulo_actual_backup;

  return NULL;
}

void inicializa_sistema_de_modulos(void)
{
  inicializa_python();
}

void carga_modulos(void)
{
  char buf_write[1000];

#ifndef DEVELOP
  sprintf(buf_write, "Cargando modulo 'chanlog'\n");
  write(1, buf_write, strlen(buf_write)); /* STDOUT */
  nuevo_modulo("chanlog.py");

  sprintf(buf_write, "Cargando modulo 'ipvirtual'\n");
  write(1, buf_write, strlen(buf_write)); /* STDOUT */
  nuevo_modulo("ipvirtual.py");

  sprintf(buf_write, "Cargando modulo 'net-view'\n");
  write(1, buf_write, strlen(buf_write)); /* STDOUT */
  nuevo_modulo("net-view.py");

  sprintf(buf_write, "Cargando modulo 'dxcluster'\n");
  write(1, buf_write, strlen(buf_write)); /* STDOUT */
  nuevo_modulo("dxcluster.py");

  sprintf(buf_write, "Cargando modulo 'agenda'\n");
  write(1, buf_write, strlen(buf_write)); /* STDOUT */
  nuevo_modulo("agenda.so");

  sprintf(buf_write, "Cargando modulo 'nick2'\n");
  write(1, buf_write, strlen(buf_write)); /* STDOUT */
  nuevo_modulo("nick2.py");

#else
  //sprintf(buf_write,"Cargando modulo 'chanfollower.py'\n");
  //write(1,buf_write,strlen(buf_write)); /* STDOUT */
  //nuevo_modulo("chanfollower.py");
#endif
}

#ifdef _0
Rutina a medio escribir void carga_modulos(void)
{
  int handle;
  char buf[16 * 1024];
  int tamano;
  int linea = 1;
  char *p;

  handle = open(LIB_PATH "/modulos", O_RDONLY);
  if (handle == -1)
    return;
  tamano = read(handle, buf, 16 * 1024 - 10);
  close(handle);
  if (tamano > 15 * 1024) {
    char buf_write[1000];

    sprintf
      (buf_write,
       "Taman~o del fichero de configuracion de modulos demasiado grande\n");
    write(2, buf_write, strlen(buf_write)); /* STDERR */
    return;
  }
  while ((p = strchr(buf, '\t')))
    *p = ' ';
  p = buf;
  buf[tamano] = '\0';
  while (p && *p) {
    while (*p == ' ')
      p++;
    if (*p == '\n') {
      linea++;
      p++;
      continue;
    }
    if (*p == '#') {
      p = strchr(p, '\n');
      if (p) {
        p++;
        linea++;
      }
      continue;
    }
    if (!strncmp(p, "module ", 7)) {
    }
    else {
      char buf_write[1000];

      sprintf(buf_write, "Syntax Error, linea %d\n", linea);
      write(2, buf_write, strlen(buf_write)); /* STDERR */
      return;
    }
  }
}

#endif

static void descarga_modulo(struct modulo *m)
{
  struct nick *n, *n2;
  char buf[1024];
  struct modulo *backup_modulo_actual;
  int t;

  backup_modulo_actual = modulo_actual;
  modulo_actual = m;
  if (m->fin) {
    if (m->python) {
      modulo_python_fin(m->python, m->fin);
    }
    else {
      m->fin();
    }
  }
  n = m->nicks;
  while (n) {
    sprintf(buf, ":%s QUIT :Hasta Luego\n", n->nick);
    if (soc > 0)
      dbuf_write(buf);
    n2 = n->siguiente;
    free(n->nick);
    free(n->modos);
    if (m->python) {
      libera_objeto_python(n->privmsg);
      libera_objeto_python(n->servmsg);
    }
    nicks_en_uso &= ~(1ull << n->id);
    free(n);
    n = n2;
  }

  if (!m->python) {
    assert(m->dl);
    if (dlclose(m->dl)) {
      char *p;

      p = dlerror();
      if (p) {
        char buf_write[1000];

        sprintf(buf_write, "%s\n", p);
        write(2, buf_write, strlen(buf_write)); /* STDERR */
      }
      assert(0);                /* Fallo a posta */
    }
    m->dl = NULL;               /* Por seguridad */
  }
  else {
    assert(!m->dl);             /* Por seguridad */
  }

  t = pthread_mutex_lock(&mutex_cambios_modulos);
  assert(!t);

  if (m->siguiente)
    m->siguiente->anterior = m->anterior;
  *(m->anterior) = m->siguiente;

  t = pthread_mutex_unlock(&mutex_cambios_modulos);
  assert(!t);

  liberar_modulo_todos_interceptacion_comandos(m);

  free(m->nombre);
  if (m->comentario)
    free(m->comentario);
  if (m->python) {
    libera_objeto_python(m->fin);
    libera_objeto_python(m->notifica_nick_entrada);
    libera_objeto_python(m->notifica_nick_salida);
    libera_objeto_python(m->notifica_nick_registrado);
    libera_objeto_python(m->notifica_server_join);
    libera_objeto_python(m->notifica_server_split);
    libera_objeto_python(m->notifica_servers_split_end);
    libera_objeto_python(m->notifica_db_nickdrop);
    libera_objeto_python(m->notifica_timer);
    libera_objeto_python(m->notifica_IRQ);
    descarga_modulo_python(m->python);
  }
  free(m);
  modulo_actual = backup_modulo_actual;
}

void descarga_modulos(void)
{
  while ((modulo_actual = modulos)) {
    descarga_modulo(modulo_actual);
  }
}

static char *handle2nick_compress(int destino)
{
  static char buf[6];

  if (destino < (64 * 64 * 64)) { /* numeric corto */
    buf[0] = convert2y[(destino >> 12) & 63];
    buf[1] = convert2y[(destino >> 6) & 63];
    buf[2] = convert2y[destino & 63];
    buf[3] = '\0';
  }
  else {                        /* numeric largo */
    buf[0] = convert2y[(destino >> 24) & 63];
    buf[1] = convert2y[(destino >> 18) & 63];
    buf[2] = convert2y[(destino >> 12) & 63];
    buf[3] = convert2y[(destino >> 6) & 63];
    buf[4] = convert2y[destino & 63];
    buf[5] = '\0';
  }

  return buf;
}

static int nick_compress2handle(char *p)
{
  int numeric = 0;
  char *p2;

  p2 = strchr(p, ' ');
  if (!p2)
    p2 = p + strlen(p);

  assert(((p2 - p) == 3) || ((p2 - p) == 5));

  if ((p2 - p) == 3) {          /* numeric corto */
    numeric =
      (convert2n[(unsigned char) p[0]] << 12) +
      (convert2n[(unsigned char) p[1]] << 6) +
      convert2n[(unsigned char) p[2]];
  }
  else {                        /* numeric largo */
    numeric =
      (convert2n[(unsigned char) p[0]] << 24) +
      (convert2n[(unsigned char) p[1]] << 18) +
      (convert2n[(unsigned char) p[2]] << 12) +
      (convert2n[(unsigned char) p[3]] << 6) +
      convert2n[(unsigned char) p[4]];
  }

  return numeric;
}

int serv_callback(int handle,
                  void (*func) (int handle, char *remitente, char *respuesta))
{
  struct nick *n;

  n = modulo_actual->nicks;
  while (n) {
    if (n->handle == handle)
      break;
    n = n->siguiente;
  }
  assert(n);

  if (modulo_actual->python) {
    libera_objeto_python(n->servmsg);
  }
  n->servmsg = func;
  return n->handle;
}

int envia_raw(int handle, char *comando)
{
  struct nick *n;
  char buf[1024];

  n = modulo_actual->nicks;
  while (n) {
    if (n->handle == handle)
      break;
    n = n->siguiente;
  }
  if (!n)
    return -1;
  sprintf(buf, ":%s %s\n", n->nick, comando);
  if (soc > 0)
    dbuf_write(buf);
  return 0;
}

int envia_raw2(char *comando)
{
  char buf[1024];

  sprintf(buf, NUM_NODO " %s\n", comando);
  if (soc > 0)
    dbuf_write(buf);
  return 0;
}

static void liberar_modulo_todos_interceptacion_comandos(struct modulo *m)
{
  struct interceptacion_comandos *ic;

  while ((ic = m->interceptacion_comandos)) {
    free(ic->comando);

    if (m->python) {
      libera_objeto_python(ic->intercepta_comando);
    }
    else {
      free(ic->intercepta_comando);
    }

    *(ic->anterior_hash) = ic->siguiente_hash;
    if (ic->siguiente_hash)
      ic->siguiente_hash->anterior_hash = ic->anterior_hash;

    *(ic->anterior_modulo) = ic->siguiente_modulo;
    if (ic->siguiente_modulo)
      ic->siguiente_modulo->anterior_modulo = ic->anterior_modulo;

    free(ic);
  }
}

void intercepta_comando(char *comando, int (*func) (char *comando))
{
  char *p;
  int hash;
  struct interceptacion_comandos *ic;

  p = malloc(strlen(comando) + 1); /* Por el '\0' del final */
  assert(p);

  strcpy(p, comando);
  comando = p - 1;
  do {
    comando++;
    *comando = toupper(*comando);
  } while (*comando);

  hash = hash_interceptacion_comandos(p);

  if (!func) {                  /* El objetivo es borrar */
    ic = hashes_interceptacion_comandos[hash];
    while (ic) {
      if (!strcmp(ic->comando, p)) {
        free(ic->comando);

        if (modulo_actual->python) {
          libera_objeto_python(ic->intercepta_comando);
        }
        else {
          free(ic->intercepta_comando);
        }

        *(ic->anterior_hash) = ic->siguiente_hash;
        if (ic->siguiente_hash)
          ic->siguiente_hash->anterior_hash = ic->anterior_hash;

        *(ic->anterior_modulo) = ic->siguiente_modulo;
        if (ic->siguiente_modulo)
          ic->siguiente_modulo->anterior_modulo = ic->anterior_modulo;

        free(ic);
        break;
      }
    }
    free(p);
    return;
  }

  ic = malloc(sizeof(struct interceptacion_comandos));
  assert(ic);

  ic->comando = p;
  ic->intercepta_comando = func;

  ic->siguiente_hash = hashes_interceptacion_comandos[hash];
  ic->anterior_hash = &(hashes_interceptacion_comandos[hash]);
  if (ic->siguiente_hash)
    ic->siguiente_hash->anterior_hash = &(ic->siguiente_hash);
  hashes_interceptacion_comandos[hash] = ic;

  ic->siguiente_modulo = modulo_actual->interceptacion_comandos;
  ic->anterior_modulo = &(modulo_actual->interceptacion_comandos);
  if (ic->siguiente_modulo)
    ic->siguiente_modulo->anterior_modulo = &(ic->siguiente_modulo);
  modulo_actual->interceptacion_comandos = ic;

  ic->modulo = modulo_actual;
}

void intercepta_comando_modulo(char *comando)
{
  static char *buf = NULL;
  static int buf_len = 0;
  char *p, *p2;
  struct modulo *backup_modulo_actual;
  int hash;
  struct interceptacion_comandos *ic;

  p = strchr(comando, ' ');
  assert(p);
  p2 = strchr(++p, ' ');
  if (!p2)
    p2 = p + strlen(p);         /* Algunos comandos no tienen ningun parametro */

  if (p2 - p + 1 > buf_len) {
    free(buf);
    buf_len = p2 - p + 1;
    buf = malloc(buf_len);
    assert(buf);
  }

  strncpy(buf, p, p2 - p);
  buf[p2 - p] = '\0';
  p = buf - 1;
  do {
    p++;
    *p = toupper(*p);
  } while (*p);

  hash = hash_interceptacion_comandos(buf);

  ic = hashes_interceptacion_comandos[hash];

  while (ic) {
    if (!strcmp(buf, ic->comando)) {
      backup_modulo_actual = modulo_actual;
      modulo_actual = ic->modulo;
      if (modulo_actual->python) {
        assert(!modulo_python_intercepta_comando
               (modulo_actual->python, ic->intercepta_comando, comando));
      }
      else {
        assert(!(ic->intercepta_comando(comando)));
      }
      modulo_actual = backup_modulo_actual;
    }
    ic = ic->siguiente_hash;
  }
}

void envia_servmsg_modulo(char *destino, char *remitente, char *comando)
{
  struct nick *n;
  int handle = -1;
  char *p;
  struct modulo *backup_modulo_actual;

  if (remitente[0] == ':') {
    char *p, *p2;

    p = strchr(remitente + 1, ' ');
    p2 = strchr(remitente + 1, '.');
    if (!p2)                    /* No es un servidor */
      return;
    if (p && (p < p2))          /* No es un servidor */
      return;
  }
  else {
    int l;
    char *p;

    p = strchr(remitente, ' ');
    if (p) {
      l = p - remitente;
    }
    else {
      l = strlen(remitente);
    }
    if ((l != 1) && (l != 2))   /* No se trata de un nodo */
      return;
  }

  if ((*destino == '#') || (*destino == '+') || (*destino == '&') ||
      (*destino == ':'))
    return;

  if ((*comando >= '0') && (*comando <= '9')) { /* Las respuestas se mandan al nick, no al numeric */
    backup_modulo_actual = modulo_actual;
    modulo_actual = modulos;
    while (modulo_actual) {
      n = modulo_actual->nicks;
      while (n) {
        if (n->servmsg) {
          unsigned char *p2, *p3;

          p2 = destino;
          p3 = n->nick;
          while (*p2 && (tolowertab[*p2] == tolowertab[*p3])) {
            p2++;
            p3++;
          }
          if ((!*p2) && (*p2 == *p3)) {
            handle = n->handle;
            goto out;
          }
        }
        n = n->siguiente;
      }
      modulo_actual = modulo_actual->siguiente;
    }
  out:
    modulo_actual = backup_modulo_actual;
  }
  else {
    int l;

    l = strlen(destino);
    if ((l != 3) && (l != 5))
      return;
    handle = nick_compress2handle(destino);
  }

  if (remitente[0] == ':') {
    p = busca_servidor(remitente + 1); /* Se salta el ':' */
  }
  else {                        /* numeric */
    p = busca_servidor_compress(remitente);
  }

  if (!p)
    return;

  p = strrchr(p, ' ');
  assert(p);
  p++;

  backup_modulo_actual = modulo_actual;
  modulo_actual = modulos;

  if (handle != -1) {
    while (modulo_actual) {
      n = modulo_actual->nicks;
      while (n) {
        if (n->servmsg && (n->handle == handle)) {
          if (modulo_actual->python) {
            modulo_python_servmsg(modulo_actual->python, n->servmsg,
                                  n->handle, p, comando);
          }
          else {
            n->servmsg(n->handle, p, comando);
          }
          return;
        }
        n = n->siguiente;
      }
      modulo_actual = modulo_actual->siguiente;
    }
  }
  modulo_actual = backup_modulo_actual;
}

int nuevo_nick(char *nick, char *modos,
               void (*func) (int handle, int remitente, char *mensaje))
{
  struct nick *n;
  unsigned int cursor;
  char buf[1024];

  if (nicks_en_uso == 0xffffffffffffffffull) {
    printf("Todos los nicks en uso\n");
    return -1;
  }
  n = malloc(sizeof(struct nick));
  n->nick = malloc(strlen(nick) + 1);
  strcpy(n->nick, nick);
  n->modos = malloc(strlen(modos) + 1);
  strcpy(n->modos, modos);
  n->privmsg = func;
  n->servmsg = NULL;
  n->siguiente = modulo_actual->nicks;
  n->anterior = &(modulo_actual->nicks);
  if (n->siguiente)
    n->siguiente->anterior = &(n->siguiente);
  modulo_actual->nicks = n;
/* El nick con cursor '0' ya esta utilizado por el nick raiz del framework */
  for (cursor = 1; nicks_en_uso & (1ull << cursor); cursor++);
  n->id = cursor;
  n->handle = cursor + (convert2n[(unsigned char) *NUM_NODO] << 12);
  nicks_en_uso |= 1ull << cursor;
  if (soc > 0) {
    sprintf(buf,
            ":%s QUIT :Recargando\n" NUM_NODO
            " NICK %s 1 000000123 - -olimpo- %s AAAAAA " NUM_NODO
            "A%c :OLIMPO\n", nick, nick, n->modos, convert2y[cursor]);
    dbuf_write(buf);
  }
  return n->handle;
}

void quit_nick(int handle)
{
  char buf[1024];
  struct nick *n;

  n = modulo_actual->nicks;
  while (n) {
    if (handle == n->handle) {
      sprintf(buf, ":%s QUIT :Hasta Luego\n", n->nick);
      if (soc > 0)
        dbuf_write(buf);
      *(n->anterior) = n->siguiente;
      if (n->siguiente)
        n->siguiente->anterior = n->anterior;
      free(n->nick);
      free(n->modos);
      if (modulo_actual->python) {
        libera_objeto_python(n->privmsg);
        libera_objeto_python(n->servmsg);
      }
      nicks_en_uso &= ~(1ull << n->id);
      free(n);
      return;
    }
    n = n->siguiente;
  }
}

void envia_modulos_nicks(void)
{
  struct modulo *p = modulos;
  struct nick *n;
  char buf[1024];

  if (soc <= 0)
    return;

  while (p) {
    n = p->nicks;
    while (n) {
      sprintf(buf, ":%s QUIT :Recargando\n"
              NUM_NODO " NICK %s 1 000000123 - -olimpo- %s AAAAAA " NUM_NODO
              "A%c :OLIMPO\n", n->nick, n->nick, n->modos,
              convert2y[(n->handle) & 63]);
      dbuf_write(buf);
      n = n->siguiente;
    }
    p = p->siguiente;
  }
}

int envia_nick(int handle, int destino, char *mensaje)
{
  struct nick *n;
  char buf[1024];

  n = modulo_actual->nicks;
  while (n) {
    if (n->handle == handle)
      break;
    n = n->siguiente;
  }
  if (!n)
    return -1;
  sprintf(buf, ":%s PRIVMSG %s :%s\n", n->nick, handle2nick_compress(destino),
          mensaje);
  if (soc > 0)
    dbuf_write(buf);
  return 0;
}

void especifica_fin(void (*fin) (void))
{
  modulo_actual->fin = fin;
}

void envia_privmsg_modulo(char *destino, char *remitente, char *mensaje)
{
  struct nick *n;
  int handle, handle2;
  char *p;
  struct modulo *backup_modulo_actual;

  if ((*destino == '#') || (*destino == '+') || (*destino == '&'))
    return;

  handle = nick_compress2handle(destino);
  p = encuentra_usuario_nick(remitente + 1); /* Se salta el ':' */
  if (!p)
    return;
  p = strchr(strchr(p, ' ') + 1, ' ') + 1;
  handle2 = nick_compress2handle(p);

  backup_modulo_actual = modulo_actual;
  modulo_actual = modulos;
  while (modulo_actual) {
    n = modulo_actual->nicks;
    while (n) {
      if (n->privmsg && (n->handle == handle)) {
        if (modulo_actual->python) {
          modulo_python_privmsg(modulo_actual->python, n->privmsg, handle,
                                handle2, mensaje);
        }
        else {
          n->privmsg(handle, handle2, mensaje);
        }
        return;
      }
      n = n->siguiente;
    }
    modulo_actual = modulo_actual->siguiente;
  }
  modulo_actual = backup_modulo_actual;
}

char *dlload(char *lib)
{
  static char *error = NULL;
  struct modulo *m;

  m = modulos;
  while (m) {
    if (!strcmp(m->nombre, lib)) {
      return "Libreria ya cargada.\n";
    }
    m = m->siguiente;
  }
  modulo_actual = nuevo_modulo(lib);
  if (modulo_actual)
    return "Modulo cargado OK.\n";

  if (error)
    free(error);
  error = malloc(strlen(dl_error) + 10);
  if (!error)
    return "Error al reservar memoria para devolver el error.\n";
  sprintf(error, "%s.\n", dl_error);
  return error;
}

char *dlunload(char *lib)
{
  struct modulo *m;

  m = modulos;
  while (m) {
    if (!strcmp(m->nombre, lib)) {
      break;
    }
    m = m->siguiente;
  }
  if (!m) {
    return "Modulo no cargado.\n";
  }
  modulo_actual = m;
  descarga_modulo(m);
  return "Modulo descargado OK.\n";
}

void dllist(char *buf_send, char *buf_write)
{
  struct modulo *m;
  struct nick *n;

  m = modulos;
  while (m) {
    strcpy(buf_write, m->nombre);

    n = m->nicks;
    while (n) {
      strcat(buf_write, " ");
      strcat(buf_write, n->nick);
      n = n->siguiente;
    }
    if (m->comentario) {
      strcat(buf_write, "   ");
      strcat(buf_write, m->comentario);
    }
    strcat(buf_write, "\n");
    dbuf_write(buf_send);
    m = m->siguiente;
  }
}

void comentario_modulo(char *comentario)
{
  if (modulo_actual->comentario)
    free(modulo_actual->comentario);
  modulo_actual->comentario = malloc(strlen(comentario) + 1);
  if (modulo_actual->comentario)
    strcpy(modulo_actual->comentario, comentario);
}

char *lee_flags_nick(int handle, char *flags)
{
  char *p, *p2 = flags;

  p = encuentra_usuario_compress(handle2nick_compress(handle));
  if (!p)
    return NULL;
  p = strchr(p, ' ') + 1;
  p = strchr(p, ' ') + 1;
  p = strchr(p, ' ') + 1;
  while (*p != ' ')
    *p2++ = *p++;
  *p2 = '\0';
  return flags;
}

char *lee_host_nick(int handle, char *host)
{
  char *p, *p2 = host;

  p = encuentra_usuario_compress(handle2nick_compress(handle));
  if (!p)
    return NULL;

  while (*p != ' ')
    *p2++ = *p++;
  *p2 = '\0';
  return host;
}

unsigned long lee_ip_nick(int handle)
{
  char *p, *p2;

  p = encuentra_usuario_compress(handle2nick_compress(handle));
  if (!p)
    return 0;

  p2 = strrchr(p, ' ');
  *p2 = '\0';
  p = strrchr(p, ' ') + 1;
  *p2 = ' ';

  return base64toint(p);
}

char *lee_nick(int handle, char *nick)
{
  char *p, *p2 = nick;

  p = encuentra_usuario_compress(handle2nick_compress(handle));
  if (!p)
    return NULL;
  p = strchr(p, ' ') + 1;
  while (*p != ' ')
    *p2++ = *p++;
  *p2 = '\0';
  return nick;
}

char *lee_nick_normalizado(int handle, char *nick)
{
  unsigned char *p;

  p = lee_nick(handle, nick);
  if (!p)
    return NULL;
  return nick_normalizado(p, p);
}

int lee_handle(char *nick)
{
  char *p;

  p = encuentra_usuario_nick(nick);
  if (!p)
    return -1;
  p = strchr(strchr(p, ' ') + 1, ' ') + 1;
  return nick_compress2handle(p);
}

void hace_log(int nick, char *comando)
{
  char *p;

  p = strrchr(modulo_actual->nombre, '/');
  if (p)
    p++;
  else
    p = modulo_actual->nombre;

  guarda_log_nick_compress(comando, p, handle2nick_compress(nick));
}

void hace_log2(char *comando)
{
  char *p;

  p = strrchr(modulo_actual->nombre, '/');
  if (p)
    p++;
  else
    p = modulo_actual->nombre;

  guarda_log(comando, p);
}

char *nombre_modulo(void)
{
  return modulo_actual->nombre;
}

int existe_nombre_modulo(char *nombre)
{
  struct modulo *m;
  int t;
  int result = 0;

  t = pthread_mutex_lock(&mutex_cambios_modulos);
  assert(!t);

  m = modulos;

  while (m) {
    if (!strcmp(m->nombre, nombre)) {
      result = !0;
      break;
    }
    m = m->siguiente;
  }

  t = pthread_mutex_unlock(&mutex_cambios_modulos);
  assert(!t);

  return result;
}

int id_modulo(char *nombre)
{
  struct modulo *m;
  int t;
  int id = -1;

  t = pthread_mutex_lock(&mutex_cambios_modulos);
  assert(!t);

  m = modulos;

  while (m) {
    if (!strcmp(m->nombre, nombre)) {
      id = m->id_modulo;
      break;
    }
    m = m->siguiente;
  }

  t = pthread_mutex_unlock(&mutex_cambios_modulos);
  assert(!t);

  return id;
}


int existe_id_modulo(int id)
{
  struct modulo *m;
  int t;
  int result = 0;

  t = pthread_mutex_lock(&mutex_cambios_modulos);
  assert(!t);

  m = modulos;

  while (m) {
    if (m->id_modulo == id) {
      result = !0;
      break;
    }
    m = m->siguiente;
  }

  t = pthread_mutex_unlock(&mutex_cambios_modulos);
  assert(!t);

  return result;
}

int debug(void)
{
#if DEVELOP
  return 1;
#else
  return 0;
#endif
}

long bdd_envia_registro(unsigned char bd, char *clave, char *valor)
{
  char buf[1000];
  unsigned long num;
  int l;

  if ((bd < 'a') || (bd > 'z'))
    return -1;

  assert(strchr(BDD_AUTHORITATIVE, bd));

  l = strlen(clave);
  if (valor)
    l += strlen(valor);
  assert(l < 450);

  assert(bdd_flujo_abierto2(bd));
  num = ++db[bd - DB_INICIO];
  assert((num > 0) && (num < (1L << 31)));

  vuelca_contadores_bdd();

  if (valor) {
    sprintf(buf, "%s DB * %lu %c %s :%s\n", NUM_NODO, num, bd, clave, valor);
  }
  else {
    sprintf(buf, "%s DB * %lu %c %s\n", NUM_NODO, num, bd, clave);
    if (clave[0] != '*') {
      if (bd == 'n')
        notifica_db_nickdrop_modulo(clave, db[DB_N]);
    }
  }
  dbuf_write(buf);
  return (long) num;
}

int bdd_flujo_abierto(unsigned char bd)
{
  if ((bd < 'a') || (bd > 'z'))
    return 0;

  assert(strchr(BDD_AUTHORITATIVE, bd));

  return bdd_flujo_abierto2(bd);
}

char *which_server(int handle, char *buf)
{
  char *h, *p;

  strcpy(buf, handle2nick_compress(handle));

/* Aislo el componente de servidor */
  if (strlen(buf) == 3)
    buf[1] = '\0';
  else
    buf[2] = '\0';

  strcat(buf, " ");             /* Necesario para busca_servidor_compress */

  h = busca_servidor_compress(buf);
  if (!h)
    return NULL;

  p = strrchr(h, ' ');
  strcpy(buf, p + 1);
  return buf;
}

int notifica_posible_kill_modulo(char *nick_compress, char *comando)
{
  struct nick *n;
  int handle;
  struct modulo *m;
  char *nombre;
  char buf[1024];

  if ((*nick_compress != *NUM_NODO) || (*(nick_compress + 3) != ' '))
    return 0;

  if ((*(nick_compress + 1) == 'A') && (*(nick_compress + 2) == 'A')) {
    /*
     ** KILL al propio Olimpo. Simplemente reinyectamos de nuevo.
     */
    sprintf(buf, "%s NICK %s 1 880000000 - - %s AAAAAA %sAA :%s\n",
            NUM_NODO, NICK, NICK_MODO, NUM_NODO, WHO);
    dbuf_write(buf);
#ifdef CANAL
    sprintf(buf, ":%s CREATE %s %lu\n", NICK, CANAL, time(NULL));
    dbuf_write(buf);
#endif

    return !0;
  }

  handle = nick_compress2handle(nick_compress);

  m = modulos;
  while (m) {
    n = m->nicks;
    while (n) {
      if (n->handle == handle) {
        nombre = malloc(strlen(m->nombre) + 1); /* Por el '\0' final */
        assert(nombre);
        strcpy(nombre, m->nombre);
        assert(strlen(comando) < 900);
        sprintf(buf,
                "Se recibe un KILL para el nick '%s', del modulo '%s': %s",
                n->nick, nombre, comando);
        guarda_log(buf, "olimpo");
        guarda_log("Descarga y recarga del modulo...", "olimpo");
        descarga_modulo(m);
        nuevo_modulo(nombre);
        free(nombre);
        return !0;
      }
      n = n->siguiente;
    }
    m = m->siguiente;
  }

  return 0;
}

void notifica_nick_entrada(void (*func) (int nick))
{
  if (modulo_actual->python) {
    libera_objeto_python(modulo_actual->notifica_nick_entrada);
  }
  modulo_actual->notifica_nick_entrada = func;
}

void notifica_nick_salida(void (*func) (int nick))
{
  if (modulo_actual->python) {
    libera_objeto_python(modulo_actual->notifica_nick_salida);
  }
  modulo_actual->notifica_nick_salida = func;
}

void notifica_nick_registrado(void (*func) (int nick))
{
  if (modulo_actual->python) {
    libera_objeto_python(modulo_actual->notifica_nick_registrado);
  }
  modulo_actual->notifica_nick_registrado = func;
}

void notifica_timer(int tiempo, void (*func) (void))
{
  if (modulo_actual->python) {
    libera_objeto_python(modulo_actual->notifica_timer);
  }
  modulo_actual->notifica_timer = func;
  modulo_actual->tiempo = tiempo;
  if (tiempo && (tiempo < tiempo_timeout_modulos))
    tiempo_timeout_modulos = tiempo;
}

void IRQ_handler(void (*func) (void))
{
  if (modulo_actual->python) {
    libera_objeto_python(modulo_actual->notifica_IRQ);
  }
  modulo_actual->notifica_IRQ = func;
}

int IRQ_nombre(char *modulo)
{
  struct modulo *m;
  int t;
  int result = 0;

  t = pthread_mutex_lock(&mutex_cambios_modulos);
  assert(!t);

  m = modulos;

  while (m) {
    if (!strcmp(m->nombre, modulo)) {
      IRQ = !0;
      m->IRQ = !0;
      result = !0;
      break;
    }
    m = m->siguiente;
  }

  t = pthread_mutex_unlock(&mutex_cambios_modulos);
  assert(!t);

  return result;
}

int IRQ_id(int id)
{
  struct modulo *m;
  int t;
  int result = 0;

  t = pthread_mutex_lock(&mutex_cambios_modulos);
  assert(!t);

  m = modulos;

  while (m) {
    if (m->id_modulo == id) {
      IRQ = !0;
      m->IRQ = !0;
      result = !0;
      break;
    }
    m = m->siguiente;
  }

  t = pthread_mutex_unlock(&mutex_cambios_modulos);
  assert(!t);

  return result;
}

void notifica_server_join(void (*func) (char *server, char *id, char *padre))
{
  if (modulo_actual->python) {
    libera_objeto_python(modulo_actual->notifica_server_join);
  }
  modulo_actual->notifica_server_join = func;
}

void notifica_server_split(void (*func) (char *server, char *causa))
{
  if (modulo_actual->python) {
    libera_objeto_python(modulo_actual->notifica_server_split);
  }
  modulo_actual->notifica_server_split = func;
}

void notifica_servers_split_end(void (*func) (void))
{
  if (modulo_actual->python) {
    libera_objeto_python(modulo_actual->notifica_servers_split_end);
  }
  modulo_actual->notifica_servers_split_end = func;
}

void notifica_nicks_registrados(void)
{
  if (!modulo_actual->notifica_nick_registrado)
    return;

  db_notifica_nicks_registrados();
}

void notifica_nicks_registrados2(char *nick_compress)
{
  int handle;

  handle = nick_compress2handle(nick_compress);
  if (modulo_actual->python) {
    modulo_python_notifica_nick_registrado(modulo_actual->python,
                                           modulo_actual->
                                           notifica_nick_registrado, handle);
  }
  else {
    modulo_actual->notifica_nick_registrado(handle);
  }
}

void notifica_nicks(void)
{
  if (!modulo_actual->notifica_nick_entrada)
    return;

  db_notifica_nicks();
}

void notifica_nicks2(char *nick_compress)
{
  int handle;

  handle = nick_compress2handle(nick_compress);
  if (modulo_actual->python) {
    modulo_python_notifica_nick_entrada(modulo_actual->python,
                                        modulo_actual->
                                        notifica_nick_entrada, handle);
  }
  else {
    modulo_actual->notifica_nick_entrada(handle);
  }
}

void notifica_servers(void)
{
  struct servidor *s;

  if (!modulo_actual->notifica_server_join)
    return;

  s = servidores;
  while (s) {
    if (modulo_actual->python) {
      modulo_python_notifica_server_join(modulo_actual->python,
                                         modulo_actual->
                                         notifica_server_join, s->nombre,
                                         s->id, s->padre);
    }
    else {
      modulo_actual->notifica_server_join(s->nombre, s->id, s->padre);
    }
    s = s->siguiente;
  }
}

void notifica_nick_registrado_modulo(char *nick_compress)
{
  int handle;
  struct modulo *backup_modulo_actual;

  handle = nick_compress2handle(nick_compress);
  backup_modulo_actual = modulo_actual;
  modulo_actual = modulos;
  while (modulo_actual) {
    if (modulo_actual->notifica_nick_registrado) {
      if (modulo_actual->python) {
        modulo_python_notifica_nick_registrado(modulo_actual->python,
                                               modulo_actual->
                                               notifica_nick_registrado,
                                               handle);
      }
      else {
        modulo_actual->notifica_nick_registrado(handle);
      }
    }
    modulo_actual = modulo_actual->siguiente;
  }
  modulo_actual = backup_modulo_actual;
}

void notifica_nick_entrada_modulo(char *nick_compress)
{
  int handle;
  struct modulo *backup_modulo_actual;

  handle = nick_compress2handle(nick_compress);
  backup_modulo_actual = modulo_actual;
  modulo_actual = modulos;
  while (modulo_actual) {
    if (modulo_actual->notifica_nick_entrada) {
      if (modulo_actual->python) {
        modulo_python_notifica_nick_entrada(modulo_actual->python,
                                            modulo_actual->
                                            notifica_nick_entrada, handle);
      }
      else {
        modulo_actual->notifica_nick_entrada(handle);
      }
    }
    modulo_actual = modulo_actual->siguiente;
  }
  modulo_actual = backup_modulo_actual;
}

void notifica_nick_salida_modulo(char *nick_compress)
{
  int handle;
  struct modulo *backup_modulo_actual;

  handle = nick_compress2handle(nick_compress);
  backup_modulo_actual = modulo_actual;
  modulo_actual = modulos;
  while (modulo_actual) {
    if (modulo_actual->notifica_nick_salida) {
      if (modulo_actual->python) {
        modulo_python_notifica_nick_salida(modulo_actual->python,
                                           modulo_actual->
                                           notifica_nick_salida, handle);
      }
      else {
        modulo_actual->notifica_nick_salida(handle);
      }
    }
    modulo_actual = modulo_actual->siguiente;
  }
  modulo_actual = backup_modulo_actual;
}

void notifica_timer_modulo(int tiempo)
{
  struct modulo *backup_modulo_actual;

  backup_modulo_actual = modulo_actual;

  modulo_actual = modulos;
  while (modulo_actual) {
    if ((modulo_actual->tiempo) && (modulo_actual->tiempo <= tiempo)) {
      assert(modulo_actual->notifica_timer);

      modulo_actual->tiempo = 0;
      if (modulo_actual->python) {
        void (*f_notifica_timer) (void) = modulo_actual->notifica_timer;

        modulo_actual->notifica_timer = NULL;
        /*
         ** La rutina que sigue, de forma excepcional,
         ** decrementa el contador de referencias de "f_notifica_timer"
         */
        modulo_python_notifica_timer(modulo_actual->python, f_notifica_timer);
      }
      else {
        modulo_actual->notifica_timer();
      }
    }
    modulo_actual = modulo_actual->siguiente;
  }

/* Ahora debemos ver cuando hay que hacer la proxima invocacion */

  modulo_actual = modulos;
  tiempo_timeout_modulos = 0x7ffffffful;
  while (modulo_actual) {
    if ((modulo_actual->tiempo) &&
        (modulo_actual->tiempo < tiempo_timeout_modulos)) {
      tiempo_timeout_modulos = modulo_actual->tiempo;
    }
    modulo_actual = modulo_actual->siguiente;
  }

  modulo_actual = backup_modulo_actual;
}

void notifica_IRQ_modulo(void)
{
  struct modulo *backup_modulo_actual;

  while (IRQ) {                 /* Optimizacion IRQ por si las moscas */
    IRQ = 0;

    backup_modulo_actual = modulo_actual;

    modulo_actual = modulos;
    while (modulo_actual) {
      if ((modulo_actual->IRQ) && (modulo_actual->notifica_IRQ)) {
        modulo_actual->IRQ = 0;
        if (modulo_actual->python) {
          modulo_python_notifica_IRQ(modulo_actual->python,
                                     modulo_actual->notifica_IRQ);
        }
        else {
          modulo_actual->notifica_IRQ();
        }
      }
      modulo_actual = modulo_actual->siguiente;
    }
  }                             /* Optimizacion IRQ por si las moscas */
}

void notifica_server_join_modulo(char *server, char *id, char *padre)
{
  struct servidor *serv;
  char *p1, *p2, *p3 = NULL;
  struct modulo *backup_modulo_actual;

  serv = malloc(sizeof(struct servidor));
  p1 = malloc(strlen(server) + 1);
  p2 = malloc(strlen(id) + 1);
  if (padre) {
    p3 = malloc(strlen(padre) + 1);
  }
  assert(serv && p1 && p2 && (!padre || p3));

  serv->nombre = p1;
  strcpy(p1, server);
  serv->id = p2;
  strcpy(p2, id);
  if (padre) {
    serv->padre = p3;
    strcpy(p3, padre);
  }
  else {
    serv->padre = NULL;
  }

  serv->siguiente = servidores;
  if (servidores)
    servidores->anterior = &(serv->siguiente);
  serv->anterior = &servidores;
  servidores = serv;

  backup_modulo_actual = modulo_actual;
  modulo_actual = modulos;
  while (modulo_actual) {
    if (modulo_actual->notifica_server_join) {
      if (modulo_actual->python) {
        modulo_python_notifica_server_join(modulo_actual->python,
                                           modulo_actual->
                                           notifica_server_join, server, id,
                                           padre);
      }
      else {
        modulo_actual->notifica_server_join(server, id, padre);
      }
    }
    modulo_actual = modulo_actual->siguiente;
  }
  modulo_actual = backup_modulo_actual;
}

void notifica_server_split_modulo(char *server, char *causa)
{
  struct modulo *backup_modulo_actual;
  struct servidor *s, *s2;

  if (!server) {
    s = servidores;
    while (s) {
      free(s->nombre);
      free(s->id);
      if (s->padre)
        free(s->padre);
      s2 = s->siguiente;
      free(s);
      s = s2;
    }
    servidores = NULL;
  }
  else {
    s = servidores;
    while (s) {
      if (!strcmp(server, s->nombre)) {
        free(s->nombre);
        free(s->id);
        if (s->padre)
          free(s->padre);
        *(s->anterior) = s->siguiente;
        if (s->siguiente)
          s->siguiente->anterior = s->anterior;
        free(s);
        break;
      }
      s = s->siguiente;
    }
  }

  backup_modulo_actual = modulo_actual;
  modulo_actual = modulos;
  while (modulo_actual) {
    if (modulo_actual->notifica_server_split) {
      if (modulo_actual->python) {
        modulo_python_notifica_server_split(modulo_actual->python,
                                            modulo_actual->
                                            notifica_server_split, server,
                                            causa);
      }
      else {
        modulo_actual->notifica_server_split(server, causa);
      }
    }
    modulo_actual = modulo_actual->siguiente;
  }
  modulo_actual = backup_modulo_actual;
}

void notifica_servers_split_end_modulo(void)
{
  struct modulo *backup_modulo_actual;

  backup_modulo_actual = modulo_actual;
  modulo_actual = modulos;
  while (modulo_actual) {
    if (modulo_actual->notifica_servers_split_end) {
      if (modulo_actual->python) {
        modulo_python_notifica_servers_split_end(modulo_actual->python,
                                                 modulo_actual->
                                                 notifica_servers_split_end);
      }
      else {
        modulo_actual->notifica_servers_split_end();
      }
    }
    modulo_actual = modulo_actual->siguiente;
  }
  modulo_actual = backup_modulo_actual;
}

void notifica_db_nickdrop(void (*func) (char *nick, long num_serie),
                          long num_serie_actual)
{
  if (modulo_actual->python) {
    libera_objeto_python(modulo_actual->notifica_db_nickdrop);
  }
  modulo_actual->notifica_db_nickdrop = func;
  modulo_actual->notifica_db_nickdrop_num_serie_actual = num_serie_actual;
}

void notifica_db_nickdrop_modulo(char *nick, long num_serie)
{
  dbt clave, contenido;
  void *transaccion;
  int st;
  char buf[1024];
  struct modulo *backup_modulo_actual;

  sprintf(buf, "%ld %s", num_serie, nick);

  clave.datos = "NICKDROPS";
  clave.len = strlen(clave.datos) + 1; /* Incluye el '\0' */
  transaccion = inicia_txn_db();
  st = get_db(db_varios, transaccion, &clave, &contenido);
  if (contenido.len == 0) {
    contenido.len = strlen(buf) + 1;
    contenido.datos = malloc(contenido.len);
    strcpy(contenido.datos, buf);
  }
  else {
    contenido.datos =
      realloc(contenido.datos, contenido.len + strlen(buf) + 1);
    strcpy(contenido.datos + contenido.len, buf);
    contenido.len += strlen(buf) + 1;
  }
  st = put_db(db_varios, transaccion, &clave, &contenido);
  compromete_txn_db(transaccion);
  free(contenido.datos);

  nick_normalizado(nick, buf);

  backup_modulo_actual = modulo_actual;
  modulo_actual = modulos;
  while (modulo_actual) {
    if (modulo_actual->notifica_db_nickdrop) {
      if (modulo_actual->python) {
        modulo_python_notifica_db_nickdrop(modulo_actual->python,
                                           modulo_actual->
                                           notifica_db_nickdrop, buf,
                                           num_serie);
      }
      else {
        modulo_actual->notifica_db_nickdrop(buf, num_serie);
      }
    }
    modulo_actual = modulo_actual->siguiente;
  }
  modulo_actual = backup_modulo_actual;
}

unsigned long long get_entropia(void)
{
  return entropia;
}

void set_entropia(unsigned int random)
{
  mas_entropia(random);
}

char *handle2nicknumeric(int handle, char *buf)
{
  char *p;

  p = handle2nick_compress(handle);
  strcpy(buf, p);
  return p;
}

int nicknumeric2handle(char *numeric)
{
  return nick_compress2handle(numeric);
}

char *handle2servernumeric(int handle, char *buf)
{
  assert(0);
  return NULL;                  /* Para evitar el warning */
}

int servernumeric2handle(char *numeric)
{
  assert(0);
  return 0;                     /* Para evitar el warning */
}
