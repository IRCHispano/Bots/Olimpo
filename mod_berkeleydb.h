/*
** MOD_BERKELEYDB.H
*/

/*
    $Id: mod_berkeleydb.h,v 1.11 2002/02/21 20:08:54 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#ifndef MOD_BERKELEYDB_H
#define MOD_BERKELEYDB_H

#include "berkeleydb2.h"

char *mod_version_db(void);

p_db mod_abre_db(char *nombre);
void mod_cierra_db(p_db);

txn mod_inicia_txn_db(void);
txn mod_inicia_txn_db_no_sync(void);
txn mod_inicia_txn_db_no_wait(void);
void mod_compromete_txn_db(txn txn);
void mod_aborta_txn_db(txn txn);


int mod_get_db(p_db db, txn transaccion, dbt * clave, dbt * contenido);
int mod_put_db(p_db db, txn transaccion, dbt * clave, dbt * contenido);
int mod_del_db(p_db db, txn transaccion, dbt * clave);

c_db *mod_cursor_db(p_db db, txn transaccion, dbt * clave, int bulk);
int mod_cursor_get_db(c_db * db_cursor, dbt * clave, dbt * contenido);
int mod_cursor_close_db(c_db * db_cursor);

char *mod_strerror_db();

#endif
