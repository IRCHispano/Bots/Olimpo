/*
** OLIMPO

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

** 
** Jes�s Cea Avi�n
** jcea@jcea.es
** http://www.jcea.es/
**
** Nodo de gesti�n IRC para IRCd Undernet 2.10
**
** 26/Nov/97 Versi�n 0.0. Simplemente conecta y mantiene la conexi�n.
**
** 27/Nov/97 Versi�n 1.0. Control de clones. Almacena lo siguiente:
**                        a) La letra "U" (usuario)
**                        b) Direcci�n IP o Hostname
**                        c) Nick Actual
**                        d) Identificador comprimido
**                        e) Modos del usuario
**                        f) M�quina a la que se conecta ese usuario
**                        g) Timestamp
**                        Cada campo se separa con espacios, y cada registro
**                        con un '\0'.
**           * Con NICK de server se comprueban clones y se crea una 
**             nueva entrada.
**           * Con QUIT, se elimina la entrada.
**           * Con KILL, se elimina la entrada.
**           * Con SQUIT, se eliminan todas las entradas de HOSTs y NICKS
**             que est�n afectados por el split.
**           * Con NICK de usuario, actualiza nick.
**           * Con MODE de usuario, actualiza modos.
**           * En estos momentos un SQUIT de un nodo fuerza un SQUIT/JOIN
**             de ORACULO, de forma que actualiza datos de forma autom�tica.
**             Si eso se ve en la red, lo cambio luego.
**
** 27/Nov/97 Versi�n 1.01.
**           Si hay lag o a la vuelta de un split, es posible
**           que en la r�faga inicial se transfiera un clono ilegal,
**           que este nodo se lo cargue, y que luego lleguen modos
**           y dem�s comandos sobre �l. Hay que hacer que si no se
**           encuentra a un usuario en la base de datos, el comando se ignore.
**
** 27/Nov/97 Versi�n 1.1
**           Comandos de control disponibles para IRCops:
**           * op   #canal nick
**           * deop #canal nick
**           Para ello creo un "bot virtual" llamado "oraculo".
**
** 28/Nov/97 Versi�n 1.11
**           Se ha cambiado el nombre de "oraculo" a "olimpo", tanto
**           para el bot como para el nodo.
**           La busqueda de nick se ha hecho case insensible.
**
** 04/Dic/97 Versi�n 1.2
**           Almacena informaci�n sobre los servidores, para poder
**           responder a un squit sin necesitar un SQUIT/CONNECT propio
**           Cada registro consta de:
**           a) La letra "S" (Servidor)
**           b) Clave de identificaci�n del servidor del que cuelga
**           c) Clave de identificaci�n del servidor
**           d) Nombre del servidor
**           Cuando hay un SQUIT, se dan de baja todos los
**           servidores que colgaban de esa m�quina y todos los usuarios
**           que cuelgan de esos servidores.
**           ATENCION: Solo se guarda la primera letra del identificador
**
** 04/Dic/97 Versi�n 1.3
**           La recogida de basuras genera un "WALLOPS".
**           Se genera recogida de basuras cuando:
**             a) La memoria se llena
**             b) El 25% de la memoria ocupada es basura y se intenta
**                almacenar un nuevo registro.
**
** 09/Ene/98 Versi�n 1.3+
**           Cada vez que entra Oraculo genera un SETTIME.
**
** 06/Ene/98 Versi�n 1.4 Pre
**           A�adida gesti�n de se�ales por si la conexi�n se cierra
**           mientras estamos escribiendo.
**           Backoff exponencial en las reconexiones, para evitar el
**           control de clones del IRCU2.10.
**           Admite ILINES, leyendo el fichero "ilines.db". Su formato
**           es el siguiente: IP/host<sp>num_clones<sp>comentarios opcionales
**
** 08/Jul/98 Version 1.4
**           Con la fractura de mi brazo derecho y el tema de registro de nicks y
**           demas no he tenido mucho tiempo para ponerme con Olimpo.
**           Completado el tema de ilines.db. Almacena lo siguiente:
**           a) La letra "I" (I-Line)
**           b) IP/host
**           c) Numero de clones
**           d) Comentarios
**           Anadido el comando "ilines" para listar las I-Lines activas.
**
** 09/Jul/98 Version 1.5
**           Se envia un SETTIME cada cuatro horas, ademas de en el connect inicial.
**           Se hace log de los comandos enviados por IRCops en "olimpo.log".
**           Anadido el comando "jupe" para jupear servidores. El efecto es
**           instantaneo. Si el olimpo se relanza o se hace squit, se pierde.
**           Los mensajes que no van especificamente dirigidos a OLIMPO (por ejemplo,
**           mensajes globales) se ignoran.
**           Cambiamos la identificador numerico de Olimpo de "0" a "A", que
**           se corresponde con el nodo 0. De otra forma surgiria una colision
**           con el nodo "0" cuando la red se amplie.
**           Derivado del anterior: el bot Olimpo pasa de "091" a "AAA".
**
** 18/Ene/99 VERSION 11
**           Empezamos a funcionar en IRC-HISPANO.
**           Todas las modificaciones subsiguientes se han movido al web.
**
**
** $Id: olimpo.c,v 1.86 2004/04/14 18:33:36 jcea Exp $
*/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include <assert.h>

#include "version.h"
#include "config.h"
#include "db.h"
#include "berkeleydb.h"
#include "comandos.h"
#include "privmsg.h"
#include "m_db.h"
#include "shared.h"
#include "modulos.h"
#include "dbuf.h"


char buf[BUF_LEN], buf2[2 * BUF_LEN];

ssize_t readx(int fildes, void *buf, size_t nbyte)
{
  static char *p = NULL;
  static char buffer[32768];
  static ssize_t cont = 0;

  if (cont < 1) {
    cont = read(fildes, buffer, 32768);
    p = buffer;
  }
  if (cont < 1)
    return cont;
  *(char *) buf = *p++;
  cont--;
  return 1;
}

static int senal = 0;

void senal_handler(int s)
{
  senal = s;
}

int main(void)
{
  char buf_write[1000];
  struct tms dummy;
  time_t ultima_conexion;
  int tiempo_backoff = 0;
  struct hostent *servidor;
  struct sockaddr_in conexion;
  time_t tiempo;
  time_t boot_time;
  int estado;
  char *p, *p2;
  int i, j;
  typedef struct {
    char *comando;
    void (*rutina) (char *p, char *buf);
  } comando;

#if defined(ALLOW_ILINE_REGISTRATION)
  assert(strchr(BDD_AUTHORITATIVE, 'i'));
#endif

  comando tabla_comandos[] = {
    {"SQUIT", m_squit},
    {"SERVER", m_server},
    {"KILL", m_kill},
    {"QUIT", m_quit},
    {"MODE", m_mode},
    {"PRIVMSG", m_privmsg},
    {"NICK", m_nick},
    {"DB", m_db},
    {"END_OF_BURST", m_eob},
    {"EOB_ACK", m_eob_ack},
    {"PING", m_ping},
    {NULL, NULL}
  };

  {
    struct timeval tv;

    gettimeofday(&tv, NULL);
    entropia ^= (tv.tv_sec ^ tv.tv_usec);
  }

  sigset(SIGHUP, senal_handler);
  sigset(SIGINT, senal_handler);
  sigset(SIGQUIT, senal_handler);
  sigset(SIGKILL, senal_handler);
  sigset(SIGPIPE, SIG_IGN);
  sigset(SIGALRM, senal_handler);
  sigset(SIGTERM, senal_handler);
  sigset(SIGUSR1, senal_handler);
  sigset(SIGUSR2, senal_handler);
  sigset(SIGPOLL, senal_handler);

  sigset(SIGVTALRM, senal_handler);
  sigset(SIGPROF, senal_handler);
  sigset(SIGXCPU, senal_handler);
  sigset(SIGXFSZ, senal_handler);
  /*
   ** De momento vamos a obviar
   ** las sen~ales en tiempo real.
   */




#if !defined(DEVELOP)
/* Nos desvinculamos del terminal */
  setsid();
#endif



  tiempo_inicial = times(&dummy); /* Esta referencia es arbitraria */
  boot_time = time(NULL);

  abre_berkeley_db();

  sprintf(buf_write, "%s\n", version_db());
  write(1, buf_write, strlen(buf_write)); /* STDOUT */

  db_ilines = abre_db("db.i");
  db_varios = abre_db("db.varios");

  lee_contadores_bdd();

  lee_historico();

/*
** Debemos inicializar el interprete de
** python antes de arriesgarnos a imprimir nada,
** especialmente temas de logs.
*/
  inicializa_sistema_de_modulos();

  carga_modulos();

inicio:
  borra_db();                   /* Inicializa las tablas */

  soc = socket(AF_INET, SOCK_STREAM, 0);

  if (soc == -1) {
    perror("No se puede abrir el socket");
    descarga_modulos();
    cierra_db(db_ilines);
    cierra_db(db_varios);
    exit(errno);
  }

  sprintf(buf_write, "Localizando servidor %s...\n", "irc.argo.es");
  write(1, buf_write, strlen(buf_write)); /* STDOUT */
  servidor = gethostbyname("irc.argo.es");
  sprintf(buf_write, "Servidor localizado. Conectando...\n");
  write(1, buf_write, strlen(buf_write)); /* STDOUT */

  if (servidor == NULL) {
    perror("No se puede resolver el nombre del servidor");
    descarga_modulos();
    cierra_db(db_ilines);
    cierra_db(db_varios);
    exit(errno);
  }

/*
** Sen~ala que no estamos conectados
*/
  uplink[0] = '\0';

  conexion.sin_family = AF_INET;
  conexion.sin_addr = *(struct in_addr *) servidor->h_addr;
  conexion.sin_port = htons(PUERTO);
  dbuf_purga();
  estado = connect(soc, (struct sockaddr *) &conexion, sizeof(conexion));
  if (estado == -1) {
    perror("Error en la conexi�n al servidor. Reintentando...");
    close(soc);
    sleep(5);
    goto inicio;
  }
  tiempo = time(NULL);
  ultima_conexion = tiempo;

  sprintf(buf, ":%s PASS %s\nSERVER %s 1 %ld %ld P10 %sD] %ld :%s v%s (%s)\n",
          NOMBRE_NODO, CLAVE, NOMBRE_NODO, tiempo, tiempo, NUM_NODO,
          boot_time, INFO_NODO, VERSION, FECHA);
  dbuf_write(buf);

  sprintf(buf, "%s NICK %s 1 880000000 - - %s AAAAAA %sAA :%s\n",
          NUM_NODO, NICK, NICK_MODO, NUM_NODO, WHO);
  dbuf_write(buf);

  envia_modulos_nicks();

  strcpy(buf, NUM_NODO " END_OF_BURST\n");
  dbuf_write(buf);

  bdd_offline();
  envia_m_db();

  envia_squit_raw();

#ifdef CANAL
  sprintf(buf, ":%s CREATE %s %lu\n", NICK, CANAL, time(NULL));
  dbuf_write(buf);
#endif

/*
** En realidad ordenamos el SETTIME desde "m_server"
*/
#if defined(ALLOW_SETTIME)
  siguiente_settime = time(NULL) + 10000;
#endif

  p = buf;
  while (!0) {                  /* Bucle infinito */

    dbuf_envia();

    estado = readx(soc, p, 1);

/*
** Ponemos la lectura primero porque las
** sen~ales habitualmente cortan el read()
*/
    if (senal) {
      sprintf(buf_write, "\n\nLlega la sen~al %d\n", senal);
      write(2, buf_write, strlen(buf_write)); /* STDERR */
      descarga_modulos();
      cierra_db(db_ilines);
      cierra_db(db_varios);
      exit(-1);
    }

    if (estado < 1) {
      time_t tiempo;

      close(soc);
      tiempo = time(NULL);
      tiempo_backoff = tiempo_backoff * 2 + 1;
      if (tiempo_backoff > MAX_BACKOFF)
        tiempo_backoff = MAX_BACKOFF;
      if (tiempo - ultima_conexion < tiempo_backoff)
        sleep(tiempo_backoff);
      goto inicio;
    }
#ifdef VERBOSE_LINK
    putchar(*p);
#endif
    mas_entropia(*p);

    if (*p == '\r')
      --p;
    *++p = '\0';
    if (p - buf > BUF_LEN - 3) { /* Curarse en salud */
      if (*p == '\n') {
        p = buf;                /* Ignoramos los overflow */
      }
      p--;                      /* Completa el comando con overflow */
      continue;
    }
    if (*(p - 1) != '\n')
      continue;                 /* Completa comando */
    *--p = '\0';

    tiempo = time(NULL);
    mas_entropia(tiempo);

    if (IRQ) {
      notifica_IRQ_modulo();
    }

    if (tiempo >= tiempo_timeout_modulos) {
      notifica_timer_modulo(tiempo);
    }

#if defined(ALLOW_SETTIME)
    if (siguiente_settime <= tiempo) {
      siguiente_settime = tiempo + 60 * 60 * 4; /* 4 Horas */
      sprintf(buf2, NUM_NODO " SETTIME %ld *\n", tiempo);
      dbuf_write(buf2);
    }
#endif

    p = strchr(buf, ' ');
    if (p == NULL)
      continue;                 /* No debe ocurrir */

    p++;

    if (!strncasecmp(p, "SERVER " NOMBRE_PEER " ", strlen(NOMBRE_PEER) + strlen("SERVER") + 2)) { /* Primera Intro */
      char *p3;

/*
** Los modulos pueden "pinchar" cualquier comando.
** Lo logico seria que los modulos se ejecutasen tras
** el framework de Olimpo, pero lo malo es que
** muchas rutinas del framework modifican la cadena recibida.
**
** Aqui gestionamos el caso particular del "SERVER" inicial.
*/
      //intercepta_comando_modulo(p);
      intercepta_comando_modulo(buf);

      tiempo_backoff = 0;       /* Reconexiones */
      strcpy(buf2, "- ");       /* Cuelga de nosotros */
      p = strchr(p, ' ') + 1;   /* Nos saltamos el "SERVER" */
      p2 = strchr(p, ' ');      /* Hops */
      *p2++ = '\0';
      p2 = strchr(p2, ' ') + 1; /* Startime */
      p2 = strchr(p2, ' ') + 1; /* Timestamp */
      p2 = strchr(p2, ' ') + 1; /* Versi�n */
      p2 = strchr(p2, ' ') + 1; /* Identificador */

      /* OJITO CON ESTO */
      p3 = strchr(p2, ' ');
      assert(((p3 - p2) == 3) || ((p3 - p2) == 5));
      if (p3 - p2 == 3) {       /* numerics cortos */
        *(p2 + 1) = '\0';       /* Nos quedamos solo con una letra */
      }
      else {                    /* numerics largos */
        assert(*p2 != 'A');     /* Los numerics <64 no pueden ser largos */
        *(p2 + 2) = '\0';       /* Nos quedamos con dos letras */
      }
      strcpy(uplink, p2);
      strcat(buf2, p2);
      strcat(buf2, p - 1);      /* Pone tambien el espacio inicial */
      guarda_servidor(buf2);
      p = buf;
      continue;
    }

/*
** Los modulos pueden "pinchar" cualquier comando.
** Lo logico seria que los modulos se ejecutasen tras
** el framework de Olimpo, pero lo malo es que 
** muchas rutinas del framework modifican la cadena recibida.
*/
    //intercepta_comando_modulo(p);
    intercepta_comando_modulo(buf);

    for (j = i = 0; tabla_comandos[i].comando != NULL; i++) {
      j = strlen(tabla_comandos[i].comando);
      if (!strncmp(p, tabla_comandos[i].comando, j)) { /* Parece ese comando */
        if ((*(p + j) == ' ') || (*(p + j) == '\0')) { /* Es ese comando */
          if (tabla_comandos[i].rutina) /* Para ignorar los no implementados */
            tabla_comandos[i].rutina(p + j + 1, buf);
          i = 0;
          break;
        }
      }
    }

    if (i) {                    /* Posible comando para modulos */
      m_manda_modulos(p + j + 1, buf);
    }


#if defined(DEVELOP) && defined(_0)
    if (i) {
      sprintf(buf_write, "***\n*** Comando desconocido: %s\n***\n", buf);
      write(2, buf_write, strlen(buf_write)); /* STDERR */
    }
#endif

    p = buf;
    continue;
  }
}
