/*
** MODULOS_PYTHON_H
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

/*
    $Id: modulos_python.h,v 1.27 2003/07/29 04:32:52 jcea Exp $
*/

#ifndef MODULOS_PYTHON_H
#define MODULOS_PYTHON_H

void inicializa_python(void);

int es_modulo_python(char *nombre);
void libera_objeto_python(void *objeto);

char *nuevo_modulo_python(char *nombre, void **modulo_python);
void descarga_modulo_python(void *modulo_python);

void fija_modulo_python_actual(void *modulo_python);

void modulo_python_inicio(void *modulo_python);
void modulo_python_fin(void *modulo_python, void *fin);

void modulo_python_privmsg(void *modulo_python, void *privmsg, int handle,
                           int handle2, char *mensaje);

void modulo_python_servmsg(void *modulo_python, void *servmsg, int handle,
                           char *remitente, char *comando);

int modulo_python_intercepta_comando(void *modulo_python,
                                     void *intercepta_comando, char *comando);

void modulo_python_notifica_nick_entrada(void *modulo_python,
                                         void *notifica_nick_registrado,
                                         int handle);
void modulo_python_notifica_nick_salida(void *modulo_python,
                                        void *notifica_nick_registrado,
                                        int handle);
void modulo_python_notifica_nick_registrado(void *modulo_python,
                                            void *notifica_nick_registrado,
                                            int handle);
void modulo_python_notifica_server_join(void *modulo_python,
                                        void *notifica_server_join,
                                        char *server, char *id, char *padre);
void modulo_python_notifica_server_split(void *modulo_python,
                                         void *notifica_server_split,
                                         char *server, char *causa);
void modulo_python_notifica_servers_split_end(void *modulo_python, void
                                              *notifica_servers_split_end);

void modulo_python_notifica_timer(void *modulo_python, void *notifica_timer);

void modulo_python_notifica_IRQ(void *modulo_python, void *notifica_IRQ);

void modulo_python_notifica_db_nickdrop(void *modulo_python,
                                        void *notifica_db_nickdrop,
                                        char *nick, int num_serie);

#endif
