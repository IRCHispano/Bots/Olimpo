/*
** MOD_PRIVMSG_H
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

/*
    $Id: mod_privmsg.h,v 1.18 2002/06/19 14:37:53 jcea Exp $
*/

#ifndef MOD_PRIVMSG_H
#define MOD_PRIVMSG_H


int nuevo_nick(char *nick, char *modos,
               void (*func) (int handle, int remitente, char *mensaje));
void quit_nick(int handle);
int envia_nick(int handle, int destino, char *mensaje);
char *lee_flags_nick(int handle, char *flags);
char *lee_host_nick(int handle, char *host);
unsigned long lee_ip_nick(int handle);
char *lee_nick(int handle, char *nick);
char *lee_nick_normalizado(int handle, char *nick);
int lee_handle(char *nick);

#endif
