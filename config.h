/*
** CONFIG_H
**
** $Id: config.h,v 1.157 2004/04/07 11:44:31 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#ifndef CONFIG_H
#define CONFIG_H

#include "malloc.h"


#define MAX_BACKOFF (2*60)      /* Tiempo maximo entre conexiones */
#define MAX_LISTADO_ILINES  10  /* Longitud maxima en "ilines" */

#define NICK_MODO   "+odkirh"

#ifdef ESNET

#define NICK        "Olimpo"
#define WHO         "Bot de Control ESNET.ORG"
#define CANAL       "#esnet"
#define NUM_NODO    "A"
#define NOMBRE_NODO "olimpo.esnet.org"
#define PUERTO      4401
#define CLAVE       "qw"
#define INFO_NODO   "Nodo de Control"
#define NOMBRE_PEER "corinto.esnet.org"
#define VERBOSE_LINK
#define VERBOSE_DB
#define VERBOSE_CLONES
#define VERBOSE_CLONES_KILL
#undef LOG_CLONES
#undef LOG_CLONES_KILL
#undef LOG_HISTORICO
#define LIB_PATH        "/export/home/irc/irc10/olimpo"

#endif /* ESNET */

#ifdef IRC_HISPANO

#define NICK        "SuX"
#define WHO         "Bot de Control"
#define CANAL       "#opers"
#define NUM_NODO    "A"
#define NOMBRE_NODO "black.hole"
#define PUERTO      4400
#define CLAVE       "qw"
#define INFO_NODO   "Nodo de Control"
#define NOMBRE_PEER "gaia.irc-hispano.org"
#undef VERBOSE_LINK
#undef VERBOSE_DB
#define VERBOSE_CLONES
#define VERBOSE_CLONES_KILL
#define LOG_CLONES
#define LOG_CLONES_KILL
#define LOG_HISTORICO
#define LIB_PATH	"/export/home/irc/olimpo/estable"

#define BDD_AUTHORITATIVE "abcdefghijklmnopqrstuvwxyz"

/*
** Cuidado con la autentificacion,
** porque en redes distintas la numeracion cambia
*/
#define ALLOW_ILINE_REGISTRATION
#define ALLOW_SETTIME

#ifdef DEVELOP

#undef NICK
#define NICK	"SuX2"
#undef NUM_NODO
#define NUM_NODO	"E"
#undef NOMBRE_NODO
#define NOMBRE_NODO	"devel.black.hole"
#undef LIB_PATH
#define LIB_PATH        "/export/home/irc/olimpo"

#undef BDD_AUTHORITATIVE
#define BDD_AUTHORITATIVE ""

#undef ALLOW_ILINE_REGISTRATION

#endif

#endif /* IRC-HISPANO */

#endif
