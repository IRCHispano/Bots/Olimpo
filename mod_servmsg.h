/*
** MOD_SERVMSG_H
**

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

/*
    $Id: mod_servmsg.h,v 1.5 2002/05/20 21:16:56 jcea Exp $
*/

#ifndef MOD_SERVMSG_H
#define MOD_SERVMSG_H

int serv_callback(int handle,
                  void (*func) (int handle, char *remitente,
                                char *respuesta));
int envia_raw(int handle, char *comando);
int envia_raw2(char *comando);

void intercepta_comando(char *comando, int (*func) (char *comando));

#endif
