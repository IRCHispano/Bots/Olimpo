/*
** COMANDOS_C
**
** $Id: comandos.c,v 1.71 2004/04/05 17:09:37 jcea Exp $

# Copyright 1997-2004 Jesus Cea Avion <jcea@jcea.es>
# http://www.jcea.es/irc/index.htm#Olimpo
# http://www.jcea.es/irc/olimpo3.htm
#
# This file is part of "OLIMPO".
#
# "OLIMPO" is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Affero General Public License as 
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# "OLIMPO" is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with "OLIMPO".  If not, see
# <http://www.gnu.org/licenses/>.

*/

#include "shared.h"
#include "comandos.h"
#include "config.h"
#include "db.h"
#include "berkeleydb.h"
#include "modulos.h"
#include "dbuf.h"

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <assert.h>


char buf2[2 * BUF_LEN];

void m_manda_modulos(char *p, char *buf)
{
  char *comando;
  char *p2;

  comando = strchr(buf, ' ');
  *comando++ = '\0';

/* Destino */
  strcpy(buf2, comando);
  p = strchr(buf2, ' ');
  if (!p)
    return;
  p2 = strchr(++p, ' ');
  if (p2)
    *p2 = '\0';

  envia_servmsg_modulo(p, buf, comando);
}

void envia_squit_raw(void)
{

#ifndef DEVELOP

  char buf[BUF_LEN];
  char *p;
  FILE *handle;

  handle = fopen("squit.raw", "r");
  if (handle == NULL)
    return;
  while (fgets(buf, BUF_LEN / 2, handle) != NULL) {
    p = strchr(buf, '\n');
    if (p == NULL)
      strcat(buf, "\n");
    dbuf_write(buf);
  }
  fclose(handle);

#endif

}

void m_squit(char *p, char *buf)
{
  char *causa;

  envia_squit_raw();

  causa = strchr(p, ' ');
  assert(causa);
  *causa++ = '\0';
  p = busca_servidor(p);

/*
** Borra un servidor
** y todo lo que tenga asociado, de forma recursiva.
** Hace falta la comprobacion porque un jupe fallido
** mataria a Olimpo.
** No hay jupes fallidos, porque aunque falle el squit,
** el SERVER entra y echa fuera al otro.
** Sin embargo *SI* se puede hacer un squit
** a un servidor JUPEADO, que no saldra en la base de datos.
*/

  if (p != NULL) {
    p = strrchr(p, ' ') + 1;
    borra_servidor(p, causa);
  }
}

void m_server(char *p, char *buf)
{
  char *p2, *p3;

  *strchr(buf, ' ') = '\0';     /* Aisla el servidor del que cuelga */
  strcpy(buf2, buf);            /* Servidor del que cuelga */

  if (!nodo_esta_conectado(buf2))
    return;

  p2 = strchr(p, ' ') + 1;      /* Hops */
  p2 = strchr(p2, ' ') + 1;     /* Startime */
  p2 = strchr(p2, ' ') + 1;     /* Timestamp */
  p2 = strchr(p2, ' ') + 1;     /* Versi\163n */

/*
** El problema de hacer un "SETTIME" desde el Server Join
** es que el nodo destino recibe el tiempo *TRAS* haberse
** chupado todo el BURST, lo que involucra un
** error en las temporizaciones.
**
** Es mejor hacer el SETTIME desde el EOB_ACK.
*/
//  if (*p2 == 'J')
//    siguiente_settime = 0;

  p2 = strchr(p2, ' ') + 1;     /* Identificador */

/* OJITO CON ESTO */
  p3 = strchr(p2, ' ');
  assert(((p3 - p2) == 3) || ((p3 - p2) == 5));
  if ((p3 - p2) == 3) {         /* numerics cortos */
    *(p2 + 1) = '\0';           /* Nos quedamos solo con una letra */
    strcat(buf2, p2 - 1);       /* Pone tambien el espacio anterior */
  }
  else {                        /* numerics largos */
    assert(*p2 != 'A');         /* Los numerics <64 no pueden ser largos */
    *(p2 + 2) = '\0';           /* Aqui nos quedamos con dos letras */
    strcat(buf2, p2 - 1);       /* Pone tambien el espacio anterior */
  }

  *strchr(p, ' ') = '\0';
  strcat(buf2, p - 1);

/*
** Comprobamos si el nodo que nos llega *YA* esta conectado.
** Seria un "ghost", y debemos quedarnos con el ultimo
** en llegar.
*/
  if (nodo_esta_conectado(p2)) {
    envia_squit_raw();
    borra_servidor(p, "GHOST");
  }

  guarda_servidor(buf2);
}

void m_kill(char *p, char *buf)
{
  if (!notifica_posible_kill_modulo(p, buf)) {
    *strchr(p, ' ') = '\0';
    borra_usuario_compress(p, 1); /* Notifica */
  }
}

void m_quit(char *p, char *buf)
{
  *strchr(buf, ' ') = '\0';
  borra_usuario_nick(buf + 1, 1); /* Nos saltamos los ":" *//* Notifica */
}

void m_eob(char *p, char *buf)
{
  p = strchr(buf, ' ');
  *p = '\0';
  if (strcmp(buf, uplink))      /* No es el uplink */
    return;
  strcpy(buf, NUM_NODO " EOB_ACK\n");
  dbuf_write(buf);
}

void m_eob_ack(char *p, char *buf)
{
/*
** Pedimos un SETTIME
*/
#if defined(ALLOW_SETTIME)
  siguiente_settime = 0;
#endif
}

/*
** Este comando es muy distinto a los demas. Este es
** :orig PING orig :dest
*/
void m_ping(char *p, char *buf)
{
  segmentos segm;

  segmentacion(&segm, buf, NULL, 4);
  if (segm.num_segm == 3) {
    /* Nos valen las mismas operaciones */
    segm.num_segm = 4;
  }
  segm.segm[1] = "PONG";
  segm.segm[3] = segm.segm[2];
  segm.segm[2] = NOMBRE_NODO;
  segm.segm[0] = ":" NOMBRE_NODO;
  dessegmentar(&segm, buf2);
  dbuf_write(buf2);
  dbuf_write("\n");
}

void m_nick(char *p, char *buf)
{
  int contador, num_clones_permitidos;
  char *p2, *p3;
  void *ip_count;
#ifdef LOG_CLONES_KILL
  int handle;
  time_t tiempo;
#endif
  int notify_nick_registrado = 0;

  p2 = strchr(p, ' ');          /* Fin del Nick */
  *p2++ = '\0';
  p2 = strchr(p2, ' ');
  if (p2 == NULL) {             /* Cambio de Nick de un usuario */
    *strchr(buf, ' ') = '\0';
    p2 = encuentra_usuario_compress(buf);
    if (p2 == NULL)             /* Nos lo acabamos de cargar */
      return;
    p3 = strchr(p2, ' ');
    *p3 = '\0';
    strcpy(buf2, p2);
    strcat(buf2, " ");
    strcat(buf2, p);            /* Nick nuevo */
    *p3 = ' ';
    p3 = strchr(p3 + 1, ' ');   /* Nos saltamos el viejo */
    strcat(buf2, p3);
    *(strrchr(buf2, ' ') + 1) = '\0';
    strcat(buf2, p + strlen(p) + 1);
    if (strlen(buf2) > BUF_LEN - 9) {
      descarga_modulos();
      cierra_berkeley_db();
      exit(1);
    }
/*
** Borramos el registro viejo
*/
    p3 = strchr(p2, ' ') + 1;
    p3 = strchr(p3, ' ') + 1;
    *strchr(p3, ' ') = '\0';
    borra_usuario_compress(p3, 0); /* No notifica */
    *(p3 + strlen(p3)) = ' ';
    guarda_usuario(buf2, 0);    /* No notifica */
    return;
  }

/* Usuario Nuevo */

  p2 = strchr(p2 + 1, ' ') + 1;
  p2 = strchr(p2, ' ') + 1;     /* IP/HOST */
  p3 = strchr(p2, ' ');         /* Fin de IP/HOST */
  *p3++ = '\0';

  strcpy(buf2, p2);             /* Host */
  strcat(buf2, " ");
  strcat(buf2, p);              /* Nick */
  strcat(buf2, " ");

  p2 = strchr(p3, ':');
  if (p2 == NULL) {
    descarga_modulos();
    cierra_berkeley_db();
    exit(1);                    /* No puede darse */
  }
  *--p2 = '\0';
  p2 = strrchr(p3, ' ');        /* P2=NICK comprimido */
  *p2++ = '\0';
  strcat(buf2, p2);
  strcat(buf2, " ");
  p2 = strrchr(p3, ' ');
/* Vemos si hay modos o no */
  if (p2 == NULL) {
    strcat(buf2, "- ");         /* No hay modos */
    p2 = p3;                    /* P2 = IP codificada */
  }
  else {
    *p2++ = '\0';               /* P2 = IP codificada */
    strcat(buf2, p3);           /* Modos */
    strcat(buf2, " ");
    if (strchr(p3, 'r'))
      notify_nick_registrado = !0;
  }
  *strchr(buf, ' ') = '\0';     /* Nodo */
  strcat(buf2, buf);
  strcat(buf2, " ");

/*
** Metemos la IP codificada
*/
  strcat(buf2, p2);
  strcat(buf2, " ");

  if (!nodo_esta_conectado(buf))
    return;

/*
** Tambi\151n me quedo con la timestamp del usuario
*/
  p = buf + strlen(buf) + 1;
  p += strlen(p) + 1;
  p = strchr(p, ' ') + 1;
  *strchr(p, ' ') = '\0';
  strcat(buf2, p);

  if (strlen(buf2) > BUF_LEN) {
    descarga_modulos();
    cierra_berkeley_db();
    exit(1);                    /* No debe ocurrir */
  }

  strcpy(buf, buf2);

/*
** Aqu\155 hacemos la comprobaci\163n de clones
*/
  p2 = strchr(buf2, ' ');
  *p2++ = '\0';                 /* Separa IP/HOST */
  contador = 0;
  num_clones_permitidos = buscar_iline(buf2, buf);

  ip_count = NULL;
  while ((encuentra_ip(&ip_count, buf2)) != NULL)
    contador++;

#ifdef DEVELOP

  num_clones_permitidos = 9999;

#endif

  if (contador >= abs(num_clones_permitidos)) {
    strcpy(buf, ":" NOMBRE_NODO " KILL ");
    p2 = strchr(p2, ' ') + 1;
    *strchr(p2, ' ') = '\0';    /* Nick a matar */
    strcat(buf, p2);
/**
    strcat(buf," ");
    p2+=strlen(p2)+1;
    strcat(buf,strrchr(p2+1,' ')+1);
**/
    sprintf(buf + strlen(buf),
            " :En esta red solo se permiten %d "
            "clones para tu IP (%s)\n", abs(num_clones_permitidos), buf2);
    dbuf_write(buf);

#ifdef VERBOSE_CLONES_KILL
    {
      char buf_write[1000];

      sprintf(buf_write, "***\n*** %s***\n", buf);
      write(1, buf_write, strlen(buf_write)); /* STDOUT */
    }
#endif

#ifdef LOG_CLONES_KILL
    tiempo = time(NULL);
    handle = open("kill.log", O_APPEND | O_WRONLY | O_CREAT, 0644);
    if (handle != -1) {
      char buf_time[100];

      write(handle, ctime_sincronizado(&tiempo, buf_time), 24); /* Con 24 no imprime el \n\0 final */
      sprintf(buf, " %d %s\n", abs(num_clones_permitidos), buf2);
      write(handle, buf, strlen(buf));
      close(handle);
    }
#endif
    /* Incrementamos numero de conexiones */

    incrementa_num_conexiones();
    return;
  }

/*
** Se permite el clon
*/

  guarda_usuario(buf, 1);       /* Lo notificamos */
/*
** Incrementamos numero de conexiones.
** Esta llamada debe hacerse DESPUES
** de "guarda_usuario()", para que
** se almacene el valor correcto de
** numero de usuarios.
*/
  incrementa_num_conexiones();

  if (notify_nick_registrado) {
    p2 = strchr(strchr(buf, ' ') + 1, ' ') + 1;
    notifica_nick_registrado_modulo(p2);
    iline_dinamica(buf);
  }

/*
** Si es negativo, se trata del valor por defecto
*/
  if ((num_clones_permitidos > 0) && (num_clones_permitidos < 999)) {
    contador++;                 /* El +1 es porque no incluye el actual */
#ifdef VERBOSE_CLONES
    {
      char buf_write[1000];

      sprintf(buf_write, "***\n*** Entrando el clon %d de %s\n***\n",
              contador, buf2);
      write(1, buf_write, strlen(buf_write)); /* STDOUT */
    }
#endif

#if defined(LOG_CLONES) &&(!defined(DEVELOP))
    tiempo = time(NULL);
    handle = open("clones.log", O_APPEND | O_WRONLY | O_CREAT, 0644);
    if (handle != -1) {
      char buf_time[100];

      write(handle, ctime_sincronizado(&tiempo, buf_time), 24); /* Con 24 no imprime el \n\0 final */
      sprintf(buf, " %d %s\n", contador, buf2);
      write(handle, buf, strlen(buf));
      close(handle);
    }
#endif
  }
}

void m_mode(char *p, char *buf)
{
  int estado;
  int notify_nick_registrado = 0;
  char *p2, *p3, *p4;

  if ((*p == '#') || (*p == '&') || (*p == '+')) /* Se trata de un canal */
    return;

  p2 = strchr(p, ' ');          /* Fin del Nick */
  *p2++ = '\0';
  p = encuentra_usuario_nick(p);
  if (p == NULL)                /* Nos lo acabamos de cargar */
    return;
  p3 = strchr(p, ' ');
  p3 = strchr(p3 + 1, ' ');
  p3 = strchr(p3 + 1, ' ');     /* Modos actuales */

  strcpy(buf2, p3 + 1);         /* Lo primero de buf2=modos */

  p2 = strchr(p2, ':') + 1;
  estado = !0;                  /* A\161adir */
  p3 = strchr(buf2, ' ');
  while (*p2) {
    switch (*p2) {
    case '+':
      estado = !0;
      break;
    case '-':
      estado = 0;
      break;
    default:
      if (estado) {
        *p3++ = *p2;
        if (*p2 == 'r')
          notify_nick_registrado = !0;
      }
      else {
        p4 = strchr(buf2, *p2);
        if (p4 != NULL)
          *p4 = *--p3;
      }
      break;
    }
    p2++;
  }
  *p3 = '\0';                   /* Modos finales */
  if (p3 - buf2 == 1)
    *buf2 = '-';                /* No hay modos */
  else
    *buf2 = '+';                /* SI hay modos */
  if (strlen(buf2) > BUF_LEN - 9) {
/*
    strcpy(buf2,p);
*/
    descarga_modulos();
    cierra_berkeley_db();
    exit(1);
  }
  strcpy(buf, buf2);            /* Copia de los modos a poner */

  p3 = strchr(p, ' ');
  p3 = strchr(p3 + 1, ' ');
  p3 = strchr(p3 + 1, ' ');     /* Modos actuales */
  *++p3 = '\0';                 /* Conserva el espacio */
  strcpy(buf2, p);
  strcat(buf2, buf);
  *p3 = '+';
  p3 = strchr(p3, ' ');         /* Copiamos el resto */
  strcat(buf2, p3);
  p = strchr(buf2, ' ') + 1;
  p = strchr(p, ' ') + 1;
  p2 = strchr(p, ' ');
  *p2 = '\0';
  borra_usuario_compress(p, 0); /* No notifica */
  *p2 = ' ';
  guarda_usuario(buf2, 0);      /* No notifica */

  if (notify_nick_registrado) {
    p2 = strchr(strchr(buf2, ' ') + 1, ' ') + 1;
    notifica_nick_registrado_modulo(p2);
  }
}
